import { IVector2 } from '../geom/Vector2';


/**
 * Data structure used to store tracking points.
 */
export interface ITrackingPoint extends IVector2 {
  time: number;
}

/**
 * Defines the known pointer types.
 */
export const enum PointerType
{
  Mouse,
  Touch,
  Pen
}

/**
 * A single pointer.
 */
export default class Pointer
{
  /**
   * The input type of this pointer.
   */
  type:PointerType;

  /**
   * The current horizontal position.
   */
  x:number;

  /**
   * The current vertical position.
   */
  y:number;

  /**
   * The previous horizontal position.
   */
  lastX:number;

  /**
   * The previous vertical position.
   */
  lastY:number;

  /**
   * The initial horizontal position.
   */
  startX:number;

  /**
   * The initial vertical position.
   */
  startY:number;

  /**
   * A list of tracking points used to calculate the velocity.
   */
  track:ITrackingPoint[] = [];

  /**
   * The duration in milliseconds tracking points should be collected.
   */
  trackPeriod:number = 100;



  /**
   * Pointer constructor.
   *
   * @param type
   *   The input type of the pointer.
   * @param x
   *   The initial horizontal position.
   * @param y
   *   The initial vertical position.
   */
  constructor(type:PointerType, x:number, y:number) {
    this.type = type;
    this.x    = this.startX = this.lastX = x;
    this.y    = this.startY = this.lastY = y;
  }


  /**
   * Set the current position of the pointer.
   *
   * @param x
   *   The current horizontal position.
   * @param y
   *   The current vertical position.
   */
  setPosition(x:number, y:number) {
    this.lastX = this.x;
    this.lastY = this.y;
    this.x     = x;
    this.y     = y;

    if (this.trackPeriod > 0) {
      this.addTrackingPoint(x, y);
    }
  }


  /**
   * Return the relative movement of the pointer between the current
   * and previous sampled position.
   *
   * @returns
   *   An object defining the relative movement.
   */
  getRelativeDelta():IVector2 {
    return {
      x: this.x - this.lastX,
      y: this.y - this.lastY
    };
  }


  /**
   * Return the absolute movement of the pointer between the current
   * and initial position.
   *
   * @returns
   *   An object defining the absolute movement.
   */
  getAbsoluteDelta():IVector2 {
    return {
      x: this.x - this.startX,
      y: this.y - this.startY
    };
  }


  /**
   * Return the absolute distance of the pointer between the current
   * and initial position.
   *
   * @returns
   *   The absolute distance.
   */
  getAbsoluteDistance():number {
    const x = this.x - this.startX;
    const y = this.y - this.startY;
    return Math.sqrt(x * x + y * y);
  }


  /**
   * Return the current velocity of the pointer.
   *
   * @returns
   *   An object defining the current velocity.
   */
  getVelocity():IVector2 {
    this.reviseTrackingPoints();
    const count = this.track.length;
    if (count < 2) {
      return {x: 0, y: 0};
    }

    const first = this.track[0];
    const last = this.track[count - 1];
    const duration = (last.time - first.time) / 15;

    return {
      x: (last.x - first.x) / duration,
      y: (last.y - first.y) / duration
    };
  }


  /**
   * Add a tracking point for velocity calculation.
   *
   * @param x
   *   The horizontal position that should be tracked.
   * @param y
   *   The vertical position that should be tracked.
   * @param time
   *   The current timestamp if known.
   */
  protected addTrackingPoint(x:number, y:number, time:number = +(new Date())) {
    this.reviseTrackingPoints(time);
    this.track.push(<ITrackingPoint>{x, y, time});
  }


  /**
   * Remove tracking points that are no longer within the tracking period.
   *
   * @param time
   *   The current timestamp if known.
   */
  protected reviseTrackingPoints(time:number = +(new Date())) {
    while (this.track.length > 0) {
      if (time - this.track[0].time <= this.trackPeriod) break;
      this.track.shift();
    }
  }
}
