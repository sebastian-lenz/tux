import PointerHandler from './PointerHandler';
import Pointer, { PointerType } from './Pointer';


/**
 * Defines all states the HoldHandler can be in.
 */
export enum HoldState
{
  Idle,
  Pressed,
  Holding
}


/**
 * A handler that watches for long button presses.
 */
export default class HoldHandler extends PointerHandler
{
  /**
   * The current state the handler is in.
   */
  state:HoldState = HoldState.Idle;

  /**
   * Minimum amount of time in milliseconds until a pointer press is qualified as a hold action.
   */
  initialDelay:number = 400;

  /**
   * Timeout handle of the initial delay after the user has pressed the pointer.
   */
  initialDelayHandle:number = Number.NaN;

  /**
   * The distance the pointer is allowed to move and still qualify as a click.
   */
  threshold:number = 5;



  /**
   * Clear all running timeouts and intervals.
   */
  protected clearAllHandles() {
    if (!isNaN(this.initialDelayHandle)) {
      clearTimeout(this.initialDelayHandle);
      this.initialDelayHandle = Number.NaN;
    }
  }


  /**
   * Triggered after the user has pressed the pointer a minimum amount of
   * time over the view. Note that the given event is the initial start event.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the click.
   * @return
   *   TRUE if the event should be seen as a hold action, FALSE to abort the action.
   */
  handleHoldStart(event:Event, pointer:Pointer):boolean {
    return true;
  }


  /**
   * Triggered after the user has releases a pointer hold action.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the click.
   */
  handleHoldEnd(event:Event, pointer:Pointer) { }


  /**
   * Triggered after a hold action has been canceled.
   *
   * Delegated to handleHoldEnd by default.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the click.
   */
  handleHoldCancel(event, pointer) {
    this.handleHoldEnd(event, pointer)
  }


  /**
   * Triggered after the user has pressed and released the pointer ere the
   * minimum amount of time to be qualified as a press.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the click.
   */
  handleClick(event:Event, pointer:Pointer) { }


  /**
   * Triggered after a pointer has been pressed.
   *
   * @param event
   *   The original DOM event.
   * @param type
   *   The type of pointer that triggered the event.
   * @param x
   *   The horizontal position of the pointer.
   * @param y
   *   The vertical position of the pointer.
   * @returns
   *   A Pointer instance if the handler should become active, NULL otherwise.
   */
  handlePointerDown(event:Event, type:PointerType, x:number, y:number):Pointer {
    if (this.state != HoldState.Idle) return null;
    this.state = HoldState.Pressed;
    const pointer = new Pointer(type, x, y);

    this.clearAllHandles();
    this.initialDelayHandle = setTimeout(() => {
      this.initialDelayHandle = Number.NaN;

      if (this.handleHoldStart(event, pointer)) {
        this.state = HoldState.Holding;
      } else {
        this.state = HoldState.Idle;
      }
    }, this.initialDelay);

    return pointer;
  }


  /**
   * Triggered after the currently handled pointer has moved.
   *
   * @param event
   *   The dom event that triggered the event.
   * @param pointer
   *   The affected point.
   * @returns
   *   TRUE if the handler should still be active, FALSE otherwise.
   */
  handlePointerMove(event:Event, pointer:Pointer):boolean {
    return pointer.getAbsoluteDistance() < this.threshold;
  }


  /**
   * Triggered after a pointer has been released.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The affected point.
   */
  handlePointerUp(event:Event, pointer:Pointer) {
    if (this.state == HoldState.Pressed) {
      this.handleClick(event, pointer);
    } else if (this.state == HoldState.Holding) {
      this.handleHoldEnd(event, pointer);
    }

    this.clearAllHandles();
    this.state = HoldState.Idle;
  }


  /**
   * Triggered after the pointer has been canceled.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The affected point.
   */
  handlePointerCancel(event:Event, pointer:Pointer) {
    if (this.state == HoldState.Holding) {
      this.handleHoldCancel(event, pointer);
    }

    this.clearAllHandles();
    this.state = HoldState.Idle;
  }
}
