import { filter, forEach } from 'underscore';

import Delegate from '../views/View/Delegate';


export interface Listener {
  type: string;
  listener: EventListener;
}

export default class Handler
{
  /**
   * The view the handler should delegate its events to.
   */
  view: Delegate;

  /**
   * A css selector to filter events for.
   */
  selector: string | null;

  /**
   * Whether delegates should be registeres as passive handlers.
   */
  passive: boolean;

  listeners: Listener[] = [];


  constructor(view: Delegate, selector?: string, passive: boolean = true) {
    this.view = view;
    this.selector = selector;
    this.passive = passive;
  }

  /**
   * Dispose this handler.
   */
  remove() {
    forEach(this.listeners, ({ type, listener }) =>
      this.undelegate(type, listener)
    );

    this.listeners.length = 0;
  }

  /**
   * Delegate an event from the dom element to the given callback.
   *
   * @param type
   *   The name of the event that should be delegated.
   * @param listener
   *   The listener function that should be called.
   */
  delegate(type: string, listener: EventListener, scope: any = this) {
    const { passive, selector, view } = this;
    this.listeners.push({ type, listener });

    view.delegate(type, listener, { scope, selector, passive });
  }

  /**
   * Undelegate an event from the dom element to the given callback.
   *
   * @param type
   *   The name of the event that should be delegated.
   * @param listener
   *   The listener function that should be called.
   */
  undelegate(type: string, listener: EventListener) {
    const { selector, view } = this;
    this.listeners = filter(this.listeners, (delegate) =>
      delegate.type !== type || delegate.listener !== listener
    );

    view.undelegate(type, listener, { selector });
  }
}
