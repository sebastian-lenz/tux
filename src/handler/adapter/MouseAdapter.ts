import Adapter from './Adapter';
import DocumentView from '../../services/DocumentView';
import PointerHandler from '../PointerHandler';
import Pointer, { PointerType } from '../Pointer';


/**
 * An adapter that delegates mouse events.
 */
export default class MouseAdapter extends Adapter
{
  /**
   * The current mouse pointer.
   */
  pointer: Pointer;

  /**
   * Whether this adapter is currently listening for events or not.
   */
  isListening: boolean = false;


  /**
   * MouseAdapter constructor.
   *
   * @param handler
   *   The handler this adapter should be bound to.
   */
  constructor(handler: PointerHandler) {
    super(handler);
    handler.delegate('mousedown', this.onMouseDown, this);
  }

  /**
   * Dispose this adapter.
   */
  remove() {
    this.handler.undelegate('mousedown', this.onMouseDown);
    this.stopListening();
  }

  /**
   * Start listening for global mouse events.
   */
  startListening() {
    if (this.isListening) return;
    this.isListening = true;

    const { passive } = this.handler;
    const document = DocumentView.getInstance();
    document.delegate('mousemove', this.onMouseMove, { scope: this, passive });
    document.delegate('mouseup', this.onMouseUp, { scope: this, passive });
  }

  /**
   * Stop listening for global mouse events.
   */
  stopListening() {
    if (!this.isListening) return;
    this.isListening = false;

    const document = DocumentView.getInstance();
    document.undelegate('mousemove', this.onMouseMove, { scope: this });
    document.undelegate('mouseup', this.onMouseUp, { scope: this });
  }

  /**
   * Release the current pointer.
   */
  releasePointer() {
    this.pointer = null;
    this.stopListening();
  }

  /**
   * Triggered if a mouse button has been pressed.
   *
   * @param event
   *   The triggering mouse event.
   */
  onMouseDown(event: MouseEvent) {
    if (this.pointer || this.handler.isMouseBlocked()) {
      return;
    }

    const point = this.handler.handlePointerDown(event, PointerType.Mouse, event.pageX, event.pageY);
    if (point) {
      event.preventDefault();
      this.pointer = point;
      this.startListening();
    }
  }

  /**
   * Triggered if the mouse has moved while the adapter is listening.
   *
   * @param event
   *   The triggering mouse event.
   */
  onMouseMove(event: MouseEvent) {
    this.pointer.setPosition(event.pageX, event.pageY);

    if (!this.handler.handlePointerMove(event, this.pointer)) {
      this.handler.handlePointerCancel(event, this.pointer);
      this.releasePointer();
    }
  }

  /**
   * Triggered if a mouse button has been released while the adapter is listening.
   *
   * @param event
   *   The triggering mouse event.
   */
  onMouseUp(event: MouseEvent) {
    this.pointer.setPosition(event.pageX, event.pageY);
    this.handler.handlePointerUp(event, this.pointer);
    this.releasePointer();
  }
}
