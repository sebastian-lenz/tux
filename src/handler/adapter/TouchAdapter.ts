import Adapter from './Adapter';
import DocumentView from '../../services/DocumentView';
import PointerHandler from '../PointerHandler';
import Pointer, { PointerType } from '../Pointer';

/**
 * Test whether touch events are supported or not.
 */
const hasTouchEvents = 'ontouchstart' in window;
if (hasTouchEvents) {
  // iOS fix for event.preventDefault() not working
  try {
    window.addEventListener('touchmove', function() {}, {
      passive: false,
    } as any);
  } catch (e) {
    window.addEventListener('touchmove', function() {});
  }
}

/**
 * An adapter that delegates touch events.
 */
export default class TouchAdapter extends Adapter {
  /**
   * The handler this adapter is bound to.
   */
  handler: PointerHandler;

  /**
   * Whether this adapter is currently listening for events or not.
   */
  isListening: boolean = false;

  /**
   * A map of all pointers the adapter is currently tracking.
   */
  pointers: { [identifier: number]: Pointer } = {};

  /**
   * The number of pointers the adapter is currently tracking.
   */
  numPointers: number = 0;

  /**
   * TouchAdapter constructor.
   *
   * @param handler
   *   The handler this adapter should be bound to.
   */
  constructor(handler: PointerHandler) {
    super(handler);
    handler.delegate('touchstart', this.onTouchStart, this);
  }

  /**
   * Dispose this adapter.
   */
  remove() {
    this.handler.undelegate('touchstart', this.onTouchStart);
    this.stopListening();
  }

  /**
   * Start listening for global touch events.
   */
  startListening() {
    if (this.isListening) return;
    this.isListening = true;

    const { passive } = this.handler;
    const document = DocumentView.getInstance();
    document.delegate('touchmove', this.onTouchMove, { scope: this, passive });
    document.delegate('touchend', this.onTouchEnd, { scope: this, passive });
    document.delegate('touchcancel', this.onTouchCancel, {
      scope: this,
      passive,
    });
  }

  /**
   * Stop listening for global touch events.
   */
  stopListening() {
    if (!this.isListening) return;
    this.isListening = false;

    const document = DocumentView.getInstance();
    document.undelegate('touchmove', this.onTouchMove, { scope: this });
    document.undelegate('touchend', this.onTouchEnd, { scope: this });
    document.undelegate('touchcancel', this.onTouchCancel, { scope: this });
  }

  /**
   * Release the pointer with the given identifier.
   *
   * @param identifier
   *   The identifier of the pointer that should be released.
   */
  releasePointer(identifier: number) {
    delete this.pointers[identifier];
    this.numPointers -= 1;

    if (!this.numPointers && this.isListening) {
      this.stopListening();
    }
  }

  /**
   * Iterate over all changed touches of the given event and call the given
   * callback if a matching pointer is registered.
   *
   * @param event
   *   The event whose changed touches should be iterated.
   * @param callback
   *   The callback that will be invoked for each matched pointer.
   */
  eachChangedTouch(
    event: TouchEvent,
    callback: { (touch: Touch, point: Pointer) }
  ) {
    const count = event.changedTouches.length;
    const points = this.pointers;

    for (let index = 0; index < count; index++) {
      const touch = event.changedTouches[index];
      if (!points[touch.identifier]) {
        continue;
      }

      callback(touch, points[touch.identifier]);
    }
  }

  /**
   * Triggered if one or more touch points have been pressed.
   *
   * @param event
   *   The triggering touch event.
   */
  onTouchStart(event: TouchEvent) {
    const count = event.changedTouches.length;
    for (let index = 0; index < count; index++) {
      const touch = event.changedTouches[index];
      const point = this.handler.handlePointerDown(
        event,
        PointerType.Touch,
        touch.pageX,
        touch.pageY
      );

      if (point) {
        this.pointers[touch.identifier] = point;
        this.numPointers += 1;
      }
    }

    if (this.numPointers && !this.isListening) {
      this.startListening();
    }
  }

  /**
   * Triggered if one or more touch points have moved while the adapter is listening.
   *
   * @param event
   *   The triggering touch event.
   */
  onTouchMove(event: TouchEvent) {
    this.eachChangedTouch(event, (touch, point) => {
      point.setPosition(touch.pageX, touch.pageY);

      if (!this.handler.handlePointerMove(event, point)) {
        this.handler.handlePointerCancel(event, point);
        this.releasePointer(touch.identifier);
      }
    });
  }

  /**
   * Triggered if one or more touch points have been released while the adapter is listening.
   *
   * @param event
   *   The triggering touch event.
   */
  onTouchEnd(event: TouchEvent) {
    this.eachChangedTouch(event, (touch, point) => {
      point.setPosition(touch.pageX, touch.pageY);

      this.handler.handlePointerUp(event, point);
      this.releasePointer(touch.identifier);
    });

    this.handler.blockMouseEvents();
  }

  /**
   * Triggered if one or more touch points have been canceled while the adapter is listening.
   *
   * @param event
   *   The triggering touch event.
   */
  onTouchCancel(event: TouchEvent) {
    this.eachChangedTouch(event, (touch, point) => {
      this.handler.handlePointerCancel(event, point);
      this.releasePointer(touch.identifier);
    });

    this.handler.blockMouseEvents();
  }

  /**
   * Return whether touch events are supported or not.
   *
   * @returns
   *   TRUE if touch events are supported or not, FALSE otherwise.
   */
  static isSupported(): boolean {
    return hasTouchEvents;
  }
}
