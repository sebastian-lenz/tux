import Adapter from './Adapter';
import DocumentView from '../../services/DocumentView';
import PointerHandler from '../PointerHandler';
import Pointer, { PointerType } from '../Pointer';


/**
 * Define the vendor prefixed event names.
 */
const eventPrefix = window.navigator.pointerEnabled ? '' : window.navigator.msPointerEnabled ? 'ms' : '';
export const eventDown   = eventPrefix ? 'MSPointerDown'   : 'pointerdown';
export const eventMove   = eventPrefix ? 'MSPointerMove'   : 'pointermove';
export const eventUp     = eventPrefix ? 'MSPointerUp'     : 'pointerup';
export const eventCancel = eventPrefix ? 'MSPointerCancel' : 'pointercancel';


/**
 * Test whether pointer events are supported or not.
 */
const hasPointerEvents = 'on' + eventDown in window;


/**
 * An adapter that delegates pointer events.
 */
export default class PointerAdapter extends Adapter
{
  /**
   * Whether this adapter is currently listening for events or not.
   */
  isListening: boolean = false;

  /**
   * A map of all pointers the adapter is currently tracking.
   */
  pointers:{[pointerId:number]:Pointer} = {};

  /**
   * The number of pointers the adapter is currently tracking.
   */
  numPointers: number = 0;


  /**
   * PointerAdapter constructor.
   *
   * @param handler
   *   The handler this adapter should be bound to.
   */
  constructor(handler:PointerHandler) {
    super(handler);
    handler.delegate(eventDown, this.onPointerDown, this);
  }

  /**
   * Dispose this adapter.
   */
  remove() {
    this.handler.undelegate(eventDown, this.onPointerDown);
    this.stopListening();
  }

  /**
   * Start listening for global pointer events.
   */
  startListening() {
    if (this.isListening) return;
    this.isListening = true;

    const { passive } = this.handler;
    const document = DocumentView.getInstance();
    document.delegate(eventMove, this.onPointerMove, { scope: this, passive });
    document.delegate(eventUp, this.onPointerUp, { scope: this, passive });
    document.delegate(eventCancel, this.onPointerCancel, { scope: this, passive });
  }

  /**
   * Stop listening for global pointer events.
   */
  stopListening() {
    if (!this.isListening) return;
    this.isListening = false;

    const document = DocumentView.getInstance();
    document.undelegate(eventMove, this.onPointerMove, { scope: this });
    document.undelegate(eventUp, this.onPointerUp, { scope: this });
    document.undelegate(eventCancel, this.onPointerCancel, { scope: this });
  }

  /**
   * Release the pointer with the given pointer id.
   *
   * @param pointerId
   *   The id of the pointer that should be released.
   */
  releasePointer(pointerId: number) {
    delete this.pointers[pointerId];
    this.numPointers -= 1;

    if (!this.numPointers && this.isListening) {
      this.stopListening();
    }
  }

  /**
   * Triggered if a pointer has been pressed.
   *
   * @param event
   *   The triggering pointer event.
   */
  onPointerDown(event: PointerEvent) {
    const type  = PointerAdapter.toPointerType(event.pointerType);
    const point = this.handler.handlePointerDown(event, type, event.pageX, event.pageY);

    if (point) {
      this.pointers[event.pointerId] = point;
      this.numPointers += 1;
    }

    if (this.numPointers && !this.isListening) {
      this.startListening();
    }
  };

  /**
   * Triggered if a pointer has moved while the adapter is listening.
   *
   * @param event
   *   The triggering pointer event.
   */
  onPointerMove(event: PointerEvent) {
    const point = this.pointers[event.pointerId];
    if (point) {
      point.setPosition(event.pageX, event.pageY);

      if (!this.handler.handlePointerMove(event, point)) {
        this.handler.handlePointerCancel(event, point);
        this.releasePointer(event.pointerId);
      }
    }
  };

  /**
   * Triggered if a pointer has been released while the adapter is listening.
   *
   * @param event
   *   The triggering pointer event.
   */
  onPointerUp(event: PointerEvent) {
    const point = this.pointers[event.pointerId];
    if (point) {
      point.setPosition(event.pageX, event.pageY);

      this.handler.handlePointerUp(event, point);
      this.releasePointer(event.pointerId);
    }
  };

  /**
   * Triggered if a pointer has been canceled while the adapter is listening.
   *
   * @param event
   *   The triggering pointer event.
   */
  onPointerCancel(event: PointerEvent) {
    const point = this.pointers[event.pointerId];
    if (point) {
      this.handler.handlePointerCancel(event, point);
      this.releasePointer(event.pointerId);
    }
  };

  /**
   * Convert the given type identifier to a PointerType value.
   *
   * @param type
   *   The type identifier as returned by a pointer event.
   * @returns
   *   The matching internal PointerType value.
   */
  static toPointerType(type:string):PointerType {
    switch (type) {
      case 'pen':   return PointerType.Pen;
      case 'touch': return PointerType.Touch;
    }

    return PointerType.Mouse;
  }

  /**
   * Return whether pointer events are supported or not.
   *
   * @returns
   *   TRUE if pointer events are supported or not, FALSE otherwise.
   */
  static isSupported(): boolean {
    return hasPointerEvents;
  }
}
