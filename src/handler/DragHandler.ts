import Delegate from '../views/View/Delegate';
import DocumentView from '../services/DocumentView';
import PointerHandler from './PointerHandler';
import Pointer, { PointerType } from './Pointer';

/**
 * Defines the states a DragHandler instance can be in.
 */
export const enum DragState {
  Idle,
  Watching,
  Draging,
}

/**
 * Defines the available drag directions.
 */
export enum DragDirection {
  Horizontal = 1,
  Vertical = 2,
  Both = Horizontal | Vertical,
}

/**
 * A pointer handler that generates drag events.
 */
export default class DragHandler extends PointerHandler {
  /**
   * The current state the drag handler is in.
   */
  state: DragState = DragState.Idle;

  /**
   * The allowed drag direction.
   */
  direction: DragDirection = DragDirection.Both;

  /**
   * The distance the pointer has to move to be seen as dragging.
   */
  threshold: number = 3;

  constructor(view: Delegate, selector?: string, passive: boolean = false) {
    super(view, selector, passive);
  }

  /**
   * Triggered after a drag has started.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the drag start.
   * @returns
   *   TRUE if the drag operation should be started, FALSE otherwise.
   */
  handleDragStart(event: Event, pointer: Pointer): boolean {
    return true;
  }

  /**
   * Triggered after a drag has moved.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the move.
   * @returns
   *   TRUE if the drag operation should be started, FALSE otherwise.
   */
  handleDragMove(event: Event, pointer: Pointer): boolean {
    return true;
  }

  /**
   * Triggered after a drag has ended.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the movement.
   */
  handleDragEnd(event: Event, pointer: Pointer) {}

  /**
   * Triggered after the drag has been canceled.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the movement.
   */
  handleDragCancel(event: Event, pointer: Pointer) {}

  /**
   * Triggered after the user has clicked into the view.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the click.
   */
  handleClick(event: Event, pointer: Pointer) {}

  /**
   * Triggered after a pointer has been pressed.
   *
   * @param event
   *   The original DOM event.
   * @param type
   *   The type of pointer that triggered the event.
   * @param x
   *   The horizontal position of the pointer.
   * @param y
   *   The vertical position of the pointer.
   * @returns
   *   A Pointer instance if the handler should become active, NULL otherwise.
   */
  handlePointerDown(
    event: Event,
    type: PointerType,
    x: number,
    y: number
  ): Pointer {
    if (this.state != DragState.Idle) {
      return null;
    }

    if (type == PointerType.Mouse) {
      event.preventDefault();
    }

    this.state = DragState.Watching;
    return new Pointer(type, x, y);
  }

  /**
   * Triggered after the currently handeled pointer has moved.
   *
   * @param event
   *   The dom event that triggered the event.
   * @param pointer
   *   The affected point.
   * @returns
   *   TRUE if the handler should still be active, FALSE otherwise.
   */
  handlePointerMove(event: Event, pointer: Pointer): boolean {
    const delta = pointer.getAbsoluteDelta();

    if (this.state === DragState.Watching) {
      if (pointer.type == PointerType.Mouse) {
        event.preventDefault();
      }

      var distance = Math.sqrt(delta.x * delta.x + delta.y * delta.y);
      if (distance < this.threshold) {
        return true;
      }

      if (this.direction == DragDirection.Horizontal) {
        if (Math.abs(delta.x) < Math.abs(delta.y)) {
          return false;
        }
      } else if (this.direction == DragDirection.Vertical) {
        if (Math.abs(delta.x) > Math.abs(delta.y)) {
          return false;
        }
      }

      if (this.handleDragStart(event, pointer)) {
        event.stopImmediatePropagation();
        this.state = DragState.Draging;
      } else {
        return false;
      }
    }

    if (this.state === DragState.Draging) {
      event.stopImmediatePropagation();
      event.preventDefault();

      if (this.handleDragMove(event, pointer)) {
        return true;
      } else {
        this.state = DragState.Idle;
        this.handleDragCancel(event, pointer);
        return false;
      }
    }

    return false;
  }

  /**
   * Triggered after a pointer has been released.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The affected pointer.
   */
  handlePointerUp(event: Event, pointer: Pointer) {
    if (this.state == DragState.Draging) {
      this.handleDragEnd(event, pointer);
      DocumentView.getInstance().preventClick();
    } else {
      this.handleClick(event, pointer);
    }

    this.state = DragState.Idle;
  }

  /**
   * Triggered after the pointer has been canceled.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The affected pointer.
   */
  handlePointerCancel(event: Event, pointer: Pointer) {
    if (this.state == DragState.Draging) {
      this.handleDragCancel(event, pointer);
    }

    this.state = DragState.Idle;
  }
}
