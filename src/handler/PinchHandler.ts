import View from '../views/View';
import { IVector2 } from '../geom/Vector2';
import DocumentView from '../services/DocumentView';
import PointerHandler from './PointerHandler';
import Pointer, { PointerType } from './Pointer';
import { DragDirection } from './DragHandler';


/**
 * Defines the states a PinchHandler instance can be in.
 */
export const enum PinchState {
  Idle,
  Watching,
  Draging,
  Pinching
}


/**
 * Data structure used to store tracking points.
 */
export interface IVelocity extends IVector2 {
  distance:number;
}

export interface ITrackingPoint extends IVelocity {
  time:number;
}


/**
 * An event listener that prevents the default action.
 */
const preventDefault = (e:Event) => e.preventDefault();


/**
 * A handler that generated multi touch transforms.
 */
export default class PinchHandler extends PointerHandler
{
  /**
   * The current state the handler is in.
   */
  state:PinchState = PinchState.Idle;

  /**
   * The allowed drag direction.
   */
  direction:DragDirection = DragDirection.Both;

  /**
   * The first pointer the handler is watching.
   */
  firstPoint:Pointer = null;

  /**
   * The second pointer the handler is watching.
   */
  secondPoint:Pointer = null;

  /**
   * The distance the pointer has to move to be seen as dragging.
   */
  threshold:number = 5;

  /**
   * The initial position of the pointer or touch center.
   */
  initialPosition:IVector2 = null;

  /**
   * The initial distance between the two touch points.
   */
  initialDistance:number = 0;

  /**
   * A list of tracking points used to calculate the velocity.
   */
  track:ITrackingPoint[] = [];

  /**
   * The duration in milliseconds tracking points should be collected.
   */
  trackPeriod:number = 100;



  /**
   * PointerHandler constructor.
   *
   * @param view
   *   The view the handler should delegate its events to.
   * @param selector
   *   An optional css selector to filter events for.
   */
  constructor(view:View, selector?:string) {
    super(view, selector);

    view.delegate('gesturestart', preventDefault, { selector });
    view.delegate('gesturechange', preventDefault, { selector });
  }


  /**
   * Return the center of the two active pointers.
   *
   * @returns
   *   An object describing the center of the two active pointers.
   */
  getPointerCenter():IVector2 {
    if (!this.firstPoint) {
      return { x:0, y:0 };
    } else if (!this.secondPoint) {
      return { x:this.firstPoint.x, y:this.firstPoint.y };
    }

    return {
      x: (this.firstPoint.x + this.secondPoint.x) * 0.5,
      y: (this.firstPoint.y + this.secondPoint.y) * 0.5,
    };
  }


  /**
   * Return the distance between the two active touch points.
   *
   * @returns
   *   The distance between the two active touch points.
   */
  getPointerDistance():number {
    if (!this.firstPoint || !this.secondPoint) {
      return 0;
    }

    const x = this.firstPoint.x - this.secondPoint.x;
    const y = this.firstPoint.y - this.secondPoint.y;
    return Math.sqrt(x * x + y * y);
  }


  /**
   * Return the current velocity of the pointer.
   *
   * @returns
   *   An object defining the current velocity.
   */
  getVelocity():IVelocity {
    this.reviseTrackingPoints();
    const count = this.track.length;
    if (count < 2) {
      return { x:0, y:0, distance:0 };
    }

    const first = this.track[0];
    const last  = this.track[count - 1];
    const duration = (last.time - first.time) / 15;

    return {
      x: (last.x - first.x) / duration,
      y: (last.y - first.y) / duration,
      distance: (last.distance - first.distance) / duration,
    };
  }


  /**
   * Add a tracking point for velocity calculation.
   *
   * @param x
   *   The horizontal position that should be tracked.
   * @param y
   *   The vertical position that should be tracked.
   * @param distance
   *   The distance that should be tracked.
   * @param time
   *   The current timestamp if known.
   */
  protected addTrackingPoint(x:number, y:number, distance:number, time:number = +(new Date())) {
    this.reviseTrackingPoints(time);
    this.track.push(<ITrackingPoint>{x, y, distance, time});
  }


  /**
   * Remove tracking points that are no longer within the tracking period.
   *
   * @param time
   *   The current timestamp if known.
   */
  protected reviseTrackingPoints(time:number = +(new Date())) {
    while (this.track.length > 0) {
      if (time - this.track[0].time <= this.trackPeriod) break;
      this.track.shift();
    }
  }


  /**
   * Triggered if the state of the adapter is about to change.
   *
   * @param newState
   *   The new state the adapter wants to transform to.
   * @param oldState
   *   The old state the adapter was in.
   */
  handleStateChange(newState:PinchState, oldState:PinchState) { }


  /**
   * Triggered if the transform of the pinch gesture has changed.
   *
   * @param x
   *   The horizontal translation of the transform.
   * @param y
   *   The vertical translation of the transform.
   * @param scale
   *   The relative scale of the transform.
   */
  handleTransform(x:number, y:number, scale:number) { }


  /**
   * Triggered after a pointer has been pressed.
   *
   * @param event
   *   The original DOM event.
   * @param type
   *   The type of pointer that triggered the event.
   * @param x
   *   The horizontal position of the pointer.
   * @param y
   *   The vertical position of the pointer.
   * @returns
   *   A Pointer instance if the handler should become active, NULL otherwise.
   */
  handlePointerDown(event:Event, type:PointerType, x:number, y:number):Pointer {
    if (this.state == PinchState.Pinching) {
      return null;
    }

    if (type == PointerType.Mouse) {
      event.preventDefault();
    }

    const oldState = this.state;
    const point = new Pointer(type, x, y);
    if (this.state == PinchState.Idle) {
      this.state      = PinchState.Watching;
      this.firstPoint = point;
    } else {
      this.state           = PinchState.Pinching;
      this.secondPoint     = point;
      this.initialPosition = this.getPointerCenter();
      this.initialDistance = this.getPointerDistance();
    }

    this.handleStateChange(this.state, oldState);
    return point;
  }


  /**
   * Triggered after the currently handeled pointer has moved.
   *
   * @param event
   *   The dom event that triggered the event.
   * @param pointer
   *   The affected point.
   * @returns
   *   TRUE if the handler should still be active, FALSE otherwise.
   */
  handlePointerMove(event:Event, pointer:Pointer):boolean {
    const delta = pointer.getAbsoluteDelta();

    if (this.state === PinchState.Watching) {
      if (pointer.type == PointerType.Mouse) {
        event.preventDefault();
      }

      var distance = Math.sqrt(delta.x * delta.x + delta.y * delta.y);
      if (distance < this.threshold) {
        return true;
      }

      if (this.direction == DragDirection.Horizontal) {
        if (Math.abs(delta.x) < Math.abs(delta.y)) {
          return false;
        }
      } else if (this.direction == DragDirection.Vertical) {
        if (Math.abs(delta.x) > Math.abs(delta.y)) {
          return false;
        }
      }

      this.state           = PinchState.Draging;
      this.initialPosition = { x:pointer.x, y:pointer.y };
      this.initialDistance = 0;

      event.stopImmediatePropagation();
      this.handleStateChange(PinchState.Draging, PinchState.Watching);
    }

    if (this.state === PinchState.Draging) {
      event.stopImmediatePropagation();
      event.preventDefault();

      if (this.trackPeriod > 0) {
        this.addTrackingPoint(pointer.x, pointer.y, 0);
      }

      this.handleTransform(
          pointer.x - this.initialPosition.x,
          pointer.y - this.initialPosition.y,
          1
      );
    }
    else if (this.state == PinchState.Pinching) {
      const center   = this.getPointerCenter();
      const distance = this.getPointerDistance();

      if (this.trackPeriod > 0) {
        this.addTrackingPoint(center.x, center.y, distance / this.initialDistance);
      }

      this.handleTransform(
          center.x - this.initialPosition.x,
          center.y - this.initialPosition.y,
          distance / this.initialDistance
      );
    }

    return true;
  }


  /**
   * Triggered after a pointer has been released.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The affected pointer.
   */
  handlePointerUp(event:Event, pointer:Pointer) {
    const oldState = this.state;
    if (this.state == PinchState.Pinching) {
      if (pointer == this.firstPoint) {
        this.firstPoint = this.secondPoint;
      }

      this.firstPoint.startX = this.firstPoint.x;
      this.firstPoint.startY = this.firstPoint.y;
      this.secondPoint       = null;
      this.initialPosition   = { x:this.firstPoint.x, y:this.firstPoint.y };
      this.state             = PinchState.Watching;
    } else {
      this.state = PinchState.Idle;
    }

    this.handleStateChange(this.state, oldState);
    this.track.length = 0;
    DocumentView.getInstance().preventClick();
  }


  /**
   * Triggered after the pointer has been canceled.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The affected pointer.
   */
  handlePointerCancel(event:Event, pointer:Pointer) {
    this.handlePointerUp(event, pointer);
  }
}
