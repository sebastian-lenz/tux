import View from '../views/View';
import { getWheelEventName, normalizeWheel, IWheelData } from '../util/polyfill/normalizeWheel';
import Handler from './Handler';


const WHEEL_EVENT: string = getWheelEventName();

export { IWheelData } from '../util/polyfill/normalizeWheel';

export default class WheelHandler extends Handler
{
  /**
   * The view the handler should delegate its events to.
   */
  view:View;

  selector:string;


  constructor(view: View, selector?: string) {
    super(view, selector);
    this.delegate(WHEEL_EVENT, this.onWheel);
  }

  /**
   * Dispose this handler.
   */
  remove() {
    this.undelegate(WHEEL_EVENT, this.onWheel);
  }

  /**
   * Triggered after the user has used the mouse wheel.
   *
   * @param event
   *   The original dom event.
   * @param data
   *   The normalized scroll data.
   * @returns
   *   TRUE if the event has been handled, FALSE otherwise.
   */
  handleWheel(event: Event, data: IWheelData):boolean {
    return false;
  }

  onWheel(event: Event) {
    if (this.handleWheel(event, normalizeWheel(event))) {
      event.preventDefault();
    }
  }
}
