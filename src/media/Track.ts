import { defaults } from 'underscore';

import Collection from '../models/Collection';
import Model from '../models/Model';


/**
 * Attribute structure of the Track model.
 */
export interface ITrackAttributes
{
  /**
   * The source url of this track.
   */
  source:string;

  /**
   * The duration of this track.
   */
  duration?:number;

  /**
   * The title of this track.
   */
  title?:string;
}


/**
 * A model representing a single media track.
 */
export class Track extends Model
{
  /**
   * The attributes of this track.
   */
  attributes:ITrackAttributes;



  /**
   * Track constructor.
   *
   * @param attributes
   *   Default Track attributes.
   * @param options
   *   Track constructor options.
   */
  constructor(attributes?:ITrackAttributes, options?:any) {
    super(defaults(attributes || {}, {
      source: null,
      duration: 0
    }), options);
  }


  /**
   * Return the source url of this track.
   */
  getSource():string {
    return this.attributes.source;
  }


  /**
   * Return the duration of this track.
   */
  getDuration():number {
    return this.attributes.duration;
  }


  /**
   * Set the duration of this track.
   */
  setDuration(value:number) {
    return this.set('duration', value);
  }


  /**
   * Return the title of this track.
   */
  getTitle():string {
    return this.attributes.title;
  }
}


/**
 * A collection of media tracks.
 */
export class TrackList extends Collection<Track>
{

}
