import { defaults } from 'underscore';

import View, { ViewOptions } from '../../views/View';
import { IPlayer, PlayState } from '../Player';


/**
 * PlayButton constructor options.
 */
export interface IPlayButtonOptions extends ViewOptions
{
  /**
   * The player the play button should be attached to.
   */
  player:IPlayer;
}


/**
 * Display a play/pause button.
 */
export class PlayButton extends View
{
  /**
   * The player this play button is attached to.
   */
  player:IPlayer;

  /**
   * The last applied play state css class.
   */
  playStateClass:string;



  /**
   * PlayButton constructor.
   *
   * @param options
   *   PlayButton constructor options.
   */
  constructor(options?:IPlayButtonOptions) {
    super(defaults(options || {}, {
      className: 'tuxPlayButton'
    }));

    this.delegateClick(this.onClick);

    if (options.player) {
      this.setPlayer(options.player);
    }
  }


  /**
   * Set the player this seek bar should be attached to.
   *
   * @param player
   *   The player this seekbar should be attached to.
   */
  setPlayer(player:IPlayer) {
    if (this.player == player) return;
    if (this.player) {
      this.stopListening(this.player);
    }

    this.player = player;
    if (player) {
      this.listenTo(player, 'state', this.onStateChanged);
      this.onStateChanged(player.getPlayState());
    }
  }


  /**
   * Triggered after the play state has changed.
   */
  onStateChanged(state:PlayState) {
    if (this.playStateClass) {
      this.element.classList.remove(this.playStateClass);
    }

    this.playStateClass = PlayState[state].toLowerCase();
    this.element.classList.add(this.playStateClass);
  }


  /**
   * Triggered after the user has clicked onto this button.
   */
  onClick():boolean {
    const player = this.player;
    if (player) {
      if (!player.getTrack()) {
        this.trigger('playEmpty');
        return true;
      }

      const state = player.getPlayState();
      if (state == PlayState.Playing) {
        this.player.pause();
      } else {
        if (state == PlayState.Ended) {
          player.setCurrentTime(0);
        }

        this.player.play();
      }
    }

    return true;
  }
}
