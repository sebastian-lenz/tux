import DragHandler, { DragDirection } from '../../handler/DragHandler';
import Pointer from '../../handler/Pointer';
import { SeekBar } from './SeekBar';
import { PlayState } from '../Player';


/**
 * User input handler for the SeekBar view.
 */
export class SeekBarHandler extends DragHandler
{
  /**
   * The view the handler should delegate its events to.
   */
  view: SeekBar;

  /**
   * Did we pause the player when seeking began?
   */
  didPause: boolean = false;


  /**
   * SeekBarHandler constructor.
   *
   * @param view
   *   The view the handler should delegate its events to.
   */
  constructor(view: SeekBar) {
    super(view);
    this.direction = DragDirection.Horizontal;
  }

  /**
   * Triggered after a drag has started.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the drag start.
   * @returns
   *   TRUE if the drag operation should be started, FALSE otherwise.
   */
  handleDragStart(event: Event, pointer: Pointer): boolean {
    const player = this.view.player;
    if (player && player.getPlayState() == PlayState.Playing) {
      player.pause();
      this.didPause = true;
    } else {
      this.didPause = false;
    }

    return true;
  }

  /**
   * Triggered after a drag has moved.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the move.
   * @returns
   *   TRUE if the drag operation should be started, FALSE otherwise.
   */
  handleDragMove(event: Event, pointer: Pointer): boolean {
    const seekbar = this.view;
    const player  = seekbar.player;
    const rect    = seekbar.element.getBoundingClientRect();
    const percent = (pointer.x - rect.left) / rect.width;

    if (player) {
      player.setCurrentTime(percent * player.getDuration());
    }

    return true;
  }

  /**
   * Triggered after a drag has ended.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the movement.
   */
  handleDragEnd(event: Event, pointer: Pointer) {
    const player = this.view.player;
    if (player && this.didPause) {
      player.play();
    }
  }

  /**
   * Triggered after the drag has been canceled.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the movement.
   */
  handleDragCancel(event: Event, pointer: Pointer) {
    this.handleDragEnd(event, pointer);
  }

  /**
   * Triggered after the user has clicked into the view.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the click.
   */
  handleClick(event: Event, pointer: Pointer) {
    if (this.view.emitClick) {
      this.view.trigger('click', event);
    } else {
      this.handleDragMove(event, pointer);
    }
  }
}
