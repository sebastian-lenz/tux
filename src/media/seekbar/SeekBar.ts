import { defaults } from 'underscore';

import View, { ViewOptions, option } from '../../views/View';
import { IPlayer } from '../Player';
import { SeekBarHandler } from './SeekBarHandler';

/**
 * Defines all available draw modes of the buffer chunks.
 */
export enum BufferDisplayMode
{
  None,
  Simple,
  Chunks
}

/**
 * SekBar constructor options.
 */
export interface ISeekBarOptions extends ViewOptions
{
  /**
   * The player the seek bar should be attached to.
   */
  player: IPlayer;

  /**
   * Should click dispatch an event?
   */
  emitClick?: boolean;
}

/**
 * Render a seek bar that displays the current play time of a player
 * and allows the user to seek the media file.
 */
export class SeekBar extends View
{
  /**
   * The player this seek bar is attached to.
   */
  player: IPlayer;

  /**
   * The element displaying the current time.
   */
  @option({
    type: 'element',
    defaultValue: '.tuxSeekBar__currentTime',
    tagName: 'div',
    className: 'tuxSeekBar__currentTime',
  })
  currentTime: HTMLDivElement;

  /**
   * The element displaying the duration.
   */
  @option({
    type: 'element',
    defaultValue: '.tuxSeekBar__duration',
    tagName: 'div',
    className: 'tuxSeekBar__duration',
  })
  duration: HTMLDivElement;

  /**
   * The element displaying the progress of the media playback.
   */
  @option({
    type: 'element',
    defaultValue: '.tuxSeekBar__progress',
    tagName: 'div',
    className: 'tuxSeekBar__progress',
  })
  progress: HTMLDivElement;

  /**
   * The element displaying the buffer state.
   */
  @option({
    type: 'element',
    defaultValue: '.tuxSeekBar__buffer',
    tagName: 'div',
    className: 'tuxSeekBar__buffer',
  })
  buffer: HTMLDivElement;

  /**
   * A list of all displayed buffer chunk elements.
   */
  bufferChunks: HTMLDivElement[] = [];

  /**
   * Specifies how buffer chunks should be rendered.
   */
  @option({
    type: 'enum',
    values: BufferDisplayMode,
    defaultValue: BufferDisplayMode.Simple,
  })
  bufferDisplayMode: BufferDisplayMode;

  /**
   * Should click dispatch an event?
   */
  @option({ type: 'bool' })
  emitClick: boolean;


  /**
   * SeekBar constructor.
   *
   * @param options
   *   SekBar constructor options.
   */
  constructor(options?: ISeekBarOptions) {
    super(defaults(options || {}, {
      className: 'tuxSeekBar'
    }));

    if (options.player) {
      this.setPlayer(options.player);
    }

    new SeekBarHandler(this);
  }

  /**
   * Set the player this seek bar should be attached to.
   *
   * @param player
   *   The player this seekbar should be attached to.
   */
  setPlayer(player: IPlayer) {
    if (this.player) {
      this.stopListening(this.player);
    }

    this.player = player;
    if (player) {
      this.listenTo(player, 'duration', this.onPlayerDuration);
      this.listenTo(player, 'currentTime', this.onPlayerCurrentTime);
      this.listenTo(player, 'buffer', this.onPlayerBuffer);

      this.onPlayerDuration(player.getDuration());
      this.onPlayerCurrentTime(player.getCurrentTime());
      this.onPlayerBuffer(player.getBuffer());
    }
  }

  formatTime(time: number): string {
    return SeekBar.formatTime(time);
  }

  /**
   * Update the progress bar.
   */
  updateProgress() {
    this.progress.style.width = ((this.player.getCurrentTime() / this.player.getDuration()) * 100).toFixed(3) + '%';
  }

  /**
   * Triggered after the position of the play head has moved.
   */
  onPlayerDuration(duration: number) {
    this.duration.innerHTML = this.formatTime(duration);
    this.updateProgress();
  }

  /**
   * Triggered after the position of the play head has moved.
   */
  onPlayerCurrentTime(currentTime: number) {
    this.currentTime.innerHTML = this.formatTime(currentTime);
    this.updateProgress();
  }

  /**
   * Triggered after the buffer state has changed.
   */
  onPlayerBuffer(buffers: TimeRanges) {
    const buffer   = this.buffer;
    const chunks   = this.bufferChunks;
    const mode     = this.bufferDisplayMode;
    const duration = this.player.getDuration();

    let count:number;
    switch (mode) {
      case BufferDisplayMode.None:
        count = 0;
        break;
      case BufferDisplayMode.Simple:
        count = 1;
        break;
      case BufferDisplayMode.Chunks:
        count = buffers.length;
        break;
    }

    while (chunks.length > count) {
      buffer.removeChild(chunks.pop());
    }

    while (chunks.length < count) {
      const chunk = document.createElement('div');
      chunk.className = 'tuxSeekBar__bufferChunk';
      buffer.appendChild(chunk);
      chunks.push(chunk);
    }

    if (mode == BufferDisplayMode.Simple) {
      const chunk = chunks[0];
      chunk.style.left  = '0';

      if (buffers.length) {
        const end = buffers.end(buffers.length - 1);
        chunk.style.right = ((1 - end / duration) * 100).toFixed(3) + '%';
      } else {
        chunk.style.right = '0';
      }
    }
    else if (mode == BufferDisplayMode.Chunks) {
      for (let index = 0; index < count; index++) {
        const start = buffers.start(index);
        const end   = buffers.end(index);
        const chunk = chunks[index];

        chunk.style.left  = ((start / duration)   * 100).toFixed(3) + '%';
        chunk.style.right = ((1 - end / duration) * 100).toFixed(3) + '%';
      }
    }
  }

  static formatTime(time: number): string {
    const minutes = Math.floor(time / 60);
    const seconds = Math.round(time % 60);
    return (minutes < 10 ? '0' : '') + minutes.toString() + ':' + (seconds < 10 ? '0' : '') + seconds.toString();
  }
}
