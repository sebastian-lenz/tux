import Events from '../Events';
import { Track, TrackList } from './Track';


export enum PlayState
{
  Empty,
  Unstarted,
  Buffering,
  Ended,
  Paused,
  Playing,
  Error
}


/**
 * Basic interface all players must implement.
 */
export interface IPlayer extends Events
{
  /**
   * Start this player.
   */
  play();

  /**
   * Pause this player.
   */
  pause();

  /**
   * Pause and rewind this player.
   */
  stop();

  /**
   * Return the total duration of the media file.
   */
  getDuration():number;

  /**
   * Return the current play state of this player.
   */
  getPlayState():PlayState;

  /**
   * Return the current play head position.
   */
  getCurrentTime():number;

  /**
   * Set the current play head position.
   */
  setCurrentTime(value:number);

  /**
   * Return the time ranges of the buffered media file parts.
   */
  getBuffer():TimeRanges;

  getTrack():Track;

  setTrack(track:Track):this;

  setTrackList(trackList:TrackList):this;

  getTrackList():TrackList;
}
