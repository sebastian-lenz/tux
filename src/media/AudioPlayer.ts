import View from '../views/View';
import { Track, TrackList } from './Track';
import { IPlayer, PlayState } from './Player';


/**
 * A basic audio player.
 *
 * abort	Sent when playback is aborted; for example, if the media is playing and is restarted from the beginning, this event is sent.
 * emptied	The media has become empty; for example, this event is sent if the media has already been loaded (or partially loaded), and the load() method is called to reload it.
 * canplay	Sent when enough data is available that the media can be played, at least for a couple of frames.  This corresponds to the HAVE_ENOUGH_DATA readyState.
 * canplaythrough	Sent when the ready state changes to CAN_PLAY_THROUGH, indicating that the entire media can be played without interruption, assuming the download rate remains at least at the current level. It will also be fired when playback is toggled between paused and playing. Note: Manually setting the currentTime will eventually fire a canplaythrough event in firefox. Other browsers might not fire this event.
 * loadeddata	The first frame of the media has finished loading.
 * loadedmetadata	The media's metadata has finished loading; all attributes now contain as much useful information as they're going to.
 * seeked	Sent when a seek operation completes.
 * seeking	Sent when a seek operation begins.
 * stalled	Sent when the user agent is trying to fetch media data, but data is unexpectedly not forthcoming.
 * suspend	Sent when loading of the media is suspended; this may happen either because the download has completed or because it has been paused for any other reason.
 * waiting   Sent when the requested operation (such as playback) is delayed pending the completion of another operation (such as a seek).
 *
 * @event "track" (track:Track):void
 *   Triggered after the track has changed.
 * @event "state" (state:PlayState):void
 *   Triggered after the play state has changed.
 * @event "currentTime" (currentTime:number):void
 *   Triggered after the position of the play head has moved.
 * @event "duration" (duration:number):void
 *   Triggered after the duration of the media has changed.
 * @event "volume" (volume:number):void
 *   Triggered after the volume has changed.
 * @event "buffer" (buffer:TimeRanges):void
 *   Triggered after the buffer state has changed.
 */
export class AudioPlayer extends View implements IPlayer
{
  /**
   * The audio element that is used to play back the sound.
   */
  element: HTMLAudioElement;

  /**
   * The current play state of this player.
   */
  state: PlayState = PlayState.Empty;

  /**
   * The position of the playhead.
   */
  currentTime: number = 0;

  duration: number = 0;

  volume: number = 0;

  track: Track;

  trackList: TrackList;



  /**
   * AudioPlayer constructor.
   */
  constructor(options?:any) {
    super({
      tagName: 'audio',
      className: 'tuxAudioPlayer'
    });

    this.volume = this.element.volume;

    this.delegate('play', null, this.onStateChanged);
    this.delegate('playing', null, this.onStateChanged);
    this.delegate('ended', null, this.onStateChanged);
    this.delegate('pause', null, this.onStateChanged);
    this.delegate('error', null, this.onStateChanged);

    this.delegate('timeupdate', null, this.onCurrentTimeChange);
    this.delegate('durationchange', null, this.onDurationChange);
    this.delegate('volumechange', null, this.onVolumeChange);
    this.delegate('progress', null, this.onBufferChange);
  }

  /**
   * Start this player.
   */
  play() {
    if (this.track) {
      this.element.play();
    }
  }

  /**
   * Pause this player.
   */
  pause() {
    if (this.track) {
      this.element.pause();
    }
  }

  /**
   * Pause and rewind this player.
   */
  stop() {
    if (this.track) {
      this.element.pause();
      this.setCurrentTime(0);
    }
  }

  previous() {
    this.skip(-1);
  }

  next() {
    this.skip(1);
  }

  skip(direction:number) {
    const trackList = this.trackList;
    if (!trackList) return;

    let index = trackList.indexOf(this.track);
    if (index == -1) return;

    index += direction;
    if (index < 0 || index >= trackList.length) return;

    this.setTrack(this.trackList.at(index));
  }

  /**
   * Return the current play state of this player.
   */
  getPlayState():PlayState {
    return this.state;
  }

  /**
   * Return the total duration of the media file.
   */
  getDuration():number {
    return this.duration;
  }

  /**
   * Return the current play head position.
   */
  getCurrentTime():number {
    return this.currentTime;
  }

  /**
   * Return the time ranges of the buffered media file parts.
   */
  getBuffer():TimeRanges {
    return this.element.buffered;
  }

  setCurrentTime(value:number) {
    this.element.currentTime = value;
  }

  getVolume():number {
    return this.volume;
  }

  setVolume(value:number) {
    this.element.volume = value;
  }

  getTrack():Track {
    return this.track;
  }

  setTrack(track:Track):this {
    if (this.track == track) return this;
    if (this.trackList && this.trackList.indexOf(track) == -1) {
      this.setTrackList(null);
    }

    this.track = track;
    if (track) {
      this.element.src = track.getSource();
      this.duration    = track.getDuration();
      this.state       = PlayState.Unstarted;
    } else {
      this.element.src = '';
      this.duration    = 0;
      this.state       = PlayState.Empty;
    }

    this.currentTime = 0;
    this.trigger('currentTime', 0);
    this.trigger('state', this.state);
    this.trigger('duration', this.duration);
    this.trigger('track', track);

    return this;
  }

  getTrackList():TrackList {
    return this.trackList;
  }

  setTrackList(trackList:TrackList):this {
    this.trackList = trackList;
    if (this.track && trackList.indexOf(this.track) == -1) {
      this.setTrack(null);
    }

    this.trackList = trackList;
    return this;
  }

  /**
   * Triggered by one of the events indicating the media element has entered a new state.
   *
   * @param event
   */
  onStateChanged = (event: Event) => {
    let state:PlayState = this.state;

    switch (event.type) {
      // Sent when loading of the media begins.
      case 'loadstart':
        state = PlayState.Buffering;
        break;

      // Sent when playback of the media starts after having been paused; that is, when playback is resumed after a prior pause event.
      case 'play':
      // Sent when the media begins to play (either for the first time, after having been paused, or after ending and then restarting).
      case 'playing':
        state = PlayState.Playing;
        break;

      // Sent when playback completes.
      case 'ended':
        state = PlayState.Ended;
        break;

      // Sent when playback is paused.
      case 'pause':
        state = PlayState.Paused;
        break;

      // Sent when an error occurs.  The element's error attribute contains more information. See Error handling for details.
      case 'error':
        state = PlayState.Error;
        break;
    }

    if (this.state != state) {
      this.state = state;
      this.trigger('state', state);
    }

    if (state == PlayState.Ended && this.trackList) {
      this.next();
    }
  };

  /**
   * The time indicated by the element's currentTime attribute has changed.
   *
   * @param event
   */
  onCurrentTimeChange = (event: Event) => {
    const currentTime = this.element.currentTime;
    if (this.currentTime == currentTime) return;

    this.currentTime = currentTime;
    this.trigger('currentTime', currentTime);
  };

  /**
   * The metadata has loaded or changed, indicating a change in duration of the media.
   *
   * This is sent, for example, when the media has loaded enough that the duration is known.
   *
   * @param event
   */
  onDurationChange = (event: Event) => {
    const duration = this.element.duration;
    if (this.duration == duration) return;

    this.duration = duration;
    this.track.setDuration(duration);
    this.trigger('duration', duration);
  };

  /**
   * Sent when the audio volume changes (both when the volume is set and when the muted attribute is changed).
   *
   * @param event
   */
  onVolumeChange = (event: Event) => {
    const volume = this.element.volume;
    if (this.volume == volume) return;

    this.volume = volume;
    this.trigger('volume', volume);
  };

  /**
   * Sent periodically to inform interested parties of progress downloading the media.
   *
   * Information about the current amount of the media that has been downloaded is available
   * in the media element's buffered attribute.
   *
   * @param event
   */
  onBufferChange = (event: Event) => {
    this.trigger('buffer', this.element.buffered);
  };
}
