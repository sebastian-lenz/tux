import services, { service } from '../services';
import Dispatcher from '../services/Dispatcher';
import { IAnimation } from './Animation';


/**
 * A central animation manager and updater.
 */
@service('AnimationManager')
export class AnimationManager
{
  /**
   * A list of all active animations.
   */
  animations: IAnimation[] = [];


  /**
   * AnimationManager constructor.
   */
  constructor() {
    Dispatcher.getInstance().on('frame', this.onFrame)
  }

  /**
   * Add an animation to the manager.
   *
   * @param animation
   *   The animation that should be added.
   */
  add(animation:IAnimation) {
    const animations = this.animations;
    const accessor = animation.accessor;
    let count = animations.length;
    let index = 0;

    while (index < count) {
      const animation = animations[index];
      if (animation.accessor.equals(accessor)) {
        animation.stop();
        animations.splice(index, 1);
        count--;
      } else {
        index++;
      }
    }

    animations.push(animation);
  }

  /**
   * Stop all animations matching the given arguments.
   *
   * @param target
   *   The target object animations should be stopped for.
   * @param property
   *   The property name animations should be stopped for.
   */
  stop(target?:any, property?:string) {
    const animations = this.animations.slice();

    for (const animation of animations) {
      const accessor = animation.accessor;
      if (target   && accessor.target   != target)   continue;
      if (property && accessor.property != property) continue;

      this.remove(animation);
    }
  }

  /**
   * Remove an animation from the manager.
   *
   * @param animation
   *   The animation that should be removed.
   */
  remove(animation:IAnimation) {
    const index = this.animations.indexOf(animation);
    if (index != -1) {
      animation.stop();
      this.animations.splice(index, 1);
    }
  }

  /**
   * Triggered on every browser repaint.
   */
  onFrame = (timeStep:number) => {
    const animations = this.animations.slice();
    const timeScale  = timeStep / 16.666;

    for (const animation of animations) {
      if (!animation.update(timeStep, timeScale)) {
        this.remove(animation);
      }
    }
  };

  /**
   * Return the current service instance of the AnimationManager.
   *
   * @returns
   *    The current service instance of the AnimationManager.
   */
  static getInstance():AnimationManager {
    return services.get('AnimationManager', AnimationManager);
  }
}


/**
 * Stop all animations matching the given arguments.
 *
 * @param target
 *   The target object animations should be stopped for.
 * @param property
 *   The property name animations should be stopped for.
 */
export function stopAnimations(target?:any, property?:string) {
  AnimationManager.getInstance().stop(target, property);
}
