import { defaults } from 'underscore';

import PluginManager, { IPlugin } from '../../services/PluginManager';
import { Accessor } from '../accessor/Accessor';
import { EasingFunction } from '../easing/Type';
import { easeInOutQuart } from '../easing/quart';
import { Animation, AnimationState, IAnimationOptions } from '../Animation';
import { AnimationManager } from '../AnimationManager';
import { IVectorType, ILerpableType } from '../../geom/Type';


export type AnyTween = Tween<AnyTweenDataType>;

export type AnyTweenDataType = number|IVectorType|ILerpableType;

export type AnyITweenOptions = ITweenOptions<AnyTweenDataType>;


/**
 * Constructor options for the Tween class.
 */
export interface ITweenOptions<T> extends IAnimationOptions
{
  /**
   * The initial property value.
   */
  from?:T;

  /**
   * The value the property should be animated to.
   */
  to:T;

  /**
   * Duration of the tween in milliseconds.
   */
  duration?:number;

  /**
   * Delay till the tween starts in milliseconds.
   */
  delay?:number;

  /**
   * The easing function that should be used.
   */
  easing?:EasingFunction;
}


/**
 * Default options applied to all tweens.
 */
export const defaultTweenOptions:ITweenOptions<any> = {
  to:       void 0,
  duration: 500,
  delay:    0,
  easing:   easeInOutQuart
};


/**
 * Tween plugin interface.
 */
export interface ITweenPlugin extends IPlugin
{
  /**
   * Try to create an instance of this tween plugin.
   *
   * @param accessor
   *   The accessor pointing to the property that should be animated.
   * @param options
   *   The desired tween constructor options.
   * @param initialValue
   *   The current value of the target property.
   * @returns
   *   An instance of the tween plugin class if applicable, NULL otherwise.
   */
  tryCreate(accessor:Accessor, options:ITweenOptions<any>, initialValue:any):AnyTween;
}


/**
 * The plugin group used by the tween factory.
 */
const pluginGroup = PluginManager.getInstance().getGroup<ITweenPlugin>('tween');


/**
 * A simple time based property tween.
 */
export abstract class Tween<T> extends Animation
{
  /**
   * The current time of this tween.
   */
  currentTime:number;

  /**
   * The total duration of this tween.
   */
  duration:number;

  /**
   * The easing function that should be used.
   */
  easing:EasingFunction;

  /**
   * The initial value of the tweened property.
   */
  initialValue:T;

  /**
   * The target value of the tweened property.
   */
  targetValue:T;



  /**
   * Tween constructor.
   *
   * @param accessor
   *   An accessor pointing to the property that should be animated.
   * @param options
   *   The desired tween options.
   */
  constructor(accessor:Accessor, options:ITweenOptions<T>) {
    super(accessor, options);

    options = defaults(options, defaultTweenOptions);
    this.currentTime  = -options.delay;
    this.duration     = options.duration;
    this.easing       = options.easing;
    this.initialValue = options.from ? options.from : accessor.getValue();
    this.targetValue  = options.to;

    this.initialize(options);
  }


  /**
   * Initialize this tween. Called by the constructor.
   *
   * @param options
   *   The processed options passed to the constructor.
   */
  protected initialize(options:ITweenOptions<T>) { }


  /**
   * Calculate and return the current tween value.
   *
   * @returns
   *   The current tween value.
   */
  abstract getCurrentValue():T;


  /**
   * Update this animation.
   *
   * @param timeStep
   *   The time passed since the last update call in milliseconds.
   * @returns
   *   TRUE if this animation continues, FALSE if it has stopped.
   */
  update(timeStep:number):boolean {
    if (this.state & AnimationState.FinishedOrStopped) {
      return false;
    }

    this.currentTime += timeStep;
    if (this.currentTime >= this.duration) {
      this.currentTime = this.duration;
      this.accessor.setValue(this.targetValue);
      this.setState(AnimationState.Finished);
      return false;
    }
    else if (this.currentTime > 0) {
      this.accessor.setValue(this.getCurrentValue());
      this.setState(AnimationState.Playing);
    }

    return true;
  }


  /**
   * Create a new tween for the given accessor.
   *
   * @param accessor
   *   An accessor pointing to the property that should be animated.
   * @param options
   *   The desired tween options.
   * @returns
   *   A tween instance or null if no tween could be created.
   */
  static create(accessor:Accessor, options:AnyITweenOptions):AnyTween {
    const initialValue = accessor.getValue();

    for (const plugin of pluginGroup.plugins) {
      const tween = plugin.tryCreate(accessor, options, initialValue);
      if (tween != null) {
        return tween;
      }
    }

    return null;
  }
}


/**
 * Create a new tween for the given property on the target object.
 *
 * @param target
 *   The object whose property should be animated.
 * @param property
 *   The property that should be animated.
 * @param options
 *   The desired tween options.
 * @returns
 *   A tween instance.
 */
export function createTween(target:any, property:string, options:AnyITweenOptions):AnyTween
{
  const accessor = Accessor.create(target, property);
  if (accessor == null) {
    throw new Error(`Could not create an accessor for the property "${property}" on "${target}", did you import all necessary accessor classes?`);
  }

  const tween = Tween.create(accessor, options);
  if (tween == null) {
    throw new Error(`Could not create a tween for the property "${property}" on "${target}", did you import all necessary tween classes?`);
  }

  AnimationManager.getInstance().add(tween);
  return tween;
}
