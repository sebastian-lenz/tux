import { plugin } from '../../services/PluginManager';
import {IVectorType, isVectorType} from '../../geom/Type';
import {Accessor} from '../accessor/Accessor';
import {Tween, ITweenPlugin, ITweenOptions} from './Tween';


/**
 * A tween that animates types with multiple numeric fields like vectors or colors.
 */
@plugin<ITweenPlugin>('tween', { tryCreate:VectorTween.tryCreate })
export class VectorTween extends Tween<IVectorType>
{
  /**
   * The initial value array.
   */
  initialValues:number[];

  /**
   * The delta between the initial values and the target values.
   */
  deltaValues:number[];



  /**
   * Initialize this tween. Called by the constructor.
   *
   * @param options
   *   The processed options passed to the constructor.
   */
  protected initialize(options:ITweenOptions<IVectorType>) {
    const from  = this.initialValue.getValues();
    const delta = this.targetValue.getValues();

    for (let index = 0, count = from.length; index < count; index++) {
      delta[index] -= from[index];
    }

    this.initialValues = from;
    this.deltaValues   = delta;
  }


  /**
   * Calculate and return the current tween value.
   *
   * @returns
   *   The current tween value.
   */
  getCurrentValue():IVectorType {
    const current  = this.currentTime;
    const duration = this.duration;
    const base     = this.initialValues;
    const change   = this.deltaValues;
    const values   = [];

    for (let index = 0, count = base.length; index < count; index++) {
      values[index] = this.easing(current, base[index], change[index], duration);
    }

    return this.initialValue.clone().setValues(values);
  }


  /**
   * Try to create an instance of this tween plugin.
   *
   * @param accessor
   *   The accessor pointing to the property that should be animated.
   * @param options
   *   The desired tween constructor options.
   * @param initialValue
   *   The current value of the target property.
   * @returns
   *   An instance of the tween plugin class if applicable, NULL otherwise.
   */
  static tryCreate(accessor:Accessor, options:ITweenOptions<any>, initialValue:any) {
    if (isVectorType(initialValue)) {
      const to = initialValue.convert(options.to);
      if (to == null) {
        throw new Error('Type mismatch: Tween initial and target value must be of the same type.');
      }

      options.to = to;
      return new VectorTween(accessor, options);
    } else {
      return null;
    }
  }
}
