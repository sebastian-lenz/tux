import { plugin } from '../../services/PluginManager';
import { ILerpableType, isLerpableType } from '../../geom/Type';
import { Accessor } from '../accessor/Accessor';
import { Tween, ITweenPlugin, ITweenOptions } from './Tween';


/**
 * A tween that animates types exposing a lerp function.
 */
@plugin<ITweenPlugin>('tween', { tryCreate:LerpTween.tryCreate })
export class LerpTween extends Tween<ILerpableType>
{
  /**
   * Calculate and return the current tween value.
   *
   * @returns
   *   The current tween value.
   */
  getCurrentValue():ILerpableType {
    return this.initialValue.lerp(this.targetValue, this.easing(this.currentTime, 0, 1, this.duration));
  }


  /**
   * Try to create an instance of this tween plugin.
   *
   * @param accessor
   *   The accessor pointing to the property that should be animated.
   * @param options
   *   The desired tween constructor options.
   * @param initialValue
   *   The current value of the target property.
   * @returns
   *   An instance of the tween plugin class if applicable, NULL otherwise.
   */
  static tryCreate(accessor:Accessor, options:ITweenOptions<any>, initialValue:any) {
    if (isLerpableType(initialValue)) {
      const to = initialValue.convert(options.to);
      if (to == null) {
        throw new Error('Type mismatch: Tween initial and target value must be of the same type.');
      }

      options.to = to;
      return new LerpTween(accessor, options);
    } else {
      return null;
    }
  }
}
