import transition from '../../util/vendor/transition';


/**
 * Run the given callback while all transitions on the given
 * element are blocked.
 */
export default function withoutTransition(
  element: HTMLElement,
  callback: Function
) {
  if (!transition.styleName) {
    callback();
  } else {
    element.style[transition.styleName] = 'none';
    callback();
    element.getBoundingClientRect();
    element.style[transition.styleName] = '';
  }
}
