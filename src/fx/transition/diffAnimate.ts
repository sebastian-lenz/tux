import { defaults, forEach } from 'underscore';

import { DiffCallback, DiffResult } from './diff';
import diffPositions, { DiffPositionsOptions } from './diffPositions';
import animation from '../../util/vendor/animation'


/**
 * Available options for [[diffAnimate]].
 */
export interface DiffAnimateOptions extends DiffPositionsOptions
{
  /**
   * Should deleted elements be detached?
   */
  detach?: boolean;

  origin?: HTMLElement;
}


/**
 * Animate the changed positions of the given elements, fade in new elements and fade out deleted elements.
 *
 * @param initialElements  A list of all persistent elements.
 * @param callback  A callback that changes the visible elements and returns the new list.
 * @param options   The advanced options used to animate the elements.
 */
export default function diffAnimate(
  initialElements: HTMLElement[],
  callback: DiffCallback,
  options?: DiffAnimateOptions
): DiffResult {
  const result = diffPositions(initialElements, callback, options);
  if (!animation.endEvent) return result;

  const { endEvent } = animation;
  const { detach, origin } = defaults(options || {}, {
    detach: false,
  });

  let shiftTop = 0, shiftLeft = 0;
  if (origin) {
    const originRect = origin.getBoundingClientRect();
    shiftTop = -originRect.top;
    shiftLeft = -originRect.left;
  }

  forEach(result.created, created => {
    const { element, inViewport } = created;
    if (!inViewport) return;

    const onEnd = () => {
      element.classList.remove('fadeIn');
      element.removeEventListener(endEvent, onEnd);
    };

    element.addEventListener(endEvent, onEnd);
    element.classList.add('fadeIn');
  });

  forEach(result.deleted, deleted => {
    const { element, inViewport, position } = deleted;
    if (!inViewport) return;

    const onEnd = () => {
      element.classList.remove('fadeOut');
      element.style.position = '';
      element.style.top = '';
      element.style.left = '';
      element.removeEventListener(endEvent, onEnd);

      if (detach && element.parentNode) {
        element.parentNode.removeChild(element);
      }
    };

    element.addEventListener(endEvent, onEnd);
    element.classList.add('fadeOut');
    element.style.position = 'absolute';
    element.style.top = `${position.top + shiftTop}px`;
    element.style.left = `${position.left + shiftLeft}px`;
  });

  return result;
}
