import { plugin } from '../../services/PluginManager';
import {IComputableType, isComputableType} from '../../geom/Type';
import {Accessor} from '../accessor/Accessor';
import {Spring, ISpringPlugin, ISpringOptions} from './Spring';


/**
 * A spring animation that operates on a computable type.
 */
@plugin<ISpringPlugin>('spring', { tryCreate:ComputeableSpring.tryCreate })
export class ComputeableSpring extends Spring<IComputableType>
{
  /**
   * Initialize this spring. Called by the constructor.
   *
   * @param options
   *   The processed options passed to the constructor.
   */
  protected initialize(options:ISpringOptions<IComputableType>) {
    this.velocity = this.currentValue.clone().reset();
    this.currentValue = this.currentValue.clone();
  }


  /**
   * Test whether the given values equal each other.
   *
   * @param oldValue
   *   The old value to test.
   * @param newValue
   *   The new value to test.
   * @returns
   *   TRUE if the two given values differ, FALSE otherwise.
   */
  protected areValuesEqual(oldValue:IComputableType, newValue:IComputableType) {
    return oldValue.equals(newValue);
  }


  /**
   * Advance the spring simulation, update the current value of the spring.
   *
   * @param progress
   *   The desired progress to apply based upon the current frame time.
   * @returns
   *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
   */
  protected advance(progress:number):boolean {
    const value  = this.currentValue;
    const target = this.targetValue;
    const delta  = target.clone().subtract(value);

    if (delta.length() < this.epsilon) {
      value.copy(target);
      return false;
    }
    else {
      delta.scale(this.acceleration);
      value.add(this.velocity.scale(this.damp).add(delta));
      return true;
    }
  }


  /**
   * Try to create an instance of this spring plugin.
   *
   * @param accessor
   *   The accessor pointing to the property that should be animated.
   * @param options
   *   The desired spring constructor options.
   * @param initialValue
   *   The current value of the target property.
   * @returns
   *   An instance of the spring plugin class if applicable, NULL otherwise.
   */
  static tryCreate(accessor:Accessor, options:ISpringOptions<any>, initialValue:any):ComputeableSpring {
    if (isComputableType(initialValue)) {
      const to = initialValue.convert(options.to);
      if (to == null) {
        throw new Error('Type mismatch: Spring initial and target value must be of the same type.');
      }

      options.to = to;
      return new ComputeableSpring(accessor, options);
    } else {
      return null;
    }
  }
}
