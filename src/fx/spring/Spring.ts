import { defaults } from 'underscore';

import { Accessor } from '../accessor/Accessor';
import { Animation, AnimationState, IAnimationOptions } from '../Animation';
import PluginManager, { IPlugin } from '../../services/PluginManager';
import { AnimationManager } from '../AnimationManager';
import { IComputableType } from '../../geom/Type';


export type AnySpringDataType = number|IComputableType;

export type AnySpring = Spring<AnySpringDataType>;

export type AnyISpringOptions = ISpringOptions<AnySpringDataType>;


/**
 * Constructor options for the Spring class.
 */
export interface ISpringOptions<T> extends IAnimationOptions
{
  /**
   * The initial property value.
   */
  from?:T;

  /**
   * The value the property should be animated to.
   */
  to:T;

  /**
   * The amount of velocity to keep between steps.
   */
  damp?:number;

  /**
   * The amount of power to add to the velocity each frame.
   */
  acceleration?:number;

  /**
   * The value difference the spring should stop at.
   */
  epsilon?:number;
}


/**
 * Spring plugin interface.
 */
export interface ISpringPlugin extends IPlugin
{
  /**
   * Try to create an instance of this tween plugin.
   *
   * @param accessor
   *   The accessor pointing to the property that should be animated.
   * @param options
   *   The desired tween constructor options.
   * @param initialValue
   *   The current value of the target property.
   * @returns
   *   An instance of the tween plugin class if applicable, NULL otherwise.
   */
  tryCreate(accessor:Accessor, options:ISpringOptions<any>, initialValue:any):AnySpring;
}


/**
 * The plugin group used by the spring factory.
 */
const pluginGroup = PluginManager
  .getInstance()
  .getGroup<ISpringPlugin>('spring');


/**
 * Default options applied to all spring animations.
 */
export const defaultSpringOptions:ISpringOptions<any> = {
  to:           void 0,
  damp:         0.3,
  acceleration: 0.15,
  epsilon:      0.001
};


/**
 * A spring based animation.
 */
export abstract class Spring<T> extends Animation
{
  /**
   * The accessor used by this animation to read and write the affected property.
   */
  accessor:Accessor;

  /**
   * The current value.
   */
  currentValue:T;

  /**
   * The target value.
   */
  targetValue:T;

  /**
   * The current velocity.
   */
  velocity:T;

  /**
   * The amount of velocity to keep between steps.
   */
  damp:number;

  /**
   * The amount of power to add to the velocity each frame.
   */
  acceleration:number;

  /**
   * The value difference the spring should stop at.
   */
  epsilon:number;



  /**
   * Spring constructor.
   *
   * @param accessor
   *   An accessor pointing to the property that should be animated.
   * @param options
   *   The desired spring options.
   */
  constructor(accessor:Accessor, options:ISpringOptions<T>) {
    super(accessor, options);

    this.currentValue = options.from ? options.from : accessor.getValue();
    this.state = AnimationState.Playing;

    options = defaults(options, defaultSpringOptions);
    this.initialize(options);
    this.proceed(options);
  }


  /**
   * Initialize this spring. Called by the constructor.
   *
   * @param options
   *   The processed options passed to the constructor.
   */
  protected initialize(options:ISpringOptions<T>) { }


  /**
   * Test whether the given values equal each other.
   *
   * @param oldValue
   *   The old value to test.
   * @param newValue
   *   The new value to test.
   * @returns
   *   TRUE if the two given values differ, FALSE otherwise.
   */
  protected areValuesEqual(oldValue:T, newValue:T) {
    return oldValue == newValue;
  }


  /**
   * Continue this spring animations with the given options.
   *
   * If the animation was finished or stopped it will be added to the list of
   * active animations again. When reusing an old spring instance keep in mind
   * that callbacks of the promise might not work as expected.
   *
   * @param options
   *   The desired spring options.
   */
  proceed(options:ISpringOptions<T>) {
    this.targetValue = options.to;

    if (options.acceleration !== void 0) {
      this.acceleration = options.acceleration;
    }

    if (options.damp !== void 0) {
      this.damp = options.damp;
    }

    if (options.epsilon !== void 0) {
      this.epsilon = options.epsilon;
    }

    if (this.state != AnimationState.Playing && !this.areValuesEqual(this.targetValue, options.to)) {
      this.state = AnimationState.Playing;
      AnimationManager.getInstance().add(this);
    }
  }


  /**
   * Update this animation.
   *
   * @param timeStep
   *   The time passed since the last update call in milliseconds.
   * @param timeScale
   *   The relative length of the last frame (timeStep / 16).
   * @returns
   *   TRUE if this animation continues, FALSE if it has stopped.
   */
  update(timeStep:number, timeScale):boolean {
    if (this.state & AnimationState.FinishedOrStopped) {
      return false;
    }

    if (this.advance(timeScale)) {
      this.accessor.setValue(this.currentValue);
      return true;
    }
    else
    {
      this.accessor.setValue(this.targetValue);
      this.setState(AnimationState.Finished);
      return false;
    }
  }


  /**
   * Advance the spring simulation, update the current value of the spring.
   *
   * @param progress
   *   The desired progress to apply based upon the current frame time.
   * @returns
   *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
   */
  protected abstract advance(progress:number):boolean;


  /**
   * Create a new spring for the given accessor.
   *
   * @param accessor
   *   An accessor pointing to the property that should be animated.
   * @param options
   *   The desired spring options.
   * @returns
   *   A spring instance or null if no spring could be created.
   */
  static create(accessor:Accessor, options:ISpringOptions<any>):AnySpring {
    const initialValue = accessor.getValue();

    for (const plugin of pluginGroup.plugins) {
      const spring = plugin.tryCreate(accessor, options, initialValue);
      if (spring != null) {
        return spring;
      }
    }

    return null;
  }
}


export function retrieveSpring(target:any, property:string):AnySpring {
  const manager = AnimationManager.getInstance();

  for (const animation of manager.animations) {
    const accessor = animation.accessor;

    if (accessor.target == target && accessor.property == property) {
      if (animation instanceof Spring) {
        return <AnySpring>animation;
      } else {
        return null;
      }
    }
  }

  return null;
}


/**
 * Create a new spring for the given property on the target object.
 *
 * @param target
 *   The object whose property should be animated.
 * @param property
 *   The property that should be animated.
 * @param options
 *   The desired spring options.
 * @returns
 *   A spring instance.
 */
export function createSpring(target:any, property:string, options:ISpringOptions<any>):AnySpring
{
  const accessor = Accessor.create(target, property);
  if (accessor == null) {
    throw new Error(`Could not create an accessor for the property "${property}" on "${target}", did you import all necessary accessor classes?`);
  }

  const spring = Spring.create(accessor, options);
  if (spring == null) {
    throw new Error(`Could not create a spring for the property "${property}" on "${target}", did you import all necessary spring classes?`);
  }

  AnimationManager.getInstance().add(spring);
  return spring;
}
