import { plugin } from '../../services/PluginManager';
import {Accessor} from '../accessor/Accessor';
import {Spring, ISpringOptions, ISpringPlugin} from './Spring';


/**
 * A spring that operates on a numeric value.
 */
@plugin<ISpringPlugin>('spring', { tryCreate:NumericSpring.tryCreate })
export class NumericSpring extends Spring<number>
{
  /**
   * Initialize this spring. Called by the constructor.
   *
   * @param options
   *   The processed options passed to the constructor.
   */
  protected initialize(options:ISpringOptions<number>) {
    this.velocity = 0;
  }


  /**
   * Advance the spring simulation, update the current value of the spring.
   *
   * @param progress
   *   The desired progress to apply based upon the current frame time.
   * @returns
   *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
   */
  protected advance(progress:number):boolean {
    const delta = this.targetValue - this.currentValue;

    if (Math.abs(delta) < this.epsilon) {
      this.currentValue = this.targetValue;
      return false
    }
    else {
      this.velocity = (this.velocity * this.damp + delta * this.acceleration);
      this.currentValue += this.velocity;
      return true;
    }
  }


  /**
   * Try to create an instance of this spring plugin.
   *
   * @param accessor
   *   The accessor pointing to the property that should be animated.
   * @param options
   *   The desired spring constructor options.
   * @param initialValue
   *   The current value of the target property.
   * @returns
   *   An instance of the spring plugin class if applicable, NULL otherwise.
   */
  static tryCreate(accessor:Accessor, options:ISpringOptions<any>, initialValue:any):NumericSpring {
    if (typeof initialValue !== 'number') {
      return null;
    }

    if (typeof options.to !== 'number') {
      throw new Error('Type mismatch: Spring target value must be numeric.');
    }

    return new NumericSpring(accessor, options);
  }
}
