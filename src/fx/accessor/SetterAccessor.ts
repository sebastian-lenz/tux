import { plugin } from "../../services/PluginManager";
import { IAccessorPlugin, Accessor } from "./Accessor";


/**
 * An accessor that maps properties that can be directly read but have a setter method.
 */
@plugin<IAccessorPlugin>('accessor', { priority:-5, isApplicable:SetterAccessor.isApplicable })
export class SetterAccessor extends Accessor
{
  /**
   * The function used to set the property value.
   */
  setter:Function;



  /**
   * Accessor constructor.
   *
   * @param target
   *   The target object the property should be fetched from.
   * @param property
   *   The name of the property that should be accessed.
   */
  constructor(target:any, property:string) {
    super(target, property);
    this.setter = SetterAccessor.getMethod(target, property, 'set');
  }


  /**
   * Return the raw value of this accessor.
   *
   * @returns
   *   The raw property value.
   */
  getValue():any {
    return this.target[this.property];
  }


  /**
   * Set the raw value of this accessor.
   *
   * @param value
   *   The raw value that should be set.
   */
  setValue(value:any) {
    this.setter.call(this.target, value);
  }


  /**
   * Test whether this accessor is applicable on the given target and property.
   *
   * @param target
   *   The target object that should be tested.
   * @param property
   *   The property name that should be tested.
   * @returns
   *   TRUE if this accessor class can access the property, FALSE otherwise.
   */
  static isApplicable(target:any, property:string):boolean {
    return !!SetterAccessor.getMethod(target, property, 'set');
  }


  /**
   * Return the method with the given name of the target object.
   *
   * @param target
   *   The target object whose method should be returned.
   * @param name
   *   The name of the method to retrieve.
   * @param prefix
   *   An optional prefix for the given name.
   * @returns
   *   The found method or null if the method does not exist.
   */
  static getMethod(target:any, name:string, prefix?:string):Function {
    if (prefix) {
      name = SetterAccessor.getMethodName(prefix, name);
    }

    if (!target[name]) {
      return null;
    }

    const method = target[name];
    if (typeof method === 'function') {
      return method;
    } else {
      return null;
    }
  }


  /**
   * Return the name of an accessor method.
   *
   * @param prefix
   *   The prefix of the method name, e.g. 'get'.
   * @param name
   *   The name of the matching property.
   * @return
   *   The name of the accessor method.
   */
  static getMethodName(prefix:string, name:string) {
    return prefix + name.charAt(0).toUpperCase() + name.slice(1);
  }
}
