import { plugin } from "../../services/PluginManager";
import { Accessor, IAccessorPlugin } from "./Accessor";


/**
 * An accessor that maps properties that are directly accessed.
 */
@plugin<IAccessorPlugin>('accessor', { priority:-10, isApplicable:PropertyAccessor.isApplicable })
export class PropertyAccessor extends Accessor
{
  /**
   * Return the raw value of this accessor.
   *
   * @returns
   *   The raw property value.
   */
  getValue():any {
    return this.target[this.property];
  }


  /**
   * Set the raw value of this accessor.
   *
   * @param value
   *   The raw value that should be set.
   */
  setValue(value:any) {
    this.target[this.property] = value;
  }


  /**
   * Test whether this accessor is applicable on the given target and property.
   *
   * @param target
   *   The target object that should be tested.
   * @param property
   *   The property name that should be tested.
   * @returns
   *   TRUE if this accessor class can access the property, FALSE otherwise.
   */
  static isApplicable(target:any, property:string):boolean {
    return target.hasOwnProperty(property);
  }
}
