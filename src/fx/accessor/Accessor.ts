import PluginManager, { IPlugin } from '../../services/PluginManager';


/**
 * The plugin group used by the accessor factory.
 */
const pluginGroup = PluginManager
  .getInstance()
  .getGroup<IAccessorPlugin>('accessor');

/**
 * Accessor plugin definition.
 */
export interface IAccessorPlugin extends IPlugin
{
  isApplicable:{(target:any, property:string):boolean};

  priority?:number;
}


/**
 * Base class of all property accessors.
 */
export abstract class Accessor
{
  /**
   * The target object the property should be fetched from.
   */
  target:any;

  /**
   * The name of the property that should be accessed.
   */
  property:string;



  /**
   * Accessor constructor.
   *
   * @param target
   *   The target object the property should be fetched from.
   * @param property
   *   The name of the property that should be accessed.
   */
  constructor(target:any, property:string) {
    this.target   = target;
    this.property = property;
  }


  /**
   * Return the raw value of this accessor.
   *
   * @returns
   *   The raw property value.
   */
  abstract getValue():any;


  /**
   * Set the raw value of this accessor.
   *
   * @param value
   *   The raw value that should be set.
   */
  abstract setValue(value:any);


  /**
   * Test whether the given accessor equals this accessor.
   *
   * @param other
   *   The other accessor that should be tested against.
   * @returns
   *   TRUE if this and the other accessor affect the same object and
   *   property, FALSE otherwise.
   */
  equals(other:Accessor):boolean {
    return this.target == other.target && this.property == other.property;
  }


  /**
   * Create a new accessor for the given property on the target object.
   *
   * @param target
   *   The target object the property should be fetched from.
   * @param property
   *   The name of the property that should be accessed.
   * @returns
   *   An accessor instance or null if no accessor could be created.
   */
  static create(target:any, property:string):Accessor {
    let pluginClass:any = null;
    let priority:number = 0;

    for (const plugin of pluginGroup.plugins) {
      if (plugin.isApplicable(target, property)) {
        const pluginPriority = plugin.priority ? plugin.priority : 0;
        if (!pluginClass || priority < pluginPriority) {
          pluginClass = plugin.pluginClass;
          priority    = pluginPriority;
        }
      }
    }

    if (pluginClass) {
      return new pluginClass(target, property);
    } else {
      return null;
    }
  }
}
