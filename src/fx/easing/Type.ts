/**
 * Easing function declaration.
 */
export type EasingFunction = { (time:number, base:number, change:number, duration:number):number; }
