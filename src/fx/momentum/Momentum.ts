import { defaults } from 'underscore';

import PluginManager, { IPlugin } from '../../services/PluginManager';
import { IVectorType } from '../../geom/Type';
import { Accessor } from '../accessor';
import { EasingFunction } from '../easing/Type';
import { easeOutExpo } from '../easing/expo';
import { Animation, IAnimationOptions, AnimationState } from '../Animation';
import { AnimationManager } from '../AnimationManager';


export type AnyMomentumDataType = number|IVectorType;

export type AnyMomentum = Momentum<AnyMomentumDataType>;

export type AnyIMomentumOptions = IMomentumOptions<AnyMomentumDataType>;


/**
 * Constructor options for the Momentum class.
 */
export interface IMomentumOptions<T> extends IAnimationOptions
{
  /**
   * The initial velocity of the momentum.
   */
  velocity:T;

  /**
   * The minimal allowed target values.
   * Leave unset to bypass the minimum check.
   */
  min?:T;

  /**
   * The maximal allowed target values.
   * Leave unset to bypass the maximum check.
   */
  max?:T;

  /**
   * The friction applied while decelerating.
   */
  friction?:number;

  /**
   * Rate of deceleration when content has overscrolled and is slowing down before bouncing back.
   */
  deceleration?:number;

  /**
   * The value difference the momentum should stop at.
   */
  epsilon?:number;

  /**
   * Duration of animation when bouncing back.
   */
  bounceDuration?:number;

  /**
   * The easing function that will be used to create the bounce animation.
   */
  bounceEasing?:EasingFunction;
}


/**
 * Default options applied to all momentum animations.
 */
export var defaultMomentumOptions:AnyIMomentumOptions = {
  velocity:       void 0,
  friction:       0.95,
  deceleration:   0.05,
  epsilon:        0.05,
  bounceDuration: 1000,
  bounceEasing:   easeOutExpo
};


/**
 * Momentum plugin interface.
 */
export interface IMomentumPlugin extends IPlugin
{
  /**
   * Try to create an instance of this momentum plugin.
   *
   * @param accessor
   *   The accessor pointing to the property that should be animated.
   * @param options
   *   The desired momentum constructor options.
   * @param initialValue
   *   The current value of the target property.
   * @returns
   *   An instance of the momentum plugin class if applicable, NULL otherwise.
   */
  tryCreate(accessor:Accessor, options:AnyIMomentumOptions, initialValue:any):AnyMomentum;
}


/**
 * The plugin group used by the spring factory.
 */
const pluginGroup = PluginManager
  .getInstance()
  .getGroup<IMomentumPlugin>('momentum');


/**
 * A momentum animation.
 */
export abstract class Momentum<T> extends Animation
{
  /**
   * The accessor used by this animation to read and write the affected property.
   */
  accessor:Accessor;

  /**
   * The current value of the momentum.
   */
  currentValue:T;



  /**
   * Momentum constructor.
   *
   * @param accessor
   *   An accessor pointing to the property that should be animated.
   * @param options
   *   The desired momentum options.
   */
  constructor(accessor:Accessor, options:IMomentumOptions<T>) {
    super(accessor, options);

    this.currentValue = accessor.getValue();
    this.state = AnimationState.Playing;

    options = defaults(options, defaultMomentumOptions);
    this.initialize(options);
  }


  /**
   * Initialize this momentum. Called by the constructor.
   *
   * @param options
   *   The processed options passed to the constructor.
   */
  protected initialize(options:IMomentumOptions<T>) { }


  /**
   * Advance the momentum animation and update the current value.
   *
   * @param timeStep
   *   The time passed since the last update call in milliseconds.
   * @returns
   *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
   */
  protected abstract advance(timeStep:number):boolean;


  /**
   * Update this animation.
   *
   * @param timeStep
   *   The time passed since the last update call in milliseconds.
   * @returns
   *   TRUE if this animation continues, FALSE if it has stopped.
   */
  update(timeStep:number):boolean {
    if (this.state & AnimationState.FinishedOrStopped) {
      return false;
    }

    if (this.advance(timeStep)) {
      this.accessor.setValue(this.currentValue);
      return true;
    } else {
      return false;
    }
  }


  /**
   * Create a new momentum for the given accessor.
   *
   * @param accessor
   *   An accessor pointing to the property that should be animated.
   * @param options
   *   The desired momentum options.
   * @returns
   *   A momentum instance or null if no momentum could be created.
   */
  static create(accessor:Accessor, options:AnyIMomentumOptions):AnyMomentum {
    const initialValue = accessor.getValue();

    for (const plugin of pluginGroup.plugins) {
      const momentum = plugin.tryCreate(accessor, options, initialValue);
      if (momentum != null) {
        return momentum;
      }
    }

    return null;
  }
}


export function retrieveMomentum(target:any, property:string):AnyMomentum {
  const manager = AnimationManager.getInstance();

  for (const animation of manager.animations) {
    const accessor = animation.accessor;

    if (accessor.target == target && accessor.property == property) {
      if (animation instanceof Momentum) {
        return <AnyMomentum>animation;
      } else {
        return null;
      }
    }
  }

  return null;
}


/**
 * Create a new momentum for the given property on the target object.
 *
 * @param target
 *   The object whose property should be animated.
 * @param property
 *   The property that should be animated.
 * @param options
 *   The desired momentum options.
 * @returns
 *   A momentum instance.
 */
export function createMomentum(target:any, property:string, options:AnyIMomentumOptions):AnyMomentum
{
  const accessor = Accessor.create(target, property);
  if (accessor == null) {
    throw new Error(`Could not create an accessor for the property "${property}" on "${target}", did you import all necessary accessor classes?`);
  }

  const momentum = Momentum.create(accessor, options);
  if (momentum == null) {
    throw new Error(`Could not create a momentum for the property "${property}" on "${target}", did you import all necessary momentum classes?`);
  }

  AnimationManager.getInstance().add(momentum);
  return momentum;
}
