import { plugin } from '../../services/PluginManager';
import { Momentum, IMomentumOptions, IMomentumPlugin } from './Momentum';
import { Axis } from './Axis';
import { Accessor } from '../accessor/Accessor';


/**
 * A momentum animation that operates on a single axis.
 */
@plugin<IMomentumPlugin>('momentum', { tryCreate:NumericMomentum.tryCreate })
export class NumericMomentum extends Momentum<number>
{
  /**
   * The axis used by this momentum.
   */
  axis:Axis;



  /**
   * Initialize this momentum. Called by the constructor.
   *
   * @param options
   *   The processed options passed to the constructor.
   */
  protected initialize(options:IMomentumOptions<number>) {
    const axis        = new Axis(options);
    axis.currentValue = this.currentValue;
    axis.minValue     = options.min;
    axis.maxValue     = options.max;
    axis.velocity     = options.velocity;

    this.axis = axis;
  }


  /**
   * Advance the momentum animation and update the current value.
   *
   * @param timeStep
   *   The time passed since the last update call in milliseconds.
   * @returns
   *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
   */
  protected advance(timeStep:number):boolean {
    const continues = this.axis.advance(timeStep);
    this.currentValue = this.axis.currentValue;

    return continues;
  }


  /**
   * Try to create an instance of this momentum plugin.
   *
   * @param accessor
   *   The accessor pointing to the property that should be animated.
   * @param options
   *   The desired momentum constructor options.
   * @param initialValue
   *   The current value of the target property.
   * @returns
   *   An instance of the momentum plugin class if applicable, NULL otherwise.
   */
  static tryCreate(accessor:Accessor, options:IMomentumOptions<any>, initialValue:any):NumericMomentum {
    if (typeof initialValue !== 'number') {
      return null;
    }

    if (typeof options.velocity !== 'number') {
      throw new Error('Type mismatch: Momentum initial and velocity value must be of the same type.');
    }

    if (options.min && typeof options.min !== 'number') {
      throw new Error('Type mismatch: Momentum initial and minimum value must be of the same type.');
    }

    if (options.max && typeof options.max !== 'number') {
      throw new Error('Type mismatch: Momentum initial and maximum value must be of the same type.');
    }

    return new NumericMomentum(accessor, options);
  }
}
