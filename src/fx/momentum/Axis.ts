import { EasingFunction } from '../easing/Type';
import { IMomentumOptions } from './Momentum';


/**
 * Calculates the momentum of a single axis.
 */
export class Axis
{
  /**
   * The current position on this axis.
   */
  currentValue:number;

  /**
   * The minimum allowed position on this axis.
   * Set to NaN to bypass the minimum check.
   */
  minValue:number = Number.NaN;

  /**
   * The maximum allowed position on this axis.
   * Set to NaN to bypass the maximum check.
   */
  maxValue:number = Number.NaN;

  /**
   * The current velocity on this axis.
   */
  velocity:number = 0;

  /**
   * The friction applied while decelerating.
   */
  friction:number;

  /**
   * Rate of deceleration when content has overscrolled and is slowing down before bouncing back.
   */
  deceleration:number;

  /**
   * The value difference the momentum should stop at.
   */
  epsilon:number;

  /**
   * Duration of animation when bouncing back.
   */
  bounceDuration:number;

  /**
   * The easing function that will be used to create the bounce animation.
   */
  bounceEasing:EasingFunction;

  /**
   * The current time step of the bounce animation.
   */
  bounceStep:number = 0;

  /**
   * The initial value of the bounce animation.
   */
  bounceInitial:number = Number.NaN;

  /**
   * The target value of the bounce animation.
   */
  bounceTarget:number = 0;

  /**
   * The value change of the bounce animation.
   */
  bounceDelta:number = 0;



  /**
   * Axis constructor.
   *
   * @param options
   *   The desired momentum options.
   */
  constructor(options:IMomentumOptions<any>) {
    this.friction       = options.friction;
    this.deceleration   = options.deceleration;
    this.epsilon        = options.epsilon;
    this.bounceDuration = options.bounceDuration;
    this.bounceEasing   = options.bounceEasing;
  }


  /**
   * Advance the movement along this axis and update the position property.
   *
   * @param timeStep
   *   The time passed since the last update call in milliseconds.
   * @returns
   *   TRUE if this animation continues, FALSE if it has stopped.
   */
  advance(timeStep:number):boolean {
    const max = this.maxValue;
    const min = this.minValue;
    let position = this.currentValue;
    let velocity = this.velocity;

    if (!isNaN(this.bounceInitial)) {
      this.bounceStep += timeStep;

      if (this.bounceStep >= this.bounceDuration) {
        this.currentValue = this.bounceTarget;
        return false;
      } else {
        this.currentValue = this.bounceEasing(this.bounceStep, this.bounceInitial, this.bounceDelta, this.bounceDuration);
        return true;
      }
    }

    if (!isNaN(max) && position > max) {
      const excess = max - position;
      if (velocity > 0) {
        velocity = (velocity + excess * this.deceleration) * this.friction;
        this.velocity  = velocity;
        this.currentValue += velocity;
      }

      if (velocity <= 0) {
        this.bounceInitial = position;
        this.bounceTarget = max;
        this.bounceDelta  = excess;
      }

      return true;
    }

    if (!isNaN(min) && position < min) {
      const excess = min - position;
      if (velocity < 0) {
        velocity = (velocity + excess * this.deceleration) * this.friction;
        this.velocity  = velocity;
        this.currentValue += velocity;
      }

      if (velocity >= 0) {
        this.bounceInitial = position;
        this.bounceTarget = min;
        this.bounceDelta  = excess;
      }

      return true;
    }

    if (Math.abs(velocity) < this.epsilon) {
      return false;
    }

    velocity *= this.friction;
    this.velocity  = velocity;
    this.currentValue += velocity;
    return true;
  }
}
