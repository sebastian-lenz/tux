import { plugin } from '../../services/PluginManager';
import { IVectorType, isVectorType } from '../../geom/Type';
import { Momentum, IMomentumOptions, IMomentumPlugin } from './Momentum';
import { Axis } from './Axis';
import { Accessor } from '../accessor/Accessor';


/**
 * A momentum animation that handles vector types.
 */
@plugin<IMomentumPlugin>('momentum', { tryCreate:VectorMomentum.tryCreate })
export class VectorMomentum extends Momentum<IVectorType>
{
  /**
   * The axes used by this momentum.
   */
  axes:Axis[];



  /**
   * Initialize this momentum. Called by the constructor.
   *
   * @param options
   *   The processed options passed to the constructor.
   */
  protected initialize(options:IMomentumOptions<IVectorType>) {
    const values     = this.currentValue.getValues();
    const velocities = options.velocity.getValues();
    const min        = options.min ? options.min.getValues() : null;
    const max        = options.max ? options.max.getValues() : null;
    const axes       = [];

    for (let index = 0, count = values.length; index < count; index++) {
      const axis        = new Axis(options);
      axis.currentValue = values[index];
      axis.minValue     = min ? min[index] : Number.NaN;
      axis.maxValue     = max ? max[index] : Number.NaN;
      axis.velocity     = velocities[index];

      axes.push(axis);
    }

    this.axes = axes;
  }


  /**
   * Advance the momentum animation and update the current value.
   *
   * @param timeStep
   *   The time passed since the last update call in milliseconds.
   * @returns
   *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
   */
  protected advance(timeStep:number):boolean {
    const values = [];
    const axes   = this.axes;
    let continues = false;

    for (let index = 0, count = axes.length; index < count; index++) {
      const axis = axes[index];
      continues = axis.advance(timeStep) || continues;
      values.push(axis.currentValue);
    }

    this.currentValue = (<IVectorType>this.currentValue.clone()).setValues(values);
    return continues;
  }


  /**
   * Try to create an instance of this momentum plugin.
   *
   * @param accessor
   *   The accessor pointing to the property that should be animated.
   * @param options
   *   The desired momentum constructor options.
   * @param initialValue
   *   The current value of the target property.
   * @returns
   *   An instance of the momentum plugin class if applicable, NULL otherwise.
   */
  static tryCreate(accessor:Accessor, options:IMomentumOptions<any>, initialValue:any):VectorMomentum {
    if (isVectorType(initialValue)) {
      const velocity = initialValue.convert(options.velocity);
      if (velocity == null) {
        throw new Error('Type mismatch: Momentum initial and velocity value must be of the same type.');
      } else {
        options.velocity = velocity;
      }

      if (options.min) {
        const min = initialValue.convert(options.min);
        if (min == null) {
          throw new Error('Type mismatch: Momentum initial and minimum value must be of the same type.');
        } else {
          options.min = min;
        }
      }

      if (options.max) {
        const max = initialValue.convert(options.max);
        if (max == null) {
          throw new Error('Type mismatch: Momentum initial and maximum value must be of the same type.');
        } else {
          options.max = max;
        }
      }

      return new VectorMomentum(accessor, options);
    } else {
      return null;
    }
  }
}
