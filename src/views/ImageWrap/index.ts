import { IDimensions } from '../../geom/Dimensions';
import Image, { RoundingMode } from '../Image';
import Visibility, { VisibilityTarget } from '../../services/Visibility';
import View, { ViewOptions, option } from '../View';


/**
 * Defines all available scale modes.
 */
export enum ScaleMode
{
  Cover,
  Fit,
  Stretch,
  Width,
  Height,
}


/**
 * Constructor options for the ImageWrap class.
 */
export interface ImageWrapOptions extends ViewOptions
{
  /**
   * Whether to disable the visibility check or not.
   */
  disableVisibilityCheck?: boolean;

  /**
   * The scale mode used to transform the image.
   */
  scaleMode?: ScaleMode;

  /**
   * The maximum scale of the image.
   */
  maxScale?: number;

  sizeRoundingMode?: RoundingMode | string;

  offsetRoundingMode?: RoundingMode | string;
}


/**
 * Wraps an Image view and allows to apply various scale modes to the image.
 */
export default class ImageWrap extends View implements VisibilityTarget
{
  /**
   * The image instance displayed by this wrapper.
   */
  image:Image;

  /**
   * The scale mode used to transform the image.
   */
  @option({ type: 'enum', values: ScaleMode, defaultValue: ScaleMode.Fit })
  scaleMode: ScaleMode;

  /**
   * The maximum scale of the image.
   */
  @option({ type: 'number', defaultValue: Number.MAX_VALUE })
  maxScale: number;

  /**
   * The horizontal focus point, a relative position between 0 and 1.
   */
  @option({ type: 'number', defaultValue: 0.5})
  focusX: number;

  /**
   * The vertical focus point, a relative position between 0 and 1.
   */
  @option({ type: 'number', defaultValue: 0.5})
  focusY: number;

  /**
   * Whether this element is currently within the visible viewport bounds or not.
   */
  inViewport:boolean;



  /**
   * ImageWrap constructor.
   *
   * @param options
   *   The constructor options.
   */
  constructor(options?: ImageWrapOptions) {
    super(options || (options = {}));

    this.image = new Image({
      element: this.element.querySelector('img'),
      disableVisibilityCheck: true,
      sizeRoundingMode: options.sizeRoundingMode,
      offsetRoundingMode: options.offsetRoundingMode,
    });

    if (!options.disableVisibilityCheck) {
      Visibility.getInstance().register(this);
    }

    this.listenToOnce(this.image, 'load', this.onImageLoad);
  }


  /**
   * Remove this view.
   */
  remove(): this {
    Visibility.getInstance().unregister(this);
    this.image.remove();
    return super.remove();
  }

  /**
   * Set the size of the image wrap.
   *
   * @param width
   *   The new width of the image wrap.
   * @param height
   *   The new height of the image wrap.
   */
  setSize(width:number, height:number):this {
    this.element.style.width  = width  + 'px';
    this.element.style.height = height + 'px';

    this.update(width, height);

    return this;
  }


  /**
   * Set whether the element of this view is currently within the visible bounds
   * of the viewport or not.
   *
   * @param inViewport
   *   TRUE if the element is visible, FALSE otherwise.
   */
  setInViewport(inViewport:boolean) {
    if (this.inViewport == inViewport) return;
    this.inViewport = inViewport;

    this.update();
    this.image.setInViewport(inViewport);
  }


  /**
   * Set the scale mode of this image wrap.
   *
   * @param mode
   *   The new image scale mode.
   */
  setScaleMode(mode:ScaleMode) {
    if (this.scaleMode == mode) return;

    this.scaleMode = mode;
    this.element.style.height = '';
    this.element.style.width  = '';
    this.update();
  }


  /**
   * Load the image.
   *
   * @returns
   *   A promise that resolves after the image has loaded.
   */
  load():Q.Promise<any> {
    this.update();
    return this.image.load();
  }


  /**
   * Update the displayed image.
   *
   * @param width
   *   The visible width of the wrapper.
   * @param height
   *   The visible height of the wrapper.
   */
  update(width?:number, height?:number) {
    width  = width  || this.element.offsetWidth;
    height = height || this.element.offsetHeight;
    if (!width && !height) return;

    const scaled = this.measure(width, height);
    let offsetX:number, offsetY:number;

    switch (this.scaleMode) {
      case ScaleMode.Cover:
        offsetX = ImageWrap.calculateShift(width, scaled.width, this.focusX);
        offsetY = ImageWrap.calculateShift(height, scaled.height, this.focusY, true);
        break;
      case ScaleMode.Fit:
        offsetX = (width  - scaled.width)  * this.focusX;
        offsetY = (height - scaled.height) * this.focusY;
        break;
      case ScaleMode.Width:
        this.element.style.height = scaled.height + 'px';
        break;
      case ScaleMode.Height:
        this.element.style.width = scaled.width + 'px';
        break;
    }

    this.image.setSize(scaled.width, scaled.height, offsetX, offsetY);
  }


  /**
   * Calculate the scaled dimensions of the image according to the given scale mode.
   *
   * @param width
   *   The display width.
   * @param height
   *   The display height.
   * @param scaleMode
   *   The scale mode that should be used to calculate the dimensions.
   * @param maxScale
   *   The maximum allowed scale factor.
   * @returns
   *   An object describing the scaled dimensions of the image.
   */
  measure(width:number, height:number, scaleMode:ScaleMode = this.scaleMode, maxScale:number = this.maxScale):IDimensions {
    const nativeWidth  = this.image.nativeWidth;
    const nativeHeight = this.image.nativeHeight;
    let scale = 1;

    switch (scaleMode) {
      case ScaleMode.Stretch:
        return {width, height};
      case ScaleMode.Cover:
        scale = Math.max(width / nativeWidth, height / nativeHeight);
        break;
      case ScaleMode.Fit:
        scale = Math.min(width / nativeWidth, height / nativeHeight);
        break;
      case ScaleMode.Width:
        scale = width / nativeWidth;
        break;
      case ScaleMode.Height:
        scale = height / nativeHeight;
        break;
    }

    scale = Math.min(scale, maxScale);
    return {
      width:  nativeWidth  * scale,
      height: nativeHeight * scale
    };
  }


  /**
   * Triggered after the image has loaded.
   */
  onImageLoad() {
    this.element.classList.add('loaded');
    this.handleViewportResize();
    this.trigger('load');
  }


  /**
   * Triggered after the size of the viewport has changed.
   *
   * If this view is used as a component this handler will be called automatically.
   *
   * @returns
   *   TRUE if the event handling should be stopped and child components should not receive
   *   the event, FALSE otherwise.
   */
  handleViewportResize(): boolean {
    this.update();
    return true;
  }


  /**
   * Calculate the shift value for a dimension based upon the given focus value.
   *
   * @param space
   *   The available space within the dimension.
   * @param size
   *   The size of the image within the dimension.
   * @param focus
   *   The focus point value within the dimension.
   * @param invert
   *   Whether the focus point should be inverted or not.
   * @returns
   *   The calculated position shift along the dimension.
   */
  static calculateShift(space:number, size:number, focus:number, invert?:boolean):number {
    if (invert) focus = 1 - focus;

    let shift = Math.round(-size * focus + space / 2);
    if (shift > 0) shift = 0;
    if (shift < space - size) shift = space - size;

    return shift;
  }
}
