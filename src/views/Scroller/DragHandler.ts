import { createMomentum } from '../../fx/momentum/Momentum';
import { stopAnimations } from '../../fx/AnimationManager';
import { Vector2 } from '../../geom/Vector2';
import DragHandlerBase, { DragDirection } from '../../handler/DragHandler';
import Scroller from './index';
import Pointer from '../../handler/Pointer';

import '../../fx/momentum/VectorMomentum';

export default class DragHandler extends DragHandlerBase {
  view: Scroller;

  initialOffset: Vector2 = new Vector2(0, 0);

  constructor(view: Scroller) {
    super(view);
    this.direction = view.direction;
  }

  /**
   * Triggered after a drag has started.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the drag start.
   * @returns
   *   TRUE if the drag operation should be started, FALSE otherwise.
   */
  handleDragStart(event: Event, pointer: Pointer): boolean {
    if (this.view.minOffset.x == 0 && this.view.minOffset.y == 0) {
      return false;
    }

    this.initialOffset.copy(this.view.offset);
    stopAnimations(this.view, 'offset');

    event.preventDefault();
    return true;
  }

  /**
   * Triggered after a drag has moved.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the move.
   * @returns
   *   TRUE if the drag operation should be started, FALSE otherwise.
   */
  handleDragMove(event: Event, pointer: Pointer): boolean {
    const offset = this.initialOffset.clone();
    const min = this.view.minOffset;
    const max = this.view.maxOffset;
    const delta = pointer.getAbsoluteDelta();

    if (this.view.direction & DragDirection.Horizontal) {
      offset.x += delta.x;

      if (offset.x < min.x) {
        offset.x = min.x + (offset.x - min.x) * 0.5;
      }

      if (offset.x > max.x) {
        offset.x = max.x + (offset.x - max.x) * 0.5;
      }
    }

    if (this.view.direction & DragDirection.Vertical) {
      offset.y += delta.y;

      if (offset.y < min.y) {
        offset.y = min.y + (offset.y - min.y) * 0.5;
      }

      if (offset.y > max.y) {
        offset.y = max.y + (offset.y - max.y) * 0.5;
      }
    }

    this.view.setOffset(offset);
    event.preventDefault();
    return true;
  }

  /**
   * Triggered after a drag has ended.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the movement.
   */
  handleDragEnd(event: Event, pointer: Pointer) {
    createMomentum(this.view, 'offset', {
      velocity: Vector2.convert(pointer.getVelocity()),
      min: this.view.minOffset,
      max: this.view.maxOffset,
    });
  }

  /**
   * Triggered after the user has clicked into the view.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the click.
   */
  handleClick(event: Event, pointer: Pointer) {
    this.view.trigger('click', event, pointer);
  }
}
