import View, { ViewOptions, option } from '../View';
import { Vector2, IVector2 } from '../../geom/Vector2';
import transform from '../../util/vendor/transform';
import { stopAnimations } from '../../fx/AnimationManager';
import { DragDirection } from '../../handler/DragHandler';
import WheelHandler from './WheelHandler';
import DragHandler from './DragHandler';


export { DragDirection } from '../../handler/DragHandler';

export interface ScrollerOptions extends ViewOptions
{
  viewport?: string|HTMLElement;

  container?: string|HTMLElement;

  direction?: DragDirection;

  roundOffset?: boolean;
}


/**
 * @event "scroll" (offset:IVector2):void
 */
export default class Scroller extends View
{
  @option({ type: 'element' })
  viewport: HTMLElement;

  @option({ type: 'element', defaultValue: '.container' })
  container: HTMLElement;

  @option({ type: 'enum', values: DragDirection, defaultValue: DragDirection.Vertical })
  direction: DragDirection;

  @option({ type: 'bool', defaultValue: false })
  roundOffset: boolean;

  dragHandler: DragHandler;
  maxOffset:Vector2 = new Vector2(0, 0);
  minOffset:Vector2 = new Vector2(0, 0);
  offset:Vector2 = new Vector2(0, 0);
  size:Vector2 = new Vector2(0, 0);
  wheelHandler: WheelHandler;


  constructor(options?: ScrollerOptions) {
    super(options);

    if (!this.viewport) {
      this.viewport = this.element;
    }

    this.wheelHandler = new WheelHandler(this);
    this.dragHandler = new DragHandler(this);
  }

  clampOffset(value: IVector2): IVector2 {
    const { maxOffset, minOffset } = this;
    if (value.x < minOffset.x) value.x = minOffset.x;
    if (value.x > maxOffset.x) value.x = maxOffset.x;
    if (value.y < minOffset.y) value.y = minOffset.y;
    if (value.y > maxOffset.y) value.y = maxOffset.y;
    return value;
  }

  setOffset(value: IVector2, clamped?: boolean) {
    const { direction, offset } = this;
    let changed = false;

    if (clamped) {
      value = this.clampOffset(value);
    }

    if (direction & DragDirection.Horizontal) {
      let x = value.x;
      if (offset.x != x) {
        changed  = true;
        offset.x = x;
      }
    }

    if (direction & DragDirection.Vertical) {
      let y = value.y;
      if (offset.y != y) {
        changed  = true;
        offset.y = y;
      }
    }

    if (changed) {
      this.handleOffsetChange(offset);
    }
  }

  setMinOffset(value:IVector2) {
    const direction = this.direction;
    const offset    = this.offset;
    const min       = this.minOffset;

    let changed = false;

    min.x = (direction & DragDirection.Horizontal) ? value.x : 0;
    min.y = (direction & DragDirection.Vertical)   ? value.y : 0;

    if (offset.x < min.x) {
      offset.x = min.x;
      changed  = true;
    }

    if (offset.y < min.y) {
      offset.y = min.y;
      changed  = true;
    }

    if (changed) {
      stopAnimations(this, 'offset');
      this.handleOffsetChange(offset);
    }
  }

  setMaxOffset(value:IVector2) {
    const direction = this.direction;
    const offset    = this.offset;
    const max       = this.maxOffset;

    let changed = false;

    max.x = (direction & DragDirection.Horizontal) ? value.x : 0;
    max.y = (direction & DragDirection.Vertical)   ? value.y : 0;

    if (offset.x > max.x) {
      offset.x = max.x;
      changed  = true;
    }

    if (offset.y > max.y) {
      offset.y = max.y;
      changed  = true;
    }

    if (changed) {
      stopAnimations(this, 'offset');
      this.handleOffsetChange(offset);
    }
  }

  handleOffsetChange(offset:Vector2) {
    const el = this.container;
    let x, y;

    if (this.roundOffset) {
      x = Math.round(offset.x);
      y = Math.round(offset.y);
    } else {
      x = offset.x;
      y = offset.y;
    }

    if (transform.styleName) {
      if (transform.has3D) {
        el.style[transform.styleName] = `translate3d(${x}px, ${y}px, 0)`;
      } else {
        el.style[transform.styleName] = `translate(${x}px, ${y}px)`;
      }
    } else {
      el.style.left = `${x}px`;
      el.style.top  = `${y}px`;
    }

    this.trigger('scroll', offset);
  }

  /**
   * Triggered after the size of the viewport has changed.
   *
   * If this view is used as a component this handler will be called automatically.
   *
   * @param node
   *   The component node that triggered the resize.
   * @returns
   *   TRUE if the event handling should be stopped and child components should not receive
   *   the event, FALSE otherwise.
   */
  handleViewportResize(resizeChildren?: Function): boolean {
    if (resizeChildren) resizeChildren();

    this.size.x = this.viewport.offsetWidth;
    this.size.y = this.viewport.offsetHeight;
    this.setMinOffset({
      x: Math.min(0, this.size.x - this.container.scrollWidth),
      y: Math.min(0, this.size.y - this.container.scrollHeight),
    });

    return true;
  }
}
