import { UserAgent } from '../../util/polyfill/UserAgent';
import WheelHandlerBase, { IWheelData } from '../../handler/WheelHandler';
import Scroller, { DragDirection } from './index';


const power = UserAgent.windows() ? 1.2 : 1;

export default class WheelHandler extends WheelHandlerBase
{
  view: Scroller;


  constructor(view: Scroller, selector?: string) {
    super(view, selector);
  }

  /**
   * Triggered after the user has used the mouse wheel.
   *
   * @param event
   *   The original dom event.
   * @param data
   *   The normalized scroll data.
   * @returns
   *   TRUE if the event has been handled, FALSE otherwise.
   */
  handleWheel(event: Event, data: IWheelData): boolean {
    const direction = this.view.direction;
    const x = (direction & DragDirection.Horizontal) ? data.pixelX * power : 0;
    const y = (direction & DragDirection.Vertical)   ? data.pixelY * power : 0;
    const offset = this.view.offset.clone().subtract(x, y);

    this.view.setOffset(offset, true);
    return x != 0 || y != 0;
  }
}
