import { defaults } from 'underscore';

import View, { ViewOptions } from '../View';
import Page from './Page';
import Menu from './Menu';


export interface ItemOptions extends ViewOptions
{
  disabled?: boolean;
  owner: Menu;
  page: Page
}

export default class Item extends View
{
  isCurrent: boolean = false;
  menu: Menu;
  page: Page;


  constructor(options?: ItemOptions) {
    super(defaults(options || {}, {
      tagName: 'li',
      className: 'tuxTabsItem'
    }));

    this.menu = options.owner;
    this.page = options.page;
    this.element.innerHTML = this.page.title;

    if (!options.disabled) {
      this.delegateClick(this.onClick);
    }
  }

  setCurrent(value:boolean) {
    if (this.isCurrent == value) return;
    this.isCurrent = value;
    this.toggleClass('current', value);
  }

  onClick() {
    this.menu.tabs.setCurrent(this.page);
    return true;
  }
}
