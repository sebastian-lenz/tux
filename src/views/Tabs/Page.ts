import View, { Registry, ViewOptions } from '../View';


export interface TabsPageOptions extends ViewOptions
{
  title?: string;
}


export default class Page extends View
{
  components: View[];
  title: string;

  constructor(options?: TabsPageOptions) {
    super(options);

    if (options.title) {
      this.title = options.title;
    } else if (options.element) {
      let previous = options.element.previousSibling;

      while (previous && previous.nodeType == 3) {
        previous = previous.previousSibling;
      }

      if (previous && previous.nodeName == 'DT') {
        this.title = (<HTMLElement>previous).innerHTML;
      }
    }
  }

  setCurrent(value: boolean) {
    this.toggleClass('current', value);
    if (!value) return;

    const { components, element } = this;
    if (!components) {
      this.components = Registry.getInstance().create(element);
    } else {
      for (const component of components) {
        component.handleViewportResize();
      }
    }
  }
}
