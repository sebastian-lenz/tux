import { defaults, find, forEach } from 'underscore';

import CycleableView, { CycleableViewOptions } from '../CycleableView';

import Item, { ItemOptions } from './Item';
import Page from './Page';
import Tabs from './index';


export interface MenuOptions extends CycleableViewOptions<Item>
{
  owner: Tabs;
}

export default class Menu extends CycleableView<Item>
{
  tabs: Tabs;


  constructor(options?: MenuOptions) {
    super(defaults(options || {}, {
      tagName: 'ol',
      className: 'tuxTabsMenu',
      childClass: Item,
    }));

    const { childClass, owner } = options;
    const { children, container, current } = owner;
    this.tabs = owner;

    forEach(children, page => {
      const options = this.alterChildOptions({
        appendTo: container,
        owner: this,
        page,
      })

      this.addChild(new childClass(options));
    });
  }

  setCurrentPage(page: Page) {
    const { children } = this;
    this.setCurrent(find(children, child => child.page === page));
  }

  protected handleTransition(newChild: Item, oldChild: Item) {
    if (newChild) newChild.setCurrent(true);
    if (oldChild) oldChild.setCurrent(false);
  }
}
