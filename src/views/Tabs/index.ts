import { defaults, map } from 'underscore';

import AnimationSwap, { TransitionOptions } from '../AnimationSwap';
import CycleableView, { CycleableViewOptions } from '../CycleableView';

import Item, { ItemOptions } from './Item';
import Menu, { MenuOptions } from './Menu';
import Page from './Page';


export { Item, Menu, Page };

export interface TabsOptions<T extends Page = Page> extends CycleableViewOptions<T> {
  menuClass?: { new (options: MenuOptions): Menu };
  menuOptions?: MenuOptions;
}

export default class Tabs<T extends Page = Page> extends CycleableView<T, TransitionOptions>
{
  defaultTransitionOptions: TransitionOptions = {
    className: 'transition',
    withHeightTransition: true,
  };

  menu: Menu;
  swap: AnimationSwap<Page>;


  constructor(options?: TabsOptions) {
    super(defaults(options || {}, {
      autoSelectFirstChild: true,
      childSelector: '.tuxTabsPage',
      childClass: Page,
      container: '.tuxTabs__container',
      menuClass: Menu,
      menuOptions: { },
    }));

    this.menu = this.createMenu(options);
    this.swap = this.createSwap();

    this.menu.setCurrentPage(this.current);
  }

  protected createMenu(options: TabsOptions): Menu {
    const { children, current, element } = this;
    const { menuClass, menuOptions } = options;

    const menu = new menuClass(defaults(menuOptions, {
      owner: this,
    }));

    element.insertBefore(menu.element, element.firstChild);
    return menu;
  }

  protected createSwap(): AnimationSwap<Page> {
    const { container, current } = this;

    const swap = new AnimationSwap<Page>({ element: container });
    this.listenTo(swap, 'swap', this.onSwap);

    if (current) {
      current.setCurrent(true);
      swap.setContent(current, { className: null });
    }

    return swap;
  }

  /**
   * Internal transition handler. Creates a transition between the two given children.
   *
   * @param newChild
   *   The child the transition should lead to.
   * @param oldChild
   *   The child the transition should come from.
   * @param options
   *   Optional transition options.
   */
  protected handleTransition(newChild: Page, oldChild: Page, options?: TransitionOptions) {
    const { menu, swap } = this;

    if (swap) {
      options = defaults(options || {}, this.defaultTransitionOptions);
      swap.setContent(newChild, options);
    }

    if (menu) {
      menu.setCurrentPage(newChild);
    }
  }

  onSwap(newContent: Page, oldContent: Page) {
    if (newContent) newContent.setCurrent(true);
    if (oldContent) oldContent.setCurrent(false);
  }

  handleViewportResize(): boolean {
    const { menu, swap } = this;
    menu.handleViewportResize();
    swap.handleViewportResize();

    return true;
  }
}
