import { IRectangle } from '../../../geom/Rectangle';


/**
 * Defines a reserved area within a column.
 */
export interface IReservation
{
  /**
   * The vertical offset the reservation begins.
   */
  min:number;

  /**
   * The vertical offset the reservation ends.
   */
  max:number;
}


/**
 * A position within a column.
 */
export interface IColumnRegion extends IRectangle
{
  /**
   * The column index of this position.
   */
  column:number;
}


/**
 * Represents a single layout column.
 */
export class Column
{
  /**
   * The zero based index of this column.
   */
  index:number;

  /**
   * The column placed on the right side of this column.
   */
  next:Column;

  /**
   * A list of all reserved areas within this column.
   */
  reserved:IReservation[] = [];


  /**
   * Create a new Column instance.
   */
  constructor(index:number) {
    this.index = index;
  }


  /**
   * Return the maximum height of this column.
   */
  getMaxHeight(columns:number = 1):number {
    const reserved = this.reserved;
    const length = reserved.length;
    let max;

    if (length == 0) {
      max = 0;
    } else {
      max = reserved[length - 1].max;
    }

    if (columns > 1) {
      return Math.max(max, this.next.getMaxHeight(columns - 1));
    } else {
      return max;
    }
  }


  /**
   * Find all available free gaps for the given dimensions.
   */
  getAllPositions(columns:number, height:number, result:IColumnRegion[] = []):IColumnRegion[] {
    this.walk(columns, (min:number, max:number) => {
      if (max - min < height) return;
      if (columns > 1 && !this.next.isFree(columns - 1, height, min)) return;
      result.push({
        column: this.index,
        x:      0,
        y:      min,
        width:  columns,
        height: height
      });
    });

    return result;
  }


  /**
   * Test whether there is free space at the given offset.
   */
  isFree(columns:number, height:number, offset:number):boolean {
    for (var index = 0, count = this.reserved.length; index < count; index++) {
      var reservation = this.reserved[index];
      if (reservation.max <= offset) continue;
      if (reservation.min >= offset + height) continue;
      return false;
    }

    if (columns > 1) {
      return this.next.isFree(columns - 1, height, offset);
    } else {
      return true;
    }
  }


  /**
   * Call the given callback for each available gap within this colum.
   */
  walk(columns:number, callback:{(min:number, max:number)}) {
    const bottom = this.getMaxHeight(columns);
    let wasBottomVisited = false;

    var count = this.reserved.length;
    if (count == 0) {
      return callback(0, Number.NaN);
    }

    var current, next = this.reserved[0];
    for (var index = 0; index < count; index++) {
      current = next;

      if (index == 0 && current.min > 0) {
        if (bottom == 0) wasBottomVisited = true;
        callback(0, current.min);
      }

      if (current.max == bottom) wasBottomVisited = true;
      if (index == count - 1) {
        callback(current.max, Number.NaN);
      } else {
        next = this.reserved[index + 1];
        callback(current.max, next.min);
      }
    }

    if (!wasBottomVisited) {
      callback(bottom, Number.NaN);
    }
  }


  /**
   * Reserve the given position.
   */
  reserve(position:IColumnRegion) {
    this.reserved.push({
      min: position.y,
      max: position.y + position.height
    });

    this.reserved.sort((a, b) => {
      if (a.min == b.min) return 0;
      return a.min > b.min ? 1 : -1;
    });

    var index = 0, count = this.reserved.length - 1;
    while (index < count) {
      var a = this.reserved[index];
      var b = this.reserved[index + 1];
      if (Math.abs(a.max - b.min) < 0.5) {
        a.max = b.max;
        this.reserved.splice(index + 1, 1);
        count -= 1;
      } else {
        index += 1;
      }
    }

    if (this.index < position.column + position.width - 1) {
      this.next.reserve(position);
    }
  }
}
