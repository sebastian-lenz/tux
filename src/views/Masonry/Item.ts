import { IDimensions } from '../../geom/Dimensions';
import { IRectangle } from '../../geom/Rectangle';
import View, { ViewOptions } from '../View';
import Masonry from './index';


export interface ItemOptions extends ViewOptions { }

export default class Item extends View
{
  /**
   * Should this item be visible?
   */
  isVisible:boolean = true;


  constructor(options?: ItemOptions) {
    super(options);
  }

  /**
   * Set the position of this item within the masonry.
   */
  setMasonryPosition(position: IRectangle) {
    const { style } = this.element;
    if (!position) {
      style.visibility = 'hidden';
      style.top  = '0';
      style.left = '0';
    } else {
      style.visibility = 'inherit';
      style.top  = `${position.y}px`;
      style.left = `${position.x}px`;
    }

    this.handleViewportResize();
  }

  /**
   * Return the desired size of this item.
   */
  getDesiredSize():IDimensions {
    const { element } = this;
    return {
      width: element.offsetWidth,
      height: element.offsetHeight
    };
  }

  setActualSize(size:IDimensions):IDimensions {
    return this.getDesiredSize();
  }

  /**
   * Triggered before the masonry is about to update the layout.
   */
  handleMasonryPreUpdate(masonry: Masonry) { }

  /**
   * Triggered after the masonry has updated the layout.
   */
  handleMasonryPostUpdate(masonry: Masonry) { }

  /**
   * Triggered when the masonry is about to become activated.
   */
  handleMasonryActivate() { }

  /**
   * Triggered when the masonry is about to become deactivated.
   */
  handleMasonryDeactivate() {
    const { style } = this.element;
    style.top = '';
    style.left = '';
  }
}
