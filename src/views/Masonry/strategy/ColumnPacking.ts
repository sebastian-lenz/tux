import Masonry, { IMasonryStrategy } from '../index';
import Item from '../Item';
import { Column, IColumnRegion } from '../util/Column';


export type ColumnCount = number | { (grid: Masonry): number };

/**
 * Constructor options for ColumnPackingStrategy.
 */
export interface IColumnPackingStrategyOptions
{
  columnCount: ColumnCount;

  gutter?: number;
}


/**
 * A layout strategy for the Masonry view that packs all items
 * into fixed columns.
 */
export default class ColumnPacking implements IMasonryStrategy
{
  /**
   * The masonry this strategy is intented to be used with.
   */
  masonry: Masonry;

  /**
   * Column instances used to store the current layout.
   */
  columns: Column[] = [];

  /**
   * The number of columns available.
   */
  columnCount: ColumnCount;

  /**
   * The width of a single column.
   */
  columnWidth: number = 0;

  /**
   * The width of the masonry while last updating the layout.
   */
  masonryWidth: number = 0;

  /**
   * The gutter width.
   */
  gutter: number = 0;



  /**
   * Create a new ColumnPackingStrategy instance.
   */
  constructor(options:IColumnPackingStrategyOptions) {
    this.columnCount = options.columnCount;
    this.gutter = options.gutter !== void 0 ? options.gutter : 0;
  }


  /**
   * Reset the layout state of this strategy. Must be called before
   * a new layout is being generated.
   */
  reset() {
    var count = this.getColumnCount();
    var width = this.masonry.container.offsetWidth;
    var columns = [];

    for (var index = count - 1; index >= 0; index--) {
      var column = new Column(index);
      column.next = columns[index + 1];
      columns[index] = column;
    }

    this.masonryWidth = width;
    this.columnWidth  = (width - this.gutter * (count - 1)) / count;
    this.columns      = columns;
  }


  /**
   * Mark the given position as being reserved.
   */
  protected reserve(position: IColumnRegion) {
    this.columns[position.column].reserve(position);
  }


  /**
   * Return whether the layout has been invalidated e.g. due to
   * browser resizes.
   */
  isUpdateRequired(): boolean {
    var width = this.masonry.element.offsetWidth;
    var count = this.getColumnCount();

    return (
        width != this.masonryWidth ||
        count != this.columns.length
    );
  }


  /**
   * Set the grid this strategy is working on.
   */
  setMasonry(masonry: Masonry) {
    this.masonry = masonry;
    this.masonryWidth = 0;
  }


  /**
   * Return the number of available columns.
   */
  getColumnCount(): number {
    if (typeof this.columnCount === 'number') {
      return <number>this.columnCount;
    } else {
      return (<any>this.columnCount)(this.masonry);
    }
  }


  /**
   * Return the maximum height of the generated layout.
   */
  getMaxHeight(): number {
    var height = 0;
    for (var column of this.columns) {
      height = Math.max(height, column.getMaxHeight());
    }

    return height - this.gutter;
  }


  /**
   * Place the given masonry item and return the assigned position.
   */
  place(item: Item): IColumnRegion {
    const position = this.getBestPosition(item);
    if (!position) {
      return null;
    }

    position.x = (this.columnWidth + this.gutter) * position.column;

    this.reserve(position);
    return position;
  }


  protected getColumnCountFromWidth(width: number): number {
    return Math.max(1, Math.min(this.getColumnCount(),
        Math.round(width / this.columnWidth)
    ));
  }


  /**
   * Return the best free position for the given masonry item.
   */
  protected getBestPosition(item: Item): IColumnRegion {
    const positions = this.getAllPositions(item);
    positions.sort((a:IColumnRegion, b:IColumnRegion) => {
      if (a.y != b.y) {
        return a.y - b.y;
      }

      if (a.column == b.column) {
        return 0;
      }

      return a.column - b.column;
    });

    return positions[0];
  }


  /**
   * Return all possible positions for the given masonry item.
   */
  protected getAllPositions(item: Item): IColumnRegion[] {
    let { width, height } = item.getDesiredSize();
    height += this.gutter;

    const columns = this.getColumnCountFromWidth(width);

    const result = [];
    const max = this.columns.length - columns + 1;
    for (let index = 0; index < max; index++) {
      this.columns[index].getAllPositions(columns, height, result);
    }

    return result;
  }


  /**
   * Return the next free position within the given column.
   */
  protected getPositionAt(index: number, item: Item): IColumnRegion {
    const { width, height } = item.getDesiredSize();
    const columns = this.getColumnCountFromWidth(width);
    const positions = [];

    this.columns[index].getAllPositions(columns, height, positions);

    return positions.pop();
  }
}
