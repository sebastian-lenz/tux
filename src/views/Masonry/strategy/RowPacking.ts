import { defaults } from 'underscore';

import { IRectangle } from '../../../geom/Rectangle';
import Masonry, { IMasonryStrategy } from '../index';
import Item from '../Item';
import { IDimensions } from '../../../geom/Dimensions';


export type RowMapping = { [cid:number]:RowEntry };

export interface RowEntry {
  item: Item;
  row: Row;
  index: number;
  width: number;
  height: number;
  x:number;
};

export enum RowAlign {
  Left,
  Center,
  Right,
  Justify,
  JustifyAll,
}

export class Row
{
  entries: RowEntry[] = [];

  y: number = 0;

  height: number = 0;

  displayWidth: number = 0;

  desiredWidth: number = 0;

  actualWidth: number = 0;

  isLastRow: boolean = false;


  push(item: Item, size: IDimensions): RowEntry {
    const entry = {
      item,
      row:    this,
      index:  this.entries.length,
      width:  size.width,
      height: size.height,
      x:      0
    };

    this.entries.push(entry);
    this.desiredWidth += size.width;

    return entry;
  }


  distribute(width:number, padding:number, maxScale?:number) {
    const entries = this.entries;
    const availableWidth = width - Math.max(0, entries.length - 1) * padding;

    let scale = availableWidth / this.desiredWidth;
    if (maxScale !== void 0) {
      scale = Math.min(maxScale, scale);
    }

    let x         = 0;
    let maxHeight = 0;
    for (const entry of entries) {
      entry.x = x;

      const size   = entry.item.setActualSize({ width:entry.width * scale, height:entry.height });
      entry.width  = size.width;
      entry.height = size.height;

      x += size.width + padding;
      if (size.height > maxHeight) {
        maxHeight = size.height;
      }
    }

    this.displayWidth = width;
    this.actualWidth  = x - padding;
    this.height       = maxHeight;
  }
}


export interface IRowPackingOptions
{
  /**
   * Minimum number of items that must be placed in a row.
   */
  minItemsPerRow?:number;

  /**
   * Width multiplier applied to the original width of the masonry.
   */
  widthMultiplier?:number;

  /**
   * Horizontal padding between items within a row.
   */
  itemPadding?:number;

  /**
   * Vertical alignment of items within a row.
   */
  itemAlign?:number;

  /**
   * Vertical padding between rows.
   */
  rowPadding?:number;

  /**
   * Horizontal alignment of items within a row.
   */
  rowAlign?:RowAlign;

  /**
   * Maximum number of allowed rows.
   */
  maxRows?:number;

  /**
   * Indention applied to the last row.
   */
  lastRowIndent?:number;
}


/**
 * A layout strategy that packs items into rows.
 */
export class RowPacking implements IMasonryStrategy
{
  /**
   * The masonry this strategy is intented to be used with.
   */
  masonry: Masonry;

  /**
   * A list of all rows the layout currently consists of.
   */
  rows: Row[];

  /**
   * A map of all child cid values and their matching row entry.
   */
  rowMapping: RowMapping;

  /**
   * The height of the current layout state.
   */
  height: number = 0;

  /**
   * The width of the masonry while last updating the layout.
   */
  masonryWidth: number = 0;

  /**
   * Minimum number of items that must be placed in a row.
   */
  minItemsPerRow: number;

  /**
   * Width multiplier applied to the original width of the masonry.
   */
  widthMultiplier: number;

  /**
   * Horizontal padding between items within a row.
   */
  itemPadding: number;

  /**
   * Vertical alignment of items within a row.
   */
  itemAlign: number;

  /**
   * Vertical padding between rows.
   */
  rowPadding: number;

  /**
   * Horizontal alignment of items within a row.
   */
  rowAlign: RowAlign = RowAlign.Left;

  /**
   * Maximum number of allowed rows.
   */
  maxRows: number;

  /**
   * Indention applied to the last row.
   */
  lastRowIndent: number;


  /**
   * RowPacking constructor.
   *
   * @param options
   *   RowPacking constructor options.
   */
  constructor(options?: IRowPackingOptions) {
    options = defaults(options || {}, {
      minItemsPerRow:   2,
      widthMultiplier:  1,
      itemPadding:     20,
      itemAlign:        0,
      rowPadding:      20,
      rowAlign:         RowAlign.Left,
      maxRows:          Number.NaN,
      lastRowIndent:    0
    });

    this.minItemsPerRow  = options.minItemsPerRow;
    this.widthMultiplier = options.widthMultiplier;
    this.itemPadding     = options.itemPadding;
    this.itemAlign       = options.itemAlign;
    this.rowPadding      = options.rowPadding;
    this.rowAlign        = options.rowAlign;
    this.maxRows         = options.maxRows ? options.maxRows : Number.NaN;
    this.lastRowIndent   = options.lastRowIndent;
  }

  reset(items: Item[]) {
    const width          = this.masonry.element.offsetWidth;
    const availableWidth = width * this.widthMultiplier;

    let row = new Row();
    const rowMapping:RowMapping = {};
    const rows:Row[] = [ row ];

    for (const item of items) {
      const size = item.getDesiredSize();

      if (row.entries.length >= this.minItemsPerRow && row.desiredWidth + size.width > availableWidth) {
        if (isNaN(this.maxRows) || rows.length < this.maxRows) {
          rows.push(row = new Row());
        } else {
          break;
        }
      }

      rowMapping[item.cid] = row.push(item, size);
    }

    row.isLastRow = true;

    let y = 0;
    for (row of rows) {
      const rowWidth = row.isLastRow ? width - this.lastRowIndent : width;
      if (this.rowAlign == RowAlign.Justify && row.isLastRow) {
        row.distribute(rowWidth, this.itemPadding, 1);
      } else {
        row.distribute(rowWidth, this.itemPadding);
      }

      row.y = y;
      y += row.height + this.rowPadding;
    }

    this.rows         = rows;
    this.rowMapping   = rowMapping;
    this.masonryWidth = width;
    this.height       = y - this.rowPadding;
  }

  place(item: Item): IRectangle {
    const entry = this.rowMapping[item.cid];
    if (entry === void 0) {
      return null;
    }

    const row = entry.row;
    let shift = row.displayWidth - row.actualWidth;

    switch (this.rowAlign) {
      case RowAlign.Left:
        shift = 0;
        break;
      case RowAlign.Center:
        shift *= 0.5;
        break;
      case RowAlign.JustifyAll:
        shift = (shift / (row.entries.length - 1)) * entry.index;
        break;
      case RowAlign.Justify:
        if (!row.isLastRow && row.entries.length > 1) {
          shift = (shift / (row.entries.length - 1)) * entry.index;
        } else {
          shift = 0
        }
        break;
    }

    return {
      x:      entry.x + shift,
      y:      row.y + (row.height - entry.height) * this.itemAlign,
      width:  entry.width,
      height: entry.height
    };
  }

  isUpdateRequired(): boolean {
    return this.masonry.element.offsetWidth != this.masonryWidth;
  }

  getMaxHeight(): number {
    return this.height;
  }

  setMasonry(masonry: Masonry) {
    this.masonry = masonry;
    this.masonryWidth = 0;
  }
}
