import { defaults, forEach } from 'underscore';

import { option } from '../View';
import { IRectangle } from '../../geom/Rectangle';
import ChildableView, { ChildableViewOptions } from '../ChildableView';
import Item from './Item';


/**
 * Constructor options for the Masonry view.
 */
export interface MasonryOptions<T extends Item = Item> extends ChildableViewOptions<T>
{
  /**
   * The layout strategy that should be used.
   */
  strategy?:IMasonryStrategy;

  /**
   * Should the masonry currently be enabled?
   */
  isEnabled?:boolean;
}


/**
 * An interface for masonry layout strategies.
 */
export interface IMasonryStrategy {
  /**
   * Reset the layout state of this strategy. Must be called before
   * a new layout is being generated.
   */
  reset(items: Item[]);

  /**
   * Place the given masonry item and return the assigned position.
   */
  place(item: Item): IRectangle;

  /**
   * Return whether the layout has been invalidated e.g. due to
   * browser resizes.
   */
  isUpdateRequired(): boolean;

  /**
   * Return the maximum height of the generated layout.
   */
  getMaxHeight(): number;

  /**
   * Set the masonry this strategy is working on.
   */
  setMasonry(masonry: Masonry);
}


/**
 * Arranges children in a masonry layout.
 *
 * The actual layout is defined by an instance of IMasonryStrategy
 * which must be passed to the constructor.
 */
export default class Masonry<T extends Item = Item> extends ChildableView<T>
{
  /**
   * The layout strategy that should be used.
   */
  strategy: IMasonryStrategy;

  /**
   * Should the masonry currently be enabled?
   */
  @option({ type: 'bool', defaultValue: true })
  isEnabled: boolean;

  /**
   * Is the grid currently active?
   */
  protected isActive: boolean = false;



  /**
   * Create a new Masonry instance.
   */
  constructor(options?: MasonryOptions<T>) {
    super(defaults(options || {}, {
      childSelector: 'li',
      childClass:    Item,
      isEnabled:     true,
    }));

    if (options.strategy) {
      this.setStrategy(options.strategy);
    }
  }


  /**
   * Render the layout depending on the current activity and masonry state.
   */
  update() {
    this.setActive(this.shouldBeActive());

    if (this.isActive) {
      this.layout();
    } else {
      forEach(this.children, child => child.handleViewportResize());
    }
  }


  /**
   * Render the masonry layout.
   */
  protected layout() {
    const strategy = this.strategy;
    const children = [];

    for (const child of this.children) {
      if (child.isVisible) {
        children.push(child);
        child.handleMasonryPreUpdate(this);
      }
    }

    strategy.reset(children);
    for (const child of children) {
      child.setMasonryPosition(strategy.place(child));
    }

    for (const child of children) {
      child.handleMasonryPostUpdate(this);
    }

    this.setHeight(strategy.getMaxHeight());
  }


  /**
   * Determine whether the masonry should currently be avtive.
   */
  shouldBeActive():boolean {
    return this.children && this.isEnabled;
  }


  /**
   * Set the strategy that is used to genarate the layout.
   */
  setStrategy(strategy:IMasonryStrategy) {
    if (this.strategy) {
      this.strategy.setMasonry(null);
    }

    this.strategy = strategy;
    this.strategy.setMasonry(this);
    this.update();
  }


  /**
   * Set whether the masonry should be enabled or not.
   */
  setEnabled(value:boolean) {
    if (this.isEnabled == value) return;
    this.isEnabled = value;

    this.update();
  }


  setHeight(value:number) {
    this.container.style.height = value + 'px';
  }


  /**
   * Set whether this masonry is active or not.
   */
  protected setActive(value:boolean) {
    if (this.isActive == value) {
      return;
    }

    this.isActive = value;
    this.element.classList.toggle('active', value);

    if (value) {
      for (const child of this.children) {
        child.handleMasonryActivate();
      }
    } else {
      this.container.style.height = '';
      for (const child of this.children) {
        child.handleMasonryDeactivate();
      }
    }
  }


  /**
   * Triggered after the size of the viewport has changed.
   *
   * If this view is used as a component this handler will be called automatically.
   *
   * @returns
   *   TRUE if the event handling should be stopped and child components should not receive
   *   the event, FALSE otherwise.
   */
  handleViewportResize(resizeChildren?: Function): boolean {
    if (!this.isActive || (this.strategy && this.strategy.isUpdateRequired())) {
      this.update();
    } else {
      forEach(this.children, child => child.handleViewportResize());
    }

    return true;
  }
}
