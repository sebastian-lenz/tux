import { defaults } from 'underscore';

import View, { ViewOptions } from '../View';
import CycleableView from '../CycleableView';


/**
 * The default template used by the Arrows class.
 */
const defaultTemplate = `
  <li class="tuxArrows__arrow previous"><a class="tuxArrows__button">&lt;</a></li>
  <li class="tuxArrows__arrow next"><a class="tuxArrows__button">&gt;</a></li>`;

/**
 * Constructor options for the Arrows class.
 */
export interface ArrowsOptions extends ViewOptions
{
  /**
   * The cycleable view that should be controlled.
   */
  owner?: CycleableView;

  /**
   * The template that should be used to render the arrows.
   */
  template?: string;
}


/**
 * Creates previous / next arrows for a CycleableView instance.
 */
export default class Arrows extends View
{
  /**
   * The CycleableView that should be controlled.
   */
  view: CycleableView;


  /**
   * Arrows constructor.
   *
   * @param options
   *   The constructor options.
   */
  constructor(options?: ArrowsOptions) {
    super(options = defaults(options || {}, {
      tagName: 'ul',
      className: 'tuxArrows'
    }));

    this.element.innerHTML = options.template ? options.template : defaultTemplate;

    if (options.owner instanceof CycleableView) {
      this.setView(options.owner);
    }

    this.delegateClick(this.onClick);
  }

  /**
   * Set the CycleableView that should be controlled.
   *
   * @param view
   *   The CycleableView that should be controlled.
   */
  setView(view: CycleableView) {
    if (this.view == view) return;

    if (this.view) {
      this.view.element.removeChild(this.element);
      this.stopListening(this.view);
    }

    this.view = view;
    if (view) {
      this.listenTo(view, 'currentIndexChanged', this.onIndexChanged);
      this.setLength(view.getLength());
      this.onIndexChanged(view.getCurrentIndex());
    }
  }

  /**
   * Set the number of children of the controlled view.
   *
   * @param length
   *   The number of children of the controlled view.
   */
  setLength(length: number): this {
    this.element.classList.toggle('disabled', length < 2);
    return this;
  }

  /**
   * Set the index of the current item.
   *
   * @param index
   *   The index of the current item.
   */
  setCurrentIndex(index: number) {
    const next     = this.element.querySelector('.next');
    const previous = this.element.querySelector('.previous');

    if (this.view.isLooped) {
      previous.classList.remove('disabled');
      next.classList.remove('disabled');
    } else {
      previous.classList.toggle('disabled', index <= 0);
      next.classList.toggle('disabled', index >= this.view.getLength() - 1);
    }
  }

  /**
   * Triggered after the current index of the view has changed.
   *
   * @param index
   *   The new index of the current child.
   */
  onIndexChanged(index: number) {
    this.setCurrentIndex(index);
  }

  /**
   * Triggered if the user clicks on this view.
   *
   * @param event
   *   The triggering event.
   */
  onClick(event: Event): boolean {
    const options: any = {};
    let index  = this.view.getCurrentIndex();
    let target = <HTMLElement>event.target;

    while (target && target.classList) {
      if (target.classList.contains('next')) {
        index += 1;
        options.direction = 'forward';
        break;
      } else if (target.classList.contains('previous')) {
        index -= 1;
        options.direction = 'backward';
        break;
      }

      target = <HTMLElement>target.parentNode;
    }

    this.view.setCurrentIndex(index, options);
    return true;
  }
}
