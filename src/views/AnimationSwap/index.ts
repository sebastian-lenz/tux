import { defaults, defer } from 'underscore';

import animation from '../../util/vendor/animation';
import heightTransition from '../../fx/transition/heightTransition';

import View, { option } from '../View';
import Swap, { SwapOptions } from '../Swap';


/**
 * Constructor options for the AnimationSwap class.
 */
export interface AnimationSwapOptions extends SwapOptions {
  container?: string | HTMLElement;
}

/**
 * Transition options for the AnimationSwap class.
 */
export interface TransitionOptions
{
  /**
   * The name of the transition css class. Set to NULL to disable the animation.
   */
  className?:string;

  /**
   * Should the height of the swap be animated too?
   */
  withHeightTransition?:boolean;
}

/**
 * A swap panel that uses css animations for transitions.
 *
 * @event "swap" (newContent:T, oldContent:T):void
 */
export default class AnimationSwap<T extends View = View> extends Swap<T, TransitionOptions>
{
  /**
   * The name of the last applied transition css class.
   */
  transitionClassName: string;

  @option({ type: 'element' })
  container: HTMLElement;

  /**
   * Default transition options.
   */
  static defaultTransitionOptions: TransitionOptions = {
    className: 'transition'
  };


  /**
   * AnimationSwap constructor.
   *
   * @param options
   *   AnimationSwap constructor options.
   */
  constructor(options?: AnimationSwapOptions) {
    super(options);

    if (animation.endEvent) {
      this.delegate(animation.endEvent, this.handleAnimationEnd);
    }

    if (!this.container) {
      this.container = this.element;
    }
  }

  /**
   * Return the default transition options.
   *
   * @returns
   *   The default transition options.
   */
  getDefaultTransitionOptions(): TransitionOptions {
    return AnimationSwap.defaultTransitionOptions;
  }

  /**
   * Play a transition between the top given views.
   *
   * @param newContent
   *   The view the transition should lead to.
   * @param oldContent
   *   The view the transition should come from.
   * @param options
   *   The transition options.
   */
  protected handleTransition(newContent: T, oldContent: T, options: TransitionOptions) {
    if (options.withHeightTransition && options.className != null) {
      return heightTransition(this.container, () => {
        this.handleTransition(newContent, oldContent, <TransitionOptions>defaults({ withHeightTransition:false }, options))
      });
    }

    newContent ? newContent.element.classList.add('current') : null;
    oldContent ? oldContent.element.classList.remove('current') : null;
    this.trigger('swap', newContent, oldContent);

    if (options.className == null) {
      this.handleTransitionEnd();
    } else {
      this.transitionClassName = options.className;
      this.container.classList.add(options.className);

      newContent ? newContent.element.classList.add('transitionTarget') : null;
      oldContent ? oldContent.element.classList.add('transitionOrigin') : null;
    }
  }

  /**
   * Triggered after a css animation has finished.
   *
   * @param event
   *   The related event object.
   */
  handleAnimationEnd(event: AnimationEvent) {
    const transition = this.transition;

    if (transition && event.target === this.container) {
      this.container.classList.remove(this.transitionClassName);

      if (transition.newContent) {
        transition.newContent.element.classList.remove('transitionTarget');
      }

      if (transition.oldContent) {
        transition.oldContent.element.classList.remove('transitionOrigin');
      }

      defer(() => this.handleTransitionEnd());
    }
  }
}
