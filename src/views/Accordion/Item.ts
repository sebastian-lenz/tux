import heightTransition from '../../fx/transition/heightTransition';

import View, { option, Registry, ViewOptions } from '../View';
import Viewport from '../../services/Viewport';


/**
 * Constructor options for the AccordionItem class.
 */
export interface ItemOptions extends ViewOptions { }


/**
 * Display a single item within an accordion.
 *
 * As accordions are definition lists theis view represents the
 * title of the item wrapped inside <dt>. The actual content is
 * the sibling <dd>.
 *
 * @event "changed" (item:AccordionItem, isExpanded:boolean):void
 *   Triggered after the expanded state of this item has changed.
 */
export default class Item extends View
{
  /**
   * The content of this item.
   */
  @option({ type: 'element', defaultValue: 'dd' })
  body: HTMLElement;

  /**
   * The child components of the body.
   */
  components: View[];

  /**
   * Is this item currently expanded?
   */
  isExpanded = false;


  /**
   * Create a new AccordionItem instance.
   */
  constructor(options?: ItemOptions) {
    super(options);
    this.delegateClick(this.onClick, { selector: 'dt' });
  }

  /**
   * Set whether this item is expanded or not.
   */
  setExpanded(value: boolean) {
    if (this.isExpanded == value) return;
    this.isExpanded = value;

    heightTransition(this.element, () => {
      this.toggleClass('expanded', value);

      if (value && this.components === void 0) {
        this.components = Registry.getInstance().create(this.element);
      }
    }).then(() => {
      Viewport.getInstance().triggerResize();
    });

    this.trigger('changed', this, value);
  }

  /**
   * Triggered when the user clicks onto the item header.
   */
  onClick() {
    this.setExpanded(!this.isExpanded);
    return true;
  }
}
