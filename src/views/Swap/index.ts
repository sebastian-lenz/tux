import * as Q from 'q';
import { defaults } from 'underscore';

import View, { ViewOptions, option } from '..//View';


/**
 * Constructor options for the Swap class.
 */
export interface SwapOptions extends ViewOptions
{
  /**
   * Whether new content view elements should be attached to this view. Defaults to FALSE.
   */
  appendNewContent?: boolean;

  /**
   * Whether hidden content views should be removed or not. Defaults to FALSE.
   */
  removeOldContent?: boolean;
}


/**
 * A view panel that can swap its content.
 */
export default abstract class Swap<T extends View, U> extends View
{
  /**
   * The content currently displayed by this view.
   */
  content:T;

  /**
   * Whether new content view elements should be attached to this view.
   */
  @option({ type: 'bool', defaultValue: false })
  appendNewContent:boolean;

  /**
   * Whether hidden content views should be removed or not.
   */
  @option({ type: 'bool', defaultValue: false })
  removeOldContent:boolean;

  /**
   * The specifications of the current transition.
   */
  protected transition:{ deferred:Q.Deferred<any>, options:U, newContent:T, oldContent:T } = null;

  /**
   * The specifications of the transition we should play after the current one has finished.
   */
  protected successor:{ deferred:Q.Deferred<any>, options:U, newContent:T } = null;



  /**
   * Swap constructor.
   *
   * @param options
   *   Swap constructor options.
   */
  constructor(options?: SwapOptions) {
    super(options);
  }

  /**
   * Return the default transition options.
   *
   * @returns
   *   The default transition options.
   */
  abstract getDefaultTransitionOptions(): U;

  removeContent(content: T) {
    if (this.removeOldContent && content) {
      content.remove();
    }
  }

  /**
   * Set the new content of the swap panel.
   *
   * @param newContent
   *   The new view that should be displayed by this swap panel.
   * @param options
   *   The desired transition options.
   */
  setContent(newContent: T, options?: U): Q.Promise<any> {
    if (this.content === newContent) {
      return Q.when();
    }

    const oldContent = this.content;
    this.content = newContent;

    options = defaults(options || {}, this.getDefaultTransitionOptions());

    if (this.successor) {
      this.successor.deferred.reject(null);
      this.removeContent(this.successor.newContent);
      this.successor = null;
    }

    if (this.transition) {
      if (this.handleInterrupt()) {
        this.transition.deferred.reject(null);
      } else {
        const deferred = Q.defer();
        this.successor = { deferred, options, newContent };
        return deferred.promise;
      }
    }

    if (this.appendNewContent && newContent) {
      this.element.appendChild(newContent.element);
    }

    const deferred = Q.defer();
    this.transition = { deferred, options, newContent, oldContent };
    this.handleTransition(newContent, oldContent, options);

    return deferred.promise;
  }


  /**
   * Try to interrupt the current transition.
   *
   * This method returns FALSE by default causing calls to setContent to become delayed
   * until the current transition has finished. If the used animation technique allows
   * interruptions, this method should be overwritten to handle interruptions gracefully.
   *
   * @returns
   *   TRUE if the current transition could be interrupted, FALSE otherwise.
   */
  protected handleInterrupt():boolean {
    return false;
  }


  /**
   * Play a transition between the top given views.
   *
   * @param newContent
   *   The view the transition should lead to.
   * @param oldContent
   *   The view the transition should come from.
   * @param options
   *   Optional transition options.
   */
  protected abstract handleTransition(newContent:T, oldContent:T, options?:U);


  /**
   * Handle a finished transition.
   *
   * This method must be called by all implementations after an transition has finished.
   */
  protected handleTransitionEnd() {
    if (!this.transition) {
      return;
    }

    if (this.transition.oldContent) {
      this.removeContent(this.transition.oldContent);
    }

    const oldTransition = this.transition;
    const successor = this.successor;
    oldTransition.deferred.resolve();

    if (successor) {
      const newTransition = defaults(this.successor, { oldContent:oldTransition.newContent });
      if (this.appendNewContent && newTransition.newContent) {
        this.element.appendChild(newTransition.newContent.element);
      }

      this.successor  = null;
      this.transition = newTransition;
      this.handleTransition(newTransition.newContent, newTransition.oldContent, newTransition.options);
    } else {
      this.transition = null;
    }
  }
}
