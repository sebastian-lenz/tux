import View from '../index';
import { Component } from './index';

/**
 * A tree structure for discovering and storing component/element relationships.
 */
export default class Node
{
  /**
   * The dom element this node represents.
   */
  element: HTMLElement;

  /**
   * A list of all child nodes attached to this node.
   */
  children: Node[] = null;

  /**
   * The component info attached to this node.
   */
  component: Component = null;

  /**
   * The instance of the component attached to this node.
   */
  instance: View = null;

  /**
   * Whether the create method has already been called or not.
   */
  isCreated: boolean = false;

  /**
   * Whether the component has a resize handler or not.
   */
  hasResizeHandler: boolean = false;



  /**
   * ComponentTree constructor.
   *
   * @param element
   *   The dom element the created tree node represents.
   */
  constructor(element: HTMLElement) {
    this.element = element;
  }


  /**
   * Search for all children of this element matching the given component specification.
   * The found children will be added to the children property of this instance.
   *
   * @param component
   *   The component specification that should be queried for.
   */
  discover(component: Component) {
    var elements = this.element.querySelectorAll(component.selector);

    for (let index = 0, count = elements.length; index < count; index++) {
      const node = this.getNode(<HTMLElement>elements[index]);
      if (node) {
        node.setComponent(component);
      }
    }
  }


  /**
   * Instantiate the component of this node and all of its children.
   *
   * @param result
   *   An optional array the results will be appended to.
   * @returns
   *   A list of all newly created components, existing components will be
   *   omitted, use getComponents() to retrieve all existing components.
   */
  create(result: View[] = []): View[] {
    if (this.isCreated) {
      if (this.children) {
        for (const child of this.children) {
          child.create(result);
        }
      }

      return result;
    }

    this.isCreated = true;
    if (this.component) {
      this.createLocalComponents(result);
    } else {
      this.createChildComponents(result);
    }

    return result;
  }


  /**
   * Find or create the node for the given element.
   *
   * @param element
   *   The element whose node should be retrieved.
   * @returns
   *   The component node representing the given dom element.
   */
  getNode(element: HTMLElement): Node {
    const path = this.getElementPath(element);
    if (path == null) {
      throw new Error('The given element is not a child of this node.');
    }

    if (path.length == 0) {
      return this;
    }

    return this.addChildHierarchy(path);
  }


  /**
   * Manually set the view instance of this node.
   *
   * @param instance
   *   The view instance of this node.
   */
  setInstance(instance: View) {
    if (this.instance) {
      throw new Error('Component instance of node already set.');
    }

    this.instance = instance;
  }


  /**
   * Return all ancestors of the given element until the element of this node is reached.
   *
   * @param element
   *   The dom element whose ancestors should be retrieved.
   * @returns
   *   A list of ancestor elements excluding the element of this node. Returns an
   *   empty array if the given element matches the element of this node. Returns
   *   NULL if the searched element is not a child of this nodes element.
   */
  getElementPath(element: HTMLElement): HTMLElement[] {
    const path = [];
    while (element && element.parentNode) {
      if (element == this.element) {
        return path;
      } else {
        path.unshift(element);
        element = <HTMLElement>element.parentNode;
      }
    }

    return null;
  }


  /**
   * Return a list of all components attached to this node and all of its children.
   *
   * @param result
   *   An optional array the results will be appended to.
   * @returns
   *   A list of all components attached to this node and all of its children.
   */
  getComponents(result: View[] = []): View[] {
    if (this.instance) {
      result.push(this.instance);
    }

    return this.getChildComponents(result);
  }


  /**
   * Return a list of all components attached to the children of this node.
   *
   * @param result
   *   An optional array the results will be appended to.
   * @returns
   *   A list of all components attached to the children of this node.
   */
  getChildComponents(result: View[] = []): View[] {
    if (this.children) {
      for (const child of this.children) {
        child.getComponents(result);
      }
    }

    return result;
  }


  /**
   * Remove an element from the node tree.
   *
   * @param element
   *   The element that should be removed from the tree.
   */
  removeElement(element: HTMLElement) {
    const path = this.getElementPath(element);

    if (path) {
      if (path.length == 0) {
        this.remove();
      } else {
        this.removeChildHierarchy(path);
      }
    }
  }


  /**
   * Create the component of this node.
   *
   * @param result
   *   An optional array the results will be appended to.
   * @returns
   *   A list of all newly created components.
   */
  protected createLocalComponents(result: View[] = []): View[] {
    const component   = this.component;
    const viewClass   = <any>component.viewClass;
    const viewOptions = <any>{ element: this.element };

    if (component.options.allowChildComponents) {
      this.createChildComponents(result);
      viewOptions.components = this.getChildComponents();
    }

    result.push(this.instance = new viewClass(viewOptions));
    return result;
  }


  /**
   * Create the components of all children of this node.
   *
   * @param result
   *   An optional array the results will be appended to.
   * @returns
   *   A list of all newly created components.
   */
  protected createChildComponents(result: View[] = []): View[] {
    if (this.instance) {
      return result;
    }

    if (this.children) {
      for (const child of this.children) {
        child.create(result);
      }
    }

    return result;
  }


  /**
   * Set the component instance assigned to this tree node.
   *
   * @param component
   *   The component instance assigned to this node.
   */
  protected setComponent(component: Component) {
    if (this.component && this.component != component) {
      console.log('Warn: Duplicate component assignment for `' +
          this.component.selector + '` and `' + component.selector + '`.');
      return;
    }

    this.component = component;
  }


  /**
   * Add a child node to this tree node.
   *
   * @param element
   *   The dom element the child should represent. Must be a direct descendant
   *   of this nodes dom element.
   * @returns
   *   The newly created tree node representing the given dom element.
   */
  protected addChild(element: HTMLElement): Node {
    if (element.parentNode != this.element) {
      return null;
    }

    const children = this.children || (this.children = []);
    for (let index = 0, count = children.length; index < count; ++index) {
      if (children[index].element == element) {
        return children[index];
      }
    }

    const child = new Node(element);
    children.push(child);
    return child;
  }


  /**
   * Add a child by its hierarchy to this node.
   *
   * @param path
   *   The path pointing to the child element.
   * @returns
   *   The node created for the last element in the given path.
   */
  protected addChildHierarchy(path: HTMLElement[]): Node {
    let child: Node;
    if (path.length) {
      child = this.addChild(path.shift());
    }

    if (path.length && child) {
      return child.addChildHierarchy(path);
    } else {
      return child;
    }
  }


  /**
   * Remove this node.
   */
  protected remove() {
    if (this.children) {
      for (const child of this.children) {
        child.remove();
      }

      this.children = null;
    }

    if (this.instance) {
      this.instance.remove();
      this.instance = null;
    }
  }


  /**
   * Remove a child by its hierarchy to this node.
   *
   * @param path
   *   The path pointing to the child element.
   * @returns
   *   TRUE if this node is empty after the removal, FALSE otherwise.
   */
  protected removeChildHierarchy(path: HTMLElement[]): boolean {
    if (!this.children) {
      return !this.instance;
    }

    const segment  = path.shift();
    const children = this.children;

    for (let index = 0, count = children.length; index < count; index++) {
      const child = children[index];

      if (child.element == segment) {
        if (path.length) {
          if (child.removeChildHierarchy(path)) {
            children.splice(index, 1);
          }
        } else {
          child.remove();
          children.splice(index, 1);
        }

        break;
      }
    }

    if (this.children.length == 0) {
      this.children = null;
    }

    return !this.instance && !this.children;
  }

  /**
   * Trigger the resize handler on all child components.
   */
  resizeChildren = () => {
    if (this.children) {
      for (const child of this.children) {
        child.handleViewportResize();
      }
    }
  };

  /**
   * Triggers the resize handler on this component and all of its children.
   */
  handleViewportResize() {
    const { instance, resizeChildren } = this;
    if (instance && instance.handleViewportResize(resizeChildren)) {
      return;
    }

    resizeChildren();
  }
}
