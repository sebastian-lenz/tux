import { find } from 'underscore';

import { Component, ComponentOptions } from './index';
import Node from './Node';
import services, { service } from '../../../services';
import View from '../index';
import Viewport from '../../../services/Viewport';


/**
 * The component registry stores data of all known components.
 */
@service('ComponentRegistry')
export default class Registry
{
  /**
   * A list of all registered components.
   */
  components: Component[] = [];

  /**
   * The root node of the component tree.
   */
  rootNode:Node;


  initialize(viewport: Viewport): this {
    viewport.on('resize', this.handleViewportResize, this);
    return this;
  }

  /**
   * Create all components within the given dom element.
   *
   * @param element
   *   The dom element components should be created for,
   * @returns
   *   A list of all newly created components.
   */
  create(element: HTMLElement = document.body) {
    const node = this.getRootNode().getNode(element);
    for (const component of this.components) {
      node.discover(component);
    }

    const result = node.create();
    node.handleViewportResize();
    return result;
  }

  getRootNode() {
    if (!this.rootNode) {
      this.rootNode = new Node(document.body);
    }

    return this.rootNode;
  }

  getNode(element:HTMLElement):Node {
    let node = this.getRootNode();
    const path = node.getElementPath(element);

    while (path.length) {
      const segment = path.shift();
      node = find(node.children, child =>
        child.element == segment
      );

      if (!node) return null;
    }

    return node;
  }

  /**
   * Register a component.
   *
   * @param viewClass
   *   The class that should be instantiated for the component.
   * @param selector
   *   A css selector that selects all dom elements for the component.
   * @param options
   *   Additional component options.
   */
  register(viewClass:typeof View, selector:string, options: ComponentOptions) {
    this.components.push({ viewClass, selector, options });
  }

  /**
   * Called by the view remove function when a view is about to be removed.
   *
   * @param element
   *   The element of the view that is about to be removed.
   */
  handleRemove(element:HTMLElement) {
    this.rootNode.removeElement(element);
  }

  /**
   * Triggered after the size of the viewport has changed.
   */
  handleViewportResize() {
    this.rootNode.handleViewportResize();
  }

  /**
   * Return the current instance of the ComponentRegistry service.
   *
   * @returns
   *    The current instance of the ComponentRegistry service.
   */
  static getInstance(): Registry {
    return services.get('ComponentRegistry', Registry);
  }

  /**
   * Register the given view instance.
   *
   * @param instance
   *   The instance that should be added to the component tree.
   */
  static registerInstance(instance:View) {
    const node = Registry.getInstance()
      .getRootNode()
      .getNode(instance.element);

    if (node.instance != instance) {
      node.setInstance(instance);
    }
  }
}
