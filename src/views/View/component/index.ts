import { defaults } from 'underscore';

import Node from './Node';
import Registry from './Registry';
import View from '../index';


export { Node, Registry };

/**
 * Component options.
 */
export interface ComponentOptions
{
  allowChildComponents?: boolean;
}

/**
 * Definition of the data stored for each component by the registry.
 */
export interface Component
{
  viewClass: typeof View;

  selector: string;

  options: ComponentOptions
}

/**
 * A class annotation that registers view classes as components.
 *
 * @param selector
 *   A css selector that selects all elements the component should be attached to.
 * @param options
 *   Additional component options.
 */
export default function component(selector: string, options?: ComponentOptions): ClassDecorator
{
  options = defaults(options || {}, {
    allowChildComponents: false
  });

  return function<T extends Function>(viewClass:T):T {
    Registry.getInstance().register(<any>viewClass, selector, options);
    return viewClass;
  }
}

