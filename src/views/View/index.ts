import { forEach, uniqueId } from 'underscore';

import component, { Node, Registry } from './component';
import ClickHandler, { ClickHandlerCallback } from '../../handler/ClickHandler';
import Delegate, { DelegateOptions } from './Delegate';
import Events from '../../Events';
import Model from '../../models/Model';
import { Initializers, child, children, option } from './initializer';

import '../../util/polyfill/classList';
import '../../util/polyfill/eventListener';
import '../../util/polyfill/matches';


export { child, children, component, option, Registry };

/**
 * Constructor options of the View class.
 */
export interface ViewOptions
{
  // The element the created view should be appended to.
  appendTo?: HTMLElement;

  attributes?: any;

  // The class name of the created element if no element is given.
  className?: string;

  // A list of all created child components.
  components?: View[];

  // The preexisting element the view should be created for.
  element?: HTMLElement;

  // The owner or parent view of the view.
  owner?: View;

  // The tag name of the created element if no element is given.
  tagName?: string;

  // A template that should be applied to the created view.
  template?: string | Function;

  model?: Model;
}

/**
 * Base class of all views.
 */
export default class View extends Delegate
{
  // The underlying html element this view represents.
  element: HTMLElement;

  model: Model;
  cid: string;

  // Whether this view has been removed or not.
  protected isRemoved: boolean;

  // A list of all initializers attached to this view.
  private _initializers: Initializers;


  /**
   * View constructor.
   *
   * @param options
   *   Thew view constructor options.
   */
  constructor(options: ViewOptions = {}) {
    super();
    this.cid = uniqueId('view');
    this.ensureElement(options);

    for (var key in this._initializers) {
      this._initializers[key].invoke(this, options);
    }
  }

  /**
   * Ensures this view has a html element.
   */
  private ensureElement(options: ViewOptions): this {
    const { appendTo, template } = options;
    let { element } = options;

    if (!element) {
      const { attributes, className, tagName = 'div' } = options;
      element = document.createElement(tagName);
      if (className) element.className = className;
      if (attributes) forEach(attributes, (value, key) =>
        element.setAttribute(<any>key, <any>value)
      );
    }

    if (appendTo) appendTo.appendChild(element);
    if (template) element.innerHTML = typeof template === 'function'
      ? template(this, options)
      : template;

    this.element = element;
    return this;
  }

  /**
   * Remove this view.
   */
  remove(): this {
    if (this.isRemoved) return this;

    const registry = Registry.getInstance();
    if (registry) {
      registry.handleRemove(this.element);
    }

    const { element } = this;
    if (element.parentNode) {
      element.parentNode.removeChild(element);
    }

    this.undelegateEvents();
    this.stopListening();
    this.isRemoved = true;
    this.element = null;
    return this;
  }

  delegateClick(listener: ClickHandlerCallback, options: DelegateOptions = {}): this {
    const { selector, scope = this } = options;
    new ClickHandler(this, listener, selector, scope);
    return this;
  }

  /**
   * Triggered after the size of the viewport has changed.
   *
   * If this view is used as a component this handler will be called automatically.
   *
   * @param resizeChildren
   *   A callback that triggers the resize event on all child components.
   * @returns
   *   TRUE if the event handling should be stopped and child components should not receive
   *   the event, FALSE otherwise.
   */
  handleViewportResize(resizeChildren?: Function): boolean {
    return false;
  }

  query<K extends keyof ElementTagNameMap>(selectors: K): ElementTagNameMap[K] | null;
  query(selectors: string): HTMLElement | null;
  query(selectors: string) {
    return this.element.querySelector(selectors);
  }

  queryAll<K extends keyof ElementListTagNameMap>(selectors: K): ElementListTagNameMap[K];
  queryAll(selectors: string): NodeListOf<Element>;
  queryAll(selectors: string) {
    return this.element.querySelectorAll(selectors);
  }

  addClass(...token: string[]): this {
    this.element.classList.add(...token);
    return this;
  }

  containsClass(token: string): boolean {
    return this.element.classList.contains(token);
  }

  removeClass(...token: string[]): this {
    this.element.classList.remove(...token);
    return this;
  }

  toggleClass(token: string, force?: boolean): boolean {
    return this.element.classList.toggle(token, force);
  }
}
