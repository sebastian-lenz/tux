import View from '../../index';
import Option, { OptionOptions } from './Option';


export interface AnyOptionOptions extends OptionOptions {
  type: 'any';

  // The default value of the option.
  defaultValue?: any;
}

export default class AnyOption extends Option<any, AnyOptionOptions> { }
