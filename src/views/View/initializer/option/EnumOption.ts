import { forEach, keys } from 'underscore';

import View from '../../index';
import Option, { OptionOptions } from './Option';


export interface EnumOptionOptions extends OptionOptions {
  type: 'enum';

  // The default value of the option.
  defaultValue?: number;

  // The enumeration that defines the valid values.
  values: any;
}

export default class EnumOption extends Option<number, EnumOptionOptions>
{
  lookupTable: any;

  values: any;


  /**
   * EnumOption constructor.
   */
  constructor(property: string, options: EnumOptionOptions) {
    super(property, options);

    const { values } = options;
    const lookupTable: any = {};
    forEach(keys(options.values), value => {
      if (typeof value === 'string') {
        lookupTable[value.toLowerCase()] = values[value];
      }
    });

    this.lookupTable = lookupTable;
  }

  /**
   * Convert the given option value.
   */
  convert(scope: View, value: any): number | undefined {
    const { lookupTable, values  } = this;

    if (typeof value == 'string') {
      value = value.toLowerCase();
      return value in lookupTable
        ? lookupTable[value]
        : undefined;
    }

    if (value in values) return value;
    return undefined;
  }
}
