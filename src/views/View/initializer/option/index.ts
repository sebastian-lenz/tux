import AnyOption, { AnyOptionOptions } from './AnyOption';
import BoolOption, { BoolOptionOptions } from './BoolOption';
import ClassOption, { ClassOptionOptions } from './ClassOption';
import ElementOption, { ElementOptionOptions } from './ElementOption';
import EnumOption, { EnumOptionOptions } from './EnumOption';
import IntOption, { IntOptionOptions } from './IntOption';
import JsonOption, { JsonOptionOptions } from './JsonOption';
import NumberOption, { NumberOptionOptions } from './NumberOption';
import OwnerOption, { OwnerOptionOptions } from './OwnerOption';
import StringOption, { StringOptionOptions } from './StringOption';

import { getInitializer } from '../index';

export type OptionOptions =
AnyOptionOptions |
  BoolOptionOptions |
  ClassOptionOptions |
  ElementOptionOptions |
  EnumOptionOptions |
  IntOptionOptions |
  JsonOptionOptions |
  NumberOptionOptions |
  OwnerOptionOptions |
  StringOptionOptions;

function create(owner: object, property: string, options: OptionOptions) {
  const name = `Option::${property}`;
  const initializer = getInitializer(owner, name, () => {
    switch (options.type) {
      case 'any': return new AnyOption(property, options);
      case 'bool': return new BoolOption(property, options);
      case 'class': return new ClassOption(property, options);
      case 'element': return new ElementOption(property, options);
      case 'enum': return new EnumOption(property, options);
      case 'int': return new IntOption(property, options);
      case 'json': return new JsonOption(property, options);
      case 'number': return new NumberOption(property, options);
      case 'owner': return new OwnerOption(property, options);
      case 'string': return new StringOption(property, options);
    }
  });
}

export function option(name: string, options: OptionOptions): ClassDecorator;
export function option(options: OptionOptions): PropertyDecorator;
export function option() {
  if (arguments.length > 0 && typeof(arguments[0]) === 'string') {
    const property = <string>arguments[0];
    const options = arguments[1];
    return function(owner: Function) {
      create(owner.prototype, name, options);
    };
  } else {
    const options = arguments[0];
    return function(owner: object, property: string) {
      create(owner, property, options);
    };
  }
}
