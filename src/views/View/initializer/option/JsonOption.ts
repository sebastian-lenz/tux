import View from '../../index';
import Option, { OptionOptions } from './Option';


export interface JsonOptionOptions extends OptionOptions {
  type: 'json';

  // The default value of the option.
  defaultValue?: object;
}

export default class JsonOption extends Option<object, JsonOptionOptions>
{
  /**
   * Convert the given option value.
   */
  convert(scope: View, value: any): object | undefined {
    if (typeof value == 'string') {
      return JSON.parse(value);
    } else if (typeof value == 'object') {
      return value;
    }

    return undefined;
  }
}
