import View from '../../index';
import Option, { OptionOptions } from './Option';


export interface StringOptionOptions extends OptionOptions {
  type: 'string';

  // The default value of the option.
  defaultValue?: string;
}

export default class StringOption extends Option<string, StringOptionOptions>
{
  /**
   * Convert the given option value.
   */
  convert(scope: View, value: any): string {
    return `${value}`;
  }
}
