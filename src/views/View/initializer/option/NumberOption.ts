import View from '../../index';
import Option, { OptionOptions } from './Option';


export interface NumberOptionOptions extends OptionOptions {
  type: 'number';

  // The default value of the option.
  defaultValue?: number;
}

export default class NumberOption extends Option<number, NumberOptionOptions>
{
  /**
   * Convert the given option value.
   */
  convert(scope: View, value: any): number | undefined {
    if (typeof value === 'string') {
      return parseFloat(value);
    } else if (typeof value === 'number') {
      return value;
    }

    return undefined;
  }
}
