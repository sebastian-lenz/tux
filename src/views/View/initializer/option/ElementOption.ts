import { isElement } from 'underscore';
import View, { ViewOptions } from '../../index';
import Option, { OptionOptions } from './Option';

export interface ElementOptionOptions extends OptionOptions {
  type: 'element';

  // The class name of the element that should be created.
  className?: string;

  defaultValue?: string;

  // The tag name of the element that should be created.
  tagName?: string;
}

export default class ElementOption extends Option<
  HTMLElement,
  ElementOptionOptions
> {
  // The class name of the element that should be created.
  className: string;

  // The tag name of the element that should be created.
  tagName: string;

  /**
   * Extract the value from the given view and options object.
   */
  getValue(scope: View, options: ViewOptions): HTMLElement {
    const value = super.getValue(scope, options);
    if (isElement(value)) return value;

    if (typeof value === 'string') {
      const element = scope.element.querySelector(value);
      if (element) {
        return <HTMLElement>element;
      }
    }

    const { className, tagName } = this;
    if (tagName) {
      const element = document.createElement(tagName);
      if (className) {
        element.className = className;
      }

      scope.element.appendChild(element);
      return element;
    }

    return undefined;
  }
}
