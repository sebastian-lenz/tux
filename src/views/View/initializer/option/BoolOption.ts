import View from '../../index';
import Option, { OptionOptions } from './Option';


export interface BoolOptionOptions extends OptionOptions {
  type: 'bool';

  // The default value of the option.
  defaultValue?: boolean;
}

export default class BoolOption extends Option<boolean, BoolOptionOptions>
{
  /**
   * Convert the given option value.
   */
  convert(scope: View, value: any): boolean {
    if (typeof value === 'string') {
      value = value.toLowerCase();
      return ((value == 'true') || (value == 'yes') || (value == '1'))
    }

    return !!value;
  }
}
