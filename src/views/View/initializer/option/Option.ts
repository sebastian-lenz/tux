import { extendOwn } from 'underscore';

import View, { ViewOptions } from '../../index';
import { dasherize, trimStart } from '../../../../util/string';
import { Initializer } from '../index';


export interface OptionOptions
{
  // The name of the html attribute the value should be read from.
  attribute?: string;
}

export default class Option<U = any, V extends OptionOptions = OptionOptions> implements Initializer
{
  // The name of the property the value should be stored to.
  property:string;

  // The name of the html attribute the value should be read from.
  attribute:string;

  // The name of the option the value should be read from.
  option:string;

  // The default value of the option.
  defaultValue: U;


  /**
   * Option constructor.
   */
  constructor(property: string, options: V) {
    const option = trimStart(property, '_');
    this.property = property;
    this.option = option;
    this.attribute = 'data-' + dasherize(option);

    extendOwn(this, options);
  }

  /**
   * Invoke this initializer.
   */
  invoke(scope: View, options: ViewOptions) {
    const { option, property } = this;
    const value: U = this.getValue(scope, options);

    if (property) {
      (<any>scope)[property] = value;
    } else if (option) {
      (<any>options)[option] = value;
    }
  }

  /**
   * Convert the given option value.
   */
  convert(scope: View, value: any): U | undefined {
    return <U>value;
  }

  /**
   * Extract the value from the given view and options object.
   */
  getValue(scope: View, options: ViewOptions): U {
    const { attribute, option } = this;
    const { element } = options;
    let value: U | undefined = void 0;

    if (element && attribute && element.hasAttribute(attribute)) {
      value = this.convert(scope, element.getAttribute(attribute));
      if (value !== void 0) return value;
    }

    if (option && option in options) {
      value = this.convert(scope, (<any>options)[option]);
      if (value !== void 0) return value;
    }

    return this.getDefaultValue();
  }

  /**
   * Return this options default value.
   */
  getDefaultValue(): U {
    return this.defaultValue;
  }
}
