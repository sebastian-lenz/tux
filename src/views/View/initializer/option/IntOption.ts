import View from '../../index';
import Option, { OptionOptions } from './Option';


export interface IntOptionOptions extends OptionOptions {
  type: 'int';

  // The default value of the option.
  defaultValue?: number;
}

export default class IntOption extends Option<number, IntOptionOptions>
{
  /**
   * Convert the given option value.
   */
  convert(scope: View, value: any): number | undefined {
    if (typeof value === 'string') {
      return parseInt(value);
    } else if (typeof value === 'number') {
      return Math.round(value);
    }

    return undefined;
  }
}
