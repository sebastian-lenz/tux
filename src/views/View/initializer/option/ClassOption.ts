import View from '../../index';
import Option, { OptionOptions } from './Option';


export interface ClassOptionOptions<T = any> extends OptionOptions {
  type: 'class';

  // The default value of the option.
  defaultValue?: T;

  // The constructor function of the class.
  ctor: { new (data: any): T };
}

export default class ClassOption<T> extends Option<T, ClassOptionOptions<T>>
{
  // The constructor function of the class.
  ctor: { new (data: any): T };


  /**
   * Convert the given option value.
   */
  convert(scope: View, value: any): T {
    const { ctor } = this;
    if (value instanceof ctor) {
      return value;
    }

    return new ctor(value);
  }
}
