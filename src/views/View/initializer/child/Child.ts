import { defaults, extendOwn, map } from 'underscore';

import View, { ViewOptions } from '../../index';
import Registry from '../../component/Registry';
import { Initializer } from '../index';


/**
 * ChildInitializer constructor options.
 */
export interface ChildOptions
{
  /**
   * The selector that will be used to retrieve the child elements.
   */
  selector?: string;

  /**
   * The view class that should be instantiated.
   */
  viewClass: any;

  /**
   * Default constructor options.
   */
  options?: any;

  /**
   * Whether multiple children should be resolved or not.
   */
  multiple?: boolean;

  /**
   * Auto create the component if the selector is missing?
   */
  autoCreate?: boolean;

  /**
   * Is the given childClass a factory function?
   */
  isFactory?: boolean;
}


/**
 * An initializer that instantiates child components.
 */
export default class Child implements Initializer
{
  /**
   * The name of the property this initializer should initialize.
   */
  propertyName:string;

  /**
   * The view class that should be instantiated.
   */
  viewClass:any;

  /**
   * Default constructor options.
   */
  options:any;

  /**
   * The selector that will be used to retrieve the child elements.
   */
  selector:string;

  /**
   * Whether multiple children should be resolved or not.
   */
  multiple:boolean;

  /**
   * Auto create the component if the selector is missing?
   */
  autoCreate:boolean;

  /**
   * Is the given childClass a factory function?
   */
  isFactory:boolean;



  /**
   * ChildInitializer constructor.
   *
   * @param propertyName
   *   The name of the property this initializer should initialize.
   */
  constructor(propertyName?: string) {
    this.propertyName = propertyName;
  }


  /**
   * Invoke this initializer for the given scope.
   *
   * @param scope
   *   The view instance that should be initialized.
   * @param options
   *   The constructor options that have been passed to the view.
   */
  invoke(scope: View, options: ViewOptions) {
    const { element } = scope;
    const { selector } = this;
    let result:any = null;

    if (this.multiple) {
      const children = selector ? scope.queryAll(selector) : [];
      result = map(children, child =>
        this.createComponent(scope, <HTMLElement>child)
      );
    }
    else {
      const child = selector ? scope.query(selector) : null;
      if (child || this.autoCreate) {
        result = this.createComponent(scope, child);
      }
    }

    scope[this.propertyName] = result;
  }


  /**
   * Create an instance of the child view class for the given element.
   *
   * @param scope
   * @param element
   *   The dom element a view instance should be created for.
   */
  createComponent(scope: View, element?: HTMLElement):View {
    let options: ViewOptions = { owner: scope };
    if (element) {
      options.element = element;
    } else {
      options.appendTo = scope.element;
    }

    if (this.options) {
      options = defaults(options, this.options);
    }

    let child: View;
    if (this.isFactory) {
      child = this.viewClass(options);
    } else {
      child = new this.viewClass(options);
    }

    const root = Registry.getInstance().getRootNode();
    const node = root.getNode(child.element);
    node.setInstance(child);

    return child;
  }


  /**
   * Apply the given options object on this initializer.
   *
   * @param options
   *   The options that should be applied.
   */
  setOptions(options: ChildOptions) {
    extendOwn(this, options);
  }
}
