import { defaults } from 'underscore';

import Child, { ChildOptions} from './Child';
import { getInitializer } from '../index';


export function child(name: string, options?: ChildOptions): ClassDecorator;
export function child(options?: ChildOptions): PropertyDecorator;
export function child(...args: any[])
{
  if (args.length > 0 && typeof(args[0]) === 'string') {
    const property = <string>args.shift();
    const options  = args.length ? args.shift() : void 0;
    const name     = 'ChildComponent::' + property;

    return function(owner: any) {
      getInitializer(owner.prototype, name, () => new Child(property))
        .setOptions(options);
    };
  }
  else {
    const options = args.length ? args.shift() : void 0;

    return function(owner: any, property: string) {
      const name = 'ChildComponent::' + property;
      getInitializer(owner, name, () => new Child(property))
        .setOptions(options);
    };
  }
}

export function children(name: string, options: ChildOptions): ClassDecorator;
export function children(options: ChildOptions): PropertyDecorator;
export function children(...args: any[])
{
  if (args.length > 0 && typeof(args[0]) === 'string') {
    args[1] = defaults(args[1] || {}, { multiple: true });
  } else {
    args[0] = defaults(args[0] || {}, { multiple: true });
  }

  return child.apply(this, args);
}
