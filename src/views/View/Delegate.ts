import Events from '../../Events';

import '../../util/polyfill/eventListener';


export interface DelegateOptions {
  passive?: boolean;
  selector?: string;
  scope?: any;
}

export interface DomEvent {
  eventName: string;
  handler: EventListener;
  listener: EventListener;
  scope: any;
  selector: string;
}

const supportsPassiveOption = (function() {
  let result = false;
  try {
    const opts = Object.defineProperty({}, 'passive', {
      get: () => result = true
    });

    window.addEventListener('test', null, opts);
    window.removeEventListener('test', null, opts);
  } catch (e) { }

  return result;
})();

/**
 * Base class of all views.
 */
export default class Delegate extends Events
{
  // The underlying html element this view represents.
  element: Element | Document | null;

  // A list of all delegated dom events.
  private _domEvents: DomEvent[] = [];


  /**
   * View constructor.
   *
   * @param options
   *   Thew view constructor options.
   */
  constructor(element: Element | Document | null = null) {
    super();
    this.element = element;
  }


  // Make a event delegation handler for the given `eventName` and `selector`
  // and attach it to `this.el`.
  // If selector is empty, the listener will be bound to `this.el`. If not, a
  // new handler that will recursively traverse up the event target's DOM
  // hierarchy looking for a node that matches the selector. If one is found,
  // the event's `delegateTarget` property is set to it and the return the
  // result of calling bound `listener` with the parameters given to the
  // handler.
  delegate(eventName: string, listener: EventListener, options: DelegateOptions = {}): EventListener {
    const { selector, scope = this } = options;
    const { element } = this;
    let handler: EventListener;

    if (selector) {
      handler = function (event: any) {
        let node = <HTMLElement>(event.target || event.srcElement);
        for (; node && node != element; node = <HTMLElement>(node.parentNode)) {
          if (!node.matches(selector)) continue;

          event.delegateTarget = node;
          listener.call(scope, event);
          break;
        }
      }
    } else {
      handler = function(event: any) {
        listener.call(scope, event);
      }
    }

    if (options.passive === false && supportsPassiveOption) {
      element.addEventListener(eventName, handler, <any>{ passive: false });
    } else {
      element.addEventListener(eventName, handler, false);
    }

    this._domEvents.push({ eventName, handler, listener, scope, selector });
    return handler;
  }

  // Remove a single delegated event. Either `eventName` or `selector` must
  // be included, `selector` and `listener` are optional.
  undelegate(eventName: string, listener: EventListener, options: DelegateOptions = {}): this {
    const { selector, scope } = options;
    const { _domEvents, element } = this;
    if (element) {
      const handlers = _domEvents.slice();
      let i = handlers.length;

      while (i--) {
        const item = handlers[i];
        const match =
          item.eventName === eventName &&
          (listener ? item.listener === listener : true) &&
          (scope ? item.scope === scope : true) &&
          (selector ? item.selector === selector : true);

        if (match) {
          element.removeEventListener(item.eventName, item.handler, false);
          _domEvents.splice(i, 1);
        }
      }
    }

    return this;
  }

  /**
   * Remove all events created with `delegate` from `el`
   */
  undelegateEvents(): this {
    const { element, _domEvents } = this;

    if (element) {
      for (let i = 0, len = _domEvents.length; i < len; i++) {
        const { eventName, handler } = _domEvents[i];
        element.removeEventListener(eventName, handler, false);
      }

      this._domEvents.length = 0;
    }

    return this;
  }
}
