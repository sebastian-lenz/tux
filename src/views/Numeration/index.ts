import { defaults, indexOf } from 'underscore';

import View, { ViewOptions } from '../View';
import CycleableView from '../CycleableView';


/**
 * Constructor options for the Numeration class.
 */
export interface NumerationOptions extends ViewOptions
{
	/**
	 * The CycleableView that should be controlled.
	 */
	owner?: CycleableView;

  itemClass?: string;

  selectedItemClass?: string;
}

export default class Numeration extends View
{
	/**
	 * The CycleableView that should be controlled.
	 */
	view: CycleableView;

	/**
	 * A list of items wihtin the numeration.
   */
	items: HTMLLIElement[] = [];

	/**
	 * The index of the currently highlighted item.
   */
	index: number = -1;

  itemClass?: string;

  selectedItemClass?: string;


	/**
	 * Numeration constructor.
	 *
	 * @param options
	 *   The constructor options.
   */
	constructor(options?: NumerationOptions) {
		super(options = defaults(options || {}, {
			tagName:   'ul',
			className: 'tuxNumeration',
      itemClass: 'tuxNumeration__item',
      selectedItemClass: 'current',
		}));

    this.itemClass = options.itemClass;
    this.selectedItemClass = options.selectedItemClass;

		this.delegateClick(this.handleClick, { selector: `.${options.itemClass}` });

    if (options.owner instanceof CycleableView) {
  		this.setView(options.owner);
    }
	}

	/**
	 * Set the CycleableView that should be controlled.
	 *
	 * @param view
	 *   The CycleableView that should be controlled.
	 */
	setView(view: CycleableView): this {
		if (this.view == view) return this;

		if (this.view) {
			this.stopListening(this.view);
		}

		if (view) {
			this
        .setLength(view.getLength())
        .setIndex(view.getCurrentIndex())
        .listenTo(view, 'currentChanged', this.handleCurrentChanged)
        .listenTo(view, 'currentWillChange', this.handleCurrentChanged)
        .listenTo(view, 'add', this.handleLengthChanged)
        .listenTo(view, 'remove', this.handleLengthChanged)
        .listenTo(view, 'reset', this.handleLengthChanged);
		}

		this.view = view;
    return this;
	}

	/**
	 * Set the number of children of the controlled view.
	 *
	 * @param length
	 *   The number of children of the controlled view.
	 */
	setLength(length: number): this {
		const items = this.items;
		let itemsLength = items.length;

		if (itemsLength == length) {
			return this;
		}

		while (itemsLength < length) {
			let item = document.createElement('li');
			item.className = this.itemClass;
			item.innerText = `${itemsLength + 1}`;

			items.push(item);
			itemsLength += 1;
			this.element.appendChild(item);
		}

		while (itemsLength > length) {
			this.element.removeChild(items.pop());
			itemsLength -= 1;

			if (this.index == itemsLength) {
				this.index = -1;
			}
		}

    return this;
	}

	/**
	 * Set the index of the current item.
	 *
	 * @param index
	 *   The index of the current item.
	 */
	setIndex(index: number): this {
    const { items, selectedItemClass } = this;
		index = Math.max(-1, Math.min(items.length - 1, Math.floor(index)));
		if (this.index == index) return this;

		if (this.index != -1) {
			items[this.index].classList.remove(selectedItemClass);
		}

		this.index = index;
		if (index != -1) {
			items[index].classList.add(selectedItemClass);
		}

    return this;
	}

	/**
	 * Triggered if the user clicks on this view.
	 *
	 * @param event
	 *   The triggering event.
	 */
	handleClick(event: Event, target: HTMLElement) {
    const index = indexOf(this.items, target);
    if (index !== -1) {
      this.handleSelectIndex(index);
    }

    return true;
	}

  handleSelectIndex(index: number) {
    this.trigger('select', index);
    if (this.view) {
      this.view.setCurrentIndex(index);
    }
  }

	/**
	 * Triggered after the current index of the view has changed.
	 */
	handleCurrentChanged(item: View) {
		this.setIndex(this.view.indexOf(item));
	}

  handleLengthChanged() {
    this.setLength(this.view.getLength());
  }
}
