import { defaults, map } from 'underscore';
import View, { option, ViewOptions } from '../View';

import Option from './Option';


export interface LabelOptions extends ViewOptions
{
  emptyLabel?: string;
}

export default class Label extends View
{
  @option({ type: 'string', defaultValue: '-' })
  emptyLabel: string;

  extraClass: string | null = null;
  option: Option | null = null;


  constructor(options: LabelOptions) {
    super(defaults(options, {
      className: 'tuxDropDownLabel',
    }));

    this.delegateClick(this.handleClick);
  }

  setExtraClass(extraClass: string) {
    if (this.extraClass === extraClass) return;
    if (this.extraClass) this.removeClass(this.extraClass);

    this.extraClass = extraClass;
    if (extraClass) this.addClass(extraClass);
  }

  setOption(option: Option | null) {
    if (this.option === option) return;

    const { element, emptyLabel } = this;
    element.innerHTML = option ? option.caption : emptyLabel;

    this.setExtraClass(option ? option.extraClass : 'empty');
    this.option = option;
  }

  handleClick() {
    this.trigger('click');
    return true;
  }
}
