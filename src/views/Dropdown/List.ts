import { defaults, find, forEach, map } from 'underscore';

import { option } from '../View';
import ChildableView, { ChildableViewOptions } from '../ChildableView';
import Viewport from '../../services/Viewport';

import Dropdown from './index';
import Group, { GroupOptions } from './Group';
import Option, { OptionOptions } from './Option';


export type ListChild = Group | Option;

export type GroupClass = { new(options: GroupOptions): Group };

export type OptionClass = { new(options: OptionOptions): Option };

export enum Direction {
  Bottom,
  Top,
};

export interface ListOptions extends ChildableViewOptions
{
  groupClass?: GroupClass;
  groupOptions?: GroupOptions;
  optionClass?: OptionClass;
  optionOptions?: OptionOptions;
  owner: Dropdown;
}

export default class List extends ChildableView<ListChild>
{
  @option({ type: 'any', defaultValue: Group })
  groupClass: GroupClass;

  @option({ type: 'any', defaultValue: {} })
  groupOptions: GroupOptions;

  @option({ type: 'any', defaultValue: Option })
  optionClass: OptionClass;

  @option({ type: 'any', defaultValue: {} })
  optionOptions: OptionOptions;

  direction: Direction = Direction.Bottom;
  dropdown: Dropdown;
  isExpanded: boolean = false;
  options: Option[] = [];


  constructor(options: ListOptions) {
    super(defaults(options, {
      className: 'tuxDropDownList',
    }));

    this.dropdown = options.owner;
    this.delegateClick(this.handleClick);
  }

  setExpanded(value: boolean) {
    if (this.isExpanded === value) return;
    this.isExpanded = value;

    if (value) this.updateDirection();
    this.toggleClass('expanded', value);
  }

  sync(select: HTMLSelectElement) {
    const { optionClass, optionOptions, groupClass, groupOptions } = this;
    if (!select) return;

    this.removeAllChildren();
    this.options.length = 0;

    forEach(select.childNodes, node => {
      if (node.nodeType !== Node.ELEMENT_NODE) return;

      let child = null;
      if (node.nodeName === 'OPTION') {
        child = this.syncOption(<HTMLOptionElement>node);
      } else if (node.nodeName === 'OPTGROUP') {
        child = this.syncGroup(<HTMLOptGroupElement>node);
      }

      if (child) this.addChild(child);
    });
  }

  protected syncGroup(group: HTMLOptionElement) {
    const { groupClass, groupOptions } = this;
    const options = defaults({}, groupOptions, {
      group,
      owner: this,
    });

    const child = new groupClass(options);
    forEach(group.childNodes, node => {
      if (node.nodeType !== Node.ELEMENT_NODE) return;
      if (node.nodeName !== 'OPTION') return;

      const option = this.syncOption(<HTMLOptionElement>node);
      child.addChild(option);
    });

    return child;
  }

  protected syncOption(option: HTMLOptGroupElement) {
    const { optionClass, optionOptions } = this;
    const options = defaults({}, optionOptions, {
      option,
      owner: this,
    });

    const child = new optionClass(options);
    this.options.push(child);
    return child;
  }

  updateDirection() {
    const { dropdown } = this;
    let direction = this.direction;

    if (dropdown.defaultDirection) {
      direction = dropdown.defaultDirection;
    } else {
      const viewport = Viewport.getInstance();
      const { top } = dropdown.element.getBoundingClientRect();
      direction = top > viewport.height * 0.5
        ? Direction.Top
        : Direction.Bottom;
    }

    if (this.direction === direction) return;
    this.direction = direction;
    this.toggleClass('top', direction === Direction.Top);
  }

  handleClick(event: Event) {
    const { element, options } = this;
    let target = <HTMLElement>event.target;

    for (; target && target !== element; target = <HTMLElement>target.parentNode) {
      const option = find(options, option => option.element === target);
      if (option) {
        this.trigger('click', option);
        return true;
      }
    }

    return false;
  }
}
