import { defaults, map } from 'underscore';

import ChildableView, { ChildableViewOptions } from '../ChildableView';


export interface GroupOptions extends ChildableViewOptions
{
  containerClassName?: string;
  group?: HTMLOptGroupElement;
  labelClassName?: string;
}

export default class Group extends ChildableView
{
  label: HTMLElement;
  container: HTMLElement;


  constructor(options: GroupOptions) {
    super(options = defaults(options, {
      className: 'tuxDropDownGroup',
    }));

    const { element } = this;
    const { className, containerClassName, group, labelClassName } = options;

    const label = this.label = document.createElement('div');
    label.className = labelClassName || `${className}--label`;
    element.appendChild(label);

    const container = this.container = document.createElement('div');
    container.className = containerClassName || `${className}--container`;
    element.appendChild(container);

    if (group) {
      this.setCaption(group.label);
    }
  }

  setCaption(value: string) {
    this.label.innerHTML = value;
  }
}
