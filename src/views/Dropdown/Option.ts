import { defaults, map } from 'underscore';
import View, { option, ViewOptions } from '../View';


export interface OptionOptions extends ViewOptions
{
  option?: HTMLOptionElement;
}

export default class Option extends View
{
  caption: string = '';
  extraClass: string = '';
  isSelected: boolean = false;
  value: string = '';


  constructor(options: OptionOptions) {
    super(defaults(options, {
      className: 'tuxDropDownItem',
    }));

    const { option } = options;
    if (option) {
      this.value = option.value;
      this.setCaption(option.innerHTML);

      if (option.hasAttribute('data-class')) {
        this.addClass(
          this.extraClass = option.getAttribute('data-class')
        );
      }
    }
  }

  setCaption(value: string) {
    this.caption = value;
    this.element.innerHTML = value;
  }

  setSelected(value: boolean) {
    if (this.isSelected === value) return;
    this.isSelected = value;

    this.toggleClass('selected', value);
  }
}
