import { defaults, findIndex, forEach, map } from 'underscore';

import DocumentView from '../../services/DocumentView';
import View, { child, option, ViewOptions } from '../View';

import Label, { LabelOptions } from './Label';
import List, { Direction, ListOptions } from './List';
import Option from './Option';


export type LabelClass = { new(options: LabelOptions): Label };

export type ListClass = { new(options: ListOptions): List };

export interface DropdownOptions extends ViewOptions
{
  select?: string | HTMLSelectElement;
  labelClass?: LabelClass;
  labelOptions?: LabelOptions;
  listClass?: ListClass;
  listOptions?: ListOptions;
}

export default class Dropdown extends View
{
  @option({ type: 'enum', values: Direction })
  defaultDirection: Direction;

  @option({ type: 'bool', defaultValue: false })
  navigate: boolean;

  @option({ type: 'element', defaultValue: 'select' })
  select: HTMLSelectElement;

  isExpanded: boolean = false;
  label: Label;
  list: List;
  selected: Option | null;


  constructor(options: DropdownOptions) {
    super(options = defaults(options, {
      labelClass: Label,
      labelOptions: {},
      listClass: List,
      listOptions: {},
    }));

    const { labelClass, labelOptions, listClass, listOptions } = options;
    const { select } = this;

    const label = this.label = new labelClass(defaults({}, labelOptions, {
      appendTo: this.element,
      owner: this,
    }));

    const list = this.list = new listClass(defaults({}, listOptions, {
      appendTo: this.element,
      owner: this,
    }));

    if (select) {
      list.sync(select);
      this.setSelected(list.options[select.selectedIndex] || null, true);
    } else {
      this.setSelected(null, true);
    }

    this.element.tabIndex = -1;

    this.listenTo(label, 'click', this.handleLabelClick);
    this.listenTo(list, 'click', this.handleListClick);
    this.delegate('focusout', this.handleFocusOut);
    this.delegate('keypress', this.handleKeyPress);
    this.delegate('change', this.handleChange, { selector: 'select' });
  }

  setExpanded(value: boolean) {
    if (this.isExpanded === value) return;
    this.isExpanded = value;
    this.list.setExpanded(value);

    const documentView = DocumentView.getInstance();
    if (value) {
      this.listenTo(documentView, 'pointerDown', this.handleDocumentPointerDown);
    } else {
      this.stopListening(documentView);
    }
  }

  setSelected(option: Option | null, silent?: boolean) {
    const { label, list, select } = this;
    this.selected = option;

    if (select) {
      const index = findIndex(list.options, option);
      select.selectedIndex = index;
    }

    label.setOption(option);
    this.setExpanded(false);
    forEach(list.options, child =>
      child.setSelected(child === option)
    );

    if (!silent) {
      this.trigger('select', option);
      if (this.navigate) {
        window.location.href = option.value;
      }
    }
  }

  handleChange() {
    const { list, select } = this;
    if (select) {
      this.setSelected(list.options[select.selectedIndex] || null);
    }
  }

  handleDocumentPointerDown(event: Event) {
    const { element } = this;
    let target = <HTMLElement>event.target;

    while (target) {
      if (target === element) return;
      target = <HTMLElement>target.parentNode;
    }

    this.setExpanded(false);
  }

  handleFocusOut(event: Event) {
    this.setExpanded(false);
  }

  handleKeyPress(event: KeyboardEvent) {

  }

  handleLabelClick() {
    this.setExpanded(true);
  }

  handleListClick(option: Option) {
    this.setSelected(option);
  }
}
