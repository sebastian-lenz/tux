import { forEach, isArray } from 'underscore';

import { trim } from '../../util/string';


/**
 * Type definition for all applicable SourceSet constructor arguments.
 */
export type ImageSourceSetSource = string | ImageSource[] | SourceSetMode;


/**
 * A regular expression used to parse string source sets.
 */
const sourceSetRegExp = /([^ ]+) (\d+)([WwXx])/;


/**
 * Defines all available source set modes.
 */
export enum SourceSetMode
{
  Width,
  Density
}


/**
 * Data structure used to store individual sources.
 */
export interface ImageSource
{
  /**
   * The image source identifier.
   */
  src: string;

  /**
   * The relevant width or pixel density bias.
   */
  bias?: number;

  /**
   * The desired bias mode.
   */
  mode?: SourceSetMode;
}


/**
 * Stores a set of image source definitions.
 */
export default class SourceSet
{
  /**
   * The list of source definitions within this set.
   */
  sources: ImageSource[] = [];

  /**
   * The mode used to determine the current source.
   */
  mode: SourceSetMode;



  /**
   * SourceSet constructor.
   *
   * @param data
   *   A source set string, an array of IImageSource objects or the desired source mode.
   */
  constructor(data?: ImageSourceSetSource) {
    if (isArray<ImageSource>(data)) {
      forEach(data, source => this.add(source));
    } else if (typeof data === 'number' && data in SourceSetMode) {
      this.mode = <SourceSetMode>data;
    } else if (typeof data === 'string') {
      this.parse(data);
    }
  }

  /**
   * Parse the given source set string.
   *
   * @param str
   *   A string containing a source set definition.
   */
  parse(str: string) {
    const sources = str.split(/,/);
    for (let source of sources) {
      source = trim(source);

      const match = sourceSetRegExp.exec(source);
      if (match) {
        const mode = (match[3] === 'W' || match[3] === 'w')
          ? SourceSetMode.Width
          : SourceSetMode.Density;

        this.add({
          src: match[1],
          bias: parseFloat(match[2]),
          mode,
        });
      } else {
        this.add({
          src: source,
        });
      }
    }
  }

  /**
   * Add a source definition to this set.
   *
   * @param source
   *   The source definition that should be added.
   */
  add(source: ImageSource) {
    if (source.mode != void 0) {
      if (!this.mode) {
        this.mode = source.mode;
      } else if (this.mode != source.mode) {
        console.warn('Mismatched image srcSet, all sources must use same mode.');
        return;
      }
    }

    if (source.bias === void 0) {
      source.bias = Number.MAX_VALUE;
    }

    this.sources.push(source);
    this.sources.sort((a, b) => a.bias - b.bias);
  }


  /**
   * Return the matching source for the given element.
   *
   * @param el
   *   The element whose dimensions should be used to determine the source.
   * @returns
   *   The best matching source identifier.
   */
  get(el: HTMLElement): string {
    const { mode, sources } = this;
    const count = sources.length;
    if (count === 0) {
      return '';
    }

    let threshold = window.devicePixelRatio
      ? Math.max(1, Math.min(1.75, window.devicePixelRatio))
      : 1;

    if (mode === SourceSetMode.Width) {
      threshold = el.offsetWidth * threshold;
    }

    for (const source of sources) {
      if (source.bias >= threshold) return source.src;
    }

    return sources[count - 1].src;
  }
}
