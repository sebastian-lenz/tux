import * as Q from 'q';
import { defaults } from 'underscore';

import SourceSet, { ImageSourceSetSource } from './SourceSet';
import View, { ViewOptions, option } from '../View';
import Visibility, { VisibilityTarget } from '../../services/Visibility';


/**
 * Defines all known load states of the Image class.
 */
export const enum LoadState
{
  Idle,
  Loading,
  Loaded
}


export enum RoundingMode {
  Round,
  Floor,
  Ceil,
}


/**
 * Constructor options for the Image class.
 */
export interface ImageOptions extends ViewOptions
{
  /**
   * Whether to disable the visibility check or not.
   */
  disableVisibilityCheck?:boolean;

  /**
   * The source set definition that should be used.
   */
  sourceSet?:ImageSourceSetSource;

  sizeRoundingMode?:RoundingMode|string;

  offsetRoundingMode?:RoundingMode|string;
}


export function round(value:number, mode?:RoundingMode):number {
  switch (mode) {
    case RoundingMode.Ceil: return Math.ceil(value);
    case RoundingMode.Floor: return Math.floor(value);
    default: return Math.round(value);
  }
}

/**
 * Displays an image.
 *
 * @event "load" ():void
 *   Triggered after the image has been loaded.
 */
export default class Image extends View implements VisibilityTarget
{
  element: HTMLImageElement;

  /**
   * The native width of this image.
   */
  @option({ type: 'int', attribute: 'width', defaultValue: -1 })
  nativeWidth: number;

  /**
   * The native height of this image.
   */
  @option({ type: 'int', attribute: 'height', defaultValue: -1 })
  nativeHeight: number;

  /**
   * The set of source files used by this image.
   */
  @option({ type: 'class', ctor: SourceSet, attribute: 'data-srcset' })
  sourceSet: SourceSet;

  @option({ type: 'enum', values: RoundingMode, defaultValue: RoundingMode.Ceil })
  sizeRoundingMode: RoundingMode;

  @option({ type: 'enum', values: RoundingMode, defaultValue: RoundingMode.Floor })
  offsetRoundingMode: RoundingMode;

  /**
   * The current load state of this image.
   */
  loadState: LoadState = LoadState.Idle;

  /**
   * Whether this element is currently within the visible viewport bounds or not.
   */
  isInViewport:boolean = false;

  /**
   * The deferred object used to generate the load promise.
   */
  private deferred:Q.Deferred<Image>;



  /**
   * Image constructor.
   *
   * @param options
   *   The constructor options.
   */
  constructor(options?: ImageOptions) {
    super(options = defaults(options || {}, {
      tagName: 'img'
    }));

    const { element } = this;
    if (element.hasAttribute('src') && !/^data:/.test(element.getAttribute('src'))) {
      if (element.complete && typeof element.naturalWidth !== 'undefined' && element.naturalWidth != 0) {
        this.onLoad();
      } else {
        this.prepareLoadEvent();
      }
    }

    if (!options.disableVisibilityCheck) {
      Visibility.getInstance().register(this);
    }
  }


  remove() {
    Visibility.getInstance().unregister(this);
    return super.remove();
  }


  /**
   * Set the size and optionally the position shift of this image.
   *
   * @param width
   *   The desired width of the image.
   * @param height
   *   The desired height of the image.
   * @param left
   *   The horizontal shift of the image.
   * @param top
   *   The vertical shift of the image.
   */
  setSize(width:number, height:number, left?:number, top?:number) {
    const { element, sizeRoundingMode, offsetRoundingMode } = this;
    const { style } = element;

    style.width  = round(width, sizeRoundingMode)  + 'px';
    style.height = round(height, sizeRoundingMode) + 'px';

    if (left != void 0) {
      style.left = round(left, offsetRoundingMode) + 'px';
    } else {
      style.left = '';
    }

    if (top != void 0) {
      style.top = round(top, offsetRoundingMode) + 'px';
    } else {
      style.top = '';
    }

    this.update();
  }


  /**
   * Set whether the element of this view is currently within the visible bounds
   * of the viewport or not.
   *
   * @param inViewport
   *   TRUE if the element is visible, FALSE otherwise.
   */
  setInViewport(inViewport:boolean) {
    if (this.isInViewport == inViewport) return;
    this.isInViewport = inViewport;

    this.update();
  }


  /**
   * Update the source location of the image due to the current dimensions.
   */
  update() {
    if (this.isInViewport && this.sourceSet) {
      this.prepareLoadEvent();
      this.element.src = this.sourceSet.get(this.element);
    }
  }


  /**
   * Load the image.
   *
   * @returns
   *   A promise that resolves after the image has loaded.
   */
  load():Q.Promise<Image> {
    if (!this.deferred) this.deferred = Q.defer<Image>();
    const deferred = this.deferred;

    if (this.loadState == LoadState.Loaded) {
      deferred.resolve();
    } else {
      this.isInViewport = true;
      this.update();
    }

    return deferred.promise;
  }


  /**
   * Setup the load event listener when loading the image for the first time.
   */
  protected prepareLoadEvent() {
    if (this.loadState != LoadState.Idle) return;
    this.loadState = LoadState.Loading;

    this.delegate('load', this.onLoad);
  }


  /**
   * Triggered after the image has been loadedf or the first time.
   */
  onLoad() {
    if (this.loadState == LoadState.Loaded) return;
    this.loadState = LoadState.Loaded;
    const { element } = this;

    if (this.nativeWidth <= 0) {
      this.nativeWidth = element.naturalWidth;
    }

    if (this.nativeHeight <= 0) {
      this.nativeHeight = element.naturalHeight;
    }

    element.classList.add('loaded');

    this.undelegate('load', null, this.onLoad);
    this.trigger('load');

    if (this.deferred) {
      this.deferred.resolve(this);
    }
  }
}
