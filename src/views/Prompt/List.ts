import { defaults } from 'underscore';

import { option } from '../View';
import CycleableView, { CycleableViewOptions } from '../CycleableView';
import Item, { PromptItemOptions } from './Item';
import { TemplateFunction } from './index';


/**
 * Constructor options for the PromptList class.
 */
export interface PromptListOptions extends CycleableViewOptions<Item>
{
  /**
   * The template that is used to render the contents of each row.
   */
  template?:TemplateFunction;
}


/**
 * Displays a list of prompting suggestions.
 *
 * @event "click" (item:PromptItem):void
 *   Triggered if the user clicks onto one of the items.
 */
export default class PromptList extends CycleableView<Item>
{
  /**
   * The template that is used to render the contents of each row.
   */
  @option({ type: 'any' })
  itemTemplate: TemplateFunction;



  /**
   * PromptList constructor.
   *
   * @param options
   *   PromptList constructor options.
   */
  constructor(options?: PromptListOptions) {
    super(defaults(options || {}, {
      tagName:          'ol',
      className:        'tuxPromptList',
      childSelector:    'li',
      childClass:       Item,
      resetOnAnyChange: true
    }));
  }


  /**
   * Set the template that is used to render the contents of each row.
   *
   * @param itemTemplate
   *   The template function that should be used to render row contents.
   */
  setItemTemplate(itemTemplate:TemplateFunction):this {
    if (this.itemTemplate == itemTemplate) return this;
    this.itemTemplate = itemTemplate;

    for (const child of this.children) {
      child.itemTemplate = itemTemplate;
      child.render();
    }

    return this;
  }


  /**
   * Allow the view class to alter the constructor options before creating
   * new child view instances.
   *
   * @param options
   *   The predefined child constructor options.
   * @returns
   *   The altered child constructor options.
   */
  protected alterChildOptions(options: PromptItemOptions): PromptItemOptions {
    options.template = this.itemTemplate;
    return options;
  }


  /**
   * Internal transition handler. Creates a transition between the two given children.
   *
   * @param newChild
   *   The child the transition should lead to.
   * @param oldChild
   *   The child the transition should come from.
   * @param options
   *   Optional transition options.
   */
  protected handleTransition(newChild:Item, oldChild:Item, options?:any) {
    if (newChild) {
      newChild.element.classList.add('selected');
    }

    if (oldChild) {
      oldChild.element.classList.remove('selected');
    }
  }
}
