import { defaults } from 'underscore';

import { option } from '../View';
import { trim } from '../../util/string';
import Collection from '../../models/Collection';
import Input, { InputOptions } from '../Input';
import Item from './Item';
import List from './List';


export type TemplateFunction = {
  (model: any): string
};


/**
 * Constructor options for the Prompt class.
 */
export interface IPromptOptions extends InputOptions
{
  url?: string | { (): string };

  urlParam: string;

  urlDefaultParams?: any;

  suggestions?: Collection;

  listClass?: typeof List;

  itemClass?: typeof Item;

  itemTemplate?: TemplateFunction;
}


/**
 * An input element with prompting support.
 *
 * @event "select" (model:Backbone.Model):void
 *   Triggered if the user selects an entry of the prompt suggestions
 *   by clicking on it or by pressing enter while it is selected.
 */
export default class Prompt extends Input
{
  /**
   * Displays the list of prompting suggestions.
   */
  list:List;

  @option({ type: 'string', defaultValue: 'q' })
  urlParam:string;

  @option({ type: 'any' })
  urlDefaultParams:any;

  suggestions: Collection;



  /**
   * Prompt constructor.
   *
   * @param options
   *   Prompt constructor options.
   */
  constructor(options?:IPromptOptions) {
    super(defaults(options || {}, {
      className: 'tuxPrompt',
      listClass: List,
      itemClass: Item,
      attributes: { autocomplete:'off' }
    }));

    let suggestions = options.suggestions;
    if (suggestions === void 0) {
      suggestions = new Collection();
      if (options.url) {
        suggestions.url = options.url;
      }
    }

    this.element.classList.add('noSuggestions');

    this.suggestions = suggestions;
    this.list = new options.listClass({
      appendTo:   this.element,
      collection: suggestions,
      childClass: options.itemClass
    });

    this.delegate('keydown', this.onKeyDown, { selector: 'input' });

    this.listenTo(this.list, 'click', this.onListClick);
    this.listenTo(this.list, 'add', this.onListChange);
    this.listenTo(this.list, 'remove', this.onListChange);
    this.listenTo(this.list, 'reset', this.onListChange);
    this.listenTo(this, 'change', this.onInputChanged);
  }


  setUrl(url?: string | { (): string }, paramName?:string, defaultParams?:any) {
    this.suggestions.url = url;
    this.urlDefaultParams = defaultParams;
    if (paramName) {
      this.urlParam = paramName;
    }

    this.list.removeAllChildren();
  }


  setItemTemplate(template:TemplateFunction):this {
    this.list.setItemTemplate(template);
    return this;
  }


  fetch() {
    const value = trim(this.value);
    if (value == '') {
      this.suggestions.reset();
    } else {
      const params:any = {};
      if (this.urlDefaultParams) {
        defaults(params, this.urlDefaultParams);
      }

      params[this.urlParam] = value;
      this.suggestions.fetch({ data:params });
    }
  }


  onKeyDown = (event:KeyboardEvent) => {
    let target;
    switch (event.keyCode) {
      case 13:
        if (this.list.current) {
          event.preventDefault();
          this.trigger('select', this.list.current.model);
        }
        break;

      case 38:
        event.preventDefault();
        target = this.list.getPreviousChild();
          if (target) {
            this.list.setCurrent(target);
          }
        break;

      case 40:
        event.preventDefault();
        target = this.list.getNextChild();
        if (target) {
          this.list.setCurrent(target);
        }
        break;
    }
  };


  /**
   * Triggered if the user clicks onto a item within the list.
   *
   * @param item
   *   The prommpt item the user has clicked onto.
   */
  onListClick(item:Item) {
    this.trigger('select', item.model);
  }


  onListChange() {
    this.element.classList.toggle('noSuggestions', this.list.getLength() == 0);
  }


  /**
   * Triggered after the value of the input has changed.
   */
  onInputChanged() {
    this.fetch();
  }
}
