import { defaults } from 'underscore';

import View, { ViewOptions, option } from '../View';
import { TemplateFunction } from './index';
import List from './List';


/**
 * Constructor options for the Prompt class.
 */
export interface PromptItemOptions extends ViewOptions
{
  itemTemplate?: TemplateFunction;
}

/**
 * Default template function.
 */
const defaultTemplateFunction: TemplateFunction = function(data: any) {
  return data.toString();
};

/**
 * Displays a single prompting suggestion.
 */
export default class PromptItem extends View
{
  /**
   * The list this item belnogs to.
   */
  list: List;

  /**
   * The template that is used to render the contents of this row.
   */
  @option({ type: 'any', defaultValue: defaultTemplateFunction })
  itemTemplate: TemplateFunction;


  /**
   * Prompt constructor.
   *
   * @param options
   *   Prompt constructor options.
   */
  constructor(options?: PromptItemOptions) {
    super(defaults(options || {}, {
      tagName:   'li',
      className: 'tuxPromptItem'
    }));

    this.list = <List>options.owner;
    this.render();

    this.delegate('mouseenter', null, this.onMouseEnter);
    this.delegateClick(this.onClick);
  }

  /**
   * Render this view.
   */
  render(): this {
    this.element.innerHTML = this.itemTemplate(this.model ? this.model.attributes : {});
    return this;
  }

  /**
   * Triggered if the mouse enters this view.
   */
  onMouseEnter() {
    if (this.list) {
      this.list.setCurrent(this);
    }
  }

  /**
   * Triggered after the user has clicked onto this item.
   */
  onClick() {
    this.list.trigger('click', this);
    return true;
  }
}
