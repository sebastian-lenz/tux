import View, { option } from '../View';
import ChildableView, { ChildableViewOptions } from '../ChildableView';


/**
 * A generic CycleableView type definition.
 */
export type AnyCycleableView = CycleableView<View, any>;


/**
 * Constructor options for the CycleableView class.
 */
export interface CycleableViewOptions<T extends View = View> extends ChildableViewOptions<T>
{
  /**
   * The child that should be initially selected.
   */
  current?: T;

  /**
   * The index of the child that should be initially selected.
   */
  index?: number;

  /**
   * Whether the first child should be automatically selected if not selected child
   * is present or not.
   */
  autoSelectFirstChild?: boolean;

  /**
   * Whether the currently selected item should be reset on any change or not.
   */
  resetOnAnyChange?: boolean;

  /**
   * Whether the children should be looped or not.
   */
  isLooped?: boolean;
}


/**
 * A childable view which is aware of a current child.
 *
 * @event "currentChanged" (newChild:T, oldChild:T, options:U):void
 *   Triggered after the current child has changed.
 */
export default class CycleableView<T extends View = View, U = any> extends ChildableView<T>
{
  /**
   * The currently selected child.
   */
  current:T = null;

  /**
   * Whether the first child should be automatically selected if not selected child
   * is present or not.
   */
  @option({ type: 'bool', defaultValue: false })
  autoSelectFirstChild: boolean;

  /**
   * Whether the children should be looped or not.
   */
  @option({ type: 'bool', defaultValue: false })
  isLooped:boolean;

  /**
   * Whether the currently selected item should be reset on any change or not.
   */
  @option({ type: 'bool', defaultValue: false })
  resetOnAnyChange:boolean;

  /**
   * Whether the current child is locked or not.
   */
  isLocked:boolean = false;



  /**
   * CycleableView constructor.
   *
   * @param options
   *   The constructor options.
   */
  constructor(options?: CycleableViewOptions<T>) {
    super(options || (options = {}));

    if (options.index !== void 0) {
      this.setCurrentIndex(options.index);
    } else if (options.current) {
      this.setCurrent(options.current);
    } else if (this.autoSelectFirstChild && this.getLength()) {
      this.setCurrentIndex(0);
    }

    this.listenTo(this, 'add',    this.onChildAdd);
    this.listenTo(this, 'remove', this.onChildRemove);
    this.listenTo(this, 'reset',  this.onChildrenReset);
  }


  /**
   * Normalize the given index.
   *
   * @param index
   *   The index value that should be normalized.
   * @returns
   *   The normalized index value.
   */
  normalize(index:number):number {
    const count = this.getLength();
    if (count < 1) {
      return -1;
    }

    let normalized = index;
    if (this.isLooped) {
      while (normalized < 0)      normalized += count;
      while (normalized >= count) normalized -= count;
    } else {
      if (normalized < 0)      return -1;
      if (normalized >= count) return -1;
    }

    return normalized;
  }


  /**
   * Internal transition handler. Creates a transition between the two given children.
   *
   * @param newChild
   *   The child the transition should lead to.
   * @param oldChild
   *   The child the transition should come from.
   * @param options
   *   Optional transition options.
   */
  protected handleTransition(newChild:T, oldChild:T, options?:U) { }


  /**
   * Set the current child.
   *
   * @param child
   *   The child that should be set as current.
   * @param options
   *   Optional transition options.
   */
  setCurrent(child: T, options?: U):this {
    if (this.isLocked || this.current === child) {
      return this;
    }

    var oldChild = this.current;
    this.current = child;

    this.handleTransition(child, oldChild, options);
    this.trigger('currentChanged', child, oldChild, options);
    return this;
  }


  /**
   * Set the index of the current child.
   *
   * @param index
   *   The index of the child that should be set as current.
   * @param options
   */
  setCurrentIndex(index:number, options?:U):this {
    index = this.normalize(index);
    return this.setCurrent(this.children[index], options);
  }


  /**
   * Return the index of the current child.
   *
   * @returns
   *   The index of the current child.
   */
  getCurrentIndex():number {
    return this.indexOf(this.current);
  }


  /**
   * Return the previous child.
   *
   * @returns
   *   The previous child.
   */
  getNextChild():T {
    return this.getChild(this.normalize(this.getCurrentIndex() + 1));
  }


  /**
   * Return the next child.
   *
   * @returns
   *   The next child.
   */
  getPreviousChild():T {
    return this.getChild(this.normalize(this.getCurrentIndex() - 1));
  }


  /**
   * Reset the currently selected child.
   */
  protected resetCurrent() {
    if (!this.current) return;

    let isRemove:boolean = false;
    if (this.getCurrentIndex() == -1) {
      this.current = null;
      isRemove = true;
    }

    if (this.autoSelectFirstChild && this.getLength()) {
      this.setCurrent(this.children[0]);
    } else {
      if (isRemove) {
        this.trigger('currentChanged', null, null);
      } else {
        this.setCurrent(null);
      }
    }
  }


  /**
   * Triggered after a child has been added to this view.
   */
  onChildAdd(child:T, at:number) {
    if (this.resetOnAnyChange) {
      this.resetCurrent();
    }
  }


  /**
   * Triggered after a child has been removed to this view.
   *
   * @param child
   *   The child view that has been removed.
   */
  onChildRemove(child:T) {
    if (this.resetOnAnyChange || (child == this.current)) {
      this.resetCurrent();
    }
  }


  /**
   * Triggered after the children have been reset.
   */
  onChildrenReset() {
    if (this.resetOnAnyChange || (this.current && this.getCurrentIndex() == -1)) {
      this.resetCurrent();
    }
  }
}
