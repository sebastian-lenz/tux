import { throttle } from 'underscore';

import DocumentView from '../../services/DocumentView';
import View, { ViewOptions, option } from '../View';


/**
 * Constructor options for the Input class.
 */
export interface InputOptions extends ViewOptions
{
  /**
   * A css selector pointing to the input element or reference to
   * the input element that should be used.
   */
  input?:string|HTMLInputElement|HTMLTextAreaElement;

  useNativeBlur?:boolean;
}


/**
 * A view that wraps an input element.
 *
 * @event "change" (value:string):void
 *   Triggered every time the value of the inout changes.
 * @event "focus" (hasFocus:boolean):void
 *   Triggered after the input got or lost focus.
 */
export default class Input extends View
{
  /**
   * The actual dom input element.
   */
  @option({
    type: 'element',
    defaultValue: 'input,textarea',
    tagName: 'input',
    className: 'tuxInput__input',
  })
  input: HTMLInputElement | HTMLTextAreaElement;

  /**
   * A view that wraps the input element for event delegation.
   */
  inputView:View;

  /**
   * The current value of the input element.
   */
  value:string;

  /**
   * Whether the input element is currently focused or not.
   */
  hasFocus:boolean = false;


  /**
   * Input constructor.
   *
   * @param options
   *   Input constructor options.
   */
  constructor(options?: InputOptions) {
    super(options);
    const { input } = this;

    this.value = input.value;
    this.inputView = new View({ element: input });

    const inputView = this.inputView;
    inputView.delegate('input', this.onInput);
    inputView.delegate('change', this.onInput);
    inputView.delegate('keyup', this.onInput);
    inputView.delegate('paste', this.onInput);
    inputView.delegate('focus', this.onFocusIn);

    if (options.useNativeBlur) {
      inputView.delegate('blur', this.onFocusOut);
    } else {
      this.listenTo(DocumentView.getInstance(), 'pointerDown', this.onDocumentPointerDown)
    }
  }

  /**
   * Set the focus on the input element.
   */
  focus(): this {
    this.input.focus();
    return this;
  }

  /**
   * Remove the focus from the input element.
   */
  blur(): this {
    this.input.blur();
    return this;
  }

  /**
   * Set the new value of the input field.
   *
   * @param value
   *   The new value of the input field.
   */
  setValue(value: string): this {
    if (this.value == value) return this;
    this.value = value;

    this.input.value = value;
    this.element.classList.toggle('hasValue', value !== '');
    this.trigger('change', value);

    return this;
  }

  /**
   * Triggered after the content of the input field might have changed.
   */
  onInput = throttle(() => {
    const { value } = this.input;
    if (this.value != value) {
      this.value = value;
      this.element.classList.toggle('hasValue', value !== '');
      this.trigger('change', value);
    }
  }, 50, { leading: false });

  /**
   * Triggered after the input element has gained focus.
   *
   * @param event
   */
  onFocusIn = (event: FocusEvent): void => {
    if (this.hasFocus) return;
    this.hasFocus = true;

    this.element.classList.add('focused');
    this.trigger('focus', true);
  };

  /**
   * Triggered after the input element has lost focus.
   */
  onFocusOut = (): void => {
    if (!this.hasFocus) return;
    this.hasFocus = false;

    this.element.classList.remove('focused');
    this.trigger('focus', false);
  };

  onDocumentPointerDown(event:Event) {
    if (!this.hasFocus) return;

    let target = <HTMLElement>event.target;
    while (target) {
      if (target == this.element) return;
      target = <HTMLElement>target.parentNode;
    }

    this.onFocusOut();
  }
}
