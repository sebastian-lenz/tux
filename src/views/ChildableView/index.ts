import { defaults, forEach, indexOf } from 'underscore';

import Collection from '../../models/Collection';
import Model from '../../models/Model';
import View, { ViewOptions, option } from '../View';


/**
 * Constructor options for the ChildableView class.
 */
export interface ChildableViewOptions<T extends View = View> extends ViewOptions
{
  /**
   * A css selector or an element used as the container for all children.
   */
  container?: string | HTMLElement;

  /**
   * A css selector used to discover existing children.
   */
  childSelector?: string;

  /**
   * Default view class used to construct children.
   */
  childClass?: { new(options?: any): T };

  childOptions?: ViewOptions;

  /**
   * A list of view instances that should be attached as children.
   */
  children?: T[];

  /**
   * Whether elements discovered by childSelector must be child elements
   * of the container or not.
   */
  allowGrandChildren?: boolean;

  collection?: Collection;
}


/**
 * A view that contains a list of child views.
 *
 * @event "add" (child:T, at:number):void
 *   Triggered after a child has been added to this view.
 * @event "remove" (child:T, at:number):void
 *   Triggered after a child has been removed to this view.
 * @event "reset" ():void
 *   Triggered after the children have been reset.
 */
export default class ChildableView<T extends View = View> extends View
{
  /**
   * The container children will be placed in.
   */
  @option({ type: 'element' })
  container:HTMLElement;

  /**
   * The list of children attached to this view.
   */
  children:T[];

  /**
   * Default view class used to construct children.
   */
  @option({ type: 'any', defaultValue: View })
  childClass: { new(options: ViewOptions): T };

  @option({ type: 'any' })
  childOptions?: ViewOptions;

  collection: Collection;



  /**
   * ChildableView constructor.
   *
   * @param options
   *   The constructor options.
   */
  constructor(options?: ChildableViewOptions<T>) {
    super(options || (options = {}));

    const container = this.container || (this.container = this.createContainer());
    const children  = this.children  = [];

    if (options.collection) {
      this.listenToCollection(options.collection);
      this.resetToCollection();
    }
    else if (options.children) {
      for (const child of options.children) {
        children.push(child);
        container.appendChild(child.element);
      }
    }
    else if (options.childSelector) {
      const elements = this.element.querySelectorAll(options.childSelector);
      forEach(elements, element => {
        if (options.allowGrandChildren || element.parentNode == container) {
          children.push(this.createChildFromElement(<HTMLElement>element));
        }
      });
    }
  }


  /**
   * Remove this view.
   */
  remove():this {
    for (var child of this.children) {
      child.remove();
    }

    return <this>super.remove();
  }


  /**
   * Set the collection should be bound to.
   *
   * @param collection
   *   The new collection this view should be bound to. Pass NULL to
   *   unbind this view from previously set collections.
   */
  setCollection(collection: Collection) {
    if (this.collection === collection) return;
    if (this.collection) {
      this.stopListeningToCollection(this.collection);
    }

    this.collection = collection;
    if (collection) {
      this.listenToCollection(collection);
      this.resetToCollection();
    } else {
      this._removeAllChildren();
    }
  }


  /**
   * Return the number of children attached to this view.
   *
   * @returns
   *   The number of children attached to this view.
   */
  getLength():number {
    return this.children.length;
  }


  /**
   * Return the child view at the given index.
   *
   * @param index
   *   The index of the child that should be returned.
   * @returns
   *   The child with the given index.
   */
  getChild(index:number):T {
    return this.children[index];
  }


  /**
   * Return the index of the given child.
   *
   * @param child
   *   The child whose index should determined.
   * @returns
   *   The index of the given child or -1 if the given child is not present.
   */
  indexOf(child:T):number {
    return indexOf(this.children, child);
  }


  /**
   * Add the given child view to this view.
   *
   * If this view is bound to a collection the call will be forwarded to the collection.
   *
   * @param child
   *   The child view that should be added to this view.
   * @param at
   *   Optional index the child should be inserted at.
   */
  addChild(child:T, at?:number):this {
    if (this.collection) {
      if (child.model) {
        const options:any = { views:{ [child.model.cid]:child } };
        if (at !== void 0) options.index = at;
        this.collection.add(child.model, options);
      } else {
        console.log('Warn: Trying to append a child without a model to a list bound to a collection.');
      }
    } else {
      this._addChild(child, at);
    }

    return this;
  }


  /**
   * Internal implementation of the addChild method.
   *
   * @param child
   *   The child view that should be added to this view.
   * @param at
   *   Optional index the child should be inserted at.
   */
  protected _addChild(child:T, at?:number) {
    const container = this.container;
    const children  = this.children;
    const count     = children.length;

    if (at === void 0 || at >= count) {
      at = count;
      container.appendChild(child.element);
      children.push(child);
    } else if (at <= 0) {
      at = 0;
      container.insertBefore(child.element, container.firstChild);
      children.unshift(child);
    } else {
      container.insertBefore(child.element, children[at].element);
      children.splice(at, 0, child);
    }

    this.trigger('add', child, at);
  }


  /**
   * Add the given list of child views to this view.
   *
   * If this view is bound to a collection the call will be forwarded to the collection.
   *
   * @param children
   *   The list of child views that should be added to this view.
   * @param at
   *   Optional index the child should be inserted at.
   */
  addChildren(children:T[], at?:number):this {
    if (this.collection) {
      const views: any = {};
      const models: Model[] = [];

      for (const child of children) {
        if (child.model) {
          views[child.model.cid] = child;
          models.push(child.model);
        } else {
          console.log('Warn: Trying to append a child without a model to a list bound to a collection.');
        }
      }

      if (models.length) {
        const options:any = { views };
        if (at !== void 0) options.index = at;
        this.collection.add(models, options);
      }
    } else {
      for (let index = 0, count = children.length; index < count; index++) {
        this._addChild(children[index], at ? at + index : at);
      }
    }

    return this;
  }


  /**
   * Remove the given child view from this view.
   *
   * If this view is bound to a collection the call will be forwarded to the collection.
   *
   * @param child
   *   The child view that should be removed.
   */
  removeChild(child:T):this {
    if (this.collection) {
      if (child.model) {
        this.collection.remove(child.model);
      } else {
        console.log('Warn: Trying to remove a child without a model from a list bound to a collection.');
      }
    } else {
      this._removeChild(child);
    }

    return this;
  }


  /**
   * Internal implementation of the removeChild method.
   *
   * @param child
   *   The child view that should be removed.
   */
  _removeChild(child:T) {
    const index = indexOf(this.children, child);
    if (index !== -1) {
      child.remove();
      this.children.splice(index, 1);
      this.trigger('remove', child, index);
    }
  }


  /**
   * Remove the given list of child views from this view.
   *
   * If this view is bound to a collection the call will be forwarded to the collection.
   *
   * @param children
   *   The list of child views that should be removed.
   */
  removeChildren(children:T[]):this {
    if (this.collection) {
      const models: Model[] = [];
      for (const child of children) {
        if (child.model) {
          models.push(child.model);
        } else {
          console.log('Warn: Trying to remove a child without a model from a list bound to a collection.');
        }
      }

      if (models.length) {
        this.collection.remove(models);
      }
    } else {
      for (const child of children) {
        this._removeChild(child);
      }
    }

    return this;
  }


  /**
   * Remove all children from this view.
   *
   * If this view is bound to a collection the call will be forwarded to the collection.
   */
  removeAllChildren() {
    if (this.collection) {
      this.collection.reset();
    } else {
      this._removeAllChildren();
    }
  }


  /**
   * Internal implementation of the removeAllChildren method.
   */
  protected _removeAllChildren(silent?:boolean) {
    for (const child of this.children) {
      child.remove();
    }

    this.children.length = 0;

    if (!silent) {
      this.trigger('reset');
    }
  }


  /**
   * Create the container children will be placed in.
   *
   * @returns
   *   The generated container.
   */
  protected createContainer():HTMLElement {
    return this.element;
  }


  /**
   * Allow the view class to alter the constructor options before creating
   * new child view instances.
   *
   * @param options
   *   The predefined child constructor options.
   * @returns
   *   The altered child constructor options.
   */
  protected alterChildOptions(options: any): any {
    const { childOptions } = this;
    if (childOptions) {
      return defaults(options, childOptions);
    }

    return options;
  }


  /**
   * Create a new view instance for the given child element.
   *
   * @param element
   *   The dom element whose view instance should be created.
   * @returns
   *   A view instance for the given child element.
   */
  protected createChildFromElement(element: HTMLElement): T {
    const options = this.alterChildOptions({
      owner: this,
      element,
    });

    return new this.childClass(options);
  }


  /**
   * Create a new view instance for the given model.
   *
   * @param model
   *   The model view instance should be created.
   * @returns
   *   A view instance for the given model.
   */
  protected createChildFromModel(model: Model):T {
    const options = this.alterChildOptions({
      owner: this,
      model,
    });

    return new this.childClass(options);
  }


  /**
   * Start listening for changed in the given collection.
   *
   * @param collection
   *   The collection events should be bound to.
   */
  protected listenToCollection(collection: Collection) {
    this.listenTo(collection, 'add',    this.onCollectionAdd);
    this.listenTo(collection, 'remove', this.onCollectionRemove);
    this.listenTo(collection, 'reset',  this.onCollectionReset);
    this.listenTo(collection, 'sort',   this.onCollectionSort);
  }


  /**
   * Stop listening to the given collection.
   *
   * @param collection
   *   The collection whose listening operations should be stopped.
   */
  protected stopListeningToCollection(collection: Collection) {
    this.stopListening(collection, 'add',    this.onCollectionAdd);
    this.stopListening(collection, 'remove', this.onCollectionRemove);
    this.stopListening(collection, 'reset',  this.onCollectionReset);
    this.stopListening(collection, 'sort',   this.onCollectionSort);
  }


  /**
   * Remove all child views and recreate all children from the bound collection.
   */
  protected resetToCollection() {
    this._removeAllChildren(true);

    for (const model of this.collection.models) {
      const child = this.createChildFromModel(model);
      this.children.push(child);
      this.container.appendChild(child.element);
    }

    this.trigger('reset');
  }


  /**
   * Synchronize the children of this view with the bound collection.
   */
  protected syncWithCollection() {
    const container = this.container;
    const existing  = this.children;
    const children  = this.children = [];

    for (const model of this.collection.models) {
      let child = null;
      for (let index = 0, count = existing.length; index < count; index++) {
        if (existing[index].model == model) {
          child = existing[index];
          existing.splice(index, 1);
          break;
        }
      }

      if (!child) {
        child = this.createChildFromModel(model);
      }

      children.push(child);
      container.appendChild(child.el);
    }

    for (const child of existing) {
      child.remove();
    }

    this.trigger('reset');
  }


  /**
   * Triggered after a model has been added to the collection.
   *
   * @param model
   *   The model that has been added.
   * @param collection
   *   The affected collection.
   * @param options
   *  Additional insert data provided by Backbone.
   */
  protected onCollectionAdd(model: Model, collection: Collection, options:{ index?:number, views?:{[cid:number]:T} }) {
    let view:T;
    if (options.views && options.views[model.cid]) {
      view = options.views[model.cid];
    } else {
      view = this.createChildFromModel(model);
    }

    this._addChild(view, options.index);
  }


  /**
   * Triggered after a model has been removed from the collection.
   *
   * @param model
   *   The model that has been removed.
   * @param collection
   *   The affected collection.
   * @param options
   *  Additional removal data provided by Backbone.
   */
  protected onCollectionRemove(model: Model, collection: Collection, options:{ index:number }) {
    const child = this.getChild(options.index);
    if (child.model === model) {
      this._removeChild(child);
    } else {
      console.log('Warn: View is out of sync with bound collection.');
      this.syncWithCollection();
    }
  }

  /**
   * Triggered if the bound collection was reset.
   */
  protected onCollectionReset() {
    this.resetToCollection();
  }

  /**
   * Triggered if the bound collection was sorted.
   */
  protected onCollectionSort() {
    this.syncWithCollection();
  }

  /**
   * Triggers the resize handler on this component and all of its children.
   */
  handleViewportResize(resizeChildren?: Function): boolean {
    forEach(this.children, child => child.handleViewportResize());
    return false;
  }
}
