import * as Q from 'q';
import { defaults } from 'underscore';

import animation from '../../util/vendor/animation';
import { option } from '../View';

import CycleableView, { CycleableViewOptions } from '../CycleableView';
import DefaultItem, { Item, ItemState } from './Item';
import Visibility, { VisibilityTarget } from '../../services/Visibility';


/**
 * Transition options for the Slideshow class.
 */
export interface TransitionOptions
{
  /**
   * Whether the transition should be omitted or not.
   */
  noTransition?: boolean;

  transitionClass?: string;

  direction?: string;
}

/**
 * Constructor options for the Slideshow class.
 */
export interface SlideshowOptions<T extends Item = Item> extends CycleableViewOptions<T>
{
  /**
   * Whether sibblings should be preloaded or not.
   */
  allowPreload?: boolean;

  /**
   * The name of the css class that should be used to generate the transition.
   */
  transitionClass?:string;

  /**
   * Whether the slideshow should use the Visibility service to determine
   * if it is currently visible or not.
   */
  disableVisibilityCheck?:boolean;
}

/**
 * A basic slideshow implementation.
 */
export default class Slideshow<T extends Item = Item>
    extends CycleableView<T, TransitionOptions>
    implements VisibilityTarget
{
  /**
   * Whether sibblings should be preloaded or not.
   */
  @option({ type: 'bool' })
  allowPreload: boolean;

  /**
   * Deferred object of the current transition.
   */
  transition: Q.Promise<any>;

  /**
   * The slide that should be transitioned to after the current
   * transition has finished.
   */
  successor: T;
  successorOptions: TransitionOptions;

  /**
   * Whether this element is currently within the visible viewport bounds or not.
   */
  inViewport: boolean = false;



  /**
   * Slideshow constructor.
   *
   * @param options
   *   The constructor options.
   */
  constructor(options?: SlideshowOptions<T>) {
    super(options = defaults(options || {}, {
      className:            'tuxSlideshow',
      container:            '.tuxSlideshow__slides',
      childSelector:        '.tuxSlideshow__slides > li',
      childClass:           DefaultItem,
      isLooped:             true,
      autoSelectFirstChild: true,
      allowPreload:         true,
    }));

    if (!options.disableVisibilityCheck) {
      Visibility.getInstance().register(this);
    } else {
      this.setInViewport(true);
    }
  }


  /**
   * Set whether the element of this view is currently within the visible bounds
   * of the viewport or not.
   *
   * @param inViewport
   *   TRUE if the element is visible, FALSE otherwise.
   */
  setInViewport(inViewport:boolean) {
    if (this.inViewport == inViewport) return;
    this.inViewport = inViewport;

    this.preloadSiblings();
  }


  /**
   * Return a string indicating the transition direction between the two
   * given slides.
   *
   * @param from
   *   The child the transition should start with.
   * @param to
   *   The child the transition should end with.
   * @returns
   *   A string indicating the transition direction ('forward' or 'backward').
   */
  getDirection(from: T, to: T): string {
    const index = this.indexOf(to) - this.indexOf(from);
    return index >= 0 ? 'forward' : 'backward';
  }


  /**
   * Return whether the slideshow is currently within a transition or not.
   *
   * @returns
   *   TRUE if the slideshow is currently within a transition, FALSE otherwise.
   */
  isInTransition() {
    return !!this.transition;
  }


  /**
   * Preload the content of the previous and next slide.
   */
  preloadSiblings() {
    if (!this.inViewport) return;

    if (this.current) {
      const current  = this.current;
      const next     = this.getNextChild();
      const previous = this.getPreviousChild();

      current.load().then(() => {
        if (this.current != current || !this.allowPreload) return;
        return next.load();
      }).then(() => {
        if (this.current != current || !this.allowPreload) return;
        return previous.load();
      });
    }
  }


  /**
   * Create the container children will be placed in.
   *
   * @returns
   *   The generated container.
   */
  protected createContainer():HTMLElement {
    const container = document.createElement('ul');
    container.className = 'tuxSlideshow__slides';

    this.element.appendChild(container);
    return container;
  }


  /**
   * Internal transition handler. Creates a transition between the two given children.
   *
   * @param newChild
   *   The child the transition should lead to.
   * @param oldChild
   *   The child the transition should come from.
   * @param options
   *   Optional transition options.
   */
  protected handleTransition(newChild: T, oldChild: T, options: TransitionOptions = {}) {
    if (!this.transition && (
      !oldChild ||
      !newChild ||
      !this.inViewport ||
      options.noTransition)
    ) {
      if (oldChild) oldChild.setState(ItemState.Inactive);
      if (newChild) newChild.setState(ItemState.Active);

      this.preloadSiblings();
      return;
    }

    if (this.transition) {
      newChild.load();
      this.successor = newChild;
      this.successorOptions = options;
    } else {
      this.transition = this.createSequence(oldChild, newChild, options);
    }
  }


  /**
   * Create the transition sequence between the two given slides.
   *
   * @param from
   *   The child the transition should start with.
   * @param to
   *   The child the transition should end with.
   * @returns
   *   A promise that resolves after the transition has finished.
   */
  protected createSequence(from: T, to: T, options: TransitionOptions):Q.Promise<any> {
    return to.load().then(() => {
      return this.createTransition(from, to, options);
    }).then(() => {
      const { successor, successorOptions } = this;
      if (successor) {
        this.successor = null;
        this.successorOptions = null;
        return this.createSequence(to, successor, successorOptions);
      } else {
        this.transition = null;
        this.preloadSiblings();
      }
    });
  }


  /**
   * Create a graphical transition between the two given slides.
   *
   * @param from
   *   The child the transition should start with.
   * @param to
   *   The child the transition should end with.
   * @param transitionClass
   *   The name of the css class that should be used to generate the transition.
   * @returns
   *   A promise that resolves after the transition has finished.
   */
  protected createTransition(from: T, to: T, options: TransitionOptions): Q.Promise<any> {
    const deferred = Q.defer();
    const {
      direction = this.getDirection(from, to),
      transitionClass = 'transition',
    } = options;

    if (!animation.endEvent) {
      from.setState(ItemState.Inactive);
      to.setState(ItemState.Active);

      deferred.resolve();
      return deferred.promise;
    }

    const complete = () => {
      this.element.classList.remove(transitionClass);
      this.element.classList.remove(direction);

      to.undelegate(animation.endEvent, null, complete);
      to.setState(ItemState.Active);
      from.setState(ItemState.Inactive);

      deferred.resolve();
    };

    this.element.classList.add(transitionClass);
    this.element.classList.add(direction);

    to.delegate(animation.endEvent, complete);
    to.setState(ItemState.BecomeActive);
    from.setState(ItemState.BecomeInactive);

    return deferred.promise;
  }
}
