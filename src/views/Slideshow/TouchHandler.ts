import transform from '../../util/vendor/transform';
import { AnyTween, createTween } from '../../fx/tween/Tween';
import { easeOutExpo } from '../../fx/easing/expo';
import { Item, ItemState } from './Item';
import DragHandler, { DragDirection } from '../../handler/DragHandler';
import Pointer, { PointerType } from '../../handler/Pointer';
import Slideshow from './index';


/**
 * Constructor options for the TouchHandler class.
 */
export interface TouchHandlerOptions
{
  /**
   * The view instance that should be controlled by the handler.
   */
  view: Slideshow;

  /**
   * A css selector used to retrieve the container whose position will be
   * manipulated by swipe gestures.
   */
  selector?: string;

  /**
   * Whether mouse users should be able to use swipe gestures or not.
   */
  allowMouse?: boolean;

  /**
   * A callback that will be invoked if the user clicks onto the slideshow.
   */
  click?: Function;

  transformElement?: HTMLElement;
  sizeElement?: HTMLElement;
}


/**
 * Defines all available shift states.
 */
export enum Shift
{
  Previous,
  Next,
  None
}


/**
 * A handler that allows browsing the contents of a slideshow through swipe gestures.
 */
export default class TouchHandler extends DragHandler
{
  /**
   * The slideshow instance that should be controlled by the handler.
   */
  slideshow: Slideshow;

  /**
   * The container whose position will be manipulated by swipe gestures.
   */
  transformElement: HTMLElement;

  /**
   * The container whose size will be used.
   */
  sizeElement: HTMLElement;

  /**
   * The current position offset of the container in pixels.
   */
  position: number = 0;

  /**
   * The position offset of the container from the beginning of a swipe gesture.
   */
  initialPosition: number = 0;

  /**
   * The currently active animation on the container.
   */
  tween: AnyTween;

  /**
   * The current shift state.
   */
  shift: Shift = Shift.None;

  /**
   * The slideshow item that is affected by the current shift state.
   */
  shiftItem: Item;

  /**
   * Whether mouse users should be able to use swipe gestures or not.
   */
  allowMouse: boolean;

  /**
   * A callback that will be invoked if the user clicks onto the slideshow.
   */
  clickCallback: Function;


  /**
   * TouchHandler constructor.
   *
   * @param options
   *   The constructor options.
   */
  constructor(options: TouchHandlerOptions) {
    super(options.view, options.selector);

    this.direction = DragDirection.Horizontal;
    this.allowMouse = !!options.allowMouse;
    this.slideshow = options.view;
    this.clickCallback = options.click;
    this.transformElement = options.transformElement || options.view.container;
    this.sizeElement = options.sizeElement || options.view.element;
  }

  getWidth(): number {
    return this.sizeElement.clientWidth;
  }

  /**
   * Set the offset position of the slideshow content.
   *
   * @param value
   *   The new offset position.
   */
  setPosition(value: number) {
    if (this.position == value) return;
    this.position = value;
    this.transformElement.style[transform.styleName] = `translate(${value}px,0)`;
  }

  /**
   * Set the shift state of the slideshow.
   *
   * The shift state is used to tag the previous or next slide that is
   * visible due to a drag operation. It triggers the necessary image load
   * chains and updates the placement of the slides.
   *
   * @param value
   *   The new shift state of the slideshow.
   */
  setShift(value: Shift) {
    if (this.shift == value) return;
    this.shift = value;

    let state: ItemState = ItemState.Inactive;
    let element: Item = null;
    if (value === Shift.Next) {
      element = this.slideshow.getNextChild();
      state = ItemState.PeakNext;
    } else if (value === Shift.Previous) {
      element = this.slideshow.getPreviousChild();
      state = ItemState.PeakPrevious;
    } else {
      element = null;
    }

    if (this.shiftItem !== element) {
      if (this.shiftItem) {
        this.shiftItem.setState(ItemState.Inactive);
      }

      this.shiftItem = element;
      if (element) element.load();
    }

    if (element) element.setState(state);
  }

  /**
   * Update the shift state from the given position offset.
   *
   * @param position
   *   The position offset the shift state should be derived from.
   */
  updateShift(position: number) {
    let shift = Shift.None;
    if (position > 0) {
      shift = Shift.Previous;
    } else if (position < 0) {
      shift = Shift.Next;
    }

    if (this.shift != shift) {
      this.setShift(shift);
    }
  }

  /**
   * Animate the position of the container to the given value.
   *
   * @param value
   *   The target offset position value.
   * @param updateElement
   *   When set to TRUE, the current shiftElement will become to current
   *   element of the slideshow after animation has finished.
   */
  animateTo(value: number, updateElement?: boolean) {
    const callback = () => {
      const { shiftItem, slideshow } = this;
      this.tween = null;
      this.setPosition(0);
      this.setShift(Shift.None);

      slideshow.isLocked = false;
      slideshow.trigger('locked', false);
      if (updateElement && shiftItem) {
        slideshow.setCurrent(shiftItem, { noTransition: true });
      }
    };

    this.tween = createTween(this, 'position', {
      to:       value,
      duration: 750,
      easing:   easeOutExpo,
      finished: callback,
      stopped:  callback
    });
  }

  /**
   * Try to interrupt the current position animation.
   *
   * @returns
   *   TRUE if the animation could be interrupted, FALSE otherwise.
   */
  interruptAnimation(): boolean {
    if (!this.tween) return true;

    const { position, slideshow, tween } = this;
    const length = slideshow.getLength();

    const from = slideshow.getCurrentIndex();
    tween.stop();
    const to = slideshow.getCurrentIndex();
    if (to === from) return true;

    let diff = from - to;
    while (diff > length * 0.5) diff -= length;
    while (diff < length * -0.5) diff += length;

    const width = this.getWidth();
    let offset = width * (diff / Math.abs(diff));
    if (length < 3 && Math.abs(position - offset) > width) offset *= -1;

    this.setPosition(position - offset);
    return true;
  }

  /**
   * Triggered after a drag has started.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the drag start.
   * @returns
   *   TRUE if the drag operation should be started, FALSE otherwise.
   */
  handleDragStart(event: Event, pointer: Pointer): boolean {
    const { allowMouse, slideshow } = this;

    if ((!this.interruptAnimation()) ||
        (pointer.type != PointerType.Touch && !allowMouse) ||
        (slideshow.getLength() < 2) ||
        (slideshow.isInTransition()))
      return false;

    slideshow.isLocked = true;
    slideshow.trigger('locked', true);
    this.initialPosition = this.position;
    return true;
  }

  /**
   * Triggered after a drag has moved.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the move.
   * @returns
   *   TRUE if the drag operation should be started, FALSE otherwise.
   */
  handleDragMove(event: Event, pointer: Pointer): boolean {
    const delta = pointer.getAbsoluteDelta();
    let position = this.initialPosition + delta.x;

    this.updateShift(position);
    if (!this.shiftItem) {
      position = this.initialPosition + delta.x * 0.5;
    }

    this.setPosition(position);
    return true;
  }

  /**
   * Triggered after a drag has ended.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the movement.
   */
  handleDragEnd(event: Event, pointer: Pointer) {
    const { position, shift, slideshow } = this;
    const width = this.getWidth();
    const relative = position / width;
    const velocity = pointer.getVelocity().x;

    if (Math.abs(relative) > 0.333 || Math.abs(velocity) > 20) {
      this.animateTo(width * (shift === Shift.Next ? -1 : 1), true);
      slideshow.trigger('currentWillChange', this.shiftItem);
    } else {
      this.animateTo(0);
    }
  }

  /**
   * Triggered after the drag has been canceled.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the movement.
   */
  handleDragCancel(event: Event, pointer: Pointer) {
    this.animateTo(0);
  }

  /**
   * Triggered after the user has clicked into the view.
   *
   * @param event
   *   The original dom event.
   * @param pointer
   *   The pointer which triggered the click.
   */
  handleClick(event: Event, pointer: Pointer) {
    if (this.clickCallback) {
      this.clickCallback(event);
    }
  }
}
