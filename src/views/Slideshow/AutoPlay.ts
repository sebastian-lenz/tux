import { defaults } from 'underscore';

import Events from '../../Events';
import CycleableView from '../CycleableView';


/**
 * Constructor options for the AutoPlay class.
 */
export interface AutoPlayOptions
{
  /**
   * The CycleableView that should be controlled.
   */
  view?: CycleableView;

  /**
   * The desired interval between each step in milliseconds.
   */
  interval?: number;

  /**
   * Whether the AutoPlay should be initially active or not.
   */
  autoPlay?: boolean;
}


/**
 * Advances a CycleableView view within a regular interval.
 */
export class AutoPlay extends Events
{
  /**
   * The CycleableView that should be controlled.
   */
  view: CycleableView;

  /**
   * The desired interval between each step in milliseconds.
   */
  interval: number = 10000;

  /**
   * Whether this instance is currently active or not.
   */
  isPlaying: boolean = false;

  /**
   * The current interval handle.
   */
  handle: number = Number.NaN;



  /**
   * AutoPlay constructor.
   *
   * @param options
   *   The constructor options.
   */
  constructor(options?: AutoPlayOptions) {
    super();

    options = defaults(options || {}, {
      interval: 5000,
      autoPlay: true
    });

    this.interval = options.interval;
    if (options.view) this.setView(options.view);
    if (options.autoPlay) this.play();
  }


  /**
   * Set the CycleableView that should be controlled.
   *
   * @param view
   *   The CycleableView that should be controlled.
   */
  setView(view: CycleableView) {
    if (this.view == view) return;

    if (this.view) {
      this.stopListening(this.view);
    }

    if (view) {
      this.listenTo(view, 'currentChanged', this.onIndexChanged);
    }

    this.view = view;
  }


  /**
   * Set the desired interval between each step.
   *
   * @param value
   *   The desired interval between each step in milliseconds.
   */
  setInterval(value:number) {
    if (this.interval == value) return;
    this.interval = value;

    if (this.isPlaying) {
      this.stopInterval();
      this.startInterval();
    }
  }


  /**
   * Start the AutoPlay instance.
   */
  play() {
    if (this.isPlaying) return;
    this.isPlaying = true;

    this.startInterval();
  }


  /**
   * Stop the AutoPlay instance.
   */
  stop() {
    if (!this.isPlaying) return;
    this.isPlaying = false;

    this.stopInterval();
  }


  /**
   * Start the timeout.
   */
  private startInterval() {
    if (!isNaN(this.handle)) return;

    this.handle = setInterval(() => {
      const { view } = this;
      if (!view) return;
      if (view.hasOwnProperty('inViewport') && !(<any>view).inViewport) return;

      clearInterval(this.handle);
      this.handle = Number.NaN;

      var index = view.getCurrentIndex() + 1;
      if (index >= view.getLength()) {
        index = 0;
      }

      view.setCurrentIndex(index);
    }, this.interval);
  }


  /**
   * Stop the current timeout.
   */
  private stopInterval() {
    if (isNaN(this.handle)) return;

    clearInterval(this.handle);
    this.handle = Number.NaN;
  }


  /**
   * Triggered after the current index of the view has changed.
   */
  onIndexChanged() {
    this.stopInterval();

    if (this.isPlaying) {
      this.startInterval();
    }
  }
}
