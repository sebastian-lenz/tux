import * as Q from 'q';

import View, { ViewOptions } from '../View';
import ImageWrap, { ScaleMode } from '../ImageWrap';


/**
 * Defines all states a slide can be in.
 */
export enum ItemState
{
  Inactive       = 0,
  Active         = 1,
  BecomeActive   = 2,
  BecomeInactive = 4,
  PeakNext       = 8,
  PeakPrevious   = 16,

  ActiveOrBecomeActive = Active | BecomeActive,
  InactiveOrBecomeInactive = Inactive | BecomeInactive
}


/**
 * The interface all elements of a slideshow must implement.
 */
export interface Item extends View
{
  /**
   * Preload this slide.
   *
   * @returns
   *   A promise that resolves after the slide has loaded.
   */
  load(): Q.Promise<any>;


  /**
   * Set the state of this slide.
   *
   * @param state
   *   The new state of this slide.
   */
  setState(state: ItemState):void;
}

export interface DefaultItemOptions extends ViewOptions
{
  imageSelector?: string;
  imageOptions?: string;
}

/**
 * An implementation of a simple slide.
 */
export default class DefaultItem extends View implements Item
{
  /**
   * The current state of this slide.
   */
  state: ItemState = ItemState.Inactive;

  /**
   * The image displayed by this slide.
   */
  imageWrap: ImageWrap;



  /**
   * Initialize this slide.
   */
  constructor(options: DefaultItemOptions) {
    super(options);

    const {
      imageSelector = '.tuxImageWrap',
      imageOptions = {},
    } = options;

    const image = this.element.querySelector(imageSelector);
    if (image) {
      this.imageWrap = new ImageWrap({
        element: <HTMLElement>image,
        disableVisibilityCheck: true,
        scaleMode: ScaleMode.Cover,
        ...imageOptions,
      });
    }
  }


  /**
   * Remove this slide.
   *
   * @returns {SlideshowItem}
   */
  remove(): this {
    if (this.imageWrap) {
      this.imageWrap.remove();
    }

    return super.remove();
  }


  /**
   * Preload this slide.
   *
   * @returns
   *   A promise that resolves after the slide has loaded.
   */
  load(): Q.Promise<any> {
    if (this.imageWrap) {
      return this.imageWrap.load();
    }

    const deferred = Q.defer();
    deferred.resolve();
    return deferred.promise;
  }


  /**
   * Set the state of this slide.
   *
   * @param state
   *   The new state of this slide.
   */
  setState(state:ItemState) {
    if (this.state == state) return;
    this.state = state;

    const classList = this.element.classList;

    classList.toggle('current',  !!(state & ItemState.ActiveOrBecomeActive));

    classList.toggle('transitionOrigin', !!(state & ItemState.BecomeInactive));
    classList.toggle('transitionTarget', !!(state & ItemState.BecomeActive));

    classList.toggle('next',     !!(state & ItemState.PeakNext));
    classList.toggle('previous', !!(state & ItemState.PeakPrevious));

    if (state != ItemState.Inactive && this.imageWrap) {
      this.imageWrap.update();
    }
  }

  handleViewportResize(resizeChildren?: Function): boolean {
    const { imageWrap, state }  = this;
    if (imageWrap && state !== ItemState.Inactive) {
      imageWrap.update();
    }

    return true;
  }
}
