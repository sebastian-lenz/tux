import { defaults } from 'underscore';

import services, { service } from '../index';


/**
 *
 */
export interface IPlugin
{
  pluginClass?:any;
}


/**
 *
 */
export interface IPluginHost
{
  registerPlugin(plugin:IPlugin);
}


/**
 * Data structure used to store plugin group data.
 */
export interface IPluginGroup<T extends IPlugin>
{
  hosts: IPluginHost[];

  plugins: T[];
}

/**
 * A central plugin manager.
 */
@service('PluginManager')
export default class PluginManager
{
  /**
   * A map of all known plugin groups.
   */
  groups: { [name: string]: IPluginGroup<IPlugin> } = {};


  /**
   * Return a plugin group by its name.
   *
   * @param name
   *   The name of the plugin group to resolve.
   * @returns
   *   The plugin group with the given name.
   */
  getGroup<T extends IPlugin>(name: string): IPluginGroup<T> {
    if (!this.groups.hasOwnProperty(name)) {
      this.groups[name] = {
        hosts:   [],
        plugins: []
      };
    }

    return <IPluginGroup<T>>this.groups[name];
  }

  /**
   * Register a plugin class.
   *
   * @param name
   *   The name of the plugin group.
   * @param plugin
   *   The plugin data that should be registered.
   */
  registerPlugin(name: string, plugin: IPlugin) {
    const group = this.getGroup(name);
    group.plugins.push(plugin);

    for (var host of group.hosts) {
      host.registerPlugin(plugin);
    }
  }

  /**
   * Register a host that is consuming plugin data.
   *
   * @param name
   *   The name of the plugin group.
   * @param host
   *   The plugin host instance that should be registered.
   */
  registerHost(name: string, host: IPluginHost) {
    const group = this.getGroup(name);
    group.hosts.push(host);

    for (var plugin of group.plugins) {
      host.registerPlugin(plugin);
    }
  }

  /**
   * Return the current service instance of the PluginManager.
   *
   * @returns
   *    The current service instance of the PluginManager.
   */
  static getInstance(): PluginManager {
    return services.get('PluginManager', PluginManager);
  }
}

/**
 * A class annotation that registers classes as plugins.
 *
 * @param name
 *   The name of the plugin group the class should be registered for.
 * @param plugin
 *   The plugin data that should be registered.
 */
export function plugin<T extends IPlugin>(name: string, plugin?: T): ClassDecorator {
  return function<T>(pluginClass: T) {
    PluginManager
      .getInstance()
      .registerPlugin(name, defaults({ pluginClass }, plugin));
  }
}
