import { indexOf } from 'underscore';

import Delegate from '../../views/View/Delegate';
import Dispatcher from '../Dispatcher';
import services, { service } from '../index';


/**
 * Test whether pageXOffset and pageYOffset are available on the window object.
 */
const supportsPageOffset = ('pageXOffset' in window);

/**
 * Test whether stupid IE is in css compat mode.
 */
const isCSS1Compat = ((document.compatMode || '') === 'CSS1Compat');

/**
 * A service that triggers events on browser resize and document scroll actions.
 *
 * Both the resize and scrolling events will be synchronized with an animation frame
 * event to speed up page rendering.
 *
 * @event "scrollbars" (enabled:boolean):void
 *   Triggered after the scrollbars have been enabled or disabled.
 * @event "resize" (width:number, height:number):void
 * @event "scroll" (scrollLeft:number, scrollTop:number):void
 */
@service('Viewport')
export default class Viewport extends Delegate
{
  element: HTMLElement;

  /**
   * The width of the viewport.
   */
  width: number = 0;

  /**
   * The height of the viewport.
   */
  height: number = 0;

  /**
   * The horizontal scroll position of the document.
   */
  scrollLeft: number = 0;

  /**
   * The vertical scroll position of the document.
   */
  scrollTop: number = 0;

  /**
   * A list of initiators that have disabled the scrollbars of the document.
   */
  scrollInitiators: any[] = [];

  /**
   * The size of the scrollbar. Only available after disabling the scrollbars once.
   */
  scrollBarSize: number = Number.NaN;

  /**
   * Whether the size of the viewport has changed or not.
   */
  hasSizeChanged: boolean = false;

  /**
   * Whether the scroll position of the document has changed or not.
   */
  hasScrollChanged: boolean = false;



  /**
   * Viewport constructor.
   */
  constructor() {
    super(document.documentElement);

    if (window.addEventListener) {
      window.addEventListener('resize', () => this.onWindowResize());
      window.addEventListener('scroll', () => this.onWindowScroll());
    } else {
      (<any>window).attachEvent('onresize', () => this.onWindowResize());
      (<any>window).attachEvent('onscroll', () => this.onWindowScroll());
      setTimeout(() => this.trigger('resize', this.width, this.height), 50);
    }

    this.listenTo(Dispatcher.getInstance(), 'frame', this.onFrame);

    this.onWindowResize();
    this.onWindowScroll();
    this.hasSizeChanged = false;
    this.hasScrollChanged = false;
  }

  setScrollLeft(value:number) {
    if (this.scrollLeft == value) return;
    window.scrollTo(value, this.scrollTop);
  }

  setScrollTop(value:number) {
    if (this.scrollTop == value) return;
    window.scrollTo(this.scrollLeft, value);
  }

  /**
   * Trigger a resize event.
   */
  triggerResize() {
    this.hasSizeChanged = true;
  }

  /**
   * Disable the document scrollbars.
   *
   * @param initiator
   *   An identifier of the service or class that disables the scrollbars.
   *   Same value must be passed to enableScrolling, used to manage multiple
   *   scripts that turn scrolling on or off.
   */
  disableScrollbars(initiator:any) {
    const index = indexOf(this.scrollInitiators, initiator);
    if (index == -1) {
      this.scrollInitiators.push(initiator);
    }

    if (this.scrollInitiators.length != 1) {
      return;
    }

    const style = this.element.style;
    if (isNaN(this.scrollBarSize)) {
      const width = this.element.offsetWidth;
      style.overflow     = 'hidden';
      this.scrollBarSize = this.element.offsetWidth - width;
      style.paddingRight = this.scrollBarSize + 'px';
    } else {
      style.overflow     = 'hidden';
      style.paddingRight = this.scrollBarSize + 'px';
    }

    this.onWindowResize();
    this.trigger('scrollbars', false);
  }

  /**
   * Enable the document scrollbars.
   *
   * @param initiator
   *   An identifier of the service or class that enabled the scrollbars.
   */
  enableScrollbars(initiator:any) {
    const index = indexOf(this.scrollInitiators, initiator);
    if (index != -1) {
      this.scrollInitiators.splice(index, 1);
    }

    if (this.scrollInitiators.length != 0) return;

    this.element.style.overflow     = '';
    this.element.style.paddingRight = '';
    this.onWindowResize();
    this.trigger('scrollbars', true);
  }

  /**
   * Triggered on every page repaint.
   */
  onFrame() {
    if (this.hasSizeChanged) {
      this.hasSizeChanged = false;
      const width  = this.width;
      const height = this.height;

      this.trigger('resize', width, height);
    }

    if (this.hasScrollChanged) {
      this.hasScrollChanged = false;
      const scrollLeft = this.scrollLeft;
      const scrollTop  = this.scrollTop;

      this.trigger('scroll', scrollLeft, scrollTop);
    }
  }

  /**
   * Triggered after the size of the window has changed.
   */
  protected onWindowResize() {
    const width  = Math.max(document.documentElement.clientWidth, 0);
    const height = Math.max(document.documentElement.clientHeight, 0);

    if (this.width != width || this.height != height) {
      this.width  = width;
      this.height = height;
      this.hasSizeChanged = true;
    }
  }

  /**
   * Triggered after the scroll position of the document has changed.
   */
  protected onWindowScroll() {
    const scrollLeft = supportsPageOffset
      ? window.pageXOffset
      : isCSS1Compat
        ? document.documentElement.scrollLeft
        : document.body.scrollLeft;

    const scrollTop  = supportsPageOffset
      ? window.pageYOffset
      : isCSS1Compat
        ? document.documentElement.scrollTop
        : document.body.scrollTop;

    if (this.scrollLeft != scrollLeft || this.scrollTop != scrollTop) {
      this.scrollLeft = scrollLeft;
      this.scrollTop  = scrollTop;
      this.hasScrollChanged = true;
    }
  }

  /**
   * Return the current instance of the Viewport service.
   *
   * @returns
   *    The current instance of the Viewport service.
   */
  static getInstance():Viewport {
    return services.get('Viewport', Viewport);
  }
}
