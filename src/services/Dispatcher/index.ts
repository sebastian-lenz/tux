import Events from '../../Events';
import services, { service } from '../index';
import { whenDomReady } from '../../util/domready';

import '../../util/polyfill/animationFrame';


/**
 * A service that triggers a frame event.
 *
 * @event "frame"
 *   Triggered on each browser repaint.
 */
@service('Dispatcher')
export default class Dispatcher extends Events
{
  /**
   * Timestamp of the last frame.
   */
  lastTime:number;


  /**
   * Dispatcher constructor.
   */
  constructor() {
    super();

    whenDomReady(() => {
      this.lastTime = window.performance.now();
      window.requestAnimationFrame(this.onFrame);
    });
  }

  /**
   * Triggered for each browser repaint.
   */
  onFrame = (time: number) => {
    const timeStep = time - this.lastTime;

    if (timeStep > 0) {
      this.trigger('frame', timeStep);
    }

    this.lastTime = time;
    window.requestAnimationFrame(this.onFrame);
  };

  /**
   * Return the current service instance of the Dispatcher.
   *
   * @returns
   *   The current service instance of the Dispatcher.
   */
  static getInstance():Dispatcher {
    return services.get('Dispatcher', Dispatcher);
  }
}
