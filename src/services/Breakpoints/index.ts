import Events from '../../Events';
import services, { service } from '../index';
import Viewport from '../Viewport';


export var getDeviceWidth: { (): number };

export var getDeviceHeight: { (): number };

(function() {
	var scope: any = window;
	var prefix = 'inner';
	if (!('innerWidth' in window )) {
		prefix = 'client';
		scope = document.documentElement || document.body;
	}

	getDeviceWidth = () => scope[prefix + 'Width'];
	getDeviceHeight = () => scope[prefix + 'Height'];
})();

export interface Breakpoint
{
	name: string;

	minWidth: number;

	containerWidth: number;

	update?: { (breakpoint: Breakpoint, width: number) }
}

/**
 * @event "change" (breakpoint:IBreakpoint):void
 *   Triggered after the breakpoint configuration has changed.
 */
@service('Breakpoints')
export default class Breakpoints extends Events
{
	index:number = 0;

	breakpoints:Breakpoint[] = [{
		name:           'xs',
		minWidth:       0,
		containerWidth: 0,
		update: (breakpoint:Breakpoint, width) => {
			breakpoint.containerWidth = width - 30
		},
	},{
		name:           'sm',
		minWidth:       768,
		containerWidth: 720,
	},{
		name:           'md',
		minWidth:       992,
		containerWidth: 940
	},{
		name:           'lg',
		minWidth:       1200,
		containerWidth: 1140
	}];


	constructor() {
		super();

		var viewport = Viewport.getInstance();
		this.listenTo(viewport, 'resize', this.onViewportResize, 10);
		this.update();
	}

	setBreakpoints(value: Breakpoint[]) {
		this.breakpoints = value;
		this.update();
	}

	getCurrent(): Breakpoint {
		return this.breakpoints[this.index];
	}

	update() {
		var viewport = Viewport.getInstance();
		this.onViewportResize(viewport.width);
	}

	onViewportResize(width: number) {
		var deviceWidth = getDeviceWidth();

		var index = 0;
		var length = this.breakpoints.length;
		while (index < length - 1) {
			if (this.breakpoints[index + 1].minWidth > deviceWidth) {
				break;
			} else {
				index += 1;
			}
		}

		var breakpoint = this.breakpoints[index];
		if (breakpoint.update) {
			breakpoint.update(breakpoint, width);
		}

		if (this.index != index || breakpoint.update) {
			this.index = index;
			this.trigger('change', breakpoint);
		}
	}

	static getInstance(): Breakpoints {
		return services.get('Breakpoints', Breakpoints);
	}
}
