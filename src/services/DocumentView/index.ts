import Delegate from '../../views/View/Delegate';
import PointerAdapter from '../../handler/adapter/PointerAdapter';
import PointerHandler from '../../handler/PointerHandler';
import services, { service } from '../index';
import TouchAdapter from '../../handler/adapter/TouchAdapter';


/**
 * A view that wraps the document element.
 *
 * This service is mainly used to capture global mouse events.
 */
@service('DocumentView')
export default class DocumentView extends Delegate
{
  /**
   * Whether the next vlick should be prevented or not.
   */
  isClickPrevented: boolean = false;

  /**
   * Timeout id for the current click preventer.
   */
  preventClickTimeout: any;


  /**
   * DocumentView constructor.
   */
  constructor() {
    super(document);

    this.delegate('click', this.onClick);

    const { element } = this;
    if ('addEventListener' in element) {
      if (PointerAdapter.isSupported()) {
        element.addEventListener('pointerdown', this.onPointerDown, true);
        element.addEventListener('mspointerdown', this.onPointerDown, true);
      } else {
        element.addEventListener('mousedown', this.onPointerDown, true);
        if (TouchAdapter.isSupported()) {
          element.addEventListener('touchstart', this.onPointerDown, true);
        }
      }
    } else {
      const handler = new PointerHandler(this);
      handler.handlePointerDown = <any>this.onPointerDown;
    }
  }

  /**
   * Prevent the next click event.
   */
  preventClick() {
    if (this.preventClickTimeout) {
      clearTimeout(this.preventClickTimeout);
    }

    this.isClickPrevented = true;
    this.preventClickTimeout = setTimeout(() => {
      this.isClickPrevented = false;
      this.preventClickTimeout = null;
    }, 500);
  }

  /**
   * Triggered after the user has clicked into the document.
   */
  onClick = (event: Event) => {
    if (this.isClickPrevented) {
      event.preventDefault();
      event.stopImmediatePropagation();
      this.isClickPrevented = false;
    }
  };

  onPointerDown = (event: Event) => {
    this.trigger('pointerDown', event);
  };

  /**
   * Return the current service instance of the YouTube API loader.
   *
   * @returns
   *    The current service instance of the YouTube API loader.
   */
  static getInstance(): DocumentView {
    return services.get('DocumentView', DocumentView);
  }
}
