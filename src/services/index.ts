export class ServiceRegistry
{
  instances:{ [serviceName: string ]: Object } = {};

  classes:{ [serviceName: string ]: any } = {};


  set<T>(serviceName: string, instance: T) {
    this.instances[serviceName] = instance;
  }

  get<T>(serviceName: string, fallbackClass?: { new(): T; }): T {
    if (this.instances[serviceName]) {
      return <T>this.instances[serviceName];
    }

    if (this.classes[serviceName]) {
      return this.instances[serviceName] = new this.classes[serviceName]();
    }

    if (fallbackClass) {
      return this.instances[serviceName] = new fallbackClass();
    }

    return null;
  }

  register(serviceName: string, serviceClass: any) {
    var instance = this.instances[serviceName];
    if (instance) {
      if (!(instance instanceof serviceClass)) {
        throw new Error('Service `' + serviceName + '` already initialized.');
      }
    } else {
      this.classes[serviceName] = serviceClass;
    }
  }
}

const services = new ServiceRegistry();
export default services;

export function service(serviceName: string) {
  return function(servicePrototype: any): any {
    services.register(serviceName, servicePrototype);
    return servicePrototype;
  }
}
