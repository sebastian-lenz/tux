import * as Q from 'q';

import services, { service } from '../index';


/**
 * A type definition of the YouTube API.
 */
export type YouTubeApi = typeof YT;

/**
 * Loader callback definition.
 */
export type YouTubeLoaderCallback = {(api?:YouTubeApi):void};

/**
 * Defines the possible load states.
 */
export const enum YouTubeApiState {
  Idle,
  Loading,
  Loaded
}

/**
 * A service class that loads the YouTube iFrame API.
 *
 * ´´´
 * YouTubeLoader.require(() => {
 *   const player = new YT.Player('player', {
 *     height: '390',
 *     width: '640',
 *     videoId: 'M7lc1UVf-VE',
 *     events: {
 *       'onReady': function(event:YT.EventArgs) {},
 *       'onStateChange': function(event:YT.EventArgs) {}
 *     }
 *   });
 * });
 * ´´´
 *
 * Documentation of the YouTube API can be found here:
 * https://developers.google.com/youtube/iframe_api_reference
 */
@service('YouTubeLoader')
export class YouTubeLoader
{
  /**
   * Current state of the API loader.
   */
  state: YouTubeApiState = YouTubeApiState.Idle;

  /**
   * Deferred object for creating load promises.
   */
  deferred: Q.Deferred<YouTubeApi> = null;


  /**
   * Return the current load state of the YoutTube API.
   *
   * @returns
   *   The current load state of the YoutTube API.
   */
  getState(): YouTubeApiState {
    return this.state;
  }

  /**
   * Return a promise that
   *
   * @returns {Promise<YouTubeApi>}
   */
  getPromise(): Q.Promise<YouTubeApi> {
    if (this.state != YouTubeApiState.Idle) {
      return this.deferred.promise;
    }

    this.state    = YouTubeApiState.Loading;
    this.deferred = Q.defer<YouTubeApi>();

    const script = document.createElement('script');
    script.src = "https://www.youtube.com/iframe_api";

    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(script, firstScriptTag);

    window['onYouTubeIframeAPIReady'] = () => {
      this.state = YouTubeApiState.Loaded;
      this.deferred.resolve(YT);
    };

    return this.deferred.promise;
  }

  /**
   * Require the YouTube API.
   *
   * @param callback
   *   The callback that should be invoked as soon as the YouTube API
   *   is loaded.
   */
  static require(callback: YouTubeLoaderCallback) {
    const loader = YouTubeLoader.getInstance();

    if (loader.state == YouTubeApiState.Loaded) {
      callback(YT);
    } else {
      loader.getPromise().then(() => {
        callback(YT);
      });
    }
  }

  /**
   * Return the current service instance of the YouTube API loader.
   *
   * @returns
   *    The current service instance of the YouTube API loader.
   */
  static getInstance(): YouTubeLoader {
    return services.get('YouTubeLoader', YouTubeLoader);
  }
}
