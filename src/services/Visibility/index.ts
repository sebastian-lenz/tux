import { find, filter, some } from 'underscore';

import services, { service } from '../index';
import Events from '../../Events';
import Viewport from '../Viewport';


export interface VisibilityTarget
{
	element?: HTMLElement;
	getBounds?: {():{min: number; max: number;}};
	setInViewport(value: boolean);
}


export class VisibilityItem
{
	isVisible: boolean = false;
	max: number = Number.MIN_VALUE;
	min: number = Number.MAX_VALUE;
	target: VisibilityTarget;


	constructor(target: VisibilityTarget) {
		this.target = target;
	}

	updateBounds(scrollTop: number, viewMin: number, viewMax: number) {
		let min = Number.MAX_VALUE;
		let max = Number.MIN_VALUE;

		if (this.target.element) {
			let bounds = this.target.element.getBoundingClientRect();
			min = bounds.top + scrollTop;
			max = min + (bounds.height || this.target.element.offsetHeight);
		} else if (this.target.getBounds) {
			let bounds = this.target.getBounds();
			min = bounds.min;
			max = bounds.max;
		}

		if (this.min != min || this.max != max) {
			this.min = min;
			this.max = max;
			this.updateVisibility(viewMin, viewMax);
		}
	}

	updateVisibility(viewMin: number, viewMax: number) {
		const isVisible = this.max > viewMin && this.min < viewMax;
		if (this.isVisible != isVisible) {
			this.isVisible = isVisible;
			this.target.setInViewport(isVisible);
		}
	}
}


@service('Visibility')
export default class Visibility extends Events
{
	bleed: number = 500;
	items: VisibilityItem[] = [];
  itemClass: typeof VisibilityItem = VisibilityItem;
	max: number = 0;
	min: number = 0;
	viewport: Viewport;


	constructor() {
		super();

		const viewport = this.viewport = Viewport.getInstance();

		this.listenTo(viewport, 'resize', this.onViewportResize, -10);
		this.listenTo(viewport, 'scroll', this.onViewportScroll, -10);
		this.updateBounds();
	}

	register(target: VisibilityTarget) {
    const { items, itemClass } = this;
		if (some(items, item => item.target === target)) {
			return;
		}

		const item = new itemClass(target);
		item.updateBounds(this.viewport.scrollTop, this.min, this.max);
		items.push(item);
	}

  unregister(target: VisibilityTarget) {
    const { items } = this;
    this.items = filter(items, item => item.target !== target);
  }

	update(target: VisibilityTarget) {
		const item = find(this.items, item => item.target === target);
		if (item) {
			item.updateBounds(this.viewport.scrollTop, this.min, this.max);
		}
	}

	updateBounds() {
		const { bleed, viewport } = this;

		this.min = viewport.scrollTop - bleed;
		this.max = viewport.scrollTop + viewport.height + bleed;
	}

	onViewportResize() {
		this.updateBounds();
		const { items, max, min } = this;
    const { scrollTop } = this.viewport;

		for (let index = 0, count = items.length; index < count; ++index) {
			items[index].updateBounds(scrollTop, min, max);
		}
	}

	onViewportScroll() {
		this.updateBounds();
		const { items, max, min } = this;

		for (let index = 0, count = items.length; index < count; ++index) {
			items[index].updateVisibility(min, max);
		}
	}

	static getInstance(): Visibility {
		return services.get('Visibility', Visibility);
	}
}
