import Events from '../../Events';
import services, { service } from '../index';
import Viewport from '../Viewport';
import Worker from './Worker';


@service('FontObserver')
export default class FontObserver extends Events
{
  private workers: Worker[] = [];


  add(markup: string) {
    this.workers.push(new Worker({ observer: this, markup }));
  }

  handleFontLoaded() {
    Viewport.getInstance().triggerResize();
    FontObserver.getInstance().trigger('fontLoaded');
  }

  static getInstance(): FontObserver {
    return services.get('FontObserver', FontObserver);
  }
}
