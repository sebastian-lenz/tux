import * as Q from 'q';

import services, { service } from '../index';


/**
 * Loader callback definition.
 */
export type GoogleMapsLoaderCallback = { (): void };

/**
 * Defines the possible load states.
 */
export const enum GoogleMapsApiState {
  Idle,
  Loading,
  Loaded
}

/**
 * A service class that loads the GoogleMaps API.
 *
 * ´´´
 * GoogleMapsLoader.require(() => {
 *   ...
 * });
 * ´´´
 */
@service('GoogleMapsLoader')
export default class GoogleMapsLoader
{
  /**
   * The API key that should be used.
   */
  apiKey: string;

  /**
   * Current state of the API loader.
   */
  state: GoogleMapsApiState = GoogleMapsApiState.Idle;

  /**
   * Deferred object for creating load promises.
   */
  deferred: Q.Deferred<any> = null;


  /**
   * Return the current load state of the loader.
   *
   * @returns
   *   The current load state of the loader.
   */
  getState(): GoogleMapsApiState {
    return this.state;
  }

  /**
   * Return a promise that
   *
   * @returns {Promise<YouTubeApi>}
   */
  getPromise(): Q.Promise<any> {
    if (this.state != GoogleMapsApiState.Idle) {
      return this.deferred.promise;
    }

    this.state    = GoogleMapsApiState.Loading;
    this.deferred = Q.defer<any>();

    const script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?key=${this.apiKey}&callback=onGoogleMapsAPIReady`;

    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(script, firstScriptTag);

    window['onGoogleMapsAPIReady'] = () => {
      this.state = GoogleMapsApiState.Loaded;
      this.deferred.resolve();
    };

    return this.deferred.promise;
  }

  /**
   * Require the GoogleMaps API.
   *
   * @param callback
   *   The callback that should be invoked as soon as the API is loaded.
   */
  static require(callback: GoogleMapsLoaderCallback) {
    const loader = GoogleMapsLoader.getInstance();

    if (loader.state == GoogleMapsApiState.Loaded) {
      callback();
    } else {
      loader.getPromise().then(() => {
        callback();
      });
    }
  }

  /**
   * Return the current service instance of the API loader.
   *
   * @returns
   *    The current service instance of the API loader.
   */
  static getInstance(): GoogleMapsLoader {
    return services.get('GoogleMapsLoader', GoogleMapsLoader);
  }
}
