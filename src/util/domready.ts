export function whenDomReady(callback:EventListener) {
  if ('addEventListener' in document) {
    document.addEventListener('DOMContentLoaded', callback);
  } else {
    (<any>window).attachEvent('onload', callback);
  }
}