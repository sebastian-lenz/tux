import { indexOf } from 'underscore';


/**
 * https://davidwalsh.name/element-matches-selector
 */
const proto = Element.prototype;

if (!Element.prototype.matches)
{
  // Chrome <34, SF<7.1, iOS<8
  if ('webkitMatchesSelector' in proto) {
    proto.matches = proto['webkitMatchesSelector'];

  // IE9/10/11 & Edge
  } else if ('msMatchesSelector' in proto) {
    proto.matches = proto['msMatchesSelector'];

  // FF <34
  } else if ('mozMatchesSelector' in proto) {
    proto.matches = proto['mozMatchesSelector'];

  // Fallback
  } else {
    proto.matches = function(selector) {
      // Use querySelectorAll to find all elements matching the selector,
      // then check if the given element is included in that list.
      // Executing the query on the parentNode reduces the resulting nodeList,
      // (document doesn't have a parentNode).
      var nodeList = (this.parentNode || document).querySelectorAll(selector) || [];
      return indexOf(nodeList, this) !== -1;
    };
  }
}
