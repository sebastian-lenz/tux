Date.now = (Date.now || function () {
  return new Date().getTime();
});

var vendors = ['webkit', 'moz'];
for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
	window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
	window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
}

if (!window.requestAnimationFrame) {
	var lastTime = 0;

	window.requestAnimationFrame = function(callback) {
		var currTime = new Date().getTime();
		var timeToCall = Math.max(0, 16 - (currTime - lastTime));
		var id = window.setTimeout(function() { callback(currTime + timeToCall); },
			timeToCall);
		lastTime = currTime + timeToCall;
		return id;
	};

	window.cancelAnimationFrame = function(id) {
		clearTimeout(id);
	};
}

if (!('performance' in window)) {
  (<any>window).performance = {};
}
  
if (!('now' in window.performance)) {
  let nowOffset = Date.now();
  if (performance.timing && performance.timing.navigationStart){
    nowOffset = performance.timing.navigationStart
  }

  window.performance.now = function now(){
    return Date.now() - nowOffset;
  }
}
