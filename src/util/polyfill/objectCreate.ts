/**
 * Polyfill for Object.create()
 */
if (typeof Object.create != 'function') {
  Object.create = (function () {
    const ctr = function () { };

    return function (prototype) {
      if (arguments.length > 1) {
        throw Error('Second argument not supported');
      }

      if (typeof prototype != 'object') {
        throw TypeError('Argument must be an object');
      }

      ctr.prototype = prototype;
      const result = new ctr();

      ctr.prototype = null;
      return result;
    };
  })();
}
