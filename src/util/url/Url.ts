import { Query } from './Query';


export class Url
{
  url:string;

  query:Query;

  hash:string;


  constructor(url?:string) {
    this.query = new Query();

    if (url) {
      this.parse(url);
    }
  }

  toString():string {
    const parts = [this.url];

    const search = this.query.toString();
    if (search != '') {
      parts.push('?', search);
    }

    if (this.hash) {
      parts.push('#', this.hash);
    }

    return parts.join('');
  }

  parse(url:string) {
    const hashIndex = url.indexOf('#');
    if (hashIndex != -1) {
      this.hash = url.substring(hashIndex + 1);
      url = url.substr(0, hashIndex);
    } else {
      this.hash = null;
    }

    const searchIndex = url.indexOf('?');
    if (searchIndex != -1) {
      this.query.parse(url.substring(searchIndex + 1));
      url = url.substr(0, searchIndex);
    } else {
      this.query.values = {};
    }

    this.url = url;
  }
}