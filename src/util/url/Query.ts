import { isArray } from 'underscore';


export class Query
{
  values:Object = {};


  constructor(query?:string) {
    if (query) {
      this.parse(query);
    }
  }

  setValue(key:string, value:string|string[]):this {
    this.values[key] = value;
    return this;
  }

  getValue(key:string, defaultValue:string = null):string {
    return this.values[key] || defaultValue;
  }

  remove(key:string):this {
    delete this.values[key];
    return this;
  }

  toString():string {
    const values = this.values;
    const parts = [];

    for (let key in values) {
      if (!values.hasOwnProperty(key)) {
        continue;
      }

      const encodedKey = encodeURIComponent(key);
      const value = values[key];

      if (isArray<string>(value)) {
        for (let index = 0; index < value.length; index++) {
          parts.push(`${encodedKey}=${encodeURIComponent(value[index])}`);
        }
      } else {
        parts.push(`${encodedKey}=${encodeURIComponent(value)}`);
      }
    }

    return parts.join('&');
  }

  parse(query:string) {
    const parts = query.split('&');
    const values = {};

    for (let index = 0; index < parts.length; index++) {
      const args = parts[index].split('=', 2);
      const key = decodeURIComponent(args[0]);
      const value = decodeURIComponent(args[1]);

      if (key in values) {
        if (isArray(values[key])) {
          values[key].push(value);
        } else {
          values[key] = [values[key], value];
        }
      } else {
        values[key] = value;
      }
    }

    this.values = values;
  }
}
