import {withDummyElement, getPrefixedStyle} from './prefixUtil';
import {whenDomReady} from "../domready";


export interface TransformInfo
{
  styleName: string;
  originStyleName: string;
  has3D: boolean;
}

const transform: TransformInfo = {
  styleName: null,
  originStyleName: null,
  has3D: false,
};

withDummyElement((el:HTMLElement) => {
  transform.styleName = getPrefixedStyle('transform', (name:string, prefix?:string) => {
    transform.has3D = !!getPrefixedStyle('perspective');
  });

  transform.originStyleName = getPrefixedStyle('transformOrigin');
});

export default transform;
