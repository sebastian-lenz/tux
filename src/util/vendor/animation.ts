import {withDummyElement, getPrefixedStyle, getPrefixedEvent} from './prefixUtil';
import {whenDomReady} from "../domready";


export interface AnimationInfo
{
  styleName: string;
  endEvent: string;
}

const animation: AnimationInfo = {
  styleName: null,
  endEvent: null,
};

withDummyElement((el) => {
  animation.styleName = getPrefixedStyle('animation', (name:string, prefix?:string) => {
    animation.endEvent = getPrefixedEvent('AnimationEnd', prefix);
  });
});

export default animation;
