export const prefixes = ['ms', 'Webkit', 'Moz', 'O'];

let el:HTMLDivElement;


export function eachPrefix(name:string, callback:{(prop:string, prefix?:string):boolean}):string {
  if (callback(name)) {
    return name;
  }

  name = name.substr(0, 1).toUpperCase() + name.substr(1);
  for (var index = 0, count = prefixes.length; index < count; index++) {
    var prefix = prefixes[index];
    var prefixedName = prefix + name;

    if (callback(prefixedName, prefix)) {
      if (index > 0) {
        prefixes.splice(index, 1);
        prefixes.unshift(prefix);
      }

      return prefixedName;
    }
  }

  return null;
}


export function withDummyElement<T>(callback:{(el:HTMLDivElement):T}):T {
  var isCreator:boolean;
  if (!el) {
    isCreator = true;
    el = document.createElement('div');
    document.body.insertBefore(el, null);
  }

  var result = callback(el);

  if (isCreator) {
    document.body.removeChild(el);
    el = void 0;
  }

  return result;
}


export function getPrefixedStyle(name:string, callback?:{(name:string, prefix?:string)}):string {
  return withDummyElement((el) => {
    return eachPrefix(name, (name:string, prefix?:string):boolean => {
      if (name in el.style) {
        if (callback) callback(name, prefix);
        return true;
      } else {
        return false;
      }
    });
  });
}


export function getPrefixedEvent(name:string, prefix?:string):string {
  if (prefix == 'Webkit') return 'webkit' + name;
  if (prefix == 'O') return 'o' + name;
  return name.toLowerCase();
}
