import {withDummyElement, getPrefixedStyle, getPrefixedEvent} from './prefixUtil';
import {whenDomReady} from "../domready";


export interface TransitionInfo {
  styleName:string;
  endEvent:string;
}

const transition: TransitionInfo = {
  styleName: null,
  endEvent: null,
};

withDummyElement((el) => {
  transition.styleName = getPrefixedStyle('transition', (name:string, prefix?:string) => {
    transition.endEvent = getPrefixedEvent('TransitionEnd', prefix);
  });
});

export default transition;
