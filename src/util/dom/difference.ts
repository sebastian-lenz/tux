import { map } from 'underscore';

import View from '../../views/View';
import { IRectangle } from '../../geom/Rectangle';
import ChildableView from '../../views/ChildableView';


export enum DifferenceAction
{
  None,
  Changed,
  Appeared,
  Disappeared,
}

export interface IDifferenceState
{
  rect:IRectangle;
  hidden:boolean;
}

export interface IDifferenceStateCallback<T>
{
  (target:T):IDifferenceState;
}

export interface IDifferenceResult<T>
{
  target:T;
  action:DifferenceAction;
  oldState:IDifferenceState;
  newState:IDifferenceState;
}

export function normalizeRect(rect):IRectangle {
  return {
    x: rect.left,
    y: rect.top,
    width: rect.right - rect.left,
    height: rect.bottom - rect.top,
  };
}

export function rectEquals(a:IRectangle, b:IRectangle):boolean {
  return ((a.x == b.x) && (a.y == b.y) && (a.width == b.width) && (a.height == b.height));
}

export function getDifference<T>(callback:Function, targets:T[], toState:IDifferenceStateCallback<T>):IDifferenceResult<T>[] {
  const oldStates = map(targets, toState);

  callback();

  return map(targets, (target, index) => {
    const oldState = oldStates[index];
    const newState = toState(target);

    let action:DifferenceAction;
    if (oldState.hidden == newState.hidden) {
      if (rectEquals(oldState.rect, newState.rect)) {
        action = DifferenceAction.None;
      } else {
        action = DifferenceAction.Changed;
      }
    } else if (newState.hidden) {
      action = DifferenceAction.Disappeared;
    } else {
      action = DifferenceAction.Appeared;
    }

    return {
      target,
      action,
      oldState,
      newState,
    }
  });
}

export interface IDomDifferenceOptions
{
  callback:Function;
  context:HTMLElement;
  selector:string;
  isHidden:{ (element:HTMLElement):boolean };
}

export function getDomDifference(options:IDomDifferenceOptions):IDifferenceResult<HTMLElement>[] {
  const {
    callback,
    context,
    selector,
    isHidden,
  } = options;

  const nodeList = context.querySelectorAll(selector);
  const elements = [];
  for (let index = 0, count = nodeList.length; index < count; index++) {
    elements.push(nodeList[index]);
  }

  return getDifference(callback, elements, (element) => ({
    rect: normalizeRect(element.getBoundingClientRect()),
    hidden: isHidden(element),
  }));
}

export interface IViewDifferenceOptions<T extends View> {
  callback:Function;
  view:ChildableView<T>;
  isHidden:{ (view:T):boolean };
}

export function getViewDifference<T extends View>(options:IViewDifferenceOptions<T>):IDifferenceResult<T>[] {
  const {
    callback,
    view,
    isHidden,
  } = options;

  return getDifference(callback, view.children, child => ({
    rect: normalizeRect(child.element.getBoundingClientRect()),
    hidden: isHidden(child),
  }));
}
