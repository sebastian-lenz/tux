import { keys, once, uniqueId } from 'underscore';

export type EventCallback = Function & {
  _callback?: Function;
};

export type EventListener = {
  obj: Events;
  objId: string;
  id: string;
  listeningTo: EventListenerMap;
  count: number;
};

export type EventListenerMap = {
  [id: string]: EventListener;
};

export type EventHandler = {
  callback: EventCallback;
  context: any;
  ctx: any;
  listening: EventListener | null;
  priority: number;
};

export type EventHandlerMap = {
  [name: string]: EventHandler[];
};

/**
 * A difficult-to-believe, but optimized internal dispatch function for
 * triggering events. Tries to keep the usual cases speedy (most internal
 * Backbone events have 3 arguments).
 */
function triggerEvents(events: EventHandler[], args: any[]) {
  let ev, i = -1;
  const l = events.length, a1 = args[0], a2 = args[1], a3 = args[2];

  switch (args.length) {
    case 0: while (++i < l) (ev = events[i]).callback.call(ev.ctx); return;
    case 1: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1); return;
    case 2: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2); return;
    case 3: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2, a3); return;
    default: while (++i < l) (ev = events[i]).callback.apply(ev.ctx, args); return;
  }
}

/**
 * An event dispatcher.
 *
 * Based on Backbone.Events
 * http://backbonejs.org/backbone.js
 */
export default class Events {
  private _events: EventHandlerMap | null = null;
  private _listenId: string | null = null;
  private _listeners: EventListenerMap | null = null;
  private _listeningTo: EventListenerMap | null = null;


  /**
   * Bind an event to a `callback` function. Passing `"all"` will bind
   * the callback to all events fired.
   */
  on(name: string, callback: EventCallback, context?: any, priority?: number): this {
    return this.addHandler(name, callback, context, null, priority);
  }

  /*
   * Bind an event to only be triggered a single time. After the first time
   * the callback is invoked, its listener will be removed. If multiple events
   * are passed in using the space-separated syntax, the handler will fire
   * once for each event, not once for a combination of all events.
   */
  once(name: string, callback: EventCallback, context?: any, priority?: number): this {
    const onceCallback = this.createOnceHandler(callback, (handler: Function) => {
      this.off(name, handler);
    });

    return this.addHandler(name, onceCallback, context, null, priority);
  }

  /**
   * Remove one or many callbacks. If `context` is null, removes all
   * callbacks with that function. If `callback` is null, removes all
   * callbacks for the event. If `name` is null, removes all bound
   * callbacks for all events.
   */
  off(name?: string, callback?: EventCallback, context?: any): this {
    const events = this._events;
    const listeners = this._listeners;
    if (events === null) return this;

    // Delete all events listeners and "drop" events.
    if (!name && !callback && !context) {
      if (listeners) {
        const ids = keys(listeners);
        for (let i = 0; i < ids.length; i++) {
          const listening = listeners[ids[i]];
          delete listeners[listening.id];
          delete listening.listeningTo[listening.objId];
        }
      }

      this._events = null;
      return this;
    }

    const names = name ? [name] : keys(events);
    for (let i = 0; i < names.length; i++) {
      const name = names[i];
      const handlers = events[name];
      if (!handlers) break;

      // Replace events if there are any remaining.  Otherwise, clean up.
      const remaining = [];
      for (let j = 0; j < handlers.length; j++) {
        const handler = handlers[j];
        if (
          callback && (
            callback !== handler.callback &&
            callback !== handler.callback._callback
          ) ||
          context && context !== handler.context
        ) {
          remaining.push(handler);
        } else {
          const { listening } = handler;
          if (listening && --listening.count === 0 && listeners) {
            delete listeners[listening.id];
            delete listening.listeningTo[listening.objId];
          }
        }
      }

      // Update tail event if the list has any events.  Otherwise, clean up.
      if (remaining.length) {
        events[name] = remaining;
      } else {
        delete events[name];
      }
    }

    return this;
  }

  /**
   * Inversion-of-control versions of `on`. Tell *this* object to listen to
   * an event in another object... keeping track of what it's listening to
   * for easier unbinding later.
   */
  listenTo(obj: Events, name: string, callback: EventCallback, priority?: number): this {
    if (!obj) return this;
    const objId = obj._listenId || (obj._listenId = uniqueId('l'));
    const listeningTo = this._listeningTo || (this._listeningTo = {});
    let listening = listeningTo[objId];

    // This object is not listening to any other events on `obj` yet.
    // Setup the necessary references to track the listening callbacks.
    if (!listening) {
      const id = this._listenId || (this._listenId = uniqueId('l'));
      listening = { count: 0, id, listeningTo, obj, objId };
      listeningTo[objId] = listening;
    }

    // Bind callbacks on obj, and keep track of them on listening.
    obj.addHandler(name, callback, this, listening, priority);
    return this;
  }

  /**
   * Inversion-of-control versions of `once`.
   */
  listenToOnce(obj: Events, name: string, callback: EventCallback, priority?: number): this {
    const onceCallback = this.createOnceHandler(callback, (handler: Function) => {
      this.stopListening(obj, name, handler);
    });

    return this.listenTo(obj, name, onceCallback, priority);
  }

  /**
   * Tell this object to stop listening to either specific events ... or
   * to every object it's currently listening to.
   */
  stopListening(obj?: Events, name?: string, callback?: EventCallback): this {
    const listeningTo = this._listeningTo;
    if (!listeningTo) return this;
    if (obj && !obj._listenId) return this;

    const ids = obj
      ? [obj._listenId ? obj._listenId : '']
      : keys(listeningTo);

    for (let i = 0; i < ids.length; i++) {
      const listening = listeningTo[ids[i]];

      // If listening doesn't exist, this object is not currently
      // listening to obj. Break out early.
      if (!listening) break;

      listening.obj.off(name, callback, this);
    }

    return this;
  }

  /**
   * Trigger one or many events, firing all bound callbacks. Callbacks are
   * passed the same arguments as `trigger` is, apart from the event name
   * (unless you're listening on `"all"`, which will cause your callback to
   * receive the true name of the event as the first argument).
   */
  trigger(name: string, ...args: any[]): this {
    const events = this._events;
    if (!events) return this;

    const handlers = events[name];
    let allHandlers = events['all'];

    if (handlers && allHandlers) allHandlers = allHandlers.slice();
    if (handlers) triggerEvents(handlers, args);
    if (allHandlers) triggerEvents(allHandlers, [name].concat(args));

    return this;
  }

  /**
   * Guard the `listening` argument from the public API.
   */
  private addHandler(name: string, callback: EventCallback, context?: any, listening: EventListener | null = null, priority: number = 0): this {
    const events = this._events || (this._events = {});
    const handlers = events[name] || (events[name] = []);

    handlers.push({
      callback,
      context,
      ctx: context || this,
      listening,
      priority,
    });

    handlers.sort((a, b) => b.priority - a.priority);

    if (listening) {
      const listeners = this._listeners || (this._listeners = {});
      listeners[listening.id] = listening;
      listening.count++;
    }

    return this;
  }

  /**
   * Reduces the event callbacks into a map of `{event: onceWrapper}`.
   * `offer` unbinds the `onceWrapper` after it has been called.
   */
  private createOnceHandler(callback: EventCallback, offCallback: Function): EventCallback {
    const that = this;
    const onceCallback = <EventCallback>once(function () {
      offCallback(onceCallback);
      callback.apply(that, arguments);
    });

    onceCallback._callback = callback;
    return onceCallback;
  }
}
