import { isArray } from 'underscore';

import { IComputableType, IVectorType } from './Type';


/**
 * Data structure of vectors with two dimensions.
 */
export interface IVector2
{
  /**
   * The horizontal position of the vector.
   */
  x: number;

  /**
   * The vertical position of the vector.
   */
  y: number;
}


/**
 * A vector with two dimensions like a point on a flat surface.
 */
export class Vector2 implements IVector2, IComputableType, IVectorType
{
  /**
   * The horizontal position of the vector.
   */
  x:number;

  /**
   * The vertical position of the vector.
   */
  y:number;



  /**
   * Vector2 constructor.
   * @param x
   *   The horizontal position of the vector.
   * @param y
   *   The vertical position of the vector.
   */
  constructor(x:number, y:number) {
    this.x = x;
    this.y = y;
  }


  /**
   * Return a copy of this type.
   * Implements IType.
   *
   * @returns
   *   A copy of this type and all of its values.
   */
  clone():Vector2 {
    return new Vector2(this.x, this.y);
  }


  /**
   * Test whether this instance equals the given type instance.
   *
   * @param other
   *   The other type instance that should be compared.
   * @returns
   *   TRUE if the values of this instance and the given instance match.
   */
  equals(other:Vector2):boolean {
    return this.x == other.x && this.y == other.y;
  }


  /**
   * Try to convert the given value to a Vector2 instance.
   * Implements IType.
   *
   * @param value
   *   The value that should be converted.
   */
  convert(value:any):Vector2 {
    return Vector2.convert(value);
  }


  /**
   * Return the length of this vector.
   * Implements IComputableType.
   *
   * @returns
   *   The length of this vector.
   */
  length():number {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }


  /**
   * Reset all dimensions of this vector to zero.
   * Implements IComputableType.
   */
  reset():this {
    this.x = 0;
    this.y = 0;
    return this;
  }


  /**
   * Copy the dimensions of the given vector to this vector.
   * Implements IComputableType.
   *
   * @param source
   *   The vector whose dimensions should be copied.
   */
  copy(source:Vector2):this {
    this.x = source.x;
    this.y = source.y;
    return this;
  }


  /**
   * Add the given vector dimensions to this vector.
   * Implements IComputableType.
   *
   * @param summand
   *   The vector whose dimensions should be added.
   */
  add(x:number, y:number):this;
  add(summand:Vector2):this;
  add(summand:IVector2):this;
  add(x:any, y?:number):this {
    if (isIVector2(x)) {
      this.x += x.x;
      this.y += x.y;
    } else {
      this.x += <number>x;
      this.y += y;
    }
    return this;
  }


  /**
   * Subtract the given vector dimensions from this vector.
   * Implements IComputableType.
   *
   * @param subtrahend
   *   The vector whose dimensions should be subtracted.
   */
  subtract(x:number, y:number):this;
  subtract(subtrahend:Vector2):this;
  subtract(subtrahend:IVector2):this;
  subtract(x:any, y?:number):this {
    if (isIVector2(x)) {
      this.x -= x.x;
      this.y -= x.y;
    } else {
      this.x -= <number>x;
      this.y -= y;
    }

    return this;
  }


  min(x:number, y:number):this;
  min(subtrahend:IVector2):this;
  min(x:IVector2|number, y?:number):this {
    if (isIVector2(x)) {
      if (this.x > x.x) this.x = x.x;
      if (this.y > x.y) this.y = x.y;
    } else {
      if (this.x > x) this.x = x;
      if (this.y > y) this.y = y;
    }

    return this;
  }


  max(x:number, y:number):this;
  max(subtrahend:IVector2):this;
  max(x:IVector2|number, y?:number):this {
    if (isIVector2(x)) {
      if (this.x < x.x) this.x = x.x;
      if (this.y < x.y) this.y = x.y;
    } else {
      if (this.x < x) this.x = x;
      if (this.y < y) this.y = y;
    }

    return this;
  }


  /**
   * Scale all dimensions of this vector by the given factor.
   * Implements IComputableType.
   *
   * @param factor
   *   The scaling factor that should be applied to this vector.
   */
  scale(factor:number):this {
    this.x *= factor;
    this.y *= factor;
    return this;
  }


  /**
   * Set the values of this vector from the given array.
   * Implements IVectorType.
   *
   * @param values
   *   An array containing the x and y values that should be set.
   */
  setValues(values:number[]):this {
    this.x = values[0];
    this.y = values[1];
    return this;
  }


  /**
   * Return the raw values of this vector as an array.
   * Implements IVectorType.
   *
   * @returns
   *   An array containing the x and y values of this vector.
   */
  getValues():number[] {
    return [this.x, this.y];
  }


  /**
   * Try to convert the given value to a Vector2 instance.
   *
   * @param value
   *   The value that should be converted.
   */
  static convert(value: any): Vector2 {
    if (value instanceof Vector2) {
      return value;
    } else if (isIVector2(value)) {
      return new Vector2(value.x * 1, value.y * 1);
    } else if (isArray<string>(value) && value.length >= 2) {
      const [x, y] = value;
      return new Vector2(parseFloat(x), parseFloat(y));
    }

    return null;
  }
}


/**
 * A type guard that checks whether the given value implements IVector2.
 *
 * @param value
 *   The value that should be tested.
 * @returns
 *   TRUE if the given value implements IVector2, FALSE otherwise.
 */
export function isIVector2(value:any):value is IVector2 {
  return (
      typeof value === 'object' &&
      'x' in value &&
      'y' in value
  );
}
