import {IComputableType, IVectorType} from "./Type";


export class NumericValue implements IComputableType, IVectorType
{
  value:number;


  constructor(value:number) {
    this.value = value;
  }


  clone():NumericValue {
    return new NumericValue(this.value);
  }


  equals(other:NumericValue):boolean {
    return this.value == other.value;
  }


  convert(value:any):NumericValue {
    if (value instanceof NumericValue) {
      return value;
    } else if (typeof value == 'number') {
      return new NumericValue(value);
    }
    
    return null;
  }


  reset():this {
    this.value = 0;
    return this;
  }


  copy(source:NumericValue):this {
    this.value = source.value;
    return this;
  }

  
  length():number {
    return Math.abs(this.value);
  }


  add(summand:NumericValue):this {
    this.value += summand.value;
    return this;
  }


  subtract(subtrahend:NumericValue):this {
    this.value -= subtrahend.value;
    return this;
  }


  scale(factor:number):this {
    this.value *= factor;
    return this;
  }


  setValues(values:number[]):this {
    this.value = values[0];
    return this;
  }


  getValues():number[] {
    return [this.value];
  }
}