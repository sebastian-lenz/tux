export interface TransitionInfo {
    styleName: string;
    endEvent: string;
}
declare const transition: TransitionInfo;
export default transition;
