import { withDummyElement, getPrefixedStyle, getPrefixedEvent } from './prefixUtil';
var transition = {
    styleName: null,
    endEvent: null,
};
withDummyElement(function (el) {
    transition.styleName = getPrefixedStyle('transition', function (name, prefix) {
        transition.endEvent = getPrefixedEvent('TransitionEnd', prefix);
    });
});
export default transition;
