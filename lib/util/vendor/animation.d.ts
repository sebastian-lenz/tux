export interface AnimationInfo {
    styleName: string;
    endEvent: string;
}
declare const animation: AnimationInfo;
export default animation;
