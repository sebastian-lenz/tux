export interface TransformInfo {
    styleName: string;
    originStyleName: string;
    has3D: boolean;
}
declare const transform: TransformInfo;
export default transform;
