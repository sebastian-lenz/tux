import { withDummyElement, getPrefixedStyle } from './prefixUtil';
var transform = {
    styleName: null,
    originStyleName: null,
    has3D: false,
};
withDummyElement(function (el) {
    transform.styleName = getPrefixedStyle('transform', function (name, prefix) {
        transform.has3D = !!getPrefixedStyle('perspective');
    });
    transform.originStyleName = getPrefixedStyle('transformOrigin');
});
export default transform;
