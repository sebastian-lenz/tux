export declare const prefixes: string[];
export declare function eachPrefix(name: string, callback: {
    (prop: string, prefix?: string): boolean;
}): string;
export declare function withDummyElement<T>(callback: {
    (el: HTMLDivElement): T;
}): T;
export declare function getPrefixedStyle(name: string, callback?: {
    (name: string, prefix?: string);
}): string;
export declare function getPrefixedEvent(name: string, prefix?: string): string;
