import { withDummyElement, getPrefixedStyle, getPrefixedEvent } from './prefixUtil';
var animation = {
    styleName: null,
    endEvent: null,
};
withDummyElement(function (el) {
    animation.styleName = getPrefixedStyle('animation', function (name, prefix) {
        animation.endEvent = getPrefixedEvent('AnimationEnd', prefix);
    });
});
export default animation;
