export var prefixes = ['ms', 'Webkit', 'Moz', 'O'];
var el;
export function eachPrefix(name, callback) {
    if (callback(name)) {
        return name;
    }
    name = name.substr(0, 1).toUpperCase() + name.substr(1);
    for (var index = 0, count = prefixes.length; index < count; index++) {
        var prefix = prefixes[index];
        var prefixedName = prefix + name;
        if (callback(prefixedName, prefix)) {
            if (index > 0) {
                prefixes.splice(index, 1);
                prefixes.unshift(prefix);
            }
            return prefixedName;
        }
    }
    return null;
}
export function withDummyElement(callback) {
    var isCreator;
    if (!el) {
        isCreator = true;
        el = document.createElement('div');
        document.body.insertBefore(el, null);
    }
    var result = callback(el);
    if (isCreator) {
        document.body.removeChild(el);
        el = void 0;
    }
    return result;
}
export function getPrefixedStyle(name, callback) {
    return withDummyElement(function (el) {
        return eachPrefix(name, function (name, prefix) {
            if (name in el.style) {
                if (callback)
                    callback(name, prefix);
                return true;
            }
            else {
                return false;
            }
        });
    });
}
export function getPrefixedEvent(name, prefix) {
    if (prefix == 'Webkit')
        return 'webkit' + name;
    if (prefix == 'O')
        return 'o' + name;
    return name.toLowerCase();
}
