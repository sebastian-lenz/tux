import View from '../../views/View';
import { IRectangle } from '../../geom/Rectangle';
import ChildableView from '../../views/ChildableView';
export declare enum DifferenceAction {
    None = 0,
    Changed = 1,
    Appeared = 2,
    Disappeared = 3,
}
export interface IDifferenceState {
    rect: IRectangle;
    hidden: boolean;
}
export interface IDifferenceStateCallback<T> {
    (target: T): IDifferenceState;
}
export interface IDifferenceResult<T> {
    target: T;
    action: DifferenceAction;
    oldState: IDifferenceState;
    newState: IDifferenceState;
}
export declare function normalizeRect(rect: any): IRectangle;
export declare function rectEquals(a: IRectangle, b: IRectangle): boolean;
export declare function getDifference<T>(callback: Function, targets: T[], toState: IDifferenceStateCallback<T>): IDifferenceResult<T>[];
export interface IDomDifferenceOptions {
    callback: Function;
    context: HTMLElement;
    selector: string;
    isHidden: {
        (element: HTMLElement): boolean;
    };
}
export declare function getDomDifference(options: IDomDifferenceOptions): IDifferenceResult<HTMLElement>[];
export interface IViewDifferenceOptions<T extends View> {
    callback: Function;
    view: ChildableView<T>;
    isHidden: {
        (view: T): boolean;
    };
}
export declare function getViewDifference<T extends View>(options: IViewDifferenceOptions<T>): IDifferenceResult<T>[];
