import { map } from 'underscore';
export var DifferenceAction;
(function (DifferenceAction) {
    DifferenceAction[DifferenceAction["None"] = 0] = "None";
    DifferenceAction[DifferenceAction["Changed"] = 1] = "Changed";
    DifferenceAction[DifferenceAction["Appeared"] = 2] = "Appeared";
    DifferenceAction[DifferenceAction["Disappeared"] = 3] = "Disappeared";
})(DifferenceAction || (DifferenceAction = {}));
export function normalizeRect(rect) {
    return {
        x: rect.left,
        y: rect.top,
        width: rect.right - rect.left,
        height: rect.bottom - rect.top,
    };
}
export function rectEquals(a, b) {
    return ((a.x == b.x) && (a.y == b.y) && (a.width == b.width) && (a.height == b.height));
}
export function getDifference(callback, targets, toState) {
    var oldStates = map(targets, toState);
    callback();
    return map(targets, function (target, index) {
        var oldState = oldStates[index];
        var newState = toState(target);
        var action;
        if (oldState.hidden == newState.hidden) {
            if (rectEquals(oldState.rect, newState.rect)) {
                action = DifferenceAction.None;
            }
            else {
                action = DifferenceAction.Changed;
            }
        }
        else if (newState.hidden) {
            action = DifferenceAction.Disappeared;
        }
        else {
            action = DifferenceAction.Appeared;
        }
        return {
            target: target,
            action: action,
            oldState: oldState,
            newState: newState,
        };
    });
}
export function getDomDifference(options) {
    var callback = options.callback, context = options.context, selector = options.selector, isHidden = options.isHidden;
    var nodeList = context.querySelectorAll(selector);
    var elements = [];
    for (var index = 0, count = nodeList.length; index < count; index++) {
        elements.push(nodeList[index]);
    }
    return getDifference(callback, elements, function (element) { return ({
        rect: normalizeRect(element.getBoundingClientRect()),
        hidden: isHidden(element),
    }); });
}
export function getViewDifference(options) {
    var callback = options.callback, view = options.view, isHidden = options.isHidden;
    return getDifference(callback, view.children, function (child) { return ({
        rect: normalizeRect(child.element.getBoundingClientRect()),
        hidden: isHidden(child),
    }); });
}
