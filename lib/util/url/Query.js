import { isArray } from 'underscore';
var Query = (function () {
    function Query(query) {
        this.values = {};
        if (query) {
            this.parse(query);
        }
    }
    Query.prototype.setValue = function (key, value) {
        this.values[key] = value;
        return this;
    };
    Query.prototype.getValue = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = null; }
        return this.values[key] || defaultValue;
    };
    Query.prototype.remove = function (key) {
        delete this.values[key];
        return this;
    };
    Query.prototype.toString = function () {
        var values = this.values;
        var parts = [];
        for (var key in values) {
            if (!values.hasOwnProperty(key)) {
                continue;
            }
            var encodedKey = encodeURIComponent(key);
            var value = values[key];
            if (isArray(value)) {
                for (var index = 0; index < value.length; index++) {
                    parts.push(encodedKey + "=" + encodeURIComponent(value[index]));
                }
            }
            else {
                parts.push(encodedKey + "=" + encodeURIComponent(value));
            }
        }
        return parts.join('&');
    };
    Query.prototype.parse = function (query) {
        var parts = query.split('&');
        var values = {};
        for (var index = 0; index < parts.length; index++) {
            var args = parts[index].split('=', 2);
            var key = decodeURIComponent(args[0]);
            var value = decodeURIComponent(args[1]);
            if (key in values) {
                if (isArray(values[key])) {
                    values[key].push(value);
                }
                else {
                    values[key] = [values[key], value];
                }
            }
            else {
                values[key] = value;
            }
        }
        this.values = values;
    };
    return Query;
}());
export { Query };
