import { Query } from './Query';
export declare class Url {
    url: string;
    query: Query;
    hash: string;
    constructor(url?: string);
    toString(): string;
    parse(url: string): void;
}
