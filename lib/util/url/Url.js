import { Query } from './Query';
var Url = (function () {
    function Url(url) {
        this.query = new Query();
        if (url) {
            this.parse(url);
        }
    }
    Url.prototype.toString = function () {
        var parts = [this.url];
        var search = this.query.toString();
        if (search != '') {
            parts.push('?', search);
        }
        if (this.hash) {
            parts.push('#', this.hash);
        }
        return parts.join('');
    };
    Url.prototype.parse = function (url) {
        var hashIndex = url.indexOf('#');
        if (hashIndex != -1) {
            this.hash = url.substring(hashIndex + 1);
            url = url.substr(0, hashIndex);
        }
        else {
            this.hash = null;
        }
        var searchIndex = url.indexOf('?');
        if (searchIndex != -1) {
            this.query.parse(url.substring(searchIndex + 1));
            url = url.substr(0, searchIndex);
        }
        else {
            this.query.values = {};
        }
        this.url = url;
    };
    return Url;
}());
export { Url };
