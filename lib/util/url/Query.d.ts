export declare class Query {
    values: Object;
    constructor(query?: string);
    setValue(key: string, value: string | string[]): this;
    getValue(key: string, defaultValue?: string): string;
    remove(key: string): this;
    toString(): string;
    parse(query: string): void;
}
