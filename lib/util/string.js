function defaultToWhiteSpace(characters) {
    if (characters == null) {
        return '\\s';
    }
    else if (characters.source) {
        return characters.source;
    }
    else {
        return '[' + escapeRegExp(characters) + ']';
    }
}
export function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
}
export function trim(str, characters) {
    characters = defaultToWhiteSpace(characters);
    return str.replace(new RegExp('^' + characters + '+|' + characters + '+$', 'g'), '');
}
export function trimStart(str, characters) {
    characters = defaultToWhiteSpace(characters);
    return str.replace(new RegExp('^' + characters + '+'), '');
}
export function dasherize(str) {
    return trim(str).replace(/([A-Z])/g, '-$1').replace(/[-_\s]+/g, '-').toLowerCase();
}
