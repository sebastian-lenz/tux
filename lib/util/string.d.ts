export declare function escapeRegExp(str: string): string;
export declare function trim(str: string, characters?: string): string;
export declare function trimStart(str: string, characters?: string): string;
export declare function dasherize(str: string): string;
