/*
 * classList.js: Cross-browser full element.classList implementation.
 * 1.1.20150312
 *
 * By Eli Grey, http://eligrey.com
 * License: Dedicated to the public domain.
 *   See https://github.com/eligrey/classList.js/blob/master/LICENSE.md
 */
import * as tslib_1 from "tslib";
import { indexOf } from 'underscore';
import { trim } from '../string';
/**
 * Most DOMException implementations don't allow calling DOMException's toString()
 * on non-DOMExceptions. Error's toString() is sufficient here.
 *
 * Vendors: please allow content code to instantiate DOMExceptions
 */
var DOMError = (function (_super) {
    tslib_1.__extends(DOMError, _super);
    /**
     * Create a new DomEx instance.
     */
    function DOMError(type, message) {
        var _this = _super.call(this) || this;
        _this.name = type;
        _this.code = DOMException[type];
        _this.message = message;
        return _this;
    }
    return DOMError;
}(Error));
/**
 * Class list polyfill.
 */
var ClassList = (function () {
    function ClassList(element) {
        var attr = trim(element.getAttribute('class') || '');
        var classes = attr ? attr.split(/\s+/) : [];
        this.element = element;
        this.classes = classes;
    }
    ClassList.prototype.updateClassName = function () {
        this.element.setAttribute('class', this.toString());
    };
    ClassList.prototype.checkTokenAndGetIndex = function (token) {
        if (token === '') {
            throw new DOMError('SYNTAX_ERR', 'An invalid or illegal string was specified');
        }
        if (/\s/.test(token)) {
            throw new DOMError('INVALID_CHARACTER_ERR', 'String contains an invalid character');
        }
        return indexOf(this.classes, token);
    };
    ClassList.prototype.item = function (index) {
        return this.classes[index] || null;
    };
    ClassList.prototype.contains = function (token) {
        token += '';
        return this.checkTokenAndGetIndex(token) !== -1;
    };
    ClassList.prototype.add = function () {
        var updated = false;
        for (var n = 0, c = arguments.length; n < c; n++) {
            var token = arguments[n] + '';
            if (this.checkTokenAndGetIndex(token) === -1) {
                this.classes.push(token);
                updated = true;
            }
        }
        if (updated) {
            this.updateClassName();
        }
    };
    ClassList.prototype.remove = function () {
        var updated = false;
        for (var n = 0, c = arguments.length; n < c; n++) {
            var token = arguments[n] + '';
            var index = this.checkTokenAndGetIndex(token);
            while (index !== -1) {
                this.classes.splice(index, 1);
                updated = true;
                index = this.checkTokenAndGetIndex(token);
            }
        }
        if (updated) {
            this.updateClassName();
        }
    };
    ClassList.prototype.toggle = function (token, force) {
        token += '';
        var result = this.contains(token);
        var method = result
            ? force !== true && 'remove'
            : force !== false && 'add';
        if (method) {
            this[method](token);
        }
        if (force === true || force === false) {
            return force;
        }
        else {
            return !result;
        }
    };
    ClassList.prototype.toString = function () {
        return this.classes.join(' ');
    };
    return ClassList;
}());
/**
 * Getter callback for the class list property.
 */
function getClassList() {
    return new ClassList(this);
}
/**
 * Full polyfill for browsers with no classList support
 * Including IE < Edge missing SVGElement.classList
 */
function polyfill(scope) {
    if (!('Element' in scope))
        return;
    var elementPrototype = scope.Element.prototype;
    if (Object.defineProperty) {
        var property = {
            get: getClassList,
            enumerable: true,
            configurable: true,
        };
        try {
            Object.defineProperty(elementPrototype, 'classList', property);
        }
        catch (ex) {
            // IE 8 doesn't support enumerable:true
            if (ex.number === -0x7FF5EC54) {
                property.enumerable = false;
                Object.defineProperty(elementPrototype, 'classList', property);
            }
        }
    }
    else if ('__defineGetter__' in Object.prototype) {
        elementPrototype['__defineGetter__']('classList', getClassList);
    }
}
/**
 * There is full or partial native classList support, so just check if we need
 * to normalize the add/remove and toggle APIs.
 */
function normalize() {
    var testElement = document.createElement('_');
    testElement.classList.add('c1', 'c2');
    // Polyfill for IE 10/11 and Firefox <26, where classList.add and
    // classList.remove exist but support only one argument at a time.
    if (!testElement.classList.contains('c2')) {
        var createMethod = function (target, method) {
            var original = target[method];
            target[method] = function (token) {
                var i, len = arguments.length;
                for (i = 0; i < len; i++) {
                    token = arguments[i];
                    original.call(this, token);
                }
            };
        };
        createMethod(DOMTokenList.prototype, 'add');
        createMethod(DOMTokenList.prototype, 'remove');
    }
    testElement.classList.toggle('c3', false);
    // Polyfill for IE 10 and Firefox <24, where classList.toggle does not
    // support the second argument.
    if (testElement.classList.contains('c3')) {
        var _toggle = DOMTokenList.prototype.toggle;
        DOMTokenList.prototype.toggle = function (token, force) {
            if (1 in arguments && !this.contains(token) === !force) {
                return force;
            }
            else {
                return _toggle.call(this, token);
            }
        };
    }
    testElement = null;
}
/**
 * Test whether the classList property is supported.
 */
function isClassListSupported() {
    var el = document.createElement('div');
    return 'classList' in el;
}
/**
 * Initialize the polyfill.
 */
if ('document' in self) {
    if (!isClassListSupported()) {
        polyfill(self);
    }
    else {
        normalize();
    }
}
