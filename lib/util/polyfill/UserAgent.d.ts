export declare var UserAgent: {
    ie: () => any;
    ieCompatibilityMode: () => boolean;
    ie64: () => any;
    firefox: () => any;
    opera: () => any;
    webkit: () => any;
    safari: () => any;
    chrome: () => any;
    windows: () => any;
    osx: () => any;
    linux: () => any;
    iphone: () => any;
    mobile: () => any;
    nativeApp: () => any;
    android: () => any;
    ipad: () => any;
};
