export function whenDomReady(callback) {
    if ('addEventListener' in document) {
        document.addEventListener('DOMContentLoaded', callback);
    }
    else {
        window.attachEvent('onload', callback);
    }
}
