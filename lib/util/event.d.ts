/**
 * Return the nearest dom element from the given event containing the given
 * css class name.
 *
 * @param event
 *   The event whose target should be analyzed.
 * @param className
 *   The name of the css class we should look for,
 * @returns
 *   The found element or NULL if the class name could not be matched.
 */
export declare function getTargetByClass(event: Event, className: string): HTMLElement;
/**
 * Return the nearest dom element from the given event target with the given node type.
 *
 * @param event
 *   The event whose target should be analyzed.
 * @param type
 *   The node type we should look for.
 * @returns
 *   The found element or NULL if the class name could not be matched.
 */
export declare function getTargetByType(event: Event, type: string): HTMLElement;
