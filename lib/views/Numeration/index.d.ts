import View, { ViewOptions } from '../View';
import CycleableView from '../CycleableView';
/**
 * Constructor options for the Numeration class.
 */
export interface NumerationOptions extends ViewOptions {
    /**
     * The CycleableView that should be controlled.
     */
    owner?: CycleableView;
    itemClass?: string;
    selectedItemClass?: string;
}
export default class Numeration extends View {
    /**
     * The CycleableView that should be controlled.
     */
    view: CycleableView;
    /**
     * A list of items wihtin the numeration.
   */
    items: HTMLLIElement[];
    /**
     * The index of the currently highlighted item.
   */
    index: number;
    itemClass?: string;
    selectedItemClass?: string;
    /**
     * Numeration constructor.
     *
     * @param options
     *   The constructor options.
   */
    constructor(options?: NumerationOptions);
    /**
     * Set the CycleableView that should be controlled.
     *
     * @param view
     *   The CycleableView that should be controlled.
     */
    setView(view: CycleableView): this;
    /**
     * Set the number of children of the controlled view.
     *
     * @param length
     *   The number of children of the controlled view.
     */
    setLength(length: number): this;
    /**
     * Set the index of the current item.
     *
     * @param index
     *   The index of the current item.
     */
    setIndex(index: number): this;
    /**
     * Triggered if the user clicks on this view.
     *
     * @param event
     *   The triggering event.
     */
    handleClick(event: Event, target: HTMLElement): boolean;
    handleSelectIndex(index: number): void;
    /**
     * Triggered after the current index of the view has changed.
     */
    handleCurrentChanged(item: View): void;
    handleLengthChanged(): void;
}
