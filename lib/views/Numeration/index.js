import * as tslib_1 from "tslib";
import { defaults, indexOf } from 'underscore';
import View from '../View';
import CycleableView from '../CycleableView';
var Numeration = (function (_super) {
    tslib_1.__extends(Numeration, _super);
    /**
     * Numeration constructor.
     *
     * @param options
     *   The constructor options.
   */
    function Numeration(options) {
        var _this = _super.call(this, options = defaults(options || {}, {
            tagName: 'ul',
            className: 'tuxNumeration',
            itemClass: 'tuxNumeration__item',
            selectedItemClass: 'current',
        })) || this;
        /**
         * A list of items wihtin the numeration.
       */
        _this.items = [];
        /**
         * The index of the currently highlighted item.
       */
        _this.index = -1;
        _this.itemClass = options.itemClass;
        _this.selectedItemClass = options.selectedItemClass;
        _this.delegateClick(_this.handleClick, { selector: "." + options.itemClass });
        if (options.owner instanceof CycleableView) {
            _this.setView(options.owner);
        }
        return _this;
    }
    /**
     * Set the CycleableView that should be controlled.
     *
     * @param view
     *   The CycleableView that should be controlled.
     */
    Numeration.prototype.setView = function (view) {
        if (this.view == view)
            return this;
        if (this.view) {
            this.stopListening(this.view);
        }
        if (view) {
            this
                .setLength(view.getLength())
                .setIndex(view.getCurrentIndex())
                .listenTo(view, 'currentChanged', this.handleCurrentChanged)
                .listenTo(view, 'currentWillChange', this.handleCurrentChanged)
                .listenTo(view, 'add', this.handleLengthChanged)
                .listenTo(view, 'remove', this.handleLengthChanged)
                .listenTo(view, 'reset', this.handleLengthChanged);
        }
        this.view = view;
        return this;
    };
    /**
     * Set the number of children of the controlled view.
     *
     * @param length
     *   The number of children of the controlled view.
     */
    Numeration.prototype.setLength = function (length) {
        var items = this.items;
        var itemsLength = items.length;
        if (itemsLength == length) {
            return this;
        }
        while (itemsLength < length) {
            var item = document.createElement('li');
            item.className = this.itemClass;
            item.innerText = "" + (itemsLength + 1);
            items.push(item);
            itemsLength += 1;
            this.element.appendChild(item);
        }
        while (itemsLength > length) {
            this.element.removeChild(items.pop());
            itemsLength -= 1;
            if (this.index == itemsLength) {
                this.index = -1;
            }
        }
        return this;
    };
    /**
     * Set the index of the current item.
     *
     * @param index
     *   The index of the current item.
     */
    Numeration.prototype.setIndex = function (index) {
        var _a = this, items = _a.items, selectedItemClass = _a.selectedItemClass;
        index = Math.max(-1, Math.min(items.length - 1, Math.floor(index)));
        if (this.index == index)
            return this;
        if (this.index != -1) {
            items[this.index].classList.remove(selectedItemClass);
        }
        this.index = index;
        if (index != -1) {
            items[index].classList.add(selectedItemClass);
        }
        return this;
    };
    /**
     * Triggered if the user clicks on this view.
     *
     * @param event
     *   The triggering event.
     */
    Numeration.prototype.handleClick = function (event, target) {
        var index = indexOf(this.items, target);
        if (index !== -1) {
            this.handleSelectIndex(index);
        }
        return true;
    };
    Numeration.prototype.handleSelectIndex = function (index) {
        this.trigger('select', index);
        if (this.view) {
            this.view.setCurrentIndex(index);
        }
    };
    /**
     * Triggered after the current index of the view has changed.
     */
    Numeration.prototype.handleCurrentChanged = function (item) {
        this.setIndex(this.view.indexOf(item));
    };
    Numeration.prototype.handleLengthChanged = function () {
        this.setLength(this.view.getLength());
    };
    return Numeration;
}(View));
export default Numeration;
