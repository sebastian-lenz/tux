import View, { ViewOptions } from '../View';
import Label, { LabelOptions } from './Label';
import List, { Direction, ListOptions } from './List';
import Option from './Option';
export declare type LabelClass = {
    new (options: LabelOptions): Label;
};
export declare type ListClass = {
    new (options: ListOptions): List;
};
export interface DropdownOptions extends ViewOptions {
    select?: string | HTMLSelectElement;
    labelClass?: LabelClass;
    labelOptions?: LabelOptions;
    listClass?: ListClass;
    listOptions?: ListOptions;
}
export default class Dropdown extends View {
    defaultDirection: Direction;
    navigate: boolean;
    select: HTMLSelectElement;
    isExpanded: boolean;
    label: Label;
    list: List;
    selected: Option | null;
    constructor(options: DropdownOptions);
    setExpanded(value: boolean): void;
    setSelected(option: Option | null, silent?: boolean): void;
    handleChange(): void;
    handleDocumentPointerDown(event: Event): void;
    handleFocusOut(event: Event): void;
    handleKeyPress(event: KeyboardEvent): void;
    handleLabelClick(): void;
    handleListClick(option: Option): void;
}
