import ChildableView, { ChildableViewOptions } from '../ChildableView';
export interface GroupOptions extends ChildableViewOptions {
    containerClassName?: string;
    group?: HTMLOptGroupElement;
    labelClassName?: string;
}
export default class Group extends ChildableView {
    label: HTMLElement;
    container: HTMLElement;
    constructor(options: GroupOptions);
    setCaption(value: string): void;
}
