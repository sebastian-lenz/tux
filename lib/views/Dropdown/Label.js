import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import View, { option } from '../View';
var Label = (function (_super) {
    tslib_1.__extends(Label, _super);
    function Label(options) {
        var _this = _super.call(this, defaults(options, {
            className: 'tuxDropDownLabel',
        })) || this;
        _this.extraClass = null;
        _this.option = null;
        _this.delegateClick(_this.handleClick);
        return _this;
    }
    Label.prototype.setExtraClass = function (extraClass) {
        if (this.extraClass === extraClass)
            return;
        if (this.extraClass)
            this.removeClass(this.extraClass);
        this.extraClass = extraClass;
        if (extraClass)
            this.addClass(extraClass);
    };
    Label.prototype.setOption = function (option) {
        if (this.option === option)
            return;
        var _a = this, element = _a.element, emptyLabel = _a.emptyLabel;
        element.innerHTML = option ? option.caption : emptyLabel;
        this.setExtraClass(option ? option.extraClass : 'empty');
        this.option = option;
    };
    Label.prototype.handleClick = function () {
        this.trigger('click');
        return true;
    };
    return Label;
}(View));
export default Label;
tslib_1.__decorate([
    option({ type: 'string', defaultValue: '-' })
], Label.prototype, "emptyLabel", void 0);
