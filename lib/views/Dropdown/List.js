import * as tslib_1 from "tslib";
import { defaults, find, forEach } from 'underscore';
import { option } from '../View';
import ChildableView from '../ChildableView';
import Viewport from '../../services/Viewport';
import Group from './Group';
import Option from './Option';
export var Direction;
(function (Direction) {
    Direction[Direction["Bottom"] = 0] = "Bottom";
    Direction[Direction["Top"] = 1] = "Top";
})(Direction || (Direction = {}));
;
var List = (function (_super) {
    tslib_1.__extends(List, _super);
    function List(options) {
        var _this = _super.call(this, defaults(options, {
            className: 'tuxDropDownList',
        })) || this;
        _this.direction = Direction.Bottom;
        _this.isExpanded = false;
        _this.options = [];
        _this.dropdown = options.owner;
        _this.delegateClick(_this.handleClick);
        return _this;
    }
    List.prototype.setExpanded = function (value) {
        if (this.isExpanded === value)
            return;
        this.isExpanded = value;
        if (value)
            this.updateDirection();
        this.toggleClass('expanded', value);
    };
    List.prototype.sync = function (select) {
        var _this = this;
        var _a = this, optionClass = _a.optionClass, optionOptions = _a.optionOptions, groupClass = _a.groupClass, groupOptions = _a.groupOptions;
        if (!select)
            return;
        this.removeAllChildren();
        this.options.length = 0;
        forEach(select.childNodes, function (node) {
            if (node.nodeType !== Node.ELEMENT_NODE)
                return;
            var child = null;
            if (node.nodeName === 'OPTION') {
                child = _this.syncOption(node);
            }
            else if (node.nodeName === 'OPTGROUP') {
                child = _this.syncGroup(node);
            }
            if (child)
                _this.addChild(child);
        });
    };
    List.prototype.syncGroup = function (group) {
        var _this = this;
        var _a = this, groupClass = _a.groupClass, groupOptions = _a.groupOptions;
        var options = defaults({}, groupOptions, {
            group: group,
            owner: this,
        });
        var child = new groupClass(options);
        forEach(group.childNodes, function (node) {
            if (node.nodeType !== Node.ELEMENT_NODE)
                return;
            if (node.nodeName !== 'OPTION')
                return;
            var option = _this.syncOption(node);
            child.addChild(option);
        });
        return child;
    };
    List.prototype.syncOption = function (option) {
        var _a = this, optionClass = _a.optionClass, optionOptions = _a.optionOptions;
        var options = defaults({}, optionOptions, {
            option: option,
            owner: this,
        });
        var child = new optionClass(options);
        this.options.push(child);
        return child;
    };
    List.prototype.updateDirection = function () {
        var dropdown = this.dropdown;
        var direction = this.direction;
        if (dropdown.defaultDirection) {
            direction = dropdown.defaultDirection;
        }
        else {
            var viewport = Viewport.getInstance();
            var top_1 = dropdown.element.getBoundingClientRect().top;
            direction = top_1 > viewport.height * 0.5
                ? Direction.Top
                : Direction.Bottom;
        }
        if (this.direction === direction)
            return;
        this.direction = direction;
        this.toggleClass('top', direction === Direction.Top);
    };
    List.prototype.handleClick = function (event) {
        var _a = this, element = _a.element, options = _a.options;
        var target = event.target;
        for (; target && target !== element; target = target.parentNode) {
            var option_1 = find(options, function (option) { return option.element === target; });
            if (option_1) {
                this.trigger('click', option_1);
                return true;
            }
        }
        return false;
    };
    return List;
}(ChildableView));
export default List;
tslib_1.__decorate([
    option({ type: 'any', defaultValue: Group })
], List.prototype, "groupClass", void 0);
tslib_1.__decorate([
    option({ type: 'any', defaultValue: {} })
], List.prototype, "groupOptions", void 0);
tslib_1.__decorate([
    option({ type: 'any', defaultValue: Option })
], List.prototype, "optionClass", void 0);
tslib_1.__decorate([
    option({ type: 'any', defaultValue: {} })
], List.prototype, "optionOptions", void 0);
