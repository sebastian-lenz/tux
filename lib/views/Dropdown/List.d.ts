import ChildableView, { ChildableViewOptions } from '../ChildableView';
import Dropdown from './index';
import Group, { GroupOptions } from './Group';
import Option, { OptionOptions } from './Option';
export declare type ListChild = Group | Option;
export declare type GroupClass = {
    new (options: GroupOptions): Group;
};
export declare type OptionClass = {
    new (options: OptionOptions): Option;
};
export declare enum Direction {
    Bottom = 0,
    Top = 1,
}
export interface ListOptions extends ChildableViewOptions {
    groupClass?: GroupClass;
    groupOptions?: GroupOptions;
    optionClass?: OptionClass;
    optionOptions?: OptionOptions;
    owner: Dropdown;
}
export default class List extends ChildableView<ListChild> {
    groupClass: GroupClass;
    groupOptions: GroupOptions;
    optionClass: OptionClass;
    optionOptions: OptionOptions;
    direction: Direction;
    dropdown: Dropdown;
    isExpanded: boolean;
    options: Option[];
    constructor(options: ListOptions);
    setExpanded(value: boolean): void;
    sync(select: HTMLSelectElement): void;
    protected syncGroup(group: HTMLOptionElement): Group;
    protected syncOption(option: HTMLOptGroupElement): Option;
    updateDirection(): void;
    handleClick(event: Event): boolean;
}
