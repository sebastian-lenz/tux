import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import ChildableView from '../ChildableView';
var Group = (function (_super) {
    tslib_1.__extends(Group, _super);
    function Group(options) {
        var _this = _super.call(this, options = defaults(options, {
            className: 'tuxDropDownGroup',
        })) || this;
        var element = _this.element;
        var className = options.className, containerClassName = options.containerClassName, group = options.group, labelClassName = options.labelClassName;
        var label = _this.label = document.createElement('div');
        label.className = labelClassName || className + "--label";
        element.appendChild(label);
        var container = _this.container = document.createElement('div');
        container.className = containerClassName || className + "--container";
        element.appendChild(container);
        if (group) {
            _this.setCaption(group.label);
        }
        return _this;
    }
    Group.prototype.setCaption = function (value) {
        this.label.innerHTML = value;
    };
    return Group;
}(ChildableView));
export default Group;
