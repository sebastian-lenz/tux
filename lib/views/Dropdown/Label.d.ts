import View, { ViewOptions } from '../View';
import Option from './Option';
export interface LabelOptions extends ViewOptions {
    emptyLabel?: string;
}
export default class Label extends View {
    emptyLabel: string;
    extraClass: string | null;
    option: Option | null;
    constructor(options: LabelOptions);
    setExtraClass(extraClass: string): void;
    setOption(option: Option | null): void;
    handleClick(): boolean;
}
