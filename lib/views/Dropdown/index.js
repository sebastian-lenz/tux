import * as tslib_1 from "tslib";
import { defaults, findIndex, forEach } from 'underscore';
import DocumentView from '../../services/DocumentView';
import View, { option } from '../View';
import Label from './Label';
import List, { Direction } from './List';
var Dropdown = (function (_super) {
    tslib_1.__extends(Dropdown, _super);
    function Dropdown(options) {
        var _this = _super.call(this, options = defaults(options, {
            labelClass: Label,
            labelOptions: {},
            listClass: List,
            listOptions: {},
        })) || this;
        _this.isExpanded = false;
        var labelClass = options.labelClass, labelOptions = options.labelOptions, listClass = options.listClass, listOptions = options.listOptions;
        var select = _this.select;
        var label = _this.label = new labelClass(defaults({}, labelOptions, {
            appendTo: _this.element,
            owner: _this,
        }));
        var list = _this.list = new listClass(defaults({}, listOptions, {
            appendTo: _this.element,
            owner: _this,
        }));
        if (select) {
            list.sync(select);
            _this.setSelected(list.options[select.selectedIndex] || null, true);
        }
        else {
            _this.setSelected(null, true);
        }
        _this.element.tabIndex = -1;
        _this.listenTo(label, 'click', _this.handleLabelClick);
        _this.listenTo(list, 'click', _this.handleListClick);
        _this.delegate('focusout', _this.handleFocusOut);
        _this.delegate('keypress', _this.handleKeyPress);
        _this.delegate('change', _this.handleChange, { selector: 'select' });
        return _this;
    }
    Dropdown.prototype.setExpanded = function (value) {
        if (this.isExpanded === value)
            return;
        this.isExpanded = value;
        this.list.setExpanded(value);
        var documentView = DocumentView.getInstance();
        if (value) {
            this.listenTo(documentView, 'pointerDown', this.handleDocumentPointerDown);
        }
        else {
            this.stopListening(documentView);
        }
    };
    Dropdown.prototype.setSelected = function (option, silent) {
        var _a = this, label = _a.label, list = _a.list, select = _a.select;
        this.selected = option;
        if (select) {
            var index = findIndex(list.options, option);
            select.selectedIndex = index;
        }
        label.setOption(option);
        this.setExpanded(false);
        forEach(list.options, function (child) {
            return child.setSelected(child === option);
        });
        if (!silent) {
            this.trigger('select', option);
            if (this.navigate) {
                window.location.href = option.value;
            }
        }
    };
    Dropdown.prototype.handleChange = function () {
        var _a = this, list = _a.list, select = _a.select;
        if (select) {
            this.setSelected(list.options[select.selectedIndex] || null);
        }
    };
    Dropdown.prototype.handleDocumentPointerDown = function (event) {
        var element = this.element;
        var target = event.target;
        while (target) {
            if (target === element)
                return;
            target = target.parentNode;
        }
        this.setExpanded(false);
    };
    Dropdown.prototype.handleFocusOut = function (event) {
        this.setExpanded(false);
    };
    Dropdown.prototype.handleKeyPress = function (event) {
    };
    Dropdown.prototype.handleLabelClick = function () {
        this.setExpanded(true);
    };
    Dropdown.prototype.handleListClick = function (option) {
        this.setSelected(option);
    };
    return Dropdown;
}(View));
export default Dropdown;
tslib_1.__decorate([
    option({ type: 'enum', values: Direction })
], Dropdown.prototype, "defaultDirection", void 0);
tslib_1.__decorate([
    option({ type: 'bool', defaultValue: false })
], Dropdown.prototype, "navigate", void 0);
tslib_1.__decorate([
    option({ type: 'element', defaultValue: 'select' })
], Dropdown.prototype, "select", void 0);
