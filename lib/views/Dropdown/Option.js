import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import View from '../View';
var Option = (function (_super) {
    tslib_1.__extends(Option, _super);
    function Option(options) {
        var _this = _super.call(this, defaults(options, {
            className: 'tuxDropDownItem',
        })) || this;
        _this.caption = '';
        _this.extraClass = '';
        _this.isSelected = false;
        _this.value = '';
        var option = options.option;
        if (option) {
            _this.value = option.value;
            _this.setCaption(option.innerHTML);
            if (option.hasAttribute('data-class')) {
                _this.addClass(_this.extraClass = option.getAttribute('data-class'));
            }
        }
        return _this;
    }
    Option.prototype.setCaption = function (value) {
        this.caption = value;
        this.element.innerHTML = value;
    };
    Option.prototype.setSelected = function (value) {
        if (this.isSelected === value)
            return;
        this.isSelected = value;
        this.toggleClass('selected', value);
    };
    return Option;
}(View));
export default Option;
