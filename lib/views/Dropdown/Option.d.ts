import View, { ViewOptions } from '../View';
export interface OptionOptions extends ViewOptions {
    option?: HTMLOptionElement;
}
export default class Option extends View {
    caption: string;
    extraClass: string;
    isSelected: boolean;
    value: string;
    constructor(options: OptionOptions);
    setCaption(value: string): void;
    setSelected(value: boolean): void;
}
