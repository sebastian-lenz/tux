import component, { Registry } from './component';
import { ClickHandlerCallback } from '../../handler/ClickHandler';
import Delegate, { DelegateOptions } from './Delegate';
import Model from '../../models/Model';
import { child, children, option } from './initializer';
import '../../util/polyfill/classList';
import '../../util/polyfill/eventListener';
import '../../util/polyfill/matches';
export { child, children, component, option, Registry };
/**
 * Constructor options of the View class.
 */
export interface ViewOptions {
    appendTo?: HTMLElement;
    attributes?: any;
    className?: string;
    components?: View[];
    element?: HTMLElement;
    owner?: View;
    tagName?: string;
    template?: string | Function;
    model?: Model;
}
/**
 * Base class of all views.
 */
export default class View extends Delegate {
    element: HTMLElement;
    model: Model;
    cid: string;
    protected isRemoved: boolean;
    private _initializers;
    /**
     * View constructor.
     *
     * @param options
     *   Thew view constructor options.
     */
    constructor(options?: ViewOptions);
    /**
     * Ensures this view has a html element.
     */
    private ensureElement(options);
    /**
     * Remove this view.
     */
    remove(): this;
    delegateClick(listener: ClickHandlerCallback, options?: DelegateOptions): this;
    /**
     * Triggered after the size of the viewport has changed.
     *
     * If this view is used as a component this handler will be called automatically.
     *
     * @param resizeChildren
     *   A callback that triggers the resize event on all child components.
     * @returns
     *   TRUE if the event handling should be stopped and child components should not receive
     *   the event, FALSE otherwise.
     */
    handleViewportResize(resizeChildren?: Function): boolean;
    query<K extends keyof ElementTagNameMap>(selectors: K): ElementTagNameMap[K] | null;
    query(selectors: string): HTMLElement | null;
    queryAll<K extends keyof ElementListTagNameMap>(selectors: K): ElementListTagNameMap[K];
    queryAll(selectors: string): NodeListOf<Element>;
    addClass(...token: string[]): this;
    containsClass(token: string): boolean;
    removeClass(...token: string[]): this;
    toggleClass(token: string, force?: boolean): boolean;
}
