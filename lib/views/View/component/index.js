import { defaults } from 'underscore';
import Node from './Node';
import Registry from './Registry';
export { Node, Registry };
/**
 * A class annotation that registers view classes as components.
 *
 * @param selector
 *   A css selector that selects all elements the component should be attached to.
 * @param options
 *   Additional component options.
 */
export default function component(selector, options) {
    options = defaults(options || {}, {
        allowChildComponents: false
    });
    return function (viewClass) {
        Registry.getInstance().register(viewClass, selector, options);
        return viewClass;
    };
}
