import View from '../index';
import { Component } from './index';
/**
 * A tree structure for discovering and storing component/element relationships.
 */
export default class Node {
    /**
     * The dom element this node represents.
     */
    element: HTMLElement;
    /**
     * A list of all child nodes attached to this node.
     */
    children: Node[];
    /**
     * The component info attached to this node.
     */
    component: Component;
    /**
     * The instance of the component attached to this node.
     */
    instance: View;
    /**
     * Whether the create method has already been called or not.
     */
    isCreated: boolean;
    /**
     * Whether the component has a resize handler or not.
     */
    hasResizeHandler: boolean;
    /**
     * ComponentTree constructor.
     *
     * @param element
     *   The dom element the created tree node represents.
     */
    constructor(element: HTMLElement);
    /**
     * Search for all children of this element matching the given component specification.
     * The found children will be added to the children property of this instance.
     *
     * @param component
     *   The component specification that should be queried for.
     */
    discover(component: Component): void;
    /**
     * Instantiate the component of this node and all of its children.
     *
     * @param result
     *   An optional array the results will be appended to.
     * @returns
     *   A list of all newly created components, existing components will be
     *   omitted, use getComponents() to retrieve all existing components.
     */
    create(result?: View[]): View[];
    /**
     * Find or create the node for the given element.
     *
     * @param element
     *   The element whose node should be retrieved.
     * @returns
     *   The component node representing the given dom element.
     */
    getNode(element: HTMLElement): Node;
    /**
     * Manually set the view instance of this node.
     *
     * @param instance
     *   The view instance of this node.
     */
    setInstance(instance: View): void;
    /**
     * Return all ancestors of the given element until the element of this node is reached.
     *
     * @param element
     *   The dom element whose ancestors should be retrieved.
     * @returns
     *   A list of ancestor elements excluding the element of this node. Returns an
     *   empty array if the given element matches the element of this node. Returns
     *   NULL if the searched element is not a child of this nodes element.
     */
    getElementPath(element: HTMLElement): HTMLElement[];
    /**
     * Return a list of all components attached to this node and all of its children.
     *
     * @param result
     *   An optional array the results will be appended to.
     * @returns
     *   A list of all components attached to this node and all of its children.
     */
    getComponents(result?: View[]): View[];
    /**
     * Return a list of all components attached to the children of this node.
     *
     * @param result
     *   An optional array the results will be appended to.
     * @returns
     *   A list of all components attached to the children of this node.
     */
    getChildComponents(result?: View[]): View[];
    /**
     * Remove an element from the node tree.
     *
     * @param element
     *   The element that should be removed from the tree.
     */
    removeElement(element: HTMLElement): void;
    /**
     * Create the component of this node.
     *
     * @param result
     *   An optional array the results will be appended to.
     * @returns
     *   A list of all newly created components.
     */
    protected createLocalComponents(result?: View[]): View[];
    /**
     * Create the components of all children of this node.
     *
     * @param result
     *   An optional array the results will be appended to.
     * @returns
     *   A list of all newly created components.
     */
    protected createChildComponents(result?: View[]): View[];
    /**
     * Set the component instance assigned to this tree node.
     *
     * @param component
     *   The component instance assigned to this node.
     */
    protected setComponent(component: Component): void;
    /**
     * Add a child node to this tree node.
     *
     * @param element
     *   The dom element the child should represent. Must be a direct descendant
     *   of this nodes dom element.
     * @returns
     *   The newly created tree node representing the given dom element.
     */
    protected addChild(element: HTMLElement): Node;
    /**
     * Add a child by its hierarchy to this node.
     *
     * @param path
     *   The path pointing to the child element.
     * @returns
     *   The node created for the last element in the given path.
     */
    protected addChildHierarchy(path: HTMLElement[]): Node;
    /**
     * Remove this node.
     */
    protected remove(): void;
    /**
     * Remove a child by its hierarchy to this node.
     *
     * @param path
     *   The path pointing to the child element.
     * @returns
     *   TRUE if this node is empty after the removal, FALSE otherwise.
     */
    protected removeChildHierarchy(path: HTMLElement[]): boolean;
    /**
     * Trigger the resize handler on all child components.
     */
    resizeChildren: () => void;
    /**
     * Triggers the resize handler on this component and all of its children.
     */
    handleViewportResize(): void;
}
