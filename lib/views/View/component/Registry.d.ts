import { Component, ComponentOptions } from './index';
import Node from './Node';
import View from '../index';
import Viewport from '../../../services/Viewport';
/**
 * The component registry stores data of all known components.
 */
export default class Registry {
    /**
     * A list of all registered components.
     */
    components: Component[];
    /**
     * The root node of the component tree.
     */
    rootNode: Node;
    initialize(viewport: Viewport): this;
    /**
     * Create all components within the given dom element.
     *
     * @param element
     *   The dom element components should be created for,
     * @returns
     *   A list of all newly created components.
     */
    create(element?: HTMLElement): View[];
    getRootNode(): Node;
    getNode(element: HTMLElement): Node;
    /**
     * Register a component.
     *
     * @param viewClass
     *   The class that should be instantiated for the component.
     * @param selector
     *   A css selector that selects all dom elements for the component.
     * @param options
     *   Additional component options.
     */
    register(viewClass: typeof View, selector: string, options: ComponentOptions): void;
    /**
     * Called by the view remove function when a view is about to be removed.
     *
     * @param element
     *   The element of the view that is about to be removed.
     */
    handleRemove(element: HTMLElement): void;
    /**
     * Triggered after the size of the viewport has changed.
     */
    handleViewportResize(): void;
    /**
     * Return the current instance of the ComponentRegistry service.
     *
     * @returns
     *    The current instance of the ComponentRegistry service.
     */
    static getInstance(): Registry;
    /**
     * Register the given view instance.
     *
     * @param instance
     *   The instance that should be added to the component tree.
     */
    static registerInstance(instance: View): void;
}
