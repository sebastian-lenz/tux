import * as tslib_1 from "tslib";
import { find } from 'underscore';
import Node from './Node';
import services, { service } from '../../../services';
/**
 * The component registry stores data of all known components.
 */
var Registry = Registry_1 = (function () {
    function Registry() {
        /**
         * A list of all registered components.
         */
        this.components = [];
    }
    Registry.prototype.initialize = function (viewport) {
        viewport.on('resize', this.handleViewportResize, this);
        return this;
    };
    /**
     * Create all components within the given dom element.
     *
     * @param element
     *   The dom element components should be created for,
     * @returns
     *   A list of all newly created components.
     */
    Registry.prototype.create = function (element) {
        if (element === void 0) { element = document.body; }
        var node = this.getRootNode().getNode(element);
        for (var _i = 0, _a = this.components; _i < _a.length; _i++) {
            var component = _a[_i];
            node.discover(component);
        }
        var result = node.create();
        node.handleViewportResize();
        return result;
    };
    Registry.prototype.getRootNode = function () {
        if (!this.rootNode) {
            this.rootNode = new Node(document.body);
        }
        return this.rootNode;
    };
    Registry.prototype.getNode = function (element) {
        var node = this.getRootNode();
        var path = node.getElementPath(element);
        var _loop_1 = function () {
            var segment = path.shift();
            node = find(node.children, function (child) {
                return child.element == segment;
            });
            if (!node)
                return { value: null };
        };
        while (path.length) {
            var state_1 = _loop_1();
            if (typeof state_1 === "object")
                return state_1.value;
        }
        return node;
    };
    /**
     * Register a component.
     *
     * @param viewClass
     *   The class that should be instantiated for the component.
     * @param selector
     *   A css selector that selects all dom elements for the component.
     * @param options
     *   Additional component options.
     */
    Registry.prototype.register = function (viewClass, selector, options) {
        this.components.push({ viewClass: viewClass, selector: selector, options: options });
    };
    /**
     * Called by the view remove function when a view is about to be removed.
     *
     * @param element
     *   The element of the view that is about to be removed.
     */
    Registry.prototype.handleRemove = function (element) {
        this.rootNode.removeElement(element);
    };
    /**
     * Triggered after the size of the viewport has changed.
     */
    Registry.prototype.handleViewportResize = function () {
        this.rootNode.handleViewportResize();
    };
    /**
     * Return the current instance of the ComponentRegistry service.
     *
     * @returns
     *    The current instance of the ComponentRegistry service.
     */
    Registry.getInstance = function () {
        return services.get('ComponentRegistry', Registry_1);
    };
    /**
     * Register the given view instance.
     *
     * @param instance
     *   The instance that should be added to the component tree.
     */
    Registry.registerInstance = function (instance) {
        var node = Registry_1.getInstance()
            .getRootNode()
            .getNode(instance.element);
        if (node.instance != instance) {
            node.setInstance(instance);
        }
    };
    return Registry;
}());
Registry = Registry_1 = tslib_1.__decorate([
    service('ComponentRegistry')
], Registry);
export default Registry;
var Registry_1;
