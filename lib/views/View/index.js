import * as tslib_1 from "tslib";
import { forEach, uniqueId } from 'underscore';
import component, { Registry } from './component';
import ClickHandler from '../../handler/ClickHandler';
import Delegate from './Delegate';
import { child, children, option } from './initializer';
import '../../util/polyfill/classList';
import '../../util/polyfill/eventListener';
import '../../util/polyfill/matches';
export { child, children, component, option, Registry };
/**
 * Base class of all views.
 */
var View = (function (_super) {
    tslib_1.__extends(View, _super);
    /**
     * View constructor.
     *
     * @param options
     *   Thew view constructor options.
     */
    function View(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this) || this;
        _this.cid = uniqueId('view');
        _this.ensureElement(options);
        for (var key in _this._initializers) {
            _this._initializers[key].invoke(_this, options);
        }
        return _this;
    }
    /**
     * Ensures this view has a html element.
     */
    View.prototype.ensureElement = function (options) {
        var appendTo = options.appendTo, template = options.template;
        var element = options.element;
        if (!element) {
            var attributes = options.attributes, className = options.className, _a = options.tagName, tagName = _a === void 0 ? 'div' : _a;
            element = document.createElement(tagName);
            if (className)
                element.className = className;
            if (attributes)
                forEach(attributes, function (value, key) {
                    return element.setAttribute(key, value);
                });
        }
        if (appendTo)
            appendTo.appendChild(element);
        if (template)
            element.innerHTML = typeof template === 'function'
                ? template(this, options)
                : template;
        this.element = element;
        return this;
    };
    /**
     * Remove this view.
     */
    View.prototype.remove = function () {
        if (this.isRemoved)
            return this;
        var registry = Registry.getInstance();
        if (registry) {
            registry.handleRemove(this.element);
        }
        var element = this.element;
        if (element.parentNode) {
            element.parentNode.removeChild(element);
        }
        this.undelegateEvents();
        this.stopListening();
        this.isRemoved = true;
        this.element = null;
        return this;
    };
    View.prototype.delegateClick = function (listener, options) {
        if (options === void 0) { options = {}; }
        var selector = options.selector, _a = options.scope, scope = _a === void 0 ? this : _a;
        new ClickHandler(this, listener, selector, scope);
        return this;
    };
    /**
     * Triggered after the size of the viewport has changed.
     *
     * If this view is used as a component this handler will be called automatically.
     *
     * @param resizeChildren
     *   A callback that triggers the resize event on all child components.
     * @returns
     *   TRUE if the event handling should be stopped and child components should not receive
     *   the event, FALSE otherwise.
     */
    View.prototype.handleViewportResize = function (resizeChildren) {
        return false;
    };
    View.prototype.query = function (selectors) {
        return this.element.querySelector(selectors);
    };
    View.prototype.queryAll = function (selectors) {
        return this.element.querySelectorAll(selectors);
    };
    View.prototype.addClass = function () {
        var token = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            token[_i] = arguments[_i];
        }
        (_a = this.element.classList).add.apply(_a, token);
        return this;
        var _a;
    };
    View.prototype.containsClass = function (token) {
        return this.element.classList.contains(token);
    };
    View.prototype.removeClass = function () {
        var token = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            token[_i] = arguments[_i];
        }
        (_a = this.element.classList).remove.apply(_a, token);
        return this;
        var _a;
    };
    View.prototype.toggleClass = function (token, force) {
        return this.element.classList.toggle(token, force);
    };
    return View;
}(Delegate));
export default View;
