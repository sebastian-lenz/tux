import Events from '../../Events';
import '../../util/polyfill/eventListener';
export interface DelegateOptions {
    passive?: boolean;
    selector?: string;
    scope?: any;
}
export interface DomEvent {
    eventName: string;
    handler: EventListener;
    listener: EventListener;
    scope: any;
    selector: string;
}
/**
 * Base class of all views.
 */
export default class Delegate extends Events {
    element: Element | Document | null;
    private _domEvents;
    /**
     * View constructor.
     *
     * @param options
     *   Thew view constructor options.
     */
    constructor(element?: Element | Document | null);
    delegate(eventName: string, listener: EventListener, options?: DelegateOptions): EventListener;
    undelegate(eventName: string, listener: EventListener, options?: DelegateOptions): this;
    /**
     * Remove all events created with `delegate` from `el`
     */
    undelegateEvents(): this;
}
