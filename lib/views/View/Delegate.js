import * as tslib_1 from "tslib";
import Events from '../../Events';
import '../../util/polyfill/eventListener';
var supportsPassiveOption = (function () {
    var result = false;
    try {
        var opts = Object.defineProperty({}, 'passive', {
            get: function () { return result = true; }
        });
        window.addEventListener('test', null, opts);
        window.removeEventListener('test', null, opts);
    }
    catch (e) { }
    return result;
})();
/**
 * Base class of all views.
 */
var Delegate = (function (_super) {
    tslib_1.__extends(Delegate, _super);
    /**
     * View constructor.
     *
     * @param options
     *   Thew view constructor options.
     */
    function Delegate(element) {
        if (element === void 0) { element = null; }
        var _this = _super.call(this) || this;
        // A list of all delegated dom events.
        _this._domEvents = [];
        _this.element = element;
        return _this;
    }
    // Make a event delegation handler for the given `eventName` and `selector`
    // and attach it to `this.el`.
    // If selector is empty, the listener will be bound to `this.el`. If not, a
    // new handler that will recursively traverse up the event target's DOM
    // hierarchy looking for a node that matches the selector. If one is found,
    // the event's `delegateTarget` property is set to it and the return the
    // result of calling bound `listener` with the parameters given to the
    // handler.
    Delegate.prototype.delegate = function (eventName, listener, options) {
        if (options === void 0) { options = {}; }
        var selector = options.selector, _a = options.scope, scope = _a === void 0 ? this : _a;
        var element = this.element;
        var handler;
        if (selector) {
            handler = function (event) {
                var node = (event.target || event.srcElement);
                for (; node && node != element; node = (node.parentNode)) {
                    if (!node.matches(selector))
                        continue;
                    event.delegateTarget = node;
                    listener.call(scope, event);
                    break;
                }
            };
        }
        else {
            handler = function (event) {
                listener.call(scope, event);
            };
        }
        if (options.passive === false && supportsPassiveOption) {
            element.addEventListener(eventName, handler, { passive: false });
        }
        else {
            element.addEventListener(eventName, handler, false);
        }
        this._domEvents.push({ eventName: eventName, handler: handler, listener: listener, scope: scope, selector: selector });
        return handler;
    };
    // Remove a single delegated event. Either `eventName` or `selector` must
    // be included, `selector` and `listener` are optional.
    Delegate.prototype.undelegate = function (eventName, listener, options) {
        if (options === void 0) { options = {}; }
        var selector = options.selector, scope = options.scope;
        var _a = this, _domEvents = _a._domEvents, element = _a.element;
        if (element) {
            var handlers = _domEvents.slice();
            var i = handlers.length;
            while (i--) {
                var item = handlers[i];
                var match = item.eventName === eventName &&
                    (listener ? item.listener === listener : true) &&
                    (scope ? item.scope === scope : true) &&
                    (selector ? item.selector === selector : true);
                if (match) {
                    element.removeEventListener(item.eventName, item.handler, false);
                    _domEvents.splice(i, 1);
                }
            }
        }
        return this;
    };
    /**
     * Remove all events created with `delegate` from `el`
     */
    Delegate.prototype.undelegateEvents = function () {
        var _a = this, element = _a.element, _domEvents = _a._domEvents;
        if (element) {
            for (var i = 0, len = _domEvents.length; i < len; i++) {
                var _b = _domEvents[i], eventName = _b.eventName, handler = _b.handler;
                element.removeEventListener(eventName, handler, false);
            }
            this._domEvents.length = 0;
        }
        return this;
    };
    return Delegate;
}(Events));
export default Delegate;
