import '../../../util/polyfill/objectCreate';
import View, { ViewOptions } from '../index';
export * from './child';
export * from './option';
export interface Initializers {
    [name: string]: Initializer;
}
export interface Initializer {
    invoke(view: View, options: ViewOptions): void;
}
/**
 * Return the initializers of the given class.
 */
export declare function getInitializers(owner: any): Initializers;
/**
 * Return or create a named initializer.
 */
export declare function getInitializer<T extends Initializer>(owner: any, name: string, factory: {
    (): T;
}): T;
