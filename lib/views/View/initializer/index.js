import '../../../util/polyfill/objectCreate';
export * from './child';
export * from './option';
/**
 * Return the initializers of the given class.
 */
export function getInitializers(owner) {
    if (owner.hasOwnProperty('_initializers')) {
        return owner._initializers;
    }
    else if (owner._initializers) {
        return owner._initializers = Object.create(owner._initializers);
    }
    else {
        return owner._initializers = {};
    }
}
/**
 * Return or create a named initializer.
 */
export function getInitializer(owner, name, factory) {
    var initializers = getInitializers(owner);
    if (initializers.hasOwnProperty(name)) {
        return (initializers[name]);
    }
    else if (initializers[name]) {
        return (initializers[name] = Object.create(initializers[name]));
    }
    else {
        return initializers[name] = factory();
    }
}
