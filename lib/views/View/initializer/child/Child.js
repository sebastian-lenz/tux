import { defaults, extendOwn, map } from 'underscore';
import Registry from '../../component/Registry';
/**
 * An initializer that instantiates child components.
 */
var Child = (function () {
    /**
     * ChildInitializer constructor.
     *
     * @param propertyName
     *   The name of the property this initializer should initialize.
     */
    function Child(propertyName) {
        this.propertyName = propertyName;
    }
    /**
     * Invoke this initializer for the given scope.
     *
     * @param scope
     *   The view instance that should be initialized.
     * @param options
     *   The constructor options that have been passed to the view.
     */
    Child.prototype.invoke = function (scope, options) {
        var _this = this;
        var element = scope.element;
        var selector = this.selector;
        var result = null;
        if (this.multiple) {
            var children = selector ? scope.queryAll(selector) : [];
            result = map(children, function (child) {
                return _this.createComponent(scope, child);
            });
        }
        else {
            var child = selector ? scope.query(selector) : null;
            if (child || this.autoCreate) {
                result = this.createComponent(scope, child);
            }
        }
        scope[this.propertyName] = result;
    };
    /**
     * Create an instance of the child view class for the given element.
     *
     * @param scope
     * @param element
     *   The dom element a view instance should be created for.
     */
    Child.prototype.createComponent = function (scope, element) {
        var options = { owner: scope };
        if (element) {
            options.element = element;
        }
        else {
            options.appendTo = scope.element;
        }
        if (this.options) {
            options = defaults(options, this.options);
        }
        var child;
        if (this.isFactory) {
            child = this.viewClass(options);
        }
        else {
            child = new this.viewClass(options);
        }
        var root = Registry.getInstance().getRootNode();
        var node = root.getNode(child.element);
        node.setInstance(child);
        return child;
    };
    /**
     * Apply the given options object on this initializer.
     *
     * @param options
     *   The options that should be applied.
     */
    Child.prototype.setOptions = function (options) {
        extendOwn(this, options);
    };
    return Child;
}());
export default Child;
