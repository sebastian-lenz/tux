import { ChildOptions } from './Child';
export declare function child(name: string, options?: ChildOptions): ClassDecorator;
export declare function child(options?: ChildOptions): PropertyDecorator;
export declare function children(name: string, options: ChildOptions): ClassDecorator;
export declare function children(options: ChildOptions): PropertyDecorator;
