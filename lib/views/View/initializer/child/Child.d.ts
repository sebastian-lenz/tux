import View, { ViewOptions } from '../../index';
import { Initializer } from '../index';
/**
 * ChildInitializer constructor options.
 */
export interface ChildOptions {
    /**
     * The selector that will be used to retrieve the child elements.
     */
    selector?: string;
    /**
     * The view class that should be instantiated.
     */
    viewClass: any;
    /**
     * Default constructor options.
     */
    options?: any;
    /**
     * Whether multiple children should be resolved or not.
     */
    multiple?: boolean;
    /**
     * Auto create the component if the selector is missing?
     */
    autoCreate?: boolean;
    /**
     * Is the given childClass a factory function?
     */
    isFactory?: boolean;
}
/**
 * An initializer that instantiates child components.
 */
export default class Child implements Initializer {
    /**
     * The name of the property this initializer should initialize.
     */
    propertyName: string;
    /**
     * The view class that should be instantiated.
     */
    viewClass: any;
    /**
     * Default constructor options.
     */
    options: any;
    /**
     * The selector that will be used to retrieve the child elements.
     */
    selector: string;
    /**
     * Whether multiple children should be resolved or not.
     */
    multiple: boolean;
    /**
     * Auto create the component if the selector is missing?
     */
    autoCreate: boolean;
    /**
     * Is the given childClass a factory function?
     */
    isFactory: boolean;
    /**
     * ChildInitializer constructor.
     *
     * @param propertyName
     *   The name of the property this initializer should initialize.
     */
    constructor(propertyName?: string);
    /**
     * Invoke this initializer for the given scope.
     *
     * @param scope
     *   The view instance that should be initialized.
     * @param options
     *   The constructor options that have been passed to the view.
     */
    invoke(scope: View, options: ViewOptions): void;
    /**
     * Create an instance of the child view class for the given element.
     *
     * @param scope
     * @param element
     *   The dom element a view instance should be created for.
     */
    createComponent(scope: View, element?: HTMLElement): View;
    /**
     * Apply the given options object on this initializer.
     *
     * @param options
     *   The options that should be applied.
     */
    setOptions(options: ChildOptions): void;
}
