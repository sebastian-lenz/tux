import { defaults } from 'underscore';
import Child from './Child';
import { getInitializer } from '../index';
export function child() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    if (args.length > 0 && typeof (args[0]) === 'string') {
        var property_1 = args.shift();
        var options_1 = args.length ? args.shift() : void 0;
        var name_1 = 'ChildComponent::' + property_1;
        return function (owner) {
            getInitializer(owner.prototype, name_1, function () { return new Child(property_1); })
                .setOptions(options_1);
        };
    }
    else {
        var options_2 = args.length ? args.shift() : void 0;
        return function (owner, property) {
            var name = 'ChildComponent::' + property;
            getInitializer(owner, name, function () { return new Child(property); })
                .setOptions(options_2);
        };
    }
}
export function children() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    if (args.length > 0 && typeof (args[0]) === 'string') {
        args[1] = defaults(args[1] || {}, { multiple: true });
    }
    else {
        args[0] = defaults(args[0] || {}, { multiple: true });
    }
    return child.apply(this, args);
}
