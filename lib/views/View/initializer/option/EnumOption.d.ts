import View from '../../index';
import Option, { OptionOptions } from './Option';
export interface EnumOptionOptions extends OptionOptions {
    type: 'enum';
    defaultValue?: number;
    values: any;
}
export default class EnumOption extends Option<number, EnumOptionOptions> {
    lookupTable: any;
    values: any;
    /**
     * EnumOption constructor.
     */
    constructor(property: string, options: EnumOptionOptions);
    /**
     * Convert the given option value.
     */
    convert(scope: View, value: any): number | undefined;
}
