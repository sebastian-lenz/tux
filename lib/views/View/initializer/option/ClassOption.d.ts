import View from '../../index';
import Option, { OptionOptions } from './Option';
export interface ClassOptionOptions<T = any> extends OptionOptions {
    type: 'class';
    defaultValue?: T;
    ctor: {
        new (data: any): T;
    };
}
export default class ClassOption<T> extends Option<T, ClassOptionOptions<T>> {
    ctor: {
        new (data: any): T;
    };
    /**
     * Convert the given option value.
     */
    convert(scope: View, value: any): T;
}
