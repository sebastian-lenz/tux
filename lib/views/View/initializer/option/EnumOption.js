import * as tslib_1 from "tslib";
import { forEach, keys } from 'underscore';
import Option from './Option';
var EnumOption = (function (_super) {
    tslib_1.__extends(EnumOption, _super);
    /**
     * EnumOption constructor.
     */
    function EnumOption(property, options) {
        var _this = _super.call(this, property, options) || this;
        var values = options.values;
        var lookupTable = {};
        forEach(keys(options.values), function (value) {
            if (typeof value === 'string') {
                lookupTable[value.toLowerCase()] = values[value];
            }
        });
        _this.lookupTable = lookupTable;
        return _this;
    }
    /**
     * Convert the given option value.
     */
    EnumOption.prototype.convert = function (scope, value) {
        var _a = this, lookupTable = _a.lookupTable, values = _a.values;
        if (typeof value == 'string') {
            value = value.toLowerCase();
            return value in lookupTable
                ? lookupTable[value]
                : undefined;
        }
        if (value in values)
            return value;
        return undefined;
    };
    return EnumOption;
}(Option));
export default EnumOption;
