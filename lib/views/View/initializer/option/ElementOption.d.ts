import View, { ViewOptions } from '../../index';
import Option, { OptionOptions } from './Option';
export interface ElementOptionOptions extends OptionOptions {
    type: 'element';
    className?: string;
    defaultValue?: string;
    tagName?: string;
}
export default class ElementOption extends Option<HTMLElement, ElementOptionOptions> {
    className: string;
    tagName: string;
    /**
     * Extract the value from the given view and options object.
     */
    getValue(scope: View, options: ViewOptions): HTMLElement;
}
