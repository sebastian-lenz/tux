import * as tslib_1 from "tslib";
import Option from './Option';
var BoolOption = (function (_super) {
    tslib_1.__extends(BoolOption, _super);
    function BoolOption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Convert the given option value.
     */
    BoolOption.prototype.convert = function (scope, value) {
        if (typeof value === 'string') {
            value = value.toLowerCase();
            return ((value == 'true') || (value == 'yes') || (value == '1'));
        }
        return !!value;
    };
    return BoolOption;
}(Option));
export default BoolOption;
