import View from '../../index';
import Option, { OptionOptions } from './Option';
export interface IntOptionOptions extends OptionOptions {
    type: 'int';
    defaultValue?: number;
}
export default class IntOption extends Option<number, IntOptionOptions> {
    /**
     * Convert the given option value.
     */
    convert(scope: View, value: any): number | undefined;
}
