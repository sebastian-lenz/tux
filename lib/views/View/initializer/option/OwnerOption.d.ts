import View, { ViewOptions } from '../../index';
import Option, { OptionOptions } from './Option';
export interface OwnerOptionOptions extends OptionOptions {
    type: 'owner';
}
export default class OwnerOption extends Option<View | undefined, OwnerOptionOptions> {
    /**
     * Extract the value from the given view and options object.
     */
    getValue(scope: View, options: ViewOptions): View | undefined;
}
