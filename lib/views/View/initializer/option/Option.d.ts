import View, { ViewOptions } from '../../index';
import { Initializer } from '../index';
export interface OptionOptions {
    attribute?: string;
}
export default class Option<U = any, V extends OptionOptions = OptionOptions> implements Initializer {
    property: string;
    attribute: string;
    option: string;
    defaultValue: U;
    /**
     * Option constructor.
     */
    constructor(property: string, options: V);
    /**
     * Invoke this initializer.
     */
    invoke(scope: View, options: ViewOptions): void;
    /**
     * Convert the given option value.
     */
    convert(scope: View, value: any): U | undefined;
    /**
     * Extract the value from the given view and options object.
     */
    getValue(scope: View, options: ViewOptions): U;
    /**
     * Return this options default value.
     */
    getDefaultValue(): U;
}
