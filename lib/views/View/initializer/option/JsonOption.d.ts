import View from '../../index';
import Option, { OptionOptions } from './Option';
export interface JsonOptionOptions extends OptionOptions {
    type: 'json';
    defaultValue?: object;
}
export default class JsonOption extends Option<object, JsonOptionOptions> {
    /**
     * Convert the given option value.
     */
    convert(scope: View, value: any): object | undefined;
}
