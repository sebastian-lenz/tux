import Option, { OptionOptions } from './Option';
export interface AnyOptionOptions extends OptionOptions {
    type: 'any';
    defaultValue?: any;
}
export default class AnyOption extends Option<any, AnyOptionOptions> {
}
