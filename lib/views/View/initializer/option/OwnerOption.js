import * as tslib_1 from "tslib";
import Option from './Option';
var OwnerOption = (function (_super) {
    tslib_1.__extends(OwnerOption, _super);
    function OwnerOption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Extract the value from the given view and options object.
     */
    OwnerOption.prototype.getValue = function (scope, options) {
        return options.owner;
    };
    return OwnerOption;
}(Option));
export default OwnerOption;
