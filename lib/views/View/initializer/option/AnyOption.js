import * as tslib_1 from "tslib";
import Option from './Option';
var AnyOption = (function (_super) {
    tslib_1.__extends(AnyOption, _super);
    function AnyOption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AnyOption;
}(Option));
export default AnyOption;
