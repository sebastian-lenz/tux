import View from '../../index';
import Option, { OptionOptions } from './Option';
export interface StringOptionOptions extends OptionOptions {
    type: 'string';
    defaultValue?: string;
}
export default class StringOption extends Option<string, StringOptionOptions> {
    /**
     * Convert the given option value.
     */
    convert(scope: View, value: any): string;
}
