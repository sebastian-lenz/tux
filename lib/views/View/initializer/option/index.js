import AnyOption from './AnyOption';
import BoolOption from './BoolOption';
import ClassOption from './ClassOption';
import ElementOption from './ElementOption';
import EnumOption from './EnumOption';
import IntOption from './IntOption';
import JsonOption from './JsonOption';
import NumberOption from './NumberOption';
import OwnerOption from './OwnerOption';
import StringOption from './StringOption';
import { getInitializer } from '../index';
function create(owner, property, options) {
    var name = "Option::" + property;
    var initializer = getInitializer(owner, name, function () {
        switch (options.type) {
            case 'any': return new AnyOption(property, options);
            case 'bool': return new BoolOption(property, options);
            case 'class': return new ClassOption(property, options);
            case 'element': return new ElementOption(property, options);
            case 'enum': return new EnumOption(property, options);
            case 'int': return new IntOption(property, options);
            case 'json': return new JsonOption(property, options);
            case 'number': return new NumberOption(property, options);
            case 'owner': return new OwnerOption(property, options);
            case 'string': return new StringOption(property, options);
        }
    });
}
export function option() {
    if (arguments.length > 0 && typeof (arguments[0]) === 'string') {
        var property = arguments[0];
        var options_1 = arguments[1];
        return function (owner) {
            create(owner.prototype, name, options_1);
        };
    }
    else {
        var options_2 = arguments[0];
        return function (owner, property) {
            create(owner, property, options_2);
        };
    }
}
