import * as tslib_1 from "tslib";
import Option from './Option';
var ClassOption = (function (_super) {
    tslib_1.__extends(ClassOption, _super);
    function ClassOption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Convert the given option value.
     */
    ClassOption.prototype.convert = function (scope, value) {
        var ctor = this.ctor;
        if (value instanceof ctor) {
            return value;
        }
        return new ctor(value);
    };
    return ClassOption;
}(Option));
export default ClassOption;
