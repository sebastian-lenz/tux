import View from '../../index';
import Option, { OptionOptions } from './Option';
export interface BoolOptionOptions extends OptionOptions {
    type: 'bool';
    defaultValue?: boolean;
}
export default class BoolOption extends Option<boolean, BoolOptionOptions> {
    /**
     * Convert the given option value.
     */
    convert(scope: View, value: any): boolean;
}
