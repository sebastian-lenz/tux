import { extendOwn } from 'underscore';
import { dasherize, trimStart } from '../../../../util/string';
var Option = (function () {
    /**
     * Option constructor.
     */
    function Option(property, options) {
        var option = trimStart(property, '_');
        this.property = property;
        this.option = option;
        this.attribute = 'data-' + dasherize(option);
        extendOwn(this, options);
    }
    /**
     * Invoke this initializer.
     */
    Option.prototype.invoke = function (scope, options) {
        var _a = this, option = _a.option, property = _a.property;
        var value = this.getValue(scope, options);
        if (property) {
            scope[property] = value;
        }
        else if (option) {
            options[option] = value;
        }
    };
    /**
     * Convert the given option value.
     */
    Option.prototype.convert = function (scope, value) {
        return value;
    };
    /**
     * Extract the value from the given view and options object.
     */
    Option.prototype.getValue = function (scope, options) {
        var _a = this, attribute = _a.attribute, option = _a.option;
        var element = options.element;
        var value = void 0;
        if (element && attribute && element.hasAttribute(attribute)) {
            value = this.convert(scope, element.getAttribute(attribute));
            if (value !== void 0)
                return value;
        }
        if (option && option in options) {
            value = this.convert(scope, options[option]);
            if (value !== void 0)
                return value;
        }
        return this.getDefaultValue();
    };
    /**
     * Return this options default value.
     */
    Option.prototype.getDefaultValue = function () {
        return this.defaultValue;
    };
    return Option;
}());
export default Option;
