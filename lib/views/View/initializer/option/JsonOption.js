import * as tslib_1 from "tslib";
import Option from './Option';
var JsonOption = (function (_super) {
    tslib_1.__extends(JsonOption, _super);
    function JsonOption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Convert the given option value.
     */
    JsonOption.prototype.convert = function (scope, value) {
        if (typeof value == 'string') {
            return JSON.parse(value);
        }
        else if (typeof value == 'object') {
            return value;
        }
        return undefined;
    };
    return JsonOption;
}(Option));
export default JsonOption;
