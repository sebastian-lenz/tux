import View from '../../index';
import Option, { OptionOptions } from './Option';
export interface NumberOptionOptions extends OptionOptions {
    type: 'number';
    defaultValue?: number;
}
export default class NumberOption extends Option<number, NumberOptionOptions> {
    /**
     * Convert the given option value.
     */
    convert(scope: View, value: any): number | undefined;
}
