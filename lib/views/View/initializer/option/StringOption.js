import * as tslib_1 from "tslib";
import Option from './Option';
var StringOption = (function (_super) {
    tslib_1.__extends(StringOption, _super);
    function StringOption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Convert the given option value.
     */
    StringOption.prototype.convert = function (scope, value) {
        return "" + value;
    };
    return StringOption;
}(Option));
export default StringOption;
