import * as tslib_1 from "tslib";
import { isElement } from 'underscore';
import Option from './Option';
var ElementOption = (function (_super) {
    tslib_1.__extends(ElementOption, _super);
    function ElementOption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Extract the value from the given view and options object.
     */
    ElementOption.prototype.getValue = function (scope, options) {
        var value = _super.prototype.getValue.call(this, scope, options);
        if (isElement(value))
            return value;
        if (typeof value === 'string') {
            var element = scope.element.querySelector(value);
            if (element) {
                return element;
            }
        }
        var _a = this, className = _a.className, tagName = _a.tagName;
        if (tagName) {
            var element = document.createElement(tagName);
            if (className) {
                element.className = className;
            }
            scope.element.appendChild(element);
            return element;
        }
        return undefined;
    };
    return ElementOption;
}(Option));
export default ElementOption;
