import * as tslib_1 from "tslib";
import Option from './Option';
var NumberOption = (function (_super) {
    tslib_1.__extends(NumberOption, _super);
    function NumberOption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Convert the given option value.
     */
    NumberOption.prototype.convert = function (scope, value) {
        if (typeof value === 'string') {
            return parseFloat(value);
        }
        else if (typeof value === 'number') {
            return value;
        }
        return undefined;
    };
    return NumberOption;
}(Option));
export default NumberOption;
