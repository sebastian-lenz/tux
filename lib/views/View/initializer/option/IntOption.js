import * as tslib_1 from "tslib";
import Option from './Option';
var IntOption = (function (_super) {
    tslib_1.__extends(IntOption, _super);
    function IntOption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Convert the given option value.
     */
    IntOption.prototype.convert = function (scope, value) {
        if (typeof value === 'string') {
            return parseInt(value);
        }
        else if (typeof value === 'number') {
            return Math.round(value);
        }
        return undefined;
    };
    return IntOption;
}(Option));
export default IntOption;
