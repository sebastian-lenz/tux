import * as tslib_1 from "tslib";
import * as Q from 'q';
import { defaults } from 'underscore';
import SourceSet from './SourceSet';
import View, { option } from '../View';
import Visibility from '../../services/Visibility';
export var RoundingMode;
(function (RoundingMode) {
    RoundingMode[RoundingMode["Round"] = 0] = "Round";
    RoundingMode[RoundingMode["Floor"] = 1] = "Floor";
    RoundingMode[RoundingMode["Ceil"] = 2] = "Ceil";
})(RoundingMode || (RoundingMode = {}));
export function round(value, mode) {
    switch (mode) {
        case RoundingMode.Ceil: return Math.ceil(value);
        case RoundingMode.Floor: return Math.floor(value);
        default: return Math.round(value);
    }
}
/**
 * Displays an image.
 *
 * @event "load" ():void
 *   Triggered after the image has been loaded.
 */
var Image = (function (_super) {
    tslib_1.__extends(Image, _super);
    /**
     * Image constructor.
     *
     * @param options
     *   The constructor options.
     */
    function Image(options) {
        var _this = _super.call(this, options = defaults(options || {}, {
            tagName: 'img'
        })) || this;
        /**
         * The current load state of this image.
         */
        _this.loadState = 0 /* Idle */;
        /**
         * Whether this element is currently within the visible viewport bounds or not.
         */
        _this.isInViewport = false;
        var element = _this.element;
        if (element.hasAttribute('src') && !/^data:/.test(element.getAttribute('src'))) {
            if (element.complete && typeof element.naturalWidth !== 'undefined' && element.naturalWidth != 0) {
                _this.onLoad();
            }
            else {
                _this.prepareLoadEvent();
            }
        }
        if (!options.disableVisibilityCheck) {
            Visibility.getInstance().register(_this);
        }
        return _this;
    }
    Image.prototype.remove = function () {
        Visibility.getInstance().unregister(this);
        return _super.prototype.remove.call(this);
    };
    /**
     * Set the size and optionally the position shift of this image.
     *
     * @param width
     *   The desired width of the image.
     * @param height
     *   The desired height of the image.
     * @param left
     *   The horizontal shift of the image.
     * @param top
     *   The vertical shift of the image.
     */
    Image.prototype.setSize = function (width, height, left, top) {
        var _a = this, element = _a.element, sizeRoundingMode = _a.sizeRoundingMode, offsetRoundingMode = _a.offsetRoundingMode;
        var style = element.style;
        style.width = round(width, sizeRoundingMode) + 'px';
        style.height = round(height, sizeRoundingMode) + 'px';
        if (left != void 0) {
            style.left = round(left, offsetRoundingMode) + 'px';
        }
        else {
            style.left = '';
        }
        if (top != void 0) {
            style.top = round(top, offsetRoundingMode) + 'px';
        }
        else {
            style.top = '';
        }
        this.update();
    };
    /**
     * Set whether the element of this view is currently within the visible bounds
     * of the viewport or not.
     *
     * @param inViewport
     *   TRUE if the element is visible, FALSE otherwise.
     */
    Image.prototype.setInViewport = function (inViewport) {
        if (this.isInViewport == inViewport)
            return;
        this.isInViewport = inViewport;
        this.update();
    };
    /**
     * Update the source location of the image due to the current dimensions.
     */
    Image.prototype.update = function () {
        if (this.isInViewport && this.sourceSet) {
            this.prepareLoadEvent();
            this.element.src = this.sourceSet.get(this.element);
        }
    };
    /**
     * Load the image.
     *
     * @returns
     *   A promise that resolves after the image has loaded.
     */
    Image.prototype.load = function () {
        if (!this.deferred)
            this.deferred = Q.defer();
        var deferred = this.deferred;
        if (this.loadState == 2 /* Loaded */) {
            deferred.resolve();
        }
        else {
            this.isInViewport = true;
            this.update();
        }
        return deferred.promise;
    };
    /**
     * Setup the load event listener when loading the image for the first time.
     */
    Image.prototype.prepareLoadEvent = function () {
        if (this.loadState != 0 /* Idle */)
            return;
        this.loadState = 1 /* Loading */;
        this.delegate('load', this.onLoad);
    };
    /**
     * Triggered after the image has been loadedf or the first time.
     */
    Image.prototype.onLoad = function () {
        if (this.loadState == 2 /* Loaded */)
            return;
        this.loadState = 2 /* Loaded */;
        var element = this.element;
        if (this.nativeWidth <= 0) {
            this.nativeWidth = element.naturalWidth;
        }
        if (this.nativeHeight <= 0) {
            this.nativeHeight = element.naturalHeight;
        }
        element.classList.add('loaded');
        this.undelegate('load', null, this.onLoad);
        this.trigger('load');
        if (this.deferred) {
            this.deferred.resolve(this);
        }
    };
    return Image;
}(View));
export default Image;
tslib_1.__decorate([
    option({ type: 'int', attribute: 'width', defaultValue: -1 })
], Image.prototype, "nativeWidth", void 0);
tslib_1.__decorate([
    option({ type: 'int', attribute: 'height', defaultValue: -1 })
], Image.prototype, "nativeHeight", void 0);
tslib_1.__decorate([
    option({ type: 'class', ctor: SourceSet, attribute: 'data-srcset' })
], Image.prototype, "sourceSet", void 0);
tslib_1.__decorate([
    option({ type: 'enum', values: RoundingMode, defaultValue: RoundingMode.Ceil })
], Image.prototype, "sizeRoundingMode", void 0);
tslib_1.__decorate([
    option({ type: 'enum', values: RoundingMode, defaultValue: RoundingMode.Floor })
], Image.prototype, "offsetRoundingMode", void 0);
