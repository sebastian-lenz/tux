import { forEach, isArray } from 'underscore';
import { trim } from '../../util/string';
/**
 * A regular expression used to parse string source sets.
 */
var sourceSetRegExp = /([^ ]+) (\d+)([WwXx])/;
/**
 * Defines all available source set modes.
 */
export var SourceSetMode;
(function (SourceSetMode) {
    SourceSetMode[SourceSetMode["Width"] = 0] = "Width";
    SourceSetMode[SourceSetMode["Density"] = 1] = "Density";
})(SourceSetMode || (SourceSetMode = {}));
/**
 * Stores a set of image source definitions.
 */
var SourceSet = (function () {
    /**
     * SourceSet constructor.
     *
     * @param data
     *   A source set string, an array of IImageSource objects or the desired source mode.
     */
    function SourceSet(data) {
        var _this = this;
        /**
         * The list of source definitions within this set.
         */
        this.sources = [];
        if (isArray(data)) {
            forEach(data, function (source) { return _this.add(source); });
        }
        else if (typeof data === 'number' && data in SourceSetMode) {
            this.mode = data;
        }
        else if (typeof data === 'string') {
            this.parse(data);
        }
    }
    /**
     * Parse the given source set string.
     *
     * @param str
     *   A string containing a source set definition.
     */
    SourceSet.prototype.parse = function (str) {
        var sources = str.split(/,/);
        for (var _i = 0, sources_1 = sources; _i < sources_1.length; _i++) {
            var source = sources_1[_i];
            source = trim(source);
            var match = sourceSetRegExp.exec(source);
            if (match) {
                var mode = (match[3] === 'W' || match[3] === 'w')
                    ? SourceSetMode.Width
                    : SourceSetMode.Density;
                this.add({
                    src: match[1],
                    bias: parseFloat(match[2]),
                    mode: mode,
                });
            }
            else {
                this.add({
                    src: source,
                });
            }
        }
    };
    /**
     * Add a source definition to this set.
     *
     * @param source
     *   The source definition that should be added.
     */
    SourceSet.prototype.add = function (source) {
        if (source.mode != void 0) {
            if (!this.mode) {
                this.mode = source.mode;
            }
            else if (this.mode != source.mode) {
                console.warn('Mismatched image srcSet, all sources must use same mode.');
                return;
            }
        }
        if (source.bias === void 0) {
            source.bias = Number.MAX_VALUE;
        }
        this.sources.push(source);
        this.sources.sort(function (a, b) { return a.bias - b.bias; });
    };
    /**
     * Return the matching source for the given element.
     *
     * @param el
     *   The element whose dimensions should be used to determine the source.
     * @returns
     *   The best matching source identifier.
     */
    SourceSet.prototype.get = function (el) {
        var _a = this, mode = _a.mode, sources = _a.sources;
        var count = sources.length;
        if (count === 0) {
            return '';
        }
        var threshold = window.devicePixelRatio
            ? Math.max(1, Math.min(1.75, window.devicePixelRatio))
            : 1;
        if (mode === SourceSetMode.Width) {
            threshold = el.offsetWidth * threshold;
        }
        for (var _i = 0, sources_2 = sources; _i < sources_2.length; _i++) {
            var source = sources_2[_i];
            if (source.bias >= threshold)
                return source.src;
        }
        return sources[count - 1].src;
    };
    return SourceSet;
}());
export default SourceSet;
