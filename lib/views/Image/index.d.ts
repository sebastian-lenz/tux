/// <reference types="q" />
import * as Q from 'q';
import SourceSet, { ImageSourceSetSource } from './SourceSet';
import View, { ViewOptions } from '../View';
import { VisibilityTarget } from '../../services/Visibility';
/**
 * Defines all known load states of the Image class.
 */
export declare const enum LoadState {
    Idle = 0,
    Loading = 1,
    Loaded = 2,
}
export declare enum RoundingMode {
    Round = 0,
    Floor = 1,
    Ceil = 2,
}
/**
 * Constructor options for the Image class.
 */
export interface ImageOptions extends ViewOptions {
    /**
     * Whether to disable the visibility check or not.
     */
    disableVisibilityCheck?: boolean;
    /**
     * The source set definition that should be used.
     */
    sourceSet?: ImageSourceSetSource;
    sizeRoundingMode?: RoundingMode | string;
    offsetRoundingMode?: RoundingMode | string;
}
export declare function round(value: number, mode?: RoundingMode): number;
/**
 * Displays an image.
 *
 * @event "load" ():void
 *   Triggered after the image has been loaded.
 */
export default class Image extends View implements VisibilityTarget {
    element: HTMLImageElement;
    /**
     * The native width of this image.
     */
    nativeWidth: number;
    /**
     * The native height of this image.
     */
    nativeHeight: number;
    /**
     * The set of source files used by this image.
     */
    sourceSet: SourceSet;
    sizeRoundingMode: RoundingMode;
    offsetRoundingMode: RoundingMode;
    /**
     * The current load state of this image.
     */
    loadState: LoadState;
    /**
     * Whether this element is currently within the visible viewport bounds or not.
     */
    isInViewport: boolean;
    /**
     * The deferred object used to generate the load promise.
     */
    private deferred;
    /**
     * Image constructor.
     *
     * @param options
     *   The constructor options.
     */
    constructor(options?: ImageOptions);
    remove(): this;
    /**
     * Set the size and optionally the position shift of this image.
     *
     * @param width
     *   The desired width of the image.
     * @param height
     *   The desired height of the image.
     * @param left
     *   The horizontal shift of the image.
     * @param top
     *   The vertical shift of the image.
     */
    setSize(width: number, height: number, left?: number, top?: number): void;
    /**
     * Set whether the element of this view is currently within the visible bounds
     * of the viewport or not.
     *
     * @param inViewport
     *   TRUE if the element is visible, FALSE otherwise.
     */
    setInViewport(inViewport: boolean): void;
    /**
     * Update the source location of the image due to the current dimensions.
     */
    update(): void;
    /**
     * Load the image.
     *
     * @returns
     *   A promise that resolves after the image has loaded.
     */
    load(): Q.Promise<Image>;
    /**
     * Setup the load event listener when loading the image for the first time.
     */
    protected prepareLoadEvent(): void;
    /**
     * Triggered after the image has been loadedf or the first time.
     */
    onLoad(): void;
}
