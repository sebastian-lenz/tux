/**
 * Type definition for all applicable SourceSet constructor arguments.
 */
export declare type ImageSourceSetSource = string | ImageSource[] | SourceSetMode;
/**
 * Defines all available source set modes.
 */
export declare enum SourceSetMode {
    Width = 0,
    Density = 1,
}
/**
 * Data structure used to store individual sources.
 */
export interface ImageSource {
    /**
     * The image source identifier.
     */
    src: string;
    /**
     * The relevant width or pixel density bias.
     */
    bias?: number;
    /**
     * The desired bias mode.
     */
    mode?: SourceSetMode;
}
/**
 * Stores a set of image source definitions.
 */
export default class SourceSet {
    /**
     * The list of source definitions within this set.
     */
    sources: ImageSource[];
    /**
     * The mode used to determine the current source.
     */
    mode: SourceSetMode;
    /**
     * SourceSet constructor.
     *
     * @param data
     *   A source set string, an array of IImageSource objects or the desired source mode.
     */
    constructor(data?: ImageSourceSetSource);
    /**
     * Parse the given source set string.
     *
     * @param str
     *   A string containing a source set definition.
     */
    parse(str: string): void;
    /**
     * Add a source definition to this set.
     *
     * @param source
     *   The source definition that should be added.
     */
    add(source: ImageSource): void;
    /**
     * Return the matching source for the given element.
     *
     * @param el
     *   The element whose dimensions should be used to determine the source.
     * @returns
     *   The best matching source identifier.
     */
    get(el: HTMLElement): string;
}
