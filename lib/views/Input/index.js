import * as tslib_1 from "tslib";
import { throttle } from 'underscore';
import DocumentView from '../../services/DocumentView';
import View, { option } from '../View';
/**
 * A view that wraps an input element.
 *
 * @event "change" (value:string):void
 *   Triggered every time the value of the inout changes.
 * @event "focus" (hasFocus:boolean):void
 *   Triggered after the input got or lost focus.
 */
var Input = (function (_super) {
    tslib_1.__extends(Input, _super);
    /**
     * Input constructor.
     *
     * @param options
     *   Input constructor options.
     */
    function Input(options) {
        var _this = _super.call(this, options) || this;
        /**
         * Whether the input element is currently focused or not.
         */
        _this.hasFocus = false;
        /**
         * Triggered after the content of the input field might have changed.
         */
        _this.onInput = throttle(function () {
            var value = _this.input.value;
            if (_this.value != value) {
                _this.value = value;
                _this.element.classList.toggle('hasValue', value !== '');
                _this.trigger('change', value);
            }
        }, 50, { leading: false });
        /**
         * Triggered after the input element has gained focus.
         *
         * @param event
         */
        _this.onFocusIn = function (event) {
            if (_this.hasFocus)
                return;
            _this.hasFocus = true;
            _this.element.classList.add('focused');
            _this.trigger('focus', true);
        };
        /**
         * Triggered after the input element has lost focus.
         */
        _this.onFocusOut = function () {
            if (!_this.hasFocus)
                return;
            _this.hasFocus = false;
            _this.element.classList.remove('focused');
            _this.trigger('focus', false);
        };
        var input = _this.input;
        _this.value = input.value;
        _this.inputView = new View({ element: input });
        var inputView = _this.inputView;
        inputView.delegate('input', _this.onInput);
        inputView.delegate('change', _this.onInput);
        inputView.delegate('keyup', _this.onInput);
        inputView.delegate('paste', _this.onInput);
        inputView.delegate('focus', _this.onFocusIn);
        if (options.useNativeBlur) {
            inputView.delegate('blur', _this.onFocusOut);
        }
        else {
            _this.listenTo(DocumentView.getInstance(), 'pointerDown', _this.onDocumentPointerDown);
        }
        return _this;
    }
    /**
     * Set the focus on the input element.
     */
    Input.prototype.focus = function () {
        this.input.focus();
        return this;
    };
    /**
     * Remove the focus from the input element.
     */
    Input.prototype.blur = function () {
        this.input.blur();
        return this;
    };
    /**
     * Set the new value of the input field.
     *
     * @param value
     *   The new value of the input field.
     */
    Input.prototype.setValue = function (value) {
        if (this.value == value)
            return this;
        this.value = value;
        this.input.value = value;
        this.element.classList.toggle('hasValue', value !== '');
        this.trigger('change', value);
        return this;
    };
    Input.prototype.onDocumentPointerDown = function (event) {
        if (!this.hasFocus)
            return;
        var target = event.target;
        while (target) {
            if (target == this.element)
                return;
            target = target.parentNode;
        }
        this.onFocusOut();
    };
    return Input;
}(View));
export default Input;
tslib_1.__decorate([
    option({
        type: 'element',
        defaultValue: 'input,textarea',
        tagName: 'input',
        className: 'tuxInput__input',
    })
], Input.prototype, "input", void 0);
