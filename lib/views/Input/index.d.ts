/// <reference types="underscore" />
import View, { ViewOptions } from '../View';
/**
 * Constructor options for the Input class.
 */
export interface InputOptions extends ViewOptions {
    /**
     * A css selector pointing to the input element or reference to
     * the input element that should be used.
     */
    input?: string | HTMLInputElement | HTMLTextAreaElement;
    useNativeBlur?: boolean;
}
/**
 * A view that wraps an input element.
 *
 * @event "change" (value:string):void
 *   Triggered every time the value of the inout changes.
 * @event "focus" (hasFocus:boolean):void
 *   Triggered after the input got or lost focus.
 */
export default class Input extends View {
    /**
     * The actual dom input element.
     */
    input: HTMLInputElement | HTMLTextAreaElement;
    /**
     * A view that wraps the input element for event delegation.
     */
    inputView: View;
    /**
     * The current value of the input element.
     */
    value: string;
    /**
     * Whether the input element is currently focused or not.
     */
    hasFocus: boolean;
    /**
     * Input constructor.
     *
     * @param options
     *   Input constructor options.
     */
    constructor(options?: InputOptions);
    /**
     * Set the focus on the input element.
     */
    focus(): this;
    /**
     * Remove the focus from the input element.
     */
    blur(): this;
    /**
     * Set the new value of the input field.
     *
     * @param value
     *   The new value of the input field.
     */
    setValue(value: string): this;
    /**
     * Triggered after the content of the input field might have changed.
     */
    onInput: (() => void) & _.Cancelable;
    /**
     * Triggered after the input element has gained focus.
     *
     * @param event
     */
    onFocusIn: (event: FocusEvent) => void;
    /**
     * Triggered after the input element has lost focus.
     */
    onFocusOut: () => void;
    onDocumentPointerDown(event: Event): void;
}
