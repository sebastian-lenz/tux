import * as tslib_1 from "tslib";
import View, { option } from '../View';
import { Vector2 } from '../../geom/Vector2';
import transform from '../../util/vendor/transform';
import { stopAnimations } from '../../fx/AnimationManager';
import { DragDirection } from '../../handler/DragHandler';
import WheelHandler from './WheelHandler';
import DragHandler from './DragHandler';
export { DragDirection } from '../../handler/DragHandler';
/**
 * @event "scroll" (offset:IVector2):void
 */
var Scroller = (function (_super) {
    tslib_1.__extends(Scroller, _super);
    function Scroller(options) {
        var _this = _super.call(this, options) || this;
        _this.maxOffset = new Vector2(0, 0);
        _this.minOffset = new Vector2(0, 0);
        _this.offset = new Vector2(0, 0);
        _this.size = new Vector2(0, 0);
        if (!_this.viewport) {
            _this.viewport = _this.element;
        }
        _this.wheelHandler = new WheelHandler(_this);
        _this.dragHandler = new DragHandler(_this);
        return _this;
    }
    Scroller.prototype.clampOffset = function (value) {
        var _a = this, maxOffset = _a.maxOffset, minOffset = _a.minOffset;
        if (value.x < minOffset.x)
            value.x = minOffset.x;
        if (value.x > maxOffset.x)
            value.x = maxOffset.x;
        if (value.y < minOffset.y)
            value.y = minOffset.y;
        if (value.y > maxOffset.y)
            value.y = maxOffset.y;
        return value;
    };
    Scroller.prototype.setOffset = function (value, clamped) {
        var _a = this, direction = _a.direction, offset = _a.offset;
        var changed = false;
        if (clamped) {
            value = this.clampOffset(value);
        }
        if (direction & DragDirection.Horizontal) {
            var x_1 = value.x;
            if (offset.x != x_1) {
                changed = true;
                offset.x = x_1;
            }
        }
        if (direction & DragDirection.Vertical) {
            var y = value.y;
            if (offset.y != y) {
                changed = true;
                offset.y = y;
            }
        }
        if (changed) {
            this.handleOffsetChange(offset);
        }
    };
    Scroller.prototype.setMinOffset = function (value) {
        var direction = this.direction;
        var offset = this.offset;
        var min = this.minOffset;
        var changed = false;
        min.x = (direction & DragDirection.Horizontal) ? value.x : 0;
        min.y = (direction & DragDirection.Vertical) ? value.y : 0;
        if (offset.x < min.x) {
            offset.x = min.x;
            changed = true;
        }
        if (offset.y < min.y) {
            offset.y = min.y;
            changed = true;
        }
        if (changed) {
            stopAnimations(this, 'offset');
            this.handleOffsetChange(offset);
        }
    };
    Scroller.prototype.setMaxOffset = function (value) {
        var direction = this.direction;
        var offset = this.offset;
        var max = this.maxOffset;
        var changed = false;
        max.x = (direction & DragDirection.Horizontal) ? value.x : 0;
        max.y = (direction & DragDirection.Vertical) ? value.y : 0;
        if (offset.x > max.x) {
            offset.x = max.x;
            changed = true;
        }
        if (offset.y > max.y) {
            offset.y = max.y;
            changed = true;
        }
        if (changed) {
            stopAnimations(this, 'offset');
            this.handleOffsetChange(offset);
        }
    };
    Scroller.prototype.handleOffsetChange = function (offset) {
        var el = this.container;
        var x, y;
        if (this.roundOffset) {
            x = Math.round(offset.x);
            y = Math.round(offset.y);
        }
        else {
            x = offset.x;
            y = offset.y;
        }
        if (transform.styleName) {
            if (transform.has3D) {
                el.style[transform.styleName] = "translate3d(" + x + "px, " + y + "px, 0)";
            }
            else {
                el.style[transform.styleName] = "translate(" + x + "px, " + y + "px)";
            }
        }
        else {
            el.style.left = x + "px";
            el.style.top = y + "px";
        }
        this.trigger('scroll', offset);
    };
    /**
     * Triggered after the size of the viewport has changed.
     *
     * If this view is used as a component this handler will be called automatically.
     *
     * @param node
     *   The component node that triggered the resize.
     * @returns
     *   TRUE if the event handling should be stopped and child components should not receive
     *   the event, FALSE otherwise.
     */
    Scroller.prototype.handleViewportResize = function (resizeChildren) {
        if (resizeChildren)
            resizeChildren();
        this.size.x = this.viewport.offsetWidth;
        this.size.y = this.viewport.offsetHeight;
        this.setMinOffset({
            x: Math.min(0, this.size.x - this.container.scrollWidth),
            y: Math.min(0, this.size.y - this.container.scrollHeight),
        });
        return true;
    };
    return Scroller;
}(View));
export default Scroller;
tslib_1.__decorate([
    option({ type: 'element' })
], Scroller.prototype, "viewport", void 0);
tslib_1.__decorate([
    option({ type: 'element', defaultValue: '.container' })
], Scroller.prototype, "container", void 0);
tslib_1.__decorate([
    option({ type: 'enum', values: DragDirection, defaultValue: DragDirection.Vertical })
], Scroller.prototype, "direction", void 0);
tslib_1.__decorate([
    option({ type: 'bool', defaultValue: false })
], Scroller.prototype, "roundOffset", void 0);
