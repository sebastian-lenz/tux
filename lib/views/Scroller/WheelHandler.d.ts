import WheelHandlerBase, { IWheelData } from '../../handler/WheelHandler';
import Scroller from './index';
export default class WheelHandler extends WheelHandlerBase {
    view: Scroller;
    constructor(view: Scroller, selector?: string);
    /**
     * Triggered after the user has used the mouse wheel.
     *
     * @param event
     *   The original dom event.
     * @param data
     *   The normalized scroll data.
     * @returns
     *   TRUE if the event has been handled, FALSE otherwise.
     */
    handleWheel(event: Event, data: IWheelData): boolean;
}
