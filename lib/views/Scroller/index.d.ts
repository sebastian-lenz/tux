import View, { ViewOptions } from '../View';
import { Vector2, IVector2 } from '../../geom/Vector2';
import { DragDirection } from '../../handler/DragHandler';
import WheelHandler from './WheelHandler';
import DragHandler from './DragHandler';
export { DragDirection } from '../../handler/DragHandler';
export interface ScrollerOptions extends ViewOptions {
    viewport?: string | HTMLElement;
    container?: string | HTMLElement;
    direction?: DragDirection;
    roundOffset?: boolean;
}
/**
 * @event "scroll" (offset:IVector2):void
 */
export default class Scroller extends View {
    viewport: HTMLElement;
    container: HTMLElement;
    direction: DragDirection;
    roundOffset: boolean;
    dragHandler: DragHandler;
    maxOffset: Vector2;
    minOffset: Vector2;
    offset: Vector2;
    size: Vector2;
    wheelHandler: WheelHandler;
    constructor(options?: ScrollerOptions);
    clampOffset(value: IVector2): IVector2;
    setOffset(value: IVector2, clamped?: boolean): void;
    setMinOffset(value: IVector2): void;
    setMaxOffset(value: IVector2): void;
    handleOffsetChange(offset: Vector2): void;
    /**
     * Triggered after the size of the viewport has changed.
     *
     * If this view is used as a component this handler will be called automatically.
     *
     * @param node
     *   The component node that triggered the resize.
     * @returns
     *   TRUE if the event handling should be stopped and child components should not receive
     *   the event, FALSE otherwise.
     */
    handleViewportResize(resizeChildren?: Function): boolean;
}
