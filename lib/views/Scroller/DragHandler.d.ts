import { Vector2 } from '../../geom/Vector2';
import DragHandlerBase from '../../handler/DragHandler';
import Scroller from './index';
import Pointer from '../../handler/Pointer';
import '../../fx/momentum/VectorMomentum';
export default class DragHandler extends DragHandlerBase {
    view: Scroller;
    initialOffset: Vector2;
    constructor(view: Scroller);
    /**
     * Triggered after a drag has started.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the drag start.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    handleDragStart(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a drag has moved.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the move.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    handleDragMove(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a drag has ended.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    handleDragEnd(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the user has clicked into the view.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    handleClick(event: Event, pointer: Pointer): void;
}
