import * as tslib_1 from "tslib";
import { UserAgent } from '../../util/polyfill/UserAgent';
import WheelHandlerBase from '../../handler/WheelHandler';
import { DragDirection } from './index';
var power = UserAgent.windows() ? 1.2 : 1;
var WheelHandler = (function (_super) {
    tslib_1.__extends(WheelHandler, _super);
    function WheelHandler(view, selector) {
        return _super.call(this, view, selector) || this;
    }
    /**
     * Triggered after the user has used the mouse wheel.
     *
     * @param event
     *   The original dom event.
     * @param data
     *   The normalized scroll data.
     * @returns
     *   TRUE if the event has been handled, FALSE otherwise.
     */
    WheelHandler.prototype.handleWheel = function (event, data) {
        var direction = this.view.direction;
        var x = (direction & DragDirection.Horizontal) ? data.pixelX * power : 0;
        var y = (direction & DragDirection.Vertical) ? data.pixelY * power : 0;
        var offset = this.view.offset.clone().subtract(x, y);
        this.view.setOffset(offset, true);
        return x != 0 || y != 0;
    };
    return WheelHandler;
}(WheelHandlerBase));
export default WheelHandler;
