import * as tslib_1 from "tslib";
import { createMomentum } from '../../fx/momentum/Momentum';
import { stopAnimations } from '../../fx/AnimationManager';
import { Vector2 } from '../../geom/Vector2';
import DragHandlerBase, { DragDirection } from '../../handler/DragHandler';
import '../../fx/momentum/VectorMomentum';
var DragHandler = (function (_super) {
    tslib_1.__extends(DragHandler, _super);
    function DragHandler(view) {
        var _this = _super.call(this, view) || this;
        _this.initialOffset = new Vector2(0, 0);
        _this.direction = view.direction;
        return _this;
    }
    /**
     * Triggered after a drag has started.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the drag start.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    DragHandler.prototype.handleDragStart = function (event, pointer) {
        if (this.view.minOffset.x == 0 && this.view.minOffset.y == 0) {
            return false;
        }
        this.initialOffset.copy(this.view.offset);
        stopAnimations(this.view, 'offset');
        event.preventDefault();
        return true;
    };
    /**
     * Triggered after a drag has moved.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the move.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    DragHandler.prototype.handleDragMove = function (event, pointer) {
        var offset = this.initialOffset.clone();
        var min = this.view.minOffset;
        var max = this.view.maxOffset;
        var delta = pointer.getAbsoluteDelta();
        if (this.view.direction & DragDirection.Horizontal) {
            offset.x += delta.x;
            if (offset.x < min.x) {
                offset.x = min.x + (offset.x - min.x) * 0.5;
            }
            if (offset.x > max.x) {
                offset.x = max.x + (offset.x - max.x) * 0.5;
            }
        }
        if (this.view.direction & DragDirection.Vertical) {
            offset.y += delta.y;
            if (offset.y < min.y) {
                offset.y = min.y + (offset.y - min.y) * 0.5;
            }
            if (offset.y > max.y) {
                offset.y = max.y + (offset.y - max.y) * 0.5;
            }
        }
        this.view.setOffset(offset);
        event.preventDefault();
        return true;
    };
    /**
     * Triggered after a drag has ended.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    DragHandler.prototype.handleDragEnd = function (event, pointer) {
        createMomentum(this.view, 'offset', {
            velocity: Vector2.convert(pointer.getVelocity()),
            min: this.view.minOffset,
            max: this.view.maxOffset,
        });
    };
    /**
     * Triggered after the user has clicked into the view.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    DragHandler.prototype.handleClick = function (event, pointer) {
        this.view.trigger('click', event, pointer);
    };
    return DragHandler;
}(DragHandlerBase));
export default DragHandler;
