import View from '../View';
import ChildableView, { ChildableViewOptions } from '../ChildableView';
/**
 * A generic CycleableView type definition.
 */
export declare type AnyCycleableView = CycleableView<View, any>;
/**
 * Constructor options for the CycleableView class.
 */
export interface CycleableViewOptions<T extends View = View> extends ChildableViewOptions<T> {
    /**
     * The child that should be initially selected.
     */
    current?: T;
    /**
     * The index of the child that should be initially selected.
     */
    index?: number;
    /**
     * Whether the first child should be automatically selected if not selected child
     * is present or not.
     */
    autoSelectFirstChild?: boolean;
    /**
     * Whether the currently selected item should be reset on any change or not.
     */
    resetOnAnyChange?: boolean;
    /**
     * Whether the children should be looped or not.
     */
    isLooped?: boolean;
}
/**
 * A childable view which is aware of a current child.
 *
 * @event "currentChanged" (newChild:T, oldChild:T, options:U):void
 *   Triggered after the current child has changed.
 */
export default class CycleableView<T extends View = View, U = any> extends ChildableView<T> {
    /**
     * The currently selected child.
     */
    current: T;
    /**
     * Whether the first child should be automatically selected if not selected child
     * is present or not.
     */
    autoSelectFirstChild: boolean;
    /**
     * Whether the children should be looped or not.
     */
    isLooped: boolean;
    /**
     * Whether the currently selected item should be reset on any change or not.
     */
    resetOnAnyChange: boolean;
    /**
     * Whether the current child is locked or not.
     */
    isLocked: boolean;
    /**
     * CycleableView constructor.
     *
     * @param options
     *   The constructor options.
     */
    constructor(options?: CycleableViewOptions<T>);
    /**
     * Normalize the given index.
     *
     * @param index
     *   The index value that should be normalized.
     * @returns
     *   The normalized index value.
     */
    normalize(index: number): number;
    /**
     * Internal transition handler. Creates a transition between the two given children.
     *
     * @param newChild
     *   The child the transition should lead to.
     * @param oldChild
     *   The child the transition should come from.
     * @param options
     *   Optional transition options.
     */
    protected handleTransition(newChild: T, oldChild: T, options?: U): void;
    /**
     * Set the current child.
     *
     * @param child
     *   The child that should be set as current.
     * @param options
     *   Optional transition options.
     */
    setCurrent(child: T, options?: U): this;
    /**
     * Set the index of the current child.
     *
     * @param index
     *   The index of the child that should be set as current.
     * @param options
     */
    setCurrentIndex(index: number, options?: U): this;
    /**
     * Return the index of the current child.
     *
     * @returns
     *   The index of the current child.
     */
    getCurrentIndex(): number;
    /**
     * Return the previous child.
     *
     * @returns
     *   The previous child.
     */
    getNextChild(): T;
    /**
     * Return the next child.
     *
     * @returns
     *   The next child.
     */
    getPreviousChild(): T;
    /**
     * Reset the currently selected child.
     */
    protected resetCurrent(): void;
    /**
     * Triggered after a child has been added to this view.
     */
    onChildAdd(child: T, at: number): void;
    /**
     * Triggered after a child has been removed to this view.
     *
     * @param child
     *   The child view that has been removed.
     */
    onChildRemove(child: T): void;
    /**
     * Triggered after the children have been reset.
     */
    onChildrenReset(): void;
}
