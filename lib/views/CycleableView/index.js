import * as tslib_1 from "tslib";
import { option } from '../View';
import ChildableView from '../ChildableView';
/**
 * A childable view which is aware of a current child.
 *
 * @event "currentChanged" (newChild:T, oldChild:T, options:U):void
 *   Triggered after the current child has changed.
 */
var CycleableView = (function (_super) {
    tslib_1.__extends(CycleableView, _super);
    /**
     * CycleableView constructor.
     *
     * @param options
     *   The constructor options.
     */
    function CycleableView(options) {
        var _this = _super.call(this, options || (options = {})) || this;
        /**
         * The currently selected child.
         */
        _this.current = null;
        /**
         * Whether the current child is locked or not.
         */
        _this.isLocked = false;
        if (options.index !== void 0) {
            _this.setCurrentIndex(options.index);
        }
        else if (options.current) {
            _this.setCurrent(options.current);
        }
        else if (_this.autoSelectFirstChild && _this.getLength()) {
            _this.setCurrentIndex(0);
        }
        _this.listenTo(_this, 'add', _this.onChildAdd);
        _this.listenTo(_this, 'remove', _this.onChildRemove);
        _this.listenTo(_this, 'reset', _this.onChildrenReset);
        return _this;
    }
    /**
     * Normalize the given index.
     *
     * @param index
     *   The index value that should be normalized.
     * @returns
     *   The normalized index value.
     */
    CycleableView.prototype.normalize = function (index) {
        var count = this.getLength();
        if (count < 1) {
            return -1;
        }
        var normalized = index;
        if (this.isLooped) {
            while (normalized < 0)
                normalized += count;
            while (normalized >= count)
                normalized -= count;
        }
        else {
            if (normalized < 0)
                return -1;
            if (normalized >= count)
                return -1;
        }
        return normalized;
    };
    /**
     * Internal transition handler. Creates a transition between the two given children.
     *
     * @param newChild
     *   The child the transition should lead to.
     * @param oldChild
     *   The child the transition should come from.
     * @param options
     *   Optional transition options.
     */
    CycleableView.prototype.handleTransition = function (newChild, oldChild, options) { };
    /**
     * Set the current child.
     *
     * @param child
     *   The child that should be set as current.
     * @param options
     *   Optional transition options.
     */
    CycleableView.prototype.setCurrent = function (child, options) {
        if (this.isLocked || this.current === child) {
            return this;
        }
        var oldChild = this.current;
        this.current = child;
        this.handleTransition(child, oldChild, options);
        this.trigger('currentChanged', child, oldChild, options);
        return this;
    };
    /**
     * Set the index of the current child.
     *
     * @param index
     *   The index of the child that should be set as current.
     * @param options
     */
    CycleableView.prototype.setCurrentIndex = function (index, options) {
        index = this.normalize(index);
        return this.setCurrent(this.children[index], options);
    };
    /**
     * Return the index of the current child.
     *
     * @returns
     *   The index of the current child.
     */
    CycleableView.prototype.getCurrentIndex = function () {
        return this.indexOf(this.current);
    };
    /**
     * Return the previous child.
     *
     * @returns
     *   The previous child.
     */
    CycleableView.prototype.getNextChild = function () {
        return this.getChild(this.normalize(this.getCurrentIndex() + 1));
    };
    /**
     * Return the next child.
     *
     * @returns
     *   The next child.
     */
    CycleableView.prototype.getPreviousChild = function () {
        return this.getChild(this.normalize(this.getCurrentIndex() - 1));
    };
    /**
     * Reset the currently selected child.
     */
    CycleableView.prototype.resetCurrent = function () {
        if (!this.current)
            return;
        var isRemove = false;
        if (this.getCurrentIndex() == -1) {
            this.current = null;
            isRemove = true;
        }
        if (this.autoSelectFirstChild && this.getLength()) {
            this.setCurrent(this.children[0]);
        }
        else {
            if (isRemove) {
                this.trigger('currentChanged', null, null);
            }
            else {
                this.setCurrent(null);
            }
        }
    };
    /**
     * Triggered after a child has been added to this view.
     */
    CycleableView.prototype.onChildAdd = function (child, at) {
        if (this.resetOnAnyChange) {
            this.resetCurrent();
        }
    };
    /**
     * Triggered after a child has been removed to this view.
     *
     * @param child
     *   The child view that has been removed.
     */
    CycleableView.prototype.onChildRemove = function (child) {
        if (this.resetOnAnyChange || (child == this.current)) {
            this.resetCurrent();
        }
    };
    /**
     * Triggered after the children have been reset.
     */
    CycleableView.prototype.onChildrenReset = function () {
        if (this.resetOnAnyChange || (this.current && this.getCurrentIndex() == -1)) {
            this.resetCurrent();
        }
    };
    return CycleableView;
}(ChildableView));
export default CycleableView;
tslib_1.__decorate([
    option({ type: 'bool', defaultValue: false })
], CycleableView.prototype, "autoSelectFirstChild", void 0);
tslib_1.__decorate([
    option({ type: 'bool', defaultValue: false })
], CycleableView.prototype, "isLooped", void 0);
tslib_1.__decorate([
    option({ type: 'bool', defaultValue: false })
], CycleableView.prototype, "resetOnAnyChange", void 0);
