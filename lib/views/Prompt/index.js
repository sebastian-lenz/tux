import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import { option } from '../View';
import { trim } from '../../util/string';
import Collection from '../../models/Collection';
import Input from '../Input';
import Item from './Item';
import List from './List';
/**
 * An input element with prompting support.
 *
 * @event "select" (model:Backbone.Model):void
 *   Triggered if the user selects an entry of the prompt suggestions
 *   by clicking on it or by pressing enter while it is selected.
 */
var Prompt = (function (_super) {
    tslib_1.__extends(Prompt, _super);
    /**
     * Prompt constructor.
     *
     * @param options
     *   Prompt constructor options.
     */
    function Prompt(options) {
        var _this = _super.call(this, defaults(options || {}, {
            className: 'tuxPrompt',
            listClass: List,
            itemClass: Item,
            attributes: { autocomplete: 'off' }
        })) || this;
        _this.onKeyDown = function (event) {
            var target;
            switch (event.keyCode) {
                case 13:
                    if (_this.list.current) {
                        event.preventDefault();
                        _this.trigger('select', _this.list.current.model);
                    }
                    break;
                case 38:
                    event.preventDefault();
                    target = _this.list.getPreviousChild();
                    if (target) {
                        _this.list.setCurrent(target);
                    }
                    break;
                case 40:
                    event.preventDefault();
                    target = _this.list.getNextChild();
                    if (target) {
                        _this.list.setCurrent(target);
                    }
                    break;
            }
        };
        var suggestions = options.suggestions;
        if (suggestions === void 0) {
            suggestions = new Collection();
            if (options.url) {
                suggestions.url = options.url;
            }
        }
        _this.element.classList.add('noSuggestions');
        _this.suggestions = suggestions;
        _this.list = new options.listClass({
            appendTo: _this.element,
            collection: suggestions,
            childClass: options.itemClass
        });
        _this.delegate('keydown', _this.onKeyDown, { selector: 'input' });
        _this.listenTo(_this.list, 'click', _this.onListClick);
        _this.listenTo(_this.list, 'add', _this.onListChange);
        _this.listenTo(_this.list, 'remove', _this.onListChange);
        _this.listenTo(_this.list, 'reset', _this.onListChange);
        _this.listenTo(_this, 'change', _this.onInputChanged);
        return _this;
    }
    Prompt.prototype.setUrl = function (url, paramName, defaultParams) {
        this.suggestions.url = url;
        this.urlDefaultParams = defaultParams;
        if (paramName) {
            this.urlParam = paramName;
        }
        this.list.removeAllChildren();
    };
    Prompt.prototype.setItemTemplate = function (template) {
        this.list.setItemTemplate(template);
        return this;
    };
    Prompt.prototype.fetch = function () {
        var value = trim(this.value);
        if (value == '') {
            this.suggestions.reset();
        }
        else {
            var params = {};
            if (this.urlDefaultParams) {
                defaults(params, this.urlDefaultParams);
            }
            params[this.urlParam] = value;
            this.suggestions.fetch({ data: params });
        }
    };
    /**
     * Triggered if the user clicks onto a item within the list.
     *
     * @param item
     *   The prommpt item the user has clicked onto.
     */
    Prompt.prototype.onListClick = function (item) {
        this.trigger('select', item.model);
    };
    Prompt.prototype.onListChange = function () {
        this.element.classList.toggle('noSuggestions', this.list.getLength() == 0);
    };
    /**
     * Triggered after the value of the input has changed.
     */
    Prompt.prototype.onInputChanged = function () {
        this.fetch();
    };
    return Prompt;
}(Input));
export default Prompt;
tslib_1.__decorate([
    option({ type: 'string', defaultValue: 'q' })
], Prompt.prototype, "urlParam", void 0);
tslib_1.__decorate([
    option({ type: 'any' })
], Prompt.prototype, "urlDefaultParams", void 0);
