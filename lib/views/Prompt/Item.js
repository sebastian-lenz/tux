import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import View, { option } from '../View';
/**
 * Default template function.
 */
var defaultTemplateFunction = function (data) {
    return data.toString();
};
/**
 * Displays a single prompting suggestion.
 */
var PromptItem = (function (_super) {
    tslib_1.__extends(PromptItem, _super);
    /**
     * Prompt constructor.
     *
     * @param options
     *   Prompt constructor options.
     */
    function PromptItem(options) {
        var _this = _super.call(this, defaults(options || {}, {
            tagName: 'li',
            className: 'tuxPromptItem'
        })) || this;
        _this.list = options.owner;
        _this.render();
        _this.delegate('mouseenter', null, _this.onMouseEnter);
        _this.delegateClick(_this.onClick);
        return _this;
    }
    /**
     * Render this view.
     */
    PromptItem.prototype.render = function () {
        this.element.innerHTML = this.itemTemplate(this.model ? this.model.attributes : {});
        return this;
    };
    /**
     * Triggered if the mouse enters this view.
     */
    PromptItem.prototype.onMouseEnter = function () {
        if (this.list) {
            this.list.setCurrent(this);
        }
    };
    /**
     * Triggered after the user has clicked onto this item.
     */
    PromptItem.prototype.onClick = function () {
        this.list.trigger('click', this);
        return true;
    };
    return PromptItem;
}(View));
export default PromptItem;
tslib_1.__decorate([
    option({ type: 'any', defaultValue: defaultTemplateFunction })
], PromptItem.prototype, "itemTemplate", void 0);
