import CycleableView, { CycleableViewOptions } from '../CycleableView';
import Item, { PromptItemOptions } from './Item';
import { TemplateFunction } from './index';
/**
 * Constructor options for the PromptList class.
 */
export interface PromptListOptions extends CycleableViewOptions<Item> {
    /**
     * The template that is used to render the contents of each row.
     */
    template?: TemplateFunction;
}
/**
 * Displays a list of prompting suggestions.
 *
 * @event "click" (item:PromptItem):void
 *   Triggered if the user clicks onto one of the items.
 */
export default class PromptList extends CycleableView<Item> {
    /**
     * The template that is used to render the contents of each row.
     */
    itemTemplate: TemplateFunction;
    /**
     * PromptList constructor.
     *
     * @param options
     *   PromptList constructor options.
     */
    constructor(options?: PromptListOptions);
    /**
     * Set the template that is used to render the contents of each row.
     *
     * @param itemTemplate
     *   The template function that should be used to render row contents.
     */
    setItemTemplate(itemTemplate: TemplateFunction): this;
    /**
     * Allow the view class to alter the constructor options before creating
     * new child view instances.
     *
     * @param options
     *   The predefined child constructor options.
     * @returns
     *   The altered child constructor options.
     */
    protected alterChildOptions(options: PromptItemOptions): PromptItemOptions;
    /**
     * Internal transition handler. Creates a transition between the two given children.
     *
     * @param newChild
     *   The child the transition should lead to.
     * @param oldChild
     *   The child the transition should come from.
     * @param options
     *   Optional transition options.
     */
    protected handleTransition(newChild: Item, oldChild: Item, options?: any): void;
}
