import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import { option } from '../View';
import CycleableView from '../CycleableView';
import Item from './Item';
/**
 * Displays a list of prompting suggestions.
 *
 * @event "click" (item:PromptItem):void
 *   Triggered if the user clicks onto one of the items.
 */
var PromptList = (function (_super) {
    tslib_1.__extends(PromptList, _super);
    /**
     * PromptList constructor.
     *
     * @param options
     *   PromptList constructor options.
     */
    function PromptList(options) {
        return _super.call(this, defaults(options || {}, {
            tagName: 'ol',
            className: 'tuxPromptList',
            childSelector: 'li',
            childClass: Item,
            resetOnAnyChange: true
        })) || this;
    }
    /**
     * Set the template that is used to render the contents of each row.
     *
     * @param itemTemplate
     *   The template function that should be used to render row contents.
     */
    PromptList.prototype.setItemTemplate = function (itemTemplate) {
        if (this.itemTemplate == itemTemplate)
            return this;
        this.itemTemplate = itemTemplate;
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            child.itemTemplate = itemTemplate;
            child.render();
        }
        return this;
    };
    /**
     * Allow the view class to alter the constructor options before creating
     * new child view instances.
     *
     * @param options
     *   The predefined child constructor options.
     * @returns
     *   The altered child constructor options.
     */
    PromptList.prototype.alterChildOptions = function (options) {
        options.template = this.itemTemplate;
        return options;
    };
    /**
     * Internal transition handler. Creates a transition between the two given children.
     *
     * @param newChild
     *   The child the transition should lead to.
     * @param oldChild
     *   The child the transition should come from.
     * @param options
     *   Optional transition options.
     */
    PromptList.prototype.handleTransition = function (newChild, oldChild, options) {
        if (newChild) {
            newChild.element.classList.add('selected');
        }
        if (oldChild) {
            oldChild.element.classList.remove('selected');
        }
    };
    return PromptList;
}(CycleableView));
export default PromptList;
tslib_1.__decorate([
    option({ type: 'any' })
], PromptList.prototype, "itemTemplate", void 0);
