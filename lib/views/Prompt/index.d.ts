import Collection from '../../models/Collection';
import Input, { InputOptions } from '../Input';
import Item from './Item';
import List from './List';
export declare type TemplateFunction = {
    (model: any): string;
};
/**
 * Constructor options for the Prompt class.
 */
export interface IPromptOptions extends InputOptions {
    url?: string | {
        (): string;
    };
    urlParam: string;
    urlDefaultParams?: any;
    suggestions?: Collection;
    listClass?: typeof List;
    itemClass?: typeof Item;
    itemTemplate?: TemplateFunction;
}
/**
 * An input element with prompting support.
 *
 * @event "select" (model:Backbone.Model):void
 *   Triggered if the user selects an entry of the prompt suggestions
 *   by clicking on it or by pressing enter while it is selected.
 */
export default class Prompt extends Input {
    /**
     * Displays the list of prompting suggestions.
     */
    list: List;
    urlParam: string;
    urlDefaultParams: any;
    suggestions: Collection;
    /**
     * Prompt constructor.
     *
     * @param options
     *   Prompt constructor options.
     */
    constructor(options?: IPromptOptions);
    setUrl(url?: string | {
        (): string;
    }, paramName?: string, defaultParams?: any): void;
    setItemTemplate(template: TemplateFunction): this;
    fetch(): void;
    onKeyDown: (event: KeyboardEvent) => void;
    /**
     * Triggered if the user clicks onto a item within the list.
     *
     * @param item
     *   The prommpt item the user has clicked onto.
     */
    onListClick(item: Item): void;
    onListChange(): void;
    /**
     * Triggered after the value of the input has changed.
     */
    onInputChanged(): void;
}
