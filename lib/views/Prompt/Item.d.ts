import View, { ViewOptions } from '../View';
import { TemplateFunction } from './index';
import List from './List';
/**
 * Constructor options for the Prompt class.
 */
export interface PromptItemOptions extends ViewOptions {
    itemTemplate?: TemplateFunction;
}
/**
 * Displays a single prompting suggestion.
 */
export default class PromptItem extends View {
    /**
     * The list this item belnogs to.
     */
    list: List;
    /**
     * The template that is used to render the contents of this row.
     */
    itemTemplate: TemplateFunction;
    /**
     * Prompt constructor.
     *
     * @param options
     *   Prompt constructor options.
     */
    constructor(options?: PromptItemOptions);
    /**
     * Render this view.
     */
    render(): this;
    /**
     * Triggered if the mouse enters this view.
     */
    onMouseEnter(): void;
    /**
     * Triggered after the user has clicked onto this item.
     */
    onClick(): boolean;
}
