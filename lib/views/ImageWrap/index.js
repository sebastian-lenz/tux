import * as tslib_1 from "tslib";
import Image from '../Image';
import Visibility from '../../services/Visibility';
import View, { option } from '../View';
/**
 * Defines all available scale modes.
 */
export var ScaleMode;
(function (ScaleMode) {
    ScaleMode[ScaleMode["Cover"] = 0] = "Cover";
    ScaleMode[ScaleMode["Fit"] = 1] = "Fit";
    ScaleMode[ScaleMode["Stretch"] = 2] = "Stretch";
    ScaleMode[ScaleMode["Width"] = 3] = "Width";
    ScaleMode[ScaleMode["Height"] = 4] = "Height";
})(ScaleMode || (ScaleMode = {}));
/**
 * Wraps an Image view and allows to apply various scale modes to the image.
 */
var ImageWrap = (function (_super) {
    tslib_1.__extends(ImageWrap, _super);
    /**
     * ImageWrap constructor.
     *
     * @param options
     *   The constructor options.
     */
    function ImageWrap(options) {
        var _this = _super.call(this, options || (options = {})) || this;
        _this.image = new Image({
            element: _this.element.querySelector('img'),
            disableVisibilityCheck: true,
            sizeRoundingMode: options.sizeRoundingMode,
            offsetRoundingMode: options.offsetRoundingMode,
        });
        if (!options.disableVisibilityCheck) {
            Visibility.getInstance().register(_this);
        }
        _this.listenToOnce(_this.image, 'load', _this.onImageLoad);
        return _this;
    }
    /**
     * Remove this view.
     */
    ImageWrap.prototype.remove = function () {
        Visibility.getInstance().unregister(this);
        this.image.remove();
        return _super.prototype.remove.call(this);
    };
    /**
     * Set the size of the image wrap.
     *
     * @param width
     *   The new width of the image wrap.
     * @param height
     *   The new height of the image wrap.
     */
    ImageWrap.prototype.setSize = function (width, height) {
        this.element.style.width = width + 'px';
        this.element.style.height = height + 'px';
        this.update(width, height);
        return this;
    };
    /**
     * Set whether the element of this view is currently within the visible bounds
     * of the viewport or not.
     *
     * @param inViewport
     *   TRUE if the element is visible, FALSE otherwise.
     */
    ImageWrap.prototype.setInViewport = function (inViewport) {
        if (this.inViewport == inViewport)
            return;
        this.inViewport = inViewport;
        this.update();
        this.image.setInViewport(inViewport);
    };
    /**
     * Set the scale mode of this image wrap.
     *
     * @param mode
     *   The new image scale mode.
     */
    ImageWrap.prototype.setScaleMode = function (mode) {
        if (this.scaleMode == mode)
            return;
        this.scaleMode = mode;
        this.element.style.height = '';
        this.element.style.width = '';
        this.update();
    };
    /**
     * Load the image.
     *
     * @returns
     *   A promise that resolves after the image has loaded.
     */
    ImageWrap.prototype.load = function () {
        this.update();
        return this.image.load();
    };
    /**
     * Update the displayed image.
     *
     * @param width
     *   The visible width of the wrapper.
     * @param height
     *   The visible height of the wrapper.
     */
    ImageWrap.prototype.update = function (width, height) {
        width = width || this.element.offsetWidth;
        height = height || this.element.offsetHeight;
        if (!width && !height)
            return;
        var scaled = this.measure(width, height);
        var offsetX, offsetY;
        switch (this.scaleMode) {
            case ScaleMode.Cover:
                offsetX = ImageWrap.calculateShift(width, scaled.width, this.focusX);
                offsetY = ImageWrap.calculateShift(height, scaled.height, this.focusY, true);
                break;
            case ScaleMode.Fit:
                offsetX = (width - scaled.width) * this.focusX;
                offsetY = (height - scaled.height) * this.focusY;
                break;
            case ScaleMode.Width:
                this.element.style.height = scaled.height + 'px';
                break;
            case ScaleMode.Height:
                this.element.style.width = scaled.width + 'px';
                break;
        }
        this.image.setSize(scaled.width, scaled.height, offsetX, offsetY);
    };
    /**
     * Calculate the scaled dimensions of the image according to the given scale mode.
     *
     * @param width
     *   The display width.
     * @param height
     *   The display height.
     * @param scaleMode
     *   The scale mode that should be used to calculate the dimensions.
     * @param maxScale
     *   The maximum allowed scale factor.
     * @returns
     *   An object describing the scaled dimensions of the image.
     */
    ImageWrap.prototype.measure = function (width, height, scaleMode, maxScale) {
        if (scaleMode === void 0) { scaleMode = this.scaleMode; }
        if (maxScale === void 0) { maxScale = this.maxScale; }
        var nativeWidth = this.image.nativeWidth;
        var nativeHeight = this.image.nativeHeight;
        var scale = 1;
        switch (scaleMode) {
            case ScaleMode.Stretch:
                return { width: width, height: height };
            case ScaleMode.Cover:
                scale = Math.max(width / nativeWidth, height / nativeHeight);
                break;
            case ScaleMode.Fit:
                scale = Math.min(width / nativeWidth, height / nativeHeight);
                break;
            case ScaleMode.Width:
                scale = width / nativeWidth;
                break;
            case ScaleMode.Height:
                scale = height / nativeHeight;
                break;
        }
        scale = Math.min(scale, maxScale);
        return {
            width: nativeWidth * scale,
            height: nativeHeight * scale
        };
    };
    /**
     * Triggered after the image has loaded.
     */
    ImageWrap.prototype.onImageLoad = function () {
        this.element.classList.add('loaded');
        this.handleViewportResize();
        this.trigger('load');
    };
    /**
     * Triggered after the size of the viewport has changed.
     *
     * If this view is used as a component this handler will be called automatically.
     *
     * @returns
     *   TRUE if the event handling should be stopped and child components should not receive
     *   the event, FALSE otherwise.
     */
    ImageWrap.prototype.handleViewportResize = function () {
        this.update();
        return true;
    };
    /**
     * Calculate the shift value for a dimension based upon the given focus value.
     *
     * @param space
     *   The available space within the dimension.
     * @param size
     *   The size of the image within the dimension.
     * @param focus
     *   The focus point value within the dimension.
     * @param invert
     *   Whether the focus point should be inverted or not.
     * @returns
     *   The calculated position shift along the dimension.
     */
    ImageWrap.calculateShift = function (space, size, focus, invert) {
        if (invert)
            focus = 1 - focus;
        var shift = Math.round(-size * focus + space / 2);
        if (shift > 0)
            shift = 0;
        if (shift < space - size)
            shift = space - size;
        return shift;
    };
    return ImageWrap;
}(View));
export default ImageWrap;
tslib_1.__decorate([
    option({ type: 'enum', values: ScaleMode, defaultValue: ScaleMode.Fit })
], ImageWrap.prototype, "scaleMode", void 0);
tslib_1.__decorate([
    option({ type: 'number', defaultValue: Number.MAX_VALUE })
], ImageWrap.prototype, "maxScale", void 0);
tslib_1.__decorate([
    option({ type: 'number', defaultValue: 0.5 })
], ImageWrap.prototype, "focusX", void 0);
tslib_1.__decorate([
    option({ type: 'number', defaultValue: 0.5 })
], ImageWrap.prototype, "focusY", void 0);
