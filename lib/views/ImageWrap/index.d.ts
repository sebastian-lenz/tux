/// <reference types="q" />
import { IDimensions } from '../../geom/Dimensions';
import Image, { RoundingMode } from '../Image';
import { VisibilityTarget } from '../../services/Visibility';
import View, { ViewOptions } from '../View';
/**
 * Defines all available scale modes.
 */
export declare enum ScaleMode {
    Cover = 0,
    Fit = 1,
    Stretch = 2,
    Width = 3,
    Height = 4,
}
/**
 * Constructor options for the ImageWrap class.
 */
export interface ImageWrapOptions extends ViewOptions {
    /**
     * Whether to disable the visibility check or not.
     */
    disableVisibilityCheck?: boolean;
    /**
     * The scale mode used to transform the image.
     */
    scaleMode?: ScaleMode;
    /**
     * The maximum scale of the image.
     */
    maxScale?: number;
    sizeRoundingMode?: RoundingMode | string;
    offsetRoundingMode?: RoundingMode | string;
}
/**
 * Wraps an Image view and allows to apply various scale modes to the image.
 */
export default class ImageWrap extends View implements VisibilityTarget {
    /**
     * The image instance displayed by this wrapper.
     */
    image: Image;
    /**
     * The scale mode used to transform the image.
     */
    scaleMode: ScaleMode;
    /**
     * The maximum scale of the image.
     */
    maxScale: number;
    /**
     * The horizontal focus point, a relative position between 0 and 1.
     */
    focusX: number;
    /**
     * The vertical focus point, a relative position between 0 and 1.
     */
    focusY: number;
    /**
     * Whether this element is currently within the visible viewport bounds or not.
     */
    inViewport: boolean;
    /**
     * ImageWrap constructor.
     *
     * @param options
     *   The constructor options.
     */
    constructor(options?: ImageWrapOptions);
    /**
     * Remove this view.
     */
    remove(): this;
    /**
     * Set the size of the image wrap.
     *
     * @param width
     *   The new width of the image wrap.
     * @param height
     *   The new height of the image wrap.
     */
    setSize(width: number, height: number): this;
    /**
     * Set whether the element of this view is currently within the visible bounds
     * of the viewport or not.
     *
     * @param inViewport
     *   TRUE if the element is visible, FALSE otherwise.
     */
    setInViewport(inViewport: boolean): void;
    /**
     * Set the scale mode of this image wrap.
     *
     * @param mode
     *   The new image scale mode.
     */
    setScaleMode(mode: ScaleMode): void;
    /**
     * Load the image.
     *
     * @returns
     *   A promise that resolves after the image has loaded.
     */
    load(): Q.Promise<any>;
    /**
     * Update the displayed image.
     *
     * @param width
     *   The visible width of the wrapper.
     * @param height
     *   The visible height of the wrapper.
     */
    update(width?: number, height?: number): void;
    /**
     * Calculate the scaled dimensions of the image according to the given scale mode.
     *
     * @param width
     *   The display width.
     * @param height
     *   The display height.
     * @param scaleMode
     *   The scale mode that should be used to calculate the dimensions.
     * @param maxScale
     *   The maximum allowed scale factor.
     * @returns
     *   An object describing the scaled dimensions of the image.
     */
    measure(width: number, height: number, scaleMode?: ScaleMode, maxScale?: number): IDimensions;
    /**
     * Triggered after the image has loaded.
     */
    onImageLoad(): void;
    /**
     * Triggered after the size of the viewport has changed.
     *
     * If this view is used as a component this handler will be called automatically.
     *
     * @returns
     *   TRUE if the event handling should be stopped and child components should not receive
     *   the event, FALSE otherwise.
     */
    handleViewportResize(): boolean;
    /**
     * Calculate the shift value for a dimension based upon the given focus value.
     *
     * @param space
     *   The available space within the dimension.
     * @param size
     *   The size of the image within the dimension.
     * @param focus
     *   The focus point value within the dimension.
     * @param invert
     *   Whether the focus point should be inverted or not.
     * @returns
     *   The calculated position shift along the dimension.
     */
    static calculateShift(space: number, size: number, focus: number, invert?: boolean): number;
}
