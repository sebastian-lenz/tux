/// <reference types="q" />
import View from '../View';
import Swap, { SwapOptions } from '../Swap';
/**
 * Constructor options for the AnimationSwap class.
 */
export interface AnimationSwapOptions extends SwapOptions {
    container?: string | HTMLElement;
}
/**
 * Transition options for the AnimationSwap class.
 */
export interface TransitionOptions {
    /**
     * The name of the transition css class. Set to NULL to disable the animation.
     */
    className?: string;
    /**
     * Should the height of the swap be animated too?
     */
    withHeightTransition?: boolean;
}
/**
 * A swap panel that uses css animations for transitions.
 *
 * @event "swap" (newContent:T, oldContent:T):void
 */
export default class AnimationSwap<T extends View = View> extends Swap<T, TransitionOptions> {
    /**
     * The name of the last applied transition css class.
     */
    transitionClassName: string;
    container: HTMLElement;
    /**
     * Default transition options.
     */
    static defaultTransitionOptions: TransitionOptions;
    /**
     * AnimationSwap constructor.
     *
     * @param options
     *   AnimationSwap constructor options.
     */
    constructor(options?: AnimationSwapOptions);
    /**
     * Return the default transition options.
     *
     * @returns
     *   The default transition options.
     */
    getDefaultTransitionOptions(): TransitionOptions;
    /**
     * Play a transition between the top given views.
     *
     * @param newContent
     *   The view the transition should lead to.
     * @param oldContent
     *   The view the transition should come from.
     * @param options
     *   The transition options.
     */
    protected handleTransition(newContent: T, oldContent: T, options: TransitionOptions): Q.Promise<any>;
    /**
     * Triggered after a css animation has finished.
     *
     * @param event
     *   The related event object.
     */
    handleAnimationEnd(event: AnimationEvent): void;
}
