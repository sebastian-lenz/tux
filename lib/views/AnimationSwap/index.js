import * as tslib_1 from "tslib";
import { defaults, defer } from 'underscore';
import animation from '../../util/vendor/animation';
import heightTransition from '../../fx/transition/heightTransition';
import { option } from '../View';
import Swap from '../Swap';
/**
 * A swap panel that uses css animations for transitions.
 *
 * @event "swap" (newContent:T, oldContent:T):void
 */
var AnimationSwap = (function (_super) {
    tslib_1.__extends(AnimationSwap, _super);
    /**
     * AnimationSwap constructor.
     *
     * @param options
     *   AnimationSwap constructor options.
     */
    function AnimationSwap(options) {
        var _this = _super.call(this, options) || this;
        if (animation.endEvent) {
            _this.delegate(animation.endEvent, _this.handleAnimationEnd);
        }
        if (!_this.container) {
            _this.container = _this.element;
        }
        return _this;
    }
    /**
     * Return the default transition options.
     *
     * @returns
     *   The default transition options.
     */
    AnimationSwap.prototype.getDefaultTransitionOptions = function () {
        return AnimationSwap.defaultTransitionOptions;
    };
    /**
     * Play a transition between the top given views.
     *
     * @param newContent
     *   The view the transition should lead to.
     * @param oldContent
     *   The view the transition should come from.
     * @param options
     *   The transition options.
     */
    AnimationSwap.prototype.handleTransition = function (newContent, oldContent, options) {
        var _this = this;
        if (options.withHeightTransition && options.className != null) {
            return heightTransition(this.container, function () {
                _this.handleTransition(newContent, oldContent, defaults({ withHeightTransition: false }, options));
            });
        }
        newContent ? newContent.element.classList.add('current') : null;
        oldContent ? oldContent.element.classList.remove('current') : null;
        this.trigger('swap', newContent, oldContent);
        if (options.className == null) {
            this.handleTransitionEnd();
        }
        else {
            this.transitionClassName = options.className;
            this.container.classList.add(options.className);
            newContent ? newContent.element.classList.add('transitionTarget') : null;
            oldContent ? oldContent.element.classList.add('transitionOrigin') : null;
        }
    };
    /**
     * Triggered after a css animation has finished.
     *
     * @param event
     *   The related event object.
     */
    AnimationSwap.prototype.handleAnimationEnd = function (event) {
        var _this = this;
        var transition = this.transition;
        if (transition && event.target === this.container) {
            this.container.classList.remove(this.transitionClassName);
            if (transition.newContent) {
                transition.newContent.element.classList.remove('transitionTarget');
            }
            if (transition.oldContent) {
                transition.oldContent.element.classList.remove('transitionOrigin');
            }
            defer(function () { return _this.handleTransitionEnd(); });
        }
    };
    return AnimationSwap;
}(Swap));
export default AnimationSwap;
/**
 * Default transition options.
 */
AnimationSwap.defaultTransitionOptions = {
    className: 'transition'
};
tslib_1.__decorate([
    option({ type: 'element' })
], AnimationSwap.prototype, "container", void 0);
