import Collection from '../../models/Collection';
import Model from '../../models/Model';
import View, { ViewOptions } from '../View';
/**
 * Constructor options for the ChildableView class.
 */
export interface ChildableViewOptions<T extends View = View> extends ViewOptions {
    /**
     * A css selector or an element used as the container for all children.
     */
    container?: string | HTMLElement;
    /**
     * A css selector used to discover existing children.
     */
    childSelector?: string;
    /**
     * Default view class used to construct children.
     */
    childClass?: {
        new (options?: any): T;
    };
    childOptions?: ViewOptions;
    /**
     * A list of view instances that should be attached as children.
     */
    children?: T[];
    /**
     * Whether elements discovered by childSelector must be child elements
     * of the container or not.
     */
    allowGrandChildren?: boolean;
    collection?: Collection;
}
/**
 * A view that contains a list of child views.
 *
 * @event "add" (child:T, at:number):void
 *   Triggered after a child has been added to this view.
 * @event "remove" (child:T, at:number):void
 *   Triggered after a child has been removed to this view.
 * @event "reset" ():void
 *   Triggered after the children have been reset.
 */
export default class ChildableView<T extends View = View> extends View {
    /**
     * The container children will be placed in.
     */
    container: HTMLElement;
    /**
     * The list of children attached to this view.
     */
    children: T[];
    /**
     * Default view class used to construct children.
     */
    childClass: {
        new (options: ViewOptions): T;
    };
    childOptions?: ViewOptions;
    collection: Collection;
    /**
     * ChildableView constructor.
     *
     * @param options
     *   The constructor options.
     */
    constructor(options?: ChildableViewOptions<T>);
    /**
     * Remove this view.
     */
    remove(): this;
    /**
     * Set the collection should be bound to.
     *
     * @param collection
     *   The new collection this view should be bound to. Pass NULL to
     *   unbind this view from previously set collections.
     */
    setCollection(collection: Collection): void;
    /**
     * Return the number of children attached to this view.
     *
     * @returns
     *   The number of children attached to this view.
     */
    getLength(): number;
    /**
     * Return the child view at the given index.
     *
     * @param index
     *   The index of the child that should be returned.
     * @returns
     *   The child with the given index.
     */
    getChild(index: number): T;
    /**
     * Return the index of the given child.
     *
     * @param child
     *   The child whose index should determined.
     * @returns
     *   The index of the given child or -1 if the given child is not present.
     */
    indexOf(child: T): number;
    /**
     * Add the given child view to this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     *
     * @param child
     *   The child view that should be added to this view.
     * @param at
     *   Optional index the child should be inserted at.
     */
    addChild(child: T, at?: number): this;
    /**
     * Internal implementation of the addChild method.
     *
     * @param child
     *   The child view that should be added to this view.
     * @param at
     *   Optional index the child should be inserted at.
     */
    protected _addChild(child: T, at?: number): void;
    /**
     * Add the given list of child views to this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     *
     * @param children
     *   The list of child views that should be added to this view.
     * @param at
     *   Optional index the child should be inserted at.
     */
    addChildren(children: T[], at?: number): this;
    /**
     * Remove the given child view from this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     *
     * @param child
     *   The child view that should be removed.
     */
    removeChild(child: T): this;
    /**
     * Internal implementation of the removeChild method.
     *
     * @param child
     *   The child view that should be removed.
     */
    _removeChild(child: T): void;
    /**
     * Remove the given list of child views from this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     *
     * @param children
     *   The list of child views that should be removed.
     */
    removeChildren(children: T[]): this;
    /**
     * Remove all children from this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     */
    removeAllChildren(): void;
    /**
     * Internal implementation of the removeAllChildren method.
     */
    protected _removeAllChildren(silent?: boolean): void;
    /**
     * Create the container children will be placed in.
     *
     * @returns
     *   The generated container.
     */
    protected createContainer(): HTMLElement;
    /**
     * Allow the view class to alter the constructor options before creating
     * new child view instances.
     *
     * @param options
     *   The predefined child constructor options.
     * @returns
     *   The altered child constructor options.
     */
    protected alterChildOptions(options: any): any;
    /**
     * Create a new view instance for the given child element.
     *
     * @param element
     *   The dom element whose view instance should be created.
     * @returns
     *   A view instance for the given child element.
     */
    protected createChildFromElement(element: HTMLElement): T;
    /**
     * Create a new view instance for the given model.
     *
     * @param model
     *   The model view instance should be created.
     * @returns
     *   A view instance for the given model.
     */
    protected createChildFromModel(model: Model): T;
    /**
     * Start listening for changed in the given collection.
     *
     * @param collection
     *   The collection events should be bound to.
     */
    protected listenToCollection(collection: Collection): void;
    /**
     * Stop listening to the given collection.
     *
     * @param collection
     *   The collection whose listening operations should be stopped.
     */
    protected stopListeningToCollection(collection: Collection): void;
    /**
     * Remove all child views and recreate all children from the bound collection.
     */
    protected resetToCollection(): void;
    /**
     * Synchronize the children of this view with the bound collection.
     */
    protected syncWithCollection(): void;
    /**
     * Triggered after a model has been added to the collection.
     *
     * @param model
     *   The model that has been added.
     * @param collection
     *   The affected collection.
     * @param options
     *  Additional insert data provided by Backbone.
     */
    protected onCollectionAdd(model: Model, collection: Collection, options: {
        index?: number;
        views?: {
            [cid: number]: T;
        };
    }): void;
    /**
     * Triggered after a model has been removed from the collection.
     *
     * @param model
     *   The model that has been removed.
     * @param collection
     *   The affected collection.
     * @param options
     *  Additional removal data provided by Backbone.
     */
    protected onCollectionRemove(model: Model, collection: Collection, options: {
        index: number;
    }): void;
    /**
     * Triggered if the bound collection was reset.
     */
    protected onCollectionReset(): void;
    /**
     * Triggered if the bound collection was sorted.
     */
    protected onCollectionSort(): void;
    /**
     * Triggers the resize handler on this component and all of its children.
     */
    handleViewportResize(resizeChildren?: Function): boolean;
}
