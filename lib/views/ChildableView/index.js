import * as tslib_1 from "tslib";
import { defaults, forEach, indexOf } from 'underscore';
import View, { option } from '../View';
/**
 * A view that contains a list of child views.
 *
 * @event "add" (child:T, at:number):void
 *   Triggered after a child has been added to this view.
 * @event "remove" (child:T, at:number):void
 *   Triggered after a child has been removed to this view.
 * @event "reset" ():void
 *   Triggered after the children have been reset.
 */
var ChildableView = (function (_super) {
    tslib_1.__extends(ChildableView, _super);
    /**
     * ChildableView constructor.
     *
     * @param options
     *   The constructor options.
     */
    function ChildableView(options) {
        var _this = _super.call(this, options || (options = {})) || this;
        var container = _this.container || (_this.container = _this.createContainer());
        var children = _this.children = [];
        if (options.collection) {
            _this.listenToCollection(options.collection);
            _this.resetToCollection();
        }
        else if (options.children) {
            for (var _i = 0, _a = options.children; _i < _a.length; _i++) {
                var child = _a[_i];
                children.push(child);
                container.appendChild(child.element);
            }
        }
        else if (options.childSelector) {
            var elements = _this.element.querySelectorAll(options.childSelector);
            forEach(elements, function (element) {
                if (options.allowGrandChildren || element.parentNode == container) {
                    children.push(_this.createChildFromElement(element));
                }
            });
        }
        return _this;
    }
    /**
     * Remove this view.
     */
    ChildableView.prototype.remove = function () {
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            child.remove();
        }
        return _super.prototype.remove.call(this);
    };
    /**
     * Set the collection should be bound to.
     *
     * @param collection
     *   The new collection this view should be bound to. Pass NULL to
     *   unbind this view from previously set collections.
     */
    ChildableView.prototype.setCollection = function (collection) {
        if (this.collection === collection)
            return;
        if (this.collection) {
            this.stopListeningToCollection(this.collection);
        }
        this.collection = collection;
        if (collection) {
            this.listenToCollection(collection);
            this.resetToCollection();
        }
        else {
            this._removeAllChildren();
        }
    };
    /**
     * Return the number of children attached to this view.
     *
     * @returns
     *   The number of children attached to this view.
     */
    ChildableView.prototype.getLength = function () {
        return this.children.length;
    };
    /**
     * Return the child view at the given index.
     *
     * @param index
     *   The index of the child that should be returned.
     * @returns
     *   The child with the given index.
     */
    ChildableView.prototype.getChild = function (index) {
        return this.children[index];
    };
    /**
     * Return the index of the given child.
     *
     * @param child
     *   The child whose index should determined.
     * @returns
     *   The index of the given child or -1 if the given child is not present.
     */
    ChildableView.prototype.indexOf = function (child) {
        return indexOf(this.children, child);
    };
    /**
     * Add the given child view to this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     *
     * @param child
     *   The child view that should be added to this view.
     * @param at
     *   Optional index the child should be inserted at.
     */
    ChildableView.prototype.addChild = function (child, at) {
        if (this.collection) {
            if (child.model) {
                var options = { views: (_a = {}, _a[child.model.cid] = child, _a) };
                if (at !== void 0)
                    options.index = at;
                this.collection.add(child.model, options);
            }
            else {
                console.log('Warn: Trying to append a child without a model to a list bound to a collection.');
            }
        }
        else {
            this._addChild(child, at);
        }
        return this;
        var _a;
    };
    /**
     * Internal implementation of the addChild method.
     *
     * @param child
     *   The child view that should be added to this view.
     * @param at
     *   Optional index the child should be inserted at.
     */
    ChildableView.prototype._addChild = function (child, at) {
        var container = this.container;
        var children = this.children;
        var count = children.length;
        if (at === void 0 || at >= count) {
            at = count;
            container.appendChild(child.element);
            children.push(child);
        }
        else if (at <= 0) {
            at = 0;
            container.insertBefore(child.element, container.firstChild);
            children.unshift(child);
        }
        else {
            container.insertBefore(child.element, children[at].element);
            children.splice(at, 0, child);
        }
        this.trigger('add', child, at);
    };
    /**
     * Add the given list of child views to this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     *
     * @param children
     *   The list of child views that should be added to this view.
     * @param at
     *   Optional index the child should be inserted at.
     */
    ChildableView.prototype.addChildren = function (children, at) {
        if (this.collection) {
            var views = {};
            var models = [];
            for (var _i = 0, children_1 = children; _i < children_1.length; _i++) {
                var child = children_1[_i];
                if (child.model) {
                    views[child.model.cid] = child;
                    models.push(child.model);
                }
                else {
                    console.log('Warn: Trying to append a child without a model to a list bound to a collection.');
                }
            }
            if (models.length) {
                var options = { views: views };
                if (at !== void 0)
                    options.index = at;
                this.collection.add(models, options);
            }
        }
        else {
            for (var index = 0, count = children.length; index < count; index++) {
                this._addChild(children[index], at ? at + index : at);
            }
        }
        return this;
    };
    /**
     * Remove the given child view from this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     *
     * @param child
     *   The child view that should be removed.
     */
    ChildableView.prototype.removeChild = function (child) {
        if (this.collection) {
            if (child.model) {
                this.collection.remove(child.model);
            }
            else {
                console.log('Warn: Trying to remove a child without a model from a list bound to a collection.');
            }
        }
        else {
            this._removeChild(child);
        }
        return this;
    };
    /**
     * Internal implementation of the removeChild method.
     *
     * @param child
     *   The child view that should be removed.
     */
    ChildableView.prototype._removeChild = function (child) {
        var index = indexOf(this.children, child);
        if (index !== -1) {
            child.remove();
            this.children.splice(index, 1);
            this.trigger('remove', child, index);
        }
    };
    /**
     * Remove the given list of child views from this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     *
     * @param children
     *   The list of child views that should be removed.
     */
    ChildableView.prototype.removeChildren = function (children) {
        if (this.collection) {
            var models = [];
            for (var _i = 0, children_2 = children; _i < children_2.length; _i++) {
                var child = children_2[_i];
                if (child.model) {
                    models.push(child.model);
                }
                else {
                    console.log('Warn: Trying to remove a child without a model from a list bound to a collection.');
                }
            }
            if (models.length) {
                this.collection.remove(models);
            }
        }
        else {
            for (var _a = 0, children_3 = children; _a < children_3.length; _a++) {
                var child = children_3[_a];
                this._removeChild(child);
            }
        }
        return this;
    };
    /**
     * Remove all children from this view.
     *
     * If this view is bound to a collection the call will be forwarded to the collection.
     */
    ChildableView.prototype.removeAllChildren = function () {
        if (this.collection) {
            this.collection.reset();
        }
        else {
            this._removeAllChildren();
        }
    };
    /**
     * Internal implementation of the removeAllChildren method.
     */
    ChildableView.prototype._removeAllChildren = function (silent) {
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            child.remove();
        }
        this.children.length = 0;
        if (!silent) {
            this.trigger('reset');
        }
    };
    /**
     * Create the container children will be placed in.
     *
     * @returns
     *   The generated container.
     */
    ChildableView.prototype.createContainer = function () {
        return this.element;
    };
    /**
     * Allow the view class to alter the constructor options before creating
     * new child view instances.
     *
     * @param options
     *   The predefined child constructor options.
     * @returns
     *   The altered child constructor options.
     */
    ChildableView.prototype.alterChildOptions = function (options) {
        var childOptions = this.childOptions;
        if (childOptions) {
            return defaults(options, childOptions);
        }
        return options;
    };
    /**
     * Create a new view instance for the given child element.
     *
     * @param element
     *   The dom element whose view instance should be created.
     * @returns
     *   A view instance for the given child element.
     */
    ChildableView.prototype.createChildFromElement = function (element) {
        var options = this.alterChildOptions({
            owner: this,
            element: element,
        });
        return new this.childClass(options);
    };
    /**
     * Create a new view instance for the given model.
     *
     * @param model
     *   The model view instance should be created.
     * @returns
     *   A view instance for the given model.
     */
    ChildableView.prototype.createChildFromModel = function (model) {
        var options = this.alterChildOptions({
            owner: this,
            model: model,
        });
        return new this.childClass(options);
    };
    /**
     * Start listening for changed in the given collection.
     *
     * @param collection
     *   The collection events should be bound to.
     */
    ChildableView.prototype.listenToCollection = function (collection) {
        this.listenTo(collection, 'add', this.onCollectionAdd);
        this.listenTo(collection, 'remove', this.onCollectionRemove);
        this.listenTo(collection, 'reset', this.onCollectionReset);
        this.listenTo(collection, 'sort', this.onCollectionSort);
    };
    /**
     * Stop listening to the given collection.
     *
     * @param collection
     *   The collection whose listening operations should be stopped.
     */
    ChildableView.prototype.stopListeningToCollection = function (collection) {
        this.stopListening(collection, 'add', this.onCollectionAdd);
        this.stopListening(collection, 'remove', this.onCollectionRemove);
        this.stopListening(collection, 'reset', this.onCollectionReset);
        this.stopListening(collection, 'sort', this.onCollectionSort);
    };
    /**
     * Remove all child views and recreate all children from the bound collection.
     */
    ChildableView.prototype.resetToCollection = function () {
        this._removeAllChildren(true);
        for (var _i = 0, _a = this.collection.models; _i < _a.length; _i++) {
            var model = _a[_i];
            var child = this.createChildFromModel(model);
            this.children.push(child);
            this.container.appendChild(child.element);
        }
        this.trigger('reset');
    };
    /**
     * Synchronize the children of this view with the bound collection.
     */
    ChildableView.prototype.syncWithCollection = function () {
        var container = this.container;
        var existing = this.children;
        var children = this.children = [];
        for (var _i = 0, _a = this.collection.models; _i < _a.length; _i++) {
            var model = _a[_i];
            var child = null;
            for (var index = 0, count = existing.length; index < count; index++) {
                if (existing[index].model == model) {
                    child = existing[index];
                    existing.splice(index, 1);
                    break;
                }
            }
            if (!child) {
                child = this.createChildFromModel(model);
            }
            children.push(child);
            container.appendChild(child.el);
        }
        for (var _b = 0, existing_1 = existing; _b < existing_1.length; _b++) {
            var child = existing_1[_b];
            child.remove();
        }
        this.trigger('reset');
    };
    /**
     * Triggered after a model has been added to the collection.
     *
     * @param model
     *   The model that has been added.
     * @param collection
     *   The affected collection.
     * @param options
     *  Additional insert data provided by Backbone.
     */
    ChildableView.prototype.onCollectionAdd = function (model, collection, options) {
        var view;
        if (options.views && options.views[model.cid]) {
            view = options.views[model.cid];
        }
        else {
            view = this.createChildFromModel(model);
        }
        this._addChild(view, options.index);
    };
    /**
     * Triggered after a model has been removed from the collection.
     *
     * @param model
     *   The model that has been removed.
     * @param collection
     *   The affected collection.
     * @param options
     *  Additional removal data provided by Backbone.
     */
    ChildableView.prototype.onCollectionRemove = function (model, collection, options) {
        var child = this.getChild(options.index);
        if (child.model === model) {
            this._removeChild(child);
        }
        else {
            console.log('Warn: View is out of sync with bound collection.');
            this.syncWithCollection();
        }
    };
    /**
     * Triggered if the bound collection was reset.
     */
    ChildableView.prototype.onCollectionReset = function () {
        this.resetToCollection();
    };
    /**
     * Triggered if the bound collection was sorted.
     */
    ChildableView.prototype.onCollectionSort = function () {
        this.syncWithCollection();
    };
    /**
     * Triggers the resize handler on this component and all of its children.
     */
    ChildableView.prototype.handleViewportResize = function (resizeChildren) {
        forEach(this.children, function (child) { return child.handleViewportResize(); });
        return false;
    };
    return ChildableView;
}(View));
export default ChildableView;
tslib_1.__decorate([
    option({ type: 'element' })
], ChildableView.prototype, "container", void 0);
tslib_1.__decorate([
    option({ type: 'any', defaultValue: View })
], ChildableView.prototype, "childClass", void 0);
tslib_1.__decorate([
    option({ type: 'any' })
], ChildableView.prototype, "childOptions", void 0);
