import * as tslib_1 from "tslib";
import * as Q from 'q';
import { defaults } from 'underscore';
import View, { option } from '..//View';
/**
 * A view panel that can swap its content.
 */
var Swap = (function (_super) {
    tslib_1.__extends(Swap, _super);
    /**
     * Swap constructor.
     *
     * @param options
     *   Swap constructor options.
     */
    function Swap(options) {
        var _this = _super.call(this, options) || this;
        /**
         * The specifications of the current transition.
         */
        _this.transition = null;
        /**
         * The specifications of the transition we should play after the current one has finished.
         */
        _this.successor = null;
        return _this;
    }
    Swap.prototype.removeContent = function (content) {
        if (this.removeOldContent && content) {
            content.remove();
        }
    };
    /**
     * Set the new content of the swap panel.
     *
     * @param newContent
     *   The new view that should be displayed by this swap panel.
     * @param options
     *   The desired transition options.
     */
    Swap.prototype.setContent = function (newContent, options) {
        if (this.content === newContent) {
            return Q.when();
        }
        var oldContent = this.content;
        this.content = newContent;
        options = defaults(options || {}, this.getDefaultTransitionOptions());
        if (this.successor) {
            this.successor.deferred.reject(null);
            this.removeContent(this.successor.newContent);
            this.successor = null;
        }
        if (this.transition) {
            if (this.handleInterrupt()) {
                this.transition.deferred.reject(null);
            }
            else {
                var deferred_1 = Q.defer();
                this.successor = { deferred: deferred_1, options: options, newContent: newContent };
                return deferred_1.promise;
            }
        }
        if (this.appendNewContent && newContent) {
            this.element.appendChild(newContent.element);
        }
        var deferred = Q.defer();
        this.transition = { deferred: deferred, options: options, newContent: newContent, oldContent: oldContent };
        this.handleTransition(newContent, oldContent, options);
        return deferred.promise;
    };
    /**
     * Try to interrupt the current transition.
     *
     * This method returns FALSE by default causing calls to setContent to become delayed
     * until the current transition has finished. If the used animation technique allows
     * interruptions, this method should be overwritten to handle interruptions gracefully.
     *
     * @returns
     *   TRUE if the current transition could be interrupted, FALSE otherwise.
     */
    Swap.prototype.handleInterrupt = function () {
        return false;
    };
    /**
     * Handle a finished transition.
     *
     * This method must be called by all implementations after an transition has finished.
     */
    Swap.prototype.handleTransitionEnd = function () {
        if (!this.transition) {
            return;
        }
        if (this.transition.oldContent) {
            this.removeContent(this.transition.oldContent);
        }
        var oldTransition = this.transition;
        var successor = this.successor;
        oldTransition.deferred.resolve();
        if (successor) {
            var newTransition = defaults(this.successor, { oldContent: oldTransition.newContent });
            if (this.appendNewContent && newTransition.newContent) {
                this.element.appendChild(newTransition.newContent.element);
            }
            this.successor = null;
            this.transition = newTransition;
            this.handleTransition(newTransition.newContent, newTransition.oldContent, newTransition.options);
        }
        else {
            this.transition = null;
        }
    };
    return Swap;
}(View));
export default Swap;
tslib_1.__decorate([
    option({ type: 'bool', defaultValue: false })
], Swap.prototype, "appendNewContent", void 0);
tslib_1.__decorate([
    option({ type: 'bool', defaultValue: false })
], Swap.prototype, "removeOldContent", void 0);
