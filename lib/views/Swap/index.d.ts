/// <reference types="q" />
import * as Q from 'q';
import View, { ViewOptions } from '..//View';
/**
 * Constructor options for the Swap class.
 */
export interface SwapOptions extends ViewOptions {
    /**
     * Whether new content view elements should be attached to this view. Defaults to FALSE.
     */
    appendNewContent?: boolean;
    /**
     * Whether hidden content views should be removed or not. Defaults to FALSE.
     */
    removeOldContent?: boolean;
}
/**
 * A view panel that can swap its content.
 */
export default abstract class Swap<T extends View, U> extends View {
    /**
     * The content currently displayed by this view.
     */
    content: T;
    /**
     * Whether new content view elements should be attached to this view.
     */
    appendNewContent: boolean;
    /**
     * Whether hidden content views should be removed or not.
     */
    removeOldContent: boolean;
    /**
     * The specifications of the current transition.
     */
    protected transition: {
        deferred: Q.Deferred<any>;
        options: U;
        newContent: T;
        oldContent: T;
    };
    /**
     * The specifications of the transition we should play after the current one has finished.
     */
    protected successor: {
        deferred: Q.Deferred<any>;
        options: U;
        newContent: T;
    };
    /**
     * Swap constructor.
     *
     * @param options
     *   Swap constructor options.
     */
    constructor(options?: SwapOptions);
    /**
     * Return the default transition options.
     *
     * @returns
     *   The default transition options.
     */
    abstract getDefaultTransitionOptions(): U;
    removeContent(content: T): void;
    /**
     * Set the new content of the swap panel.
     *
     * @param newContent
     *   The new view that should be displayed by this swap panel.
     * @param options
     *   The desired transition options.
     */
    setContent(newContent: T, options?: U): Q.Promise<any>;
    /**
     * Try to interrupt the current transition.
     *
     * This method returns FALSE by default causing calls to setContent to become delayed
     * until the current transition has finished. If the used animation technique allows
     * interruptions, this method should be overwritten to handle interruptions gracefully.
     *
     * @returns
     *   TRUE if the current transition could be interrupted, FALSE otherwise.
     */
    protected handleInterrupt(): boolean;
    /**
     * Play a transition between the top given views.
     *
     * @param newContent
     *   The view the transition should lead to.
     * @param oldContent
     *   The view the transition should come from.
     * @param options
     *   Optional transition options.
     */
    protected abstract handleTransition(newContent: T, oldContent: T, options?: U): any;
    /**
     * Handle a finished transition.
     *
     * This method must be called by all implementations after an transition has finished.
     */
    protected handleTransitionEnd(): void;
}
