import View, { ViewOptions } from '../View';
import CycleableView from '../CycleableView';
/**
 * Constructor options for the Arrows class.
 */
export interface ArrowsOptions extends ViewOptions {
    /**
     * The cycleable view that should be controlled.
     */
    owner?: CycleableView;
    /**
     * The template that should be used to render the arrows.
     */
    template?: string;
}
/**
 * Creates previous / next arrows for a CycleableView instance.
 */
export default class Arrows extends View {
    /**
     * The CycleableView that should be controlled.
     */
    view: CycleableView;
    /**
     * Arrows constructor.
     *
     * @param options
     *   The constructor options.
     */
    constructor(options?: ArrowsOptions);
    /**
     * Set the CycleableView that should be controlled.
     *
     * @param view
     *   The CycleableView that should be controlled.
     */
    setView(view: CycleableView): void;
    /**
     * Set the number of children of the controlled view.
     *
     * @param length
     *   The number of children of the controlled view.
     */
    setLength(length: number): this;
    /**
     * Set the index of the current item.
     *
     * @param index
     *   The index of the current item.
     */
    setCurrentIndex(index: number): void;
    /**
     * Triggered after the current index of the view has changed.
     *
     * @param index
     *   The new index of the current child.
     */
    onIndexChanged(index: number): void;
    /**
     * Triggered if the user clicks on this view.
     *
     * @param event
     *   The triggering event.
     */
    onClick(event: Event): boolean;
}
