import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import View from '../View';
import CycleableView from '../CycleableView';
/**
 * The default template used by the Arrows class.
 */
var defaultTemplate = "\n  <li class=\"tuxArrows__arrow previous\"><a class=\"tuxArrows__button\">&lt;</a></li>\n  <li class=\"tuxArrows__arrow next\"><a class=\"tuxArrows__button\">&gt;</a></li>";
/**
 * Creates previous / next arrows for a CycleableView instance.
 */
var Arrows = (function (_super) {
    tslib_1.__extends(Arrows, _super);
    /**
     * Arrows constructor.
     *
     * @param options
     *   The constructor options.
     */
    function Arrows(options) {
        var _this = _super.call(this, options = defaults(options || {}, {
            tagName: 'ul',
            className: 'tuxArrows'
        })) || this;
        _this.element.innerHTML = options.template ? options.template : defaultTemplate;
        if (options.owner instanceof CycleableView) {
            _this.setView(options.owner);
        }
        _this.delegateClick(_this.onClick);
        return _this;
    }
    /**
     * Set the CycleableView that should be controlled.
     *
     * @param view
     *   The CycleableView that should be controlled.
     */
    Arrows.prototype.setView = function (view) {
        if (this.view == view)
            return;
        if (this.view) {
            this.view.element.removeChild(this.element);
            this.stopListening(this.view);
        }
        this.view = view;
        if (view) {
            this.listenTo(view, 'currentIndexChanged', this.onIndexChanged);
            this.setLength(view.getLength());
            this.onIndexChanged(view.getCurrentIndex());
        }
    };
    /**
     * Set the number of children of the controlled view.
     *
     * @param length
     *   The number of children of the controlled view.
     */
    Arrows.prototype.setLength = function (length) {
        this.element.classList.toggle('disabled', length < 2);
        return this;
    };
    /**
     * Set the index of the current item.
     *
     * @param index
     *   The index of the current item.
     */
    Arrows.prototype.setCurrentIndex = function (index) {
        var next = this.element.querySelector('.next');
        var previous = this.element.querySelector('.previous');
        if (this.view.isLooped) {
            previous.classList.remove('disabled');
            next.classList.remove('disabled');
        }
        else {
            previous.classList.toggle('disabled', index <= 0);
            next.classList.toggle('disabled', index >= this.view.getLength() - 1);
        }
    };
    /**
     * Triggered after the current index of the view has changed.
     *
     * @param index
     *   The new index of the current child.
     */
    Arrows.prototype.onIndexChanged = function (index) {
        this.setCurrentIndex(index);
    };
    /**
     * Triggered if the user clicks on this view.
     *
     * @param event
     *   The triggering event.
     */
    Arrows.prototype.onClick = function (event) {
        var options = {};
        var index = this.view.getCurrentIndex();
        var target = event.target;
        while (target && target.classList) {
            if (target.classList.contains('next')) {
                index += 1;
                options.direction = 'forward';
                break;
            }
            else if (target.classList.contains('previous')) {
                index -= 1;
                options.direction = 'backward';
                break;
            }
            target = target.parentNode;
        }
        this.view.setCurrentIndex(index, options);
        return true;
    };
    return Arrows;
}(View));
export default Arrows;
