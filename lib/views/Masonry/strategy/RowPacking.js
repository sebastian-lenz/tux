import { defaults } from 'underscore';
;
export var RowAlign;
(function (RowAlign) {
    RowAlign[RowAlign["Left"] = 0] = "Left";
    RowAlign[RowAlign["Center"] = 1] = "Center";
    RowAlign[RowAlign["Right"] = 2] = "Right";
    RowAlign[RowAlign["Justify"] = 3] = "Justify";
    RowAlign[RowAlign["JustifyAll"] = 4] = "JustifyAll";
})(RowAlign || (RowAlign = {}));
var Row = (function () {
    function Row() {
        this.entries = [];
        this.y = 0;
        this.height = 0;
        this.displayWidth = 0;
        this.desiredWidth = 0;
        this.actualWidth = 0;
        this.isLastRow = false;
    }
    Row.prototype.push = function (item, size) {
        var entry = {
            item: item,
            row: this,
            index: this.entries.length,
            width: size.width,
            height: size.height,
            x: 0
        };
        this.entries.push(entry);
        this.desiredWidth += size.width;
        return entry;
    };
    Row.prototype.distribute = function (width, padding, maxScale) {
        var entries = this.entries;
        var availableWidth = width - Math.max(0, entries.length - 1) * padding;
        var scale = availableWidth / this.desiredWidth;
        if (maxScale !== void 0) {
            scale = Math.min(maxScale, scale);
        }
        var x = 0;
        var maxHeight = 0;
        for (var _i = 0, entries_1 = entries; _i < entries_1.length; _i++) {
            var entry = entries_1[_i];
            entry.x = x;
            var size = entry.item.setActualSize({ width: entry.width * scale, height: entry.height });
            entry.width = size.width;
            entry.height = size.height;
            x += size.width + padding;
            if (size.height > maxHeight) {
                maxHeight = size.height;
            }
        }
        this.displayWidth = width;
        this.actualWidth = x - padding;
        this.height = maxHeight;
    };
    return Row;
}());
export { Row };
/**
 * A layout strategy that packs items into rows.
 */
var RowPacking = (function () {
    /**
     * RowPacking constructor.
     *
     * @param options
     *   RowPacking constructor options.
     */
    function RowPacking(options) {
        /**
         * The height of the current layout state.
         */
        this.height = 0;
        /**
         * The width of the masonry while last updating the layout.
         */
        this.masonryWidth = 0;
        /**
         * Horizontal alignment of items within a row.
         */
        this.rowAlign = RowAlign.Left;
        options = defaults(options || {}, {
            minItemsPerRow: 2,
            widthMultiplier: 1,
            itemPadding: 20,
            itemAlign: 0,
            rowPadding: 20,
            rowAlign: RowAlign.Left,
            maxRows: Number.NaN,
            lastRowIndent: 0
        });
        this.minItemsPerRow = options.minItemsPerRow;
        this.widthMultiplier = options.widthMultiplier;
        this.itemPadding = options.itemPadding;
        this.itemAlign = options.itemAlign;
        this.rowPadding = options.rowPadding;
        this.rowAlign = options.rowAlign;
        this.maxRows = options.maxRows ? options.maxRows : Number.NaN;
        this.lastRowIndent = options.lastRowIndent;
    }
    RowPacking.prototype.reset = function (items) {
        var width = this.masonry.element.offsetWidth;
        var availableWidth = width * this.widthMultiplier;
        var row = new Row();
        var rowMapping = {};
        var rows = [row];
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            var size = item.getDesiredSize();
            if (row.entries.length >= this.minItemsPerRow && row.desiredWidth + size.width > availableWidth) {
                if (isNaN(this.maxRows) || rows.length < this.maxRows) {
                    rows.push(row = new Row());
                }
                else {
                    break;
                }
            }
            rowMapping[item.cid] = row.push(item, size);
        }
        row.isLastRow = true;
        var y = 0;
        for (var _a = 0, rows_1 = rows; _a < rows_1.length; _a++) {
            row = rows_1[_a];
            var rowWidth = row.isLastRow ? width - this.lastRowIndent : width;
            if (this.rowAlign == RowAlign.Justify && row.isLastRow) {
                row.distribute(rowWidth, this.itemPadding, 1);
            }
            else {
                row.distribute(rowWidth, this.itemPadding);
            }
            row.y = y;
            y += row.height + this.rowPadding;
        }
        this.rows = rows;
        this.rowMapping = rowMapping;
        this.masonryWidth = width;
        this.height = y - this.rowPadding;
    };
    RowPacking.prototype.place = function (item) {
        var entry = this.rowMapping[item.cid];
        if (entry === void 0) {
            return null;
        }
        var row = entry.row;
        var shift = row.displayWidth - row.actualWidth;
        switch (this.rowAlign) {
            case RowAlign.Left:
                shift = 0;
                break;
            case RowAlign.Center:
                shift *= 0.5;
                break;
            case RowAlign.JustifyAll:
                shift = (shift / (row.entries.length - 1)) * entry.index;
                break;
            case RowAlign.Justify:
                if (!row.isLastRow && row.entries.length > 1) {
                    shift = (shift / (row.entries.length - 1)) * entry.index;
                }
                else {
                    shift = 0;
                }
                break;
        }
        return {
            x: entry.x + shift,
            y: row.y + (row.height - entry.height) * this.itemAlign,
            width: entry.width,
            height: entry.height
        };
    };
    RowPacking.prototype.isUpdateRequired = function () {
        return this.masonry.element.offsetWidth != this.masonryWidth;
    };
    RowPacking.prototype.getMaxHeight = function () {
        return this.height;
    };
    RowPacking.prototype.setMasonry = function (masonry) {
        this.masonry = masonry;
        this.masonryWidth = 0;
    };
    return RowPacking;
}());
export { RowPacking };
