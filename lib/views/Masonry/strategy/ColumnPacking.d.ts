import Masonry, { IMasonryStrategy } from '../index';
import Item from '../Item';
import { Column, IColumnRegion } from '../util/Column';
export declare type ColumnCount = number | {
    (grid: Masonry): number;
};
/**
 * Constructor options for ColumnPackingStrategy.
 */
export interface IColumnPackingStrategyOptions {
    columnCount: ColumnCount;
    gutter?: number;
}
/**
 * A layout strategy for the Masonry view that packs all items
 * into fixed columns.
 */
export default class ColumnPacking implements IMasonryStrategy {
    /**
     * The masonry this strategy is intented to be used with.
     */
    masonry: Masonry;
    /**
     * Column instances used to store the current layout.
     */
    columns: Column[];
    /**
     * The number of columns available.
     */
    columnCount: ColumnCount;
    /**
     * The width of a single column.
     */
    columnWidth: number;
    /**
     * The width of the masonry while last updating the layout.
     */
    masonryWidth: number;
    /**
     * The gutter width.
     */
    gutter: number;
    /**
     * Create a new ColumnPackingStrategy instance.
     */
    constructor(options: IColumnPackingStrategyOptions);
    /**
     * Reset the layout state of this strategy. Must be called before
     * a new layout is being generated.
     */
    reset(): void;
    /**
     * Mark the given position as being reserved.
     */
    protected reserve(position: IColumnRegion): void;
    /**
     * Return whether the layout has been invalidated e.g. due to
     * browser resizes.
     */
    isUpdateRequired(): boolean;
    /**
     * Set the grid this strategy is working on.
     */
    setMasonry(masonry: Masonry): void;
    /**
     * Return the number of available columns.
     */
    getColumnCount(): number;
    /**
     * Return the maximum height of the generated layout.
     */
    getMaxHeight(): number;
    /**
     * Place the given masonry item and return the assigned position.
     */
    place(item: Item): IColumnRegion;
    protected getColumnCountFromWidth(width: number): number;
    /**
     * Return the best free position for the given masonry item.
     */
    protected getBestPosition(item: Item): IColumnRegion;
    /**
     * Return all possible positions for the given masonry item.
     */
    protected getAllPositions(item: Item): IColumnRegion[];
    /**
     * Return the next free position within the given column.
     */
    protected getPositionAt(index: number, item: Item): IColumnRegion;
}
