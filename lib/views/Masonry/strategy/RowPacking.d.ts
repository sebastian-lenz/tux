import { IRectangle } from '../../../geom/Rectangle';
import Masonry, { IMasonryStrategy } from '../index';
import Item from '../Item';
import { IDimensions } from '../../../geom/Dimensions';
export declare type RowMapping = {
    [cid: number]: RowEntry;
};
export interface RowEntry {
    item: Item;
    row: Row;
    index: number;
    width: number;
    height: number;
    x: number;
}
export declare enum RowAlign {
    Left = 0,
    Center = 1,
    Right = 2,
    Justify = 3,
    JustifyAll = 4,
}
export declare class Row {
    entries: RowEntry[];
    y: number;
    height: number;
    displayWidth: number;
    desiredWidth: number;
    actualWidth: number;
    isLastRow: boolean;
    push(item: Item, size: IDimensions): RowEntry;
    distribute(width: number, padding: number, maxScale?: number): void;
}
export interface IRowPackingOptions {
    /**
     * Minimum number of items that must be placed in a row.
     */
    minItemsPerRow?: number;
    /**
     * Width multiplier applied to the original width of the masonry.
     */
    widthMultiplier?: number;
    /**
     * Horizontal padding between items within a row.
     */
    itemPadding?: number;
    /**
     * Vertical alignment of items within a row.
     */
    itemAlign?: number;
    /**
     * Vertical padding between rows.
     */
    rowPadding?: number;
    /**
     * Horizontal alignment of items within a row.
     */
    rowAlign?: RowAlign;
    /**
     * Maximum number of allowed rows.
     */
    maxRows?: number;
    /**
     * Indention applied to the last row.
     */
    lastRowIndent?: number;
}
/**
 * A layout strategy that packs items into rows.
 */
export declare class RowPacking implements IMasonryStrategy {
    /**
     * The masonry this strategy is intented to be used with.
     */
    masonry: Masonry;
    /**
     * A list of all rows the layout currently consists of.
     */
    rows: Row[];
    /**
     * A map of all child cid values and their matching row entry.
     */
    rowMapping: RowMapping;
    /**
     * The height of the current layout state.
     */
    height: number;
    /**
     * The width of the masonry while last updating the layout.
     */
    masonryWidth: number;
    /**
     * Minimum number of items that must be placed in a row.
     */
    minItemsPerRow: number;
    /**
     * Width multiplier applied to the original width of the masonry.
     */
    widthMultiplier: number;
    /**
     * Horizontal padding between items within a row.
     */
    itemPadding: number;
    /**
     * Vertical alignment of items within a row.
     */
    itemAlign: number;
    /**
     * Vertical padding between rows.
     */
    rowPadding: number;
    /**
     * Horizontal alignment of items within a row.
     */
    rowAlign: RowAlign;
    /**
     * Maximum number of allowed rows.
     */
    maxRows: number;
    /**
     * Indention applied to the last row.
     */
    lastRowIndent: number;
    /**
     * RowPacking constructor.
     *
     * @param options
     *   RowPacking constructor options.
     */
    constructor(options?: IRowPackingOptions);
    reset(items: Item[]): void;
    place(item: Item): IRectangle;
    isUpdateRequired(): boolean;
    getMaxHeight(): number;
    setMasonry(masonry: Masonry): void;
}
