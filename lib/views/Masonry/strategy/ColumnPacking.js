import { Column } from '../util/Column';
/**
 * A layout strategy for the Masonry view that packs all items
 * into fixed columns.
 */
var ColumnPacking = (function () {
    /**
     * Create a new ColumnPackingStrategy instance.
     */
    function ColumnPacking(options) {
        /**
         * Column instances used to store the current layout.
         */
        this.columns = [];
        /**
         * The width of a single column.
         */
        this.columnWidth = 0;
        /**
         * The width of the masonry while last updating the layout.
         */
        this.masonryWidth = 0;
        /**
         * The gutter width.
         */
        this.gutter = 0;
        this.columnCount = options.columnCount;
        this.gutter = options.gutter !== void 0 ? options.gutter : 0;
    }
    /**
     * Reset the layout state of this strategy. Must be called before
     * a new layout is being generated.
     */
    ColumnPacking.prototype.reset = function () {
        var count = this.getColumnCount();
        var width = this.masonry.container.offsetWidth;
        var columns = [];
        for (var index = count - 1; index >= 0; index--) {
            var column = new Column(index);
            column.next = columns[index + 1];
            columns[index] = column;
        }
        this.masonryWidth = width;
        this.columnWidth = (width - this.gutter * (count - 1)) / count;
        this.columns = columns;
    };
    /**
     * Mark the given position as being reserved.
     */
    ColumnPacking.prototype.reserve = function (position) {
        this.columns[position.column].reserve(position);
    };
    /**
     * Return whether the layout has been invalidated e.g. due to
     * browser resizes.
     */
    ColumnPacking.prototype.isUpdateRequired = function () {
        var width = this.masonry.element.offsetWidth;
        var count = this.getColumnCount();
        return (width != this.masonryWidth ||
            count != this.columns.length);
    };
    /**
     * Set the grid this strategy is working on.
     */
    ColumnPacking.prototype.setMasonry = function (masonry) {
        this.masonry = masonry;
        this.masonryWidth = 0;
    };
    /**
     * Return the number of available columns.
     */
    ColumnPacking.prototype.getColumnCount = function () {
        if (typeof this.columnCount === 'number') {
            return this.columnCount;
        }
        else {
            return this.columnCount(this.masonry);
        }
    };
    /**
     * Return the maximum height of the generated layout.
     */
    ColumnPacking.prototype.getMaxHeight = function () {
        var height = 0;
        for (var _i = 0, _a = this.columns; _i < _a.length; _i++) {
            var column = _a[_i];
            height = Math.max(height, column.getMaxHeight());
        }
        return height - this.gutter;
    };
    /**
     * Place the given masonry item and return the assigned position.
     */
    ColumnPacking.prototype.place = function (item) {
        var position = this.getBestPosition(item);
        if (!position) {
            return null;
        }
        position.x = (this.columnWidth + this.gutter) * position.column;
        this.reserve(position);
        return position;
    };
    ColumnPacking.prototype.getColumnCountFromWidth = function (width) {
        return Math.max(1, Math.min(this.getColumnCount(), Math.round(width / this.columnWidth)));
    };
    /**
     * Return the best free position for the given masonry item.
     */
    ColumnPacking.prototype.getBestPosition = function (item) {
        var positions = this.getAllPositions(item);
        positions.sort(function (a, b) {
            if (a.y != b.y) {
                return a.y - b.y;
            }
            if (a.column == b.column) {
                return 0;
            }
            return a.column - b.column;
        });
        return positions[0];
    };
    /**
     * Return all possible positions for the given masonry item.
     */
    ColumnPacking.prototype.getAllPositions = function (item) {
        var _a = item.getDesiredSize(), width = _a.width, height = _a.height;
        height += this.gutter;
        var columns = this.getColumnCountFromWidth(width);
        var result = [];
        var max = this.columns.length - columns + 1;
        for (var index = 0; index < max; index++) {
            this.columns[index].getAllPositions(columns, height, result);
        }
        return result;
    };
    /**
     * Return the next free position within the given column.
     */
    ColumnPacking.prototype.getPositionAt = function (index, item) {
        var _a = item.getDesiredSize(), width = _a.width, height = _a.height;
        var columns = this.getColumnCountFromWidth(width);
        var positions = [];
        this.columns[index].getAllPositions(columns, height, positions);
        return positions.pop();
    };
    return ColumnPacking;
}());
export default ColumnPacking;
