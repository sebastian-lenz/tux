import * as tslib_1 from "tslib";
import View from '../View';
var Item = (function (_super) {
    tslib_1.__extends(Item, _super);
    function Item(options) {
        var _this = _super.call(this, options) || this;
        /**
         * Should this item be visible?
         */
        _this.isVisible = true;
        return _this;
    }
    /**
     * Set the position of this item within the masonry.
     */
    Item.prototype.setMasonryPosition = function (position) {
        var style = this.element.style;
        if (!position) {
            style.visibility = 'hidden';
            style.top = '0';
            style.left = '0';
        }
        else {
            style.visibility = 'inherit';
            style.top = position.y + "px";
            style.left = position.x + "px";
        }
        this.handleViewportResize();
    };
    /**
     * Return the desired size of this item.
     */
    Item.prototype.getDesiredSize = function () {
        var element = this.element;
        return {
            width: element.offsetWidth,
            height: element.offsetHeight
        };
    };
    Item.prototype.setActualSize = function (size) {
        return this.getDesiredSize();
    };
    /**
     * Triggered before the masonry is about to update the layout.
     */
    Item.prototype.handleMasonryPreUpdate = function (masonry) { };
    /**
     * Triggered after the masonry has updated the layout.
     */
    Item.prototype.handleMasonryPostUpdate = function (masonry) { };
    /**
     * Triggered when the masonry is about to become activated.
     */
    Item.prototype.handleMasonryActivate = function () { };
    /**
     * Triggered when the masonry is about to become deactivated.
     */
    Item.prototype.handleMasonryDeactivate = function () {
        var style = this.element.style;
        style.top = '';
        style.left = '';
    };
    return Item;
}(View));
export default Item;
