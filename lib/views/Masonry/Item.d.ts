import { IDimensions } from '../../geom/Dimensions';
import { IRectangle } from '../../geom/Rectangle';
import View, { ViewOptions } from '../View';
import Masonry from './index';
export interface ItemOptions extends ViewOptions {
}
export default class Item extends View {
    /**
     * Should this item be visible?
     */
    isVisible: boolean;
    constructor(options?: ItemOptions);
    /**
     * Set the position of this item within the masonry.
     */
    setMasonryPosition(position: IRectangle): void;
    /**
     * Return the desired size of this item.
     */
    getDesiredSize(): IDimensions;
    setActualSize(size: IDimensions): IDimensions;
    /**
     * Triggered before the masonry is about to update the layout.
     */
    handleMasonryPreUpdate(masonry: Masonry): void;
    /**
     * Triggered after the masonry has updated the layout.
     */
    handleMasonryPostUpdate(masonry: Masonry): void;
    /**
     * Triggered when the masonry is about to become activated.
     */
    handleMasonryActivate(): void;
    /**
     * Triggered when the masonry is about to become deactivated.
     */
    handleMasonryDeactivate(): void;
}
