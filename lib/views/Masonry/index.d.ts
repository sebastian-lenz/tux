import { IRectangle } from '../../geom/Rectangle';
import ChildableView, { ChildableViewOptions } from '../ChildableView';
import Item from './Item';
/**
 * Constructor options for the Masonry view.
 */
export interface MasonryOptions<T extends Item = Item> extends ChildableViewOptions<T> {
    /**
     * The layout strategy that should be used.
     */
    strategy?: IMasonryStrategy;
    /**
     * Should the masonry currently be enabled?
     */
    isEnabled?: boolean;
}
/**
 * An interface for masonry layout strategies.
 */
export interface IMasonryStrategy {
    /**
     * Reset the layout state of this strategy. Must be called before
     * a new layout is being generated.
     */
    reset(items: Item[]): any;
    /**
     * Place the given masonry item and return the assigned position.
     */
    place(item: Item): IRectangle;
    /**
     * Return whether the layout has been invalidated e.g. due to
     * browser resizes.
     */
    isUpdateRequired(): boolean;
    /**
     * Return the maximum height of the generated layout.
     */
    getMaxHeight(): number;
    /**
     * Set the masonry this strategy is working on.
     */
    setMasonry(masonry: Masonry): any;
}
/**
 * Arranges children in a masonry layout.
 *
 * The actual layout is defined by an instance of IMasonryStrategy
 * which must be passed to the constructor.
 */
export default class Masonry<T extends Item = Item> extends ChildableView<T> {
    /**
     * The layout strategy that should be used.
     */
    strategy: IMasonryStrategy;
    /**
     * Should the masonry currently be enabled?
     */
    isEnabled: boolean;
    /**
     * Is the grid currently active?
     */
    protected isActive: boolean;
    /**
     * Create a new Masonry instance.
     */
    constructor(options?: MasonryOptions<T>);
    /**
     * Render the layout depending on the current activity and masonry state.
     */
    update(): void;
    /**
     * Render the masonry layout.
     */
    protected layout(): void;
    /**
     * Determine whether the masonry should currently be avtive.
     */
    shouldBeActive(): boolean;
    /**
     * Set the strategy that is used to genarate the layout.
     */
    setStrategy(strategy: IMasonryStrategy): void;
    /**
     * Set whether the masonry should be enabled or not.
     */
    setEnabled(value: boolean): void;
    setHeight(value: number): void;
    /**
     * Set whether this masonry is active or not.
     */
    protected setActive(value: boolean): void;
    /**
     * Triggered after the size of the viewport has changed.
     *
     * If this view is used as a component this handler will be called automatically.
     *
     * @returns
     *   TRUE if the event handling should be stopped and child components should not receive
     *   the event, FALSE otherwise.
     */
    handleViewportResize(resizeChildren?: Function): boolean;
}
