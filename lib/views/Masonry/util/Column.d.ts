import { IRectangle } from '../../../geom/Rectangle';
/**
 * Defines a reserved area within a column.
 */
export interface IReservation {
    /**
     * The vertical offset the reservation begins.
     */
    min: number;
    /**
     * The vertical offset the reservation ends.
     */
    max: number;
}
/**
 * A position within a column.
 */
export interface IColumnRegion extends IRectangle {
    /**
     * The column index of this position.
     */
    column: number;
}
/**
 * Represents a single layout column.
 */
export declare class Column {
    /**
     * The zero based index of this column.
     */
    index: number;
    /**
     * The column placed on the right side of this column.
     */
    next: Column;
    /**
     * A list of all reserved areas within this column.
     */
    reserved: IReservation[];
    /**
     * Create a new Column instance.
     */
    constructor(index: number);
    /**
     * Return the maximum height of this column.
     */
    getMaxHeight(columns?: number): number;
    /**
     * Find all available free gaps for the given dimensions.
     */
    getAllPositions(columns: number, height: number, result?: IColumnRegion[]): IColumnRegion[];
    /**
     * Test whether there is free space at the given offset.
     */
    isFree(columns: number, height: number, offset: number): boolean;
    /**
     * Call the given callback for each available gap within this colum.
     */
    walk(columns: number, callback: {
        (min: number, max: number);
    }): any;
    /**
     * Reserve the given position.
     */
    reserve(position: IColumnRegion): void;
}
