/**
 * Represents a single layout column.
 */
var Column = (function () {
    /**
     * Create a new Column instance.
     */
    function Column(index) {
        /**
         * A list of all reserved areas within this column.
         */
        this.reserved = [];
        this.index = index;
    }
    /**
     * Return the maximum height of this column.
     */
    Column.prototype.getMaxHeight = function (columns) {
        if (columns === void 0) { columns = 1; }
        var reserved = this.reserved;
        var length = reserved.length;
        var max;
        if (length == 0) {
            max = 0;
        }
        else {
            max = reserved[length - 1].max;
        }
        if (columns > 1) {
            return Math.max(max, this.next.getMaxHeight(columns - 1));
        }
        else {
            return max;
        }
    };
    /**
     * Find all available free gaps for the given dimensions.
     */
    Column.prototype.getAllPositions = function (columns, height, result) {
        var _this = this;
        if (result === void 0) { result = []; }
        this.walk(columns, function (min, max) {
            if (max - min < height)
                return;
            if (columns > 1 && !_this.next.isFree(columns - 1, height, min))
                return;
            result.push({
                column: _this.index,
                x: 0,
                y: min,
                width: columns,
                height: height
            });
        });
        return result;
    };
    /**
     * Test whether there is free space at the given offset.
     */
    Column.prototype.isFree = function (columns, height, offset) {
        for (var index = 0, count = this.reserved.length; index < count; index++) {
            var reservation = this.reserved[index];
            if (reservation.max <= offset)
                continue;
            if (reservation.min >= offset + height)
                continue;
            return false;
        }
        if (columns > 1) {
            return this.next.isFree(columns - 1, height, offset);
        }
        else {
            return true;
        }
    };
    /**
     * Call the given callback for each available gap within this colum.
     */
    Column.prototype.walk = function (columns, callback) {
        var bottom = this.getMaxHeight(columns);
        var wasBottomVisited = false;
        var count = this.reserved.length;
        if (count == 0) {
            return callback(0, Number.NaN);
        }
        var current, next = this.reserved[0];
        for (var index = 0; index < count; index++) {
            current = next;
            if (index == 0 && current.min > 0) {
                if (bottom == 0)
                    wasBottomVisited = true;
                callback(0, current.min);
            }
            if (current.max == bottom)
                wasBottomVisited = true;
            if (index == count - 1) {
                callback(current.max, Number.NaN);
            }
            else {
                next = this.reserved[index + 1];
                callback(current.max, next.min);
            }
        }
        if (!wasBottomVisited) {
            callback(bottom, Number.NaN);
        }
    };
    /**
     * Reserve the given position.
     */
    Column.prototype.reserve = function (position) {
        this.reserved.push({
            min: position.y,
            max: position.y + position.height
        });
        this.reserved.sort(function (a, b) {
            if (a.min == b.min)
                return 0;
            return a.min > b.min ? 1 : -1;
        });
        var index = 0, count = this.reserved.length - 1;
        while (index < count) {
            var a = this.reserved[index];
            var b = this.reserved[index + 1];
            if (Math.abs(a.max - b.min) < 0.5) {
                a.max = b.max;
                this.reserved.splice(index + 1, 1);
                count -= 1;
            }
            else {
                index += 1;
            }
        }
        if (this.index < position.column + position.width - 1) {
            this.next.reserve(position);
        }
    };
    return Column;
}());
export { Column };
