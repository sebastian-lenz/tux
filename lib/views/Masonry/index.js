import * as tslib_1 from "tslib";
import { defaults, forEach } from 'underscore';
import { option } from '../View';
import ChildableView from '../ChildableView';
import Item from './Item';
/**
 * Arranges children in a masonry layout.
 *
 * The actual layout is defined by an instance of IMasonryStrategy
 * which must be passed to the constructor.
 */
var Masonry = (function (_super) {
    tslib_1.__extends(Masonry, _super);
    /**
     * Create a new Masonry instance.
     */
    function Masonry(options) {
        var _this = _super.call(this, defaults(options || {}, {
            childSelector: 'li',
            childClass: Item,
            isEnabled: true,
        })) || this;
        /**
         * Is the grid currently active?
         */
        _this.isActive = false;
        if (options.strategy) {
            _this.setStrategy(options.strategy);
        }
        return _this;
    }
    /**
     * Render the layout depending on the current activity and masonry state.
     */
    Masonry.prototype.update = function () {
        this.setActive(this.shouldBeActive());
        if (this.isActive) {
            this.layout();
        }
        else {
            forEach(this.children, function (child) { return child.handleViewportResize(); });
        }
    };
    /**
     * Render the masonry layout.
     */
    Masonry.prototype.layout = function () {
        var strategy = this.strategy;
        var children = [];
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            if (child.isVisible) {
                children.push(child);
                child.handleMasonryPreUpdate(this);
            }
        }
        strategy.reset(children);
        for (var _b = 0, children_1 = children; _b < children_1.length; _b++) {
            var child = children_1[_b];
            child.setMasonryPosition(strategy.place(child));
        }
        for (var _c = 0, children_2 = children; _c < children_2.length; _c++) {
            var child = children_2[_c];
            child.handleMasonryPostUpdate(this);
        }
        this.setHeight(strategy.getMaxHeight());
    };
    /**
     * Determine whether the masonry should currently be avtive.
     */
    Masonry.prototype.shouldBeActive = function () {
        return this.children && this.isEnabled;
    };
    /**
     * Set the strategy that is used to genarate the layout.
     */
    Masonry.prototype.setStrategy = function (strategy) {
        if (this.strategy) {
            this.strategy.setMasonry(null);
        }
        this.strategy = strategy;
        this.strategy.setMasonry(this);
        this.update();
    };
    /**
     * Set whether the masonry should be enabled or not.
     */
    Masonry.prototype.setEnabled = function (value) {
        if (this.isEnabled == value)
            return;
        this.isEnabled = value;
        this.update();
    };
    Masonry.prototype.setHeight = function (value) {
        this.container.style.height = value + 'px';
    };
    /**
     * Set whether this masonry is active or not.
     */
    Masonry.prototype.setActive = function (value) {
        if (this.isActive == value) {
            return;
        }
        this.isActive = value;
        this.element.classList.toggle('active', value);
        if (value) {
            for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
                var child = _a[_i];
                child.handleMasonryActivate();
            }
        }
        else {
            this.container.style.height = '';
            for (var _b = 0, _c = this.children; _b < _c.length; _b++) {
                var child = _c[_b];
                child.handleMasonryDeactivate();
            }
        }
    };
    /**
     * Triggered after the size of the viewport has changed.
     *
     * If this view is used as a component this handler will be called automatically.
     *
     * @returns
     *   TRUE if the event handling should be stopped and child components should not receive
     *   the event, FALSE otherwise.
     */
    Masonry.prototype.handleViewportResize = function (resizeChildren) {
        if (!this.isActive || (this.strategy && this.strategy.isUpdateRequired())) {
            this.update();
        }
        else {
            forEach(this.children, function (child) { return child.handleViewportResize(); });
        }
        return true;
    };
    return Masonry;
}(ChildableView));
export default Masonry;
tslib_1.__decorate([
    option({ type: 'bool', defaultValue: true })
], Masonry.prototype, "isEnabled", void 0);
