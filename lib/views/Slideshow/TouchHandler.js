import * as tslib_1 from "tslib";
import transform from '../../util/vendor/transform';
import { createTween } from '../../fx/tween/Tween';
import { easeOutExpo } from '../../fx/easing/expo';
import { ItemState } from './Item';
import DragHandler, { DragDirection } from '../../handler/DragHandler';
/**
 * Defines all available shift states.
 */
export var Shift;
(function (Shift) {
    Shift[Shift["Previous"] = 0] = "Previous";
    Shift[Shift["Next"] = 1] = "Next";
    Shift[Shift["None"] = 2] = "None";
})(Shift || (Shift = {}));
/**
 * A handler that allows browsing the contents of a slideshow through swipe gestures.
 */
var TouchHandler = (function (_super) {
    tslib_1.__extends(TouchHandler, _super);
    /**
     * TouchHandler constructor.
     *
     * @param options
     *   The constructor options.
     */
    function TouchHandler(options) {
        var _this = _super.call(this, options.view, options.selector) || this;
        /**
         * The current position offset of the container in pixels.
         */
        _this.position = 0;
        /**
         * The position offset of the container from the beginning of a swipe gesture.
         */
        _this.initialPosition = 0;
        /**
         * The current shift state.
         */
        _this.shift = Shift.None;
        _this.direction = DragDirection.Horizontal;
        _this.allowMouse = !!options.allowMouse;
        _this.slideshow = options.view;
        _this.clickCallback = options.click;
        _this.transformElement = options.transformElement || options.view.container;
        _this.sizeElement = options.sizeElement || options.view.element;
        return _this;
    }
    TouchHandler.prototype.getWidth = function () {
        return this.sizeElement.clientWidth;
    };
    /**
     * Set the offset position of the slideshow content.
     *
     * @param value
     *   The new offset position.
     */
    TouchHandler.prototype.setPosition = function (value) {
        if (this.position == value)
            return;
        this.position = value;
        this.transformElement.style[transform.styleName] = "translate(" + value + "px,0)";
    };
    /**
     * Set the shift state of the slideshow.
     *
     * The shift state is used to tag the previous or next slide that is
     * visible due to a drag operation. It triggers the necessary image load
     * chains and updates the placement of the slides.
     *
     * @param value
     *   The new shift state of the slideshow.
     */
    TouchHandler.prototype.setShift = function (value) {
        if (this.shift == value)
            return;
        this.shift = value;
        var state = ItemState.Inactive;
        var element = null;
        if (value === Shift.Next) {
            element = this.slideshow.getNextChild();
            state = ItemState.PeakNext;
        }
        else if (value === Shift.Previous) {
            element = this.slideshow.getPreviousChild();
            state = ItemState.PeakPrevious;
        }
        else {
            element = null;
        }
        if (this.shiftItem !== element) {
            if (this.shiftItem) {
                this.shiftItem.setState(ItemState.Inactive);
            }
            this.shiftItem = element;
            if (element)
                element.load();
        }
        if (element)
            element.setState(state);
    };
    /**
     * Update the shift state from the given position offset.
     *
     * @param position
     *   The position offset the shift state should be derived from.
     */
    TouchHandler.prototype.updateShift = function (position) {
        var shift = Shift.None;
        if (position > 0) {
            shift = Shift.Previous;
        }
        else if (position < 0) {
            shift = Shift.Next;
        }
        if (this.shift != shift) {
            this.setShift(shift);
        }
    };
    /**
     * Animate the position of the container to the given value.
     *
     * @param value
     *   The target offset position value.
     * @param updateElement
     *   When set to TRUE, the current shiftElement will become to current
     *   element of the slideshow after animation has finished.
     */
    TouchHandler.prototype.animateTo = function (value, updateElement) {
        var _this = this;
        var callback = function () {
            var _a = _this, shiftItem = _a.shiftItem, slideshow = _a.slideshow;
            _this.tween = null;
            _this.setPosition(0);
            _this.setShift(Shift.None);
            slideshow.isLocked = false;
            slideshow.trigger('locked', false);
            if (updateElement && shiftItem) {
                slideshow.setCurrent(shiftItem, { noTransition: true });
            }
        };
        this.tween = createTween(this, 'position', {
            to: value,
            duration: 750,
            easing: easeOutExpo,
            finished: callback,
            stopped: callback
        });
    };
    /**
     * Try to interrupt the current position animation.
     *
     * @returns
     *   TRUE if the animation could be interrupted, FALSE otherwise.
     */
    TouchHandler.prototype.interruptAnimation = function () {
        if (!this.tween)
            return true;
        var _a = this, position = _a.position, slideshow = _a.slideshow, tween = _a.tween;
        var length = slideshow.getLength();
        var from = slideshow.getCurrentIndex();
        tween.stop();
        var to = slideshow.getCurrentIndex();
        if (to === from)
            return true;
        var diff = from - to;
        while (diff > length * 0.5)
            diff -= length;
        while (diff < length * -0.5)
            diff += length;
        var width = this.getWidth();
        var offset = width * (diff / Math.abs(diff));
        if (length < 3 && Math.abs(position - offset) > width)
            offset *= -1;
        this.setPosition(position - offset);
        return true;
    };
    /**
     * Triggered after a drag has started.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the drag start.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    TouchHandler.prototype.handleDragStart = function (event, pointer) {
        var _a = this, allowMouse = _a.allowMouse, slideshow = _a.slideshow;
        if ((!this.interruptAnimation()) ||
            (pointer.type != 1 /* Touch */ && !allowMouse) ||
            (slideshow.getLength() < 2) ||
            (slideshow.isInTransition()))
            return false;
        slideshow.isLocked = true;
        slideshow.trigger('locked', true);
        this.initialPosition = this.position;
        return true;
    };
    /**
     * Triggered after a drag has moved.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the move.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    TouchHandler.prototype.handleDragMove = function (event, pointer) {
        var delta = pointer.getAbsoluteDelta();
        var position = this.initialPosition + delta.x;
        this.updateShift(position);
        if (!this.shiftItem) {
            position = this.initialPosition + delta.x * 0.5;
        }
        this.setPosition(position);
        return true;
    };
    /**
     * Triggered after a drag has ended.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    TouchHandler.prototype.handleDragEnd = function (event, pointer) {
        var _a = this, position = _a.position, shift = _a.shift, slideshow = _a.slideshow;
        var width = this.getWidth();
        var relative = position / width;
        var velocity = pointer.getVelocity().x;
        if (Math.abs(relative) > 0.333 || Math.abs(velocity) > 20) {
            this.animateTo(width * (shift === Shift.Next ? -1 : 1), true);
            slideshow.trigger('currentWillChange', this.shiftItem);
        }
        else {
            this.animateTo(0);
        }
    };
    /**
     * Triggered after the drag has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    TouchHandler.prototype.handleDragCancel = function (event, pointer) {
        this.animateTo(0);
    };
    /**
     * Triggered after the user has clicked into the view.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    TouchHandler.prototype.handleClick = function (event, pointer) {
        if (this.clickCallback) {
            this.clickCallback(event);
        }
    };
    return TouchHandler;
}(DragHandler));
export default TouchHandler;
