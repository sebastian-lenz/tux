/// <reference types="q" />
import * as Q from 'q';
import View, { ViewOptions } from '../View';
import ImageWrap from '../ImageWrap';
/**
 * Defines all states a slide can be in.
 */
export declare enum ItemState {
    Inactive = 0,
    Active = 1,
    BecomeActive = 2,
    BecomeInactive = 4,
    PeakNext = 8,
    PeakPrevious = 16,
    ActiveOrBecomeActive = 3,
    InactiveOrBecomeInactive = 4,
}
/**
 * The interface all elements of a slideshow must implement.
 */
export interface Item extends View {
    /**
     * Preload this slide.
     *
     * @returns
     *   A promise that resolves after the slide has loaded.
     */
    load(): Q.Promise<any>;
    /**
     * Set the state of this slide.
     *
     * @param state
     *   The new state of this slide.
     */
    setState(state: ItemState): void;
}
export interface DefaultItemOptions extends ViewOptions {
    imageSelector?: string;
    imageOptions?: string;
}
/**
 * An implementation of a simple slide.
 */
export default class DefaultItem extends View implements Item {
    /**
     * The current state of this slide.
     */
    state: ItemState;
    /**
     * The image displayed by this slide.
     */
    imageWrap: ImageWrap;
    /**
     * Initialize this slide.
     */
    constructor(options: DefaultItemOptions);
    /**
     * Remove this slide.
     *
     * @returns {SlideshowItem}
     */
    remove(): this;
    /**
     * Preload this slide.
     *
     * @returns
     *   A promise that resolves after the slide has loaded.
     */
    load(): Q.Promise<any>;
    /**
     * Set the state of this slide.
     *
     * @param state
     *   The new state of this slide.
     */
    setState(state: ItemState): void;
    handleViewportResize(resizeChildren?: Function): boolean;
}
