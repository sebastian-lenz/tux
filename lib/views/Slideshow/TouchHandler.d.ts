import { AnyTween } from '../../fx/tween/Tween';
import { Item } from './Item';
import DragHandler from '../../handler/DragHandler';
import Pointer from '../../handler/Pointer';
import Slideshow from './index';
/**
 * Constructor options for the TouchHandler class.
 */
export interface TouchHandlerOptions {
    /**
     * The view instance that should be controlled by the handler.
     */
    view: Slideshow;
    /**
     * A css selector used to retrieve the container whose position will be
     * manipulated by swipe gestures.
     */
    selector?: string;
    /**
     * Whether mouse users should be able to use swipe gestures or not.
     */
    allowMouse?: boolean;
    /**
     * A callback that will be invoked if the user clicks onto the slideshow.
     */
    click?: Function;
    transformElement?: HTMLElement;
    sizeElement?: HTMLElement;
}
/**
 * Defines all available shift states.
 */
export declare enum Shift {
    Previous = 0,
    Next = 1,
    None = 2,
}
/**
 * A handler that allows browsing the contents of a slideshow through swipe gestures.
 */
export default class TouchHandler extends DragHandler {
    /**
     * The slideshow instance that should be controlled by the handler.
     */
    slideshow: Slideshow;
    /**
     * The container whose position will be manipulated by swipe gestures.
     */
    transformElement: HTMLElement;
    /**
     * The container whose size will be used.
     */
    sizeElement: HTMLElement;
    /**
     * The current position offset of the container in pixels.
     */
    position: number;
    /**
     * The position offset of the container from the beginning of a swipe gesture.
     */
    initialPosition: number;
    /**
     * The currently active animation on the container.
     */
    tween: AnyTween;
    /**
     * The current shift state.
     */
    shift: Shift;
    /**
     * The slideshow item that is affected by the current shift state.
     */
    shiftItem: Item;
    /**
     * Whether mouse users should be able to use swipe gestures or not.
     */
    allowMouse: boolean;
    /**
     * A callback that will be invoked if the user clicks onto the slideshow.
     */
    clickCallback: Function;
    /**
     * TouchHandler constructor.
     *
     * @param options
     *   The constructor options.
     */
    constructor(options: TouchHandlerOptions);
    getWidth(): number;
    /**
     * Set the offset position of the slideshow content.
     *
     * @param value
     *   The new offset position.
     */
    setPosition(value: number): void;
    /**
     * Set the shift state of the slideshow.
     *
     * The shift state is used to tag the previous or next slide that is
     * visible due to a drag operation. It triggers the necessary image load
     * chains and updates the placement of the slides.
     *
     * @param value
     *   The new shift state of the slideshow.
     */
    setShift(value: Shift): void;
    /**
     * Update the shift state from the given position offset.
     *
     * @param position
     *   The position offset the shift state should be derived from.
     */
    updateShift(position: number): void;
    /**
     * Animate the position of the container to the given value.
     *
     * @param value
     *   The target offset position value.
     * @param updateElement
     *   When set to TRUE, the current shiftElement will become to current
     *   element of the slideshow after animation has finished.
     */
    animateTo(value: number, updateElement?: boolean): void;
    /**
     * Try to interrupt the current position animation.
     *
     * @returns
     *   TRUE if the animation could be interrupted, FALSE otherwise.
     */
    interruptAnimation(): boolean;
    /**
     * Triggered after a drag has started.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the drag start.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    handleDragStart(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a drag has moved.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the move.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    handleDragMove(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a drag has ended.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    handleDragEnd(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the drag has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    handleDragCancel(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the user has clicked into the view.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    handleClick(event: Event, pointer: Pointer): void;
}
