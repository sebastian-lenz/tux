import * as tslib_1 from "tslib";
import * as Q from 'q';
import { defaults } from 'underscore';
import animation from '../../util/vendor/animation';
import { option } from '../View';
import CycleableView from '../CycleableView';
import DefaultItem, { ItemState } from './Item';
import Visibility from '../../services/Visibility';
/**
 * A basic slideshow implementation.
 */
var Slideshow = (function (_super) {
    tslib_1.__extends(Slideshow, _super);
    /**
     * Slideshow constructor.
     *
     * @param options
     *   The constructor options.
     */
    function Slideshow(options) {
        var _this = _super.call(this, options = defaults(options || {}, {
            className: 'tuxSlideshow',
            container: '.tuxSlideshow__slides',
            childSelector: '.tuxSlideshow__slides > li',
            childClass: DefaultItem,
            isLooped: true,
            autoSelectFirstChild: true,
            allowPreload: true,
        })) || this;
        /**
         * Whether this element is currently within the visible viewport bounds or not.
         */
        _this.inViewport = false;
        if (!options.disableVisibilityCheck) {
            Visibility.getInstance().register(_this);
        }
        else {
            _this.setInViewport(true);
        }
        return _this;
    }
    /**
     * Set whether the element of this view is currently within the visible bounds
     * of the viewport or not.
     *
     * @param inViewport
     *   TRUE if the element is visible, FALSE otherwise.
     */
    Slideshow.prototype.setInViewport = function (inViewport) {
        if (this.inViewport == inViewport)
            return;
        this.inViewport = inViewport;
        this.preloadSiblings();
    };
    /**
     * Return a string indicating the transition direction between the two
     * given slides.
     *
     * @param from
     *   The child the transition should start with.
     * @param to
     *   The child the transition should end with.
     * @returns
     *   A string indicating the transition direction ('forward' or 'backward').
     */
    Slideshow.prototype.getDirection = function (from, to) {
        var index = this.indexOf(to) - this.indexOf(from);
        return index >= 0 ? 'forward' : 'backward';
    };
    /**
     * Return whether the slideshow is currently within a transition or not.
     *
     * @returns
     *   TRUE if the slideshow is currently within a transition, FALSE otherwise.
     */
    Slideshow.prototype.isInTransition = function () {
        return !!this.transition;
    };
    /**
     * Preload the content of the previous and next slide.
     */
    Slideshow.prototype.preloadSiblings = function () {
        var _this = this;
        if (!this.inViewport)
            return;
        if (this.current) {
            var current_1 = this.current;
            var next_1 = this.getNextChild();
            var previous_1 = this.getPreviousChild();
            current_1.load().then(function () {
                if (_this.current != current_1 || !_this.allowPreload)
                    return;
                return next_1.load();
            }).then(function () {
                if (_this.current != current_1 || !_this.allowPreload)
                    return;
                return previous_1.load();
            });
        }
    };
    /**
     * Create the container children will be placed in.
     *
     * @returns
     *   The generated container.
     */
    Slideshow.prototype.createContainer = function () {
        var container = document.createElement('ul');
        container.className = 'tuxSlideshow__slides';
        this.element.appendChild(container);
        return container;
    };
    /**
     * Internal transition handler. Creates a transition between the two given children.
     *
     * @param newChild
     *   The child the transition should lead to.
     * @param oldChild
     *   The child the transition should come from.
     * @param options
     *   Optional transition options.
     */
    Slideshow.prototype.handleTransition = function (newChild, oldChild, options) {
        if (options === void 0) { options = {}; }
        if (!this.transition && (!oldChild ||
            !newChild ||
            !this.inViewport ||
            options.noTransition)) {
            if (oldChild)
                oldChild.setState(ItemState.Inactive);
            if (newChild)
                newChild.setState(ItemState.Active);
            this.preloadSiblings();
            return;
        }
        if (this.transition) {
            newChild.load();
            this.successor = newChild;
            this.successorOptions = options;
        }
        else {
            this.transition = this.createSequence(oldChild, newChild, options);
        }
    };
    /**
     * Create the transition sequence between the two given slides.
     *
     * @param from
     *   The child the transition should start with.
     * @param to
     *   The child the transition should end with.
     * @returns
     *   A promise that resolves after the transition has finished.
     */
    Slideshow.prototype.createSequence = function (from, to, options) {
        var _this = this;
        return to.load().then(function () {
            return _this.createTransition(from, to, options);
        }).then(function () {
            var _a = _this, successor = _a.successor, successorOptions = _a.successorOptions;
            if (successor) {
                _this.successor = null;
                _this.successorOptions = null;
                return _this.createSequence(to, successor, successorOptions);
            }
            else {
                _this.transition = null;
                _this.preloadSiblings();
            }
        });
    };
    /**
     * Create a graphical transition between the two given slides.
     *
     * @param from
     *   The child the transition should start with.
     * @param to
     *   The child the transition should end with.
     * @param transitionClass
     *   The name of the css class that should be used to generate the transition.
     * @returns
     *   A promise that resolves after the transition has finished.
     */
    Slideshow.prototype.createTransition = function (from, to, options) {
        var _this = this;
        var deferred = Q.defer();
        var _a = options.direction, direction = _a === void 0 ? this.getDirection(from, to) : _a, _b = options.transitionClass, transitionClass = _b === void 0 ? 'transition' : _b;
        if (!animation.endEvent) {
            from.setState(ItemState.Inactive);
            to.setState(ItemState.Active);
            deferred.resolve();
            return deferred.promise;
        }
        var complete = function () {
            _this.element.classList.remove(transitionClass);
            _this.element.classList.remove(direction);
            to.undelegate(animation.endEvent, null, complete);
            to.setState(ItemState.Active);
            from.setState(ItemState.Inactive);
            deferred.resolve();
        };
        this.element.classList.add(transitionClass);
        this.element.classList.add(direction);
        to.delegate(animation.endEvent, complete);
        to.setState(ItemState.BecomeActive);
        from.setState(ItemState.BecomeInactive);
        return deferred.promise;
    };
    return Slideshow;
}(CycleableView));
export default Slideshow;
tslib_1.__decorate([
    option({ type: 'bool' })
], Slideshow.prototype, "allowPreload", void 0);
