import Events from '../../Events';
import CycleableView from '../CycleableView';
/**
 * Constructor options for the AutoPlay class.
 */
export interface AutoPlayOptions {
    /**
     * The CycleableView that should be controlled.
     */
    view?: CycleableView;
    /**
     * The desired interval between each step in milliseconds.
     */
    interval?: number;
    /**
     * Whether the AutoPlay should be initially active or not.
     */
    autoPlay?: boolean;
}
/**
 * Advances a CycleableView view within a regular interval.
 */
export declare class AutoPlay extends Events {
    /**
     * The CycleableView that should be controlled.
     */
    view: CycleableView;
    /**
     * The desired interval between each step in milliseconds.
     */
    interval: number;
    /**
     * Whether this instance is currently active or not.
     */
    isPlaying: boolean;
    /**
     * The current interval handle.
     */
    handle: number;
    /**
     * AutoPlay constructor.
     *
     * @param options
     *   The constructor options.
     */
    constructor(options?: AutoPlayOptions);
    /**
     * Set the CycleableView that should be controlled.
     *
     * @param view
     *   The CycleableView that should be controlled.
     */
    setView(view: CycleableView): void;
    /**
     * Set the desired interval between each step.
     *
     * @param value
     *   The desired interval between each step in milliseconds.
     */
    setInterval(value: number): void;
    /**
     * Start the AutoPlay instance.
     */
    play(): void;
    /**
     * Stop the AutoPlay instance.
     */
    stop(): void;
    /**
     * Start the timeout.
     */
    private startInterval();
    /**
     * Stop the current timeout.
     */
    private stopInterval();
    /**
     * Triggered after the current index of the view has changed.
     */
    onIndexChanged(): void;
}
