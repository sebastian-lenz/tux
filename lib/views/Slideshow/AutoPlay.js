import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import Events from '../../Events';
/**
 * Advances a CycleableView view within a regular interval.
 */
var AutoPlay = (function (_super) {
    tslib_1.__extends(AutoPlay, _super);
    /**
     * AutoPlay constructor.
     *
     * @param options
     *   The constructor options.
     */
    function AutoPlay(options) {
        var _this = _super.call(this) || this;
        /**
         * The desired interval between each step in milliseconds.
         */
        _this.interval = 10000;
        /**
         * Whether this instance is currently active or not.
         */
        _this.isPlaying = false;
        /**
         * The current interval handle.
         */
        _this.handle = Number.NaN;
        options = defaults(options || {}, {
            interval: 5000,
            autoPlay: true
        });
        _this.interval = options.interval;
        if (options.view)
            _this.setView(options.view);
        if (options.autoPlay)
            _this.play();
        return _this;
    }
    /**
     * Set the CycleableView that should be controlled.
     *
     * @param view
     *   The CycleableView that should be controlled.
     */
    AutoPlay.prototype.setView = function (view) {
        if (this.view == view)
            return;
        if (this.view) {
            this.stopListening(this.view);
        }
        if (view) {
            this.listenTo(view, 'currentChanged', this.onIndexChanged);
        }
        this.view = view;
    };
    /**
     * Set the desired interval between each step.
     *
     * @param value
     *   The desired interval between each step in milliseconds.
     */
    AutoPlay.prototype.setInterval = function (value) {
        if (this.interval == value)
            return;
        this.interval = value;
        if (this.isPlaying) {
            this.stopInterval();
            this.startInterval();
        }
    };
    /**
     * Start the AutoPlay instance.
     */
    AutoPlay.prototype.play = function () {
        if (this.isPlaying)
            return;
        this.isPlaying = true;
        this.startInterval();
    };
    /**
     * Stop the AutoPlay instance.
     */
    AutoPlay.prototype.stop = function () {
        if (!this.isPlaying)
            return;
        this.isPlaying = false;
        this.stopInterval();
    };
    /**
     * Start the timeout.
     */
    AutoPlay.prototype.startInterval = function () {
        var _this = this;
        if (!isNaN(this.handle))
            return;
        this.handle = setInterval(function () {
            var view = _this.view;
            if (!view)
                return;
            if (view.hasOwnProperty('inViewport') && !view.inViewport)
                return;
            clearInterval(_this.handle);
            _this.handle = Number.NaN;
            var index = view.getCurrentIndex() + 1;
            if (index >= view.getLength()) {
                index = 0;
            }
            view.setCurrentIndex(index);
        }, this.interval);
    };
    /**
     * Stop the current timeout.
     */
    AutoPlay.prototype.stopInterval = function () {
        if (isNaN(this.handle))
            return;
        clearInterval(this.handle);
        this.handle = Number.NaN;
    };
    /**
     * Triggered after the current index of the view has changed.
     */
    AutoPlay.prototype.onIndexChanged = function () {
        this.stopInterval();
        if (this.isPlaying) {
            this.startInterval();
        }
    };
    return AutoPlay;
}(Events));
export { AutoPlay };
