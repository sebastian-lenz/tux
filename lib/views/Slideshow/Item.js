import * as tslib_1 from "tslib";
import * as Q from 'q';
import View from '../View';
import ImageWrap, { ScaleMode } from '../ImageWrap';
/**
 * Defines all states a slide can be in.
 */
export var ItemState;
(function (ItemState) {
    ItemState[ItemState["Inactive"] = 0] = "Inactive";
    ItemState[ItemState["Active"] = 1] = "Active";
    ItemState[ItemState["BecomeActive"] = 2] = "BecomeActive";
    ItemState[ItemState["BecomeInactive"] = 4] = "BecomeInactive";
    ItemState[ItemState["PeakNext"] = 8] = "PeakNext";
    ItemState[ItemState["PeakPrevious"] = 16] = "PeakPrevious";
    ItemState[ItemState["ActiveOrBecomeActive"] = 3] = "ActiveOrBecomeActive";
    ItemState[ItemState["InactiveOrBecomeInactive"] = 4] = "InactiveOrBecomeInactive";
})(ItemState || (ItemState = {}));
/**
 * An implementation of a simple slide.
 */
var DefaultItem = (function (_super) {
    tslib_1.__extends(DefaultItem, _super);
    /**
     * Initialize this slide.
     */
    function DefaultItem(options) {
        var _this = _super.call(this, options) || this;
        /**
         * The current state of this slide.
         */
        _this.state = ItemState.Inactive;
        var _a = options.imageSelector, imageSelector = _a === void 0 ? '.tuxImageWrap' : _a, _b = options.imageOptions, imageOptions = _b === void 0 ? {} : _b;
        var image = _this.element.querySelector(imageSelector);
        if (image) {
            _this.imageWrap = new ImageWrap(tslib_1.__assign({ element: image, disableVisibilityCheck: true, scaleMode: ScaleMode.Cover }, imageOptions));
        }
        return _this;
    }
    /**
     * Remove this slide.
     *
     * @returns {SlideshowItem}
     */
    DefaultItem.prototype.remove = function () {
        if (this.imageWrap) {
            this.imageWrap.remove();
        }
        return _super.prototype.remove.call(this);
    };
    /**
     * Preload this slide.
     *
     * @returns
     *   A promise that resolves after the slide has loaded.
     */
    DefaultItem.prototype.load = function () {
        if (this.imageWrap) {
            return this.imageWrap.load();
        }
        var deferred = Q.defer();
        deferred.resolve();
        return deferred.promise;
    };
    /**
     * Set the state of this slide.
     *
     * @param state
     *   The new state of this slide.
     */
    DefaultItem.prototype.setState = function (state) {
        if (this.state == state)
            return;
        this.state = state;
        var classList = this.element.classList;
        classList.toggle('current', !!(state & ItemState.ActiveOrBecomeActive));
        classList.toggle('transitionOrigin', !!(state & ItemState.BecomeInactive));
        classList.toggle('transitionTarget', !!(state & ItemState.BecomeActive));
        classList.toggle('next', !!(state & ItemState.PeakNext));
        classList.toggle('previous', !!(state & ItemState.PeakPrevious));
        if (state != ItemState.Inactive && this.imageWrap) {
            this.imageWrap.update();
        }
    };
    DefaultItem.prototype.handleViewportResize = function (resizeChildren) {
        var _a = this, imageWrap = _a.imageWrap, state = _a.state;
        if (imageWrap && state !== ItemState.Inactive) {
            imageWrap.update();
        }
        return true;
    };
    return DefaultItem;
}(View));
export default DefaultItem;
