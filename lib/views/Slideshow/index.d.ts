/// <reference types="q" />
import * as Q from 'q';
import CycleableView, { CycleableViewOptions } from '../CycleableView';
import { Item } from './Item';
import { VisibilityTarget } from '../../services/Visibility';
/**
 * Transition options for the Slideshow class.
 */
export interface TransitionOptions {
    /**
     * Whether the transition should be omitted or not.
     */
    noTransition?: boolean;
    transitionClass?: string;
    direction?: string;
}
/**
 * Constructor options for the Slideshow class.
 */
export interface SlideshowOptions<T extends Item = Item> extends CycleableViewOptions<T> {
    /**
     * Whether sibblings should be preloaded or not.
     */
    allowPreload?: boolean;
    /**
     * The name of the css class that should be used to generate the transition.
     */
    transitionClass?: string;
    /**
     * Whether the slideshow should use the Visibility service to determine
     * if it is currently visible or not.
     */
    disableVisibilityCheck?: boolean;
}
/**
 * A basic slideshow implementation.
 */
export default class Slideshow<T extends Item = Item> extends CycleableView<T, TransitionOptions> implements VisibilityTarget {
    /**
     * Whether sibblings should be preloaded or not.
     */
    allowPreload: boolean;
    /**
     * Deferred object of the current transition.
     */
    transition: Q.Promise<any>;
    /**
     * The slide that should be transitioned to after the current
     * transition has finished.
     */
    successor: T;
    successorOptions: TransitionOptions;
    /**
     * Whether this element is currently within the visible viewport bounds or not.
     */
    inViewport: boolean;
    /**
     * Slideshow constructor.
     *
     * @param options
     *   The constructor options.
     */
    constructor(options?: SlideshowOptions<T>);
    /**
     * Set whether the element of this view is currently within the visible bounds
     * of the viewport or not.
     *
     * @param inViewport
     *   TRUE if the element is visible, FALSE otherwise.
     */
    setInViewport(inViewport: boolean): void;
    /**
     * Return a string indicating the transition direction between the two
     * given slides.
     *
     * @param from
     *   The child the transition should start with.
     * @param to
     *   The child the transition should end with.
     * @returns
     *   A string indicating the transition direction ('forward' or 'backward').
     */
    getDirection(from: T, to: T): string;
    /**
     * Return whether the slideshow is currently within a transition or not.
     *
     * @returns
     *   TRUE if the slideshow is currently within a transition, FALSE otherwise.
     */
    isInTransition(): boolean;
    /**
     * Preload the content of the previous and next slide.
     */
    preloadSiblings(): void;
    /**
     * Create the container children will be placed in.
     *
     * @returns
     *   The generated container.
     */
    protected createContainer(): HTMLElement;
    /**
     * Internal transition handler. Creates a transition between the two given children.
     *
     * @param newChild
     *   The child the transition should lead to.
     * @param oldChild
     *   The child the transition should come from.
     * @param options
     *   Optional transition options.
     */
    protected handleTransition(newChild: T, oldChild: T, options?: TransitionOptions): void;
    /**
     * Create the transition sequence between the two given slides.
     *
     * @param from
     *   The child the transition should start with.
     * @param to
     *   The child the transition should end with.
     * @returns
     *   A promise that resolves after the transition has finished.
     */
    protected createSequence(from: T, to: T, options: TransitionOptions): Q.Promise<any>;
    /**
     * Create a graphical transition between the two given slides.
     *
     * @param from
     *   The child the transition should start with.
     * @param to
     *   The child the transition should end with.
     * @param transitionClass
     *   The name of the css class that should be used to generate the transition.
     * @returns
     *   A promise that resolves after the transition has finished.
     */
    protected createTransition(from: T, to: T, options: TransitionOptions): Q.Promise<any>;
}
