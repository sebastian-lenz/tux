import View, { ViewOptions } from '../View';
import Page from './Page';
import Menu from './Menu';
export interface ItemOptions extends ViewOptions {
    disabled?: boolean;
    owner: Menu;
    page: Page;
}
export default class Item extends View {
    isCurrent: boolean;
    menu: Menu;
    page: Page;
    constructor(options?: ItemOptions);
    setCurrent(value: boolean): void;
    onClick(): boolean;
}
