import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import View from '../View';
var Item = (function (_super) {
    tslib_1.__extends(Item, _super);
    function Item(options) {
        var _this = _super.call(this, defaults(options || {}, {
            tagName: 'li',
            className: 'tuxTabsItem'
        })) || this;
        _this.isCurrent = false;
        _this.menu = options.owner;
        _this.page = options.page;
        _this.element.innerHTML = _this.page.title;
        if (!options.disabled) {
            _this.delegateClick(_this.onClick);
        }
        return _this;
    }
    Item.prototype.setCurrent = function (value) {
        if (this.isCurrent == value)
            return;
        this.isCurrent = value;
        this.toggleClass('current', value);
    };
    Item.prototype.onClick = function () {
        this.menu.tabs.setCurrent(this.page);
        return true;
    };
    return Item;
}(View));
export default Item;
