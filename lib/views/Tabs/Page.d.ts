import View, { ViewOptions } from '../View';
export interface TabsPageOptions extends ViewOptions {
    title?: string;
}
export default class Page extends View {
    components: View[];
    title: string;
    constructor(options?: TabsPageOptions);
    setCurrent(value: boolean): void;
}
