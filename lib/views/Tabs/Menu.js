import * as tslib_1 from "tslib";
import { defaults, find, forEach } from 'underscore';
import CycleableView from '../CycleableView';
import Item from './Item';
var Menu = (function (_super) {
    tslib_1.__extends(Menu, _super);
    function Menu(options) {
        var _this = _super.call(this, defaults(options || {}, {
            tagName: 'ol',
            className: 'tuxTabsMenu',
            childClass: Item,
        })) || this;
        var childClass = options.childClass, owner = options.owner;
        var children = owner.children, container = owner.container, current = owner.current;
        _this.tabs = owner;
        forEach(children, function (page) {
            var options = _this.alterChildOptions({
                appendTo: container,
                owner: _this,
                page: page,
            });
            _this.addChild(new childClass(options));
        });
        return _this;
    }
    Menu.prototype.setCurrentPage = function (page) {
        var children = this.children;
        this.setCurrent(find(children, function (child) { return child.page === page; }));
    };
    Menu.prototype.handleTransition = function (newChild, oldChild) {
        if (newChild)
            newChild.setCurrent(true);
        if (oldChild)
            oldChild.setCurrent(false);
    };
    return Menu;
}(CycleableView));
export default Menu;
