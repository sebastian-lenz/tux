import AnimationSwap, { TransitionOptions } from '../AnimationSwap';
import CycleableView, { CycleableViewOptions } from '../CycleableView';
import Item from './Item';
import Menu, { MenuOptions } from './Menu';
import Page from './Page';
export { Item, Menu, Page };
export interface TabsOptions<T extends Page = Page> extends CycleableViewOptions<T> {
    menuClass?: {
        new (options: MenuOptions): Menu;
    };
    menuOptions?: MenuOptions;
}
export default class Tabs<T extends Page = Page> extends CycleableView<T, TransitionOptions> {
    defaultTransitionOptions: TransitionOptions;
    menu: Menu;
    swap: AnimationSwap<Page>;
    constructor(options?: TabsOptions);
    protected createMenu(options: TabsOptions): Menu;
    protected createSwap(): AnimationSwap<Page>;
    /**
     * Internal transition handler. Creates a transition between the two given children.
     *
     * @param newChild
     *   The child the transition should lead to.
     * @param oldChild
     *   The child the transition should come from.
     * @param options
     *   Optional transition options.
     */
    protected handleTransition(newChild: Page, oldChild: Page, options?: TransitionOptions): void;
    onSwap(newContent: Page, oldContent: Page): void;
    handleViewportResize(): boolean;
}
