import * as tslib_1 from "tslib";
import View, { Registry } from '../View';
var Page = (function (_super) {
    tslib_1.__extends(Page, _super);
    function Page(options) {
        var _this = _super.call(this, options) || this;
        if (options.title) {
            _this.title = options.title;
        }
        else if (options.element) {
            var previous = options.element.previousSibling;
            while (previous && previous.nodeType == 3) {
                previous = previous.previousSibling;
            }
            if (previous && previous.nodeName == 'DT') {
                _this.title = previous.innerHTML;
            }
        }
        return _this;
    }
    Page.prototype.setCurrent = function (value) {
        this.toggleClass('current', value);
        if (!value)
            return;
        var _a = this, components = _a.components, element = _a.element;
        if (!components) {
            this.components = Registry.getInstance().create(element);
        }
        else {
            for (var _i = 0, components_1 = components; _i < components_1.length; _i++) {
                var component = components_1[_i];
                component.handleViewportResize();
            }
        }
    };
    return Page;
}(View));
export default Page;
