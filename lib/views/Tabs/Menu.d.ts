import CycleableView, { CycleableViewOptions } from '../CycleableView';
import Item from './Item';
import Page from './Page';
import Tabs from './index';
export interface MenuOptions extends CycleableViewOptions<Item> {
    owner: Tabs;
}
export default class Menu extends CycleableView<Item> {
    tabs: Tabs;
    constructor(options?: MenuOptions);
    setCurrentPage(page: Page): void;
    protected handleTransition(newChild: Item, oldChild: Item): void;
}
