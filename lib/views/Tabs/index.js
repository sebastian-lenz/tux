import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import AnimationSwap from '../AnimationSwap';
import CycleableView from '../CycleableView';
import Item from './Item';
import Menu from './Menu';
import Page from './Page';
export { Item, Menu, Page };
var Tabs = (function (_super) {
    tslib_1.__extends(Tabs, _super);
    function Tabs(options) {
        var _this = _super.call(this, defaults(options || {}, {
            autoSelectFirstChild: true,
            childSelector: '.tuxTabsPage',
            childClass: Page,
            container: '.tuxTabs__container',
            menuClass: Menu,
            menuOptions: {},
        })) || this;
        _this.defaultTransitionOptions = {
            className: 'transition',
            withHeightTransition: true,
        };
        _this.menu = _this.createMenu(options);
        _this.swap = _this.createSwap();
        _this.menu.setCurrentPage(_this.current);
        return _this;
    }
    Tabs.prototype.createMenu = function (options) {
        var _a = this, children = _a.children, current = _a.current, element = _a.element;
        var menuClass = options.menuClass, menuOptions = options.menuOptions;
        var menu = new menuClass(defaults(menuOptions, {
            owner: this,
        }));
        element.insertBefore(menu.element, element.firstChild);
        return menu;
    };
    Tabs.prototype.createSwap = function () {
        var _a = this, container = _a.container, current = _a.current;
        var swap = new AnimationSwap({ element: container });
        this.listenTo(swap, 'swap', this.onSwap);
        if (current) {
            current.setCurrent(true);
            swap.setContent(current, { className: null });
        }
        return swap;
    };
    /**
     * Internal transition handler. Creates a transition between the two given children.
     *
     * @param newChild
     *   The child the transition should lead to.
     * @param oldChild
     *   The child the transition should come from.
     * @param options
     *   Optional transition options.
     */
    Tabs.prototype.handleTransition = function (newChild, oldChild, options) {
        var _a = this, menu = _a.menu, swap = _a.swap;
        if (swap) {
            options = defaults(options || {}, this.defaultTransitionOptions);
            swap.setContent(newChild, options);
        }
        if (menu) {
            menu.setCurrentPage(newChild);
        }
    };
    Tabs.prototype.onSwap = function (newContent, oldContent) {
        if (newContent)
            newContent.setCurrent(true);
        if (oldContent)
            oldContent.setCurrent(false);
    };
    Tabs.prototype.handleViewportResize = function () {
        var _a = this, menu = _a.menu, swap = _a.swap;
        menu.handleViewportResize();
        swap.handleViewportResize();
        return true;
    };
    return Tabs;
}(CycleableView));
export default Tabs;
