import ChildableView from '../ChildableView';
import Item from './Item';
/**
 * Constructor options for the Accordion class.
 */
export interface AccordionOptions {
    /**
     * Whether only one child can be expanded at any time or not.
     */
    isSingleSelect?: boolean;
}
/**
 * Display a list of expandable elements.
 */
export default class Accordion<T extends Item = Item> extends ChildableView<T> {
    /**
     * Whether only one child can be expanded at any time or not.
     */
    protected isSingleSelect: boolean;
    /**
     * Create a new Accordion instance.
     */
    constructor(options?: AccordionOptions);
    /**
     * Triggered after an item has changed the expanded state.
     */
    onChildChanged(item: T, isExpanded: boolean): void;
}
