import * as tslib_1 from "tslib";
import heightTransition from '../../fx/transition/heightTransition';
import View, { option, Registry } from '../View';
import Viewport from '../../services/Viewport';
/**
 * Display a single item within an accordion.
 *
 * As accordions are definition lists theis view represents the
 * title of the item wrapped inside <dt>. The actual content is
 * the sibling <dd>.
 *
 * @event "changed" (item:AccordionItem, isExpanded:boolean):void
 *   Triggered after the expanded state of this item has changed.
 */
var Item = (function (_super) {
    tslib_1.__extends(Item, _super);
    /**
     * Create a new AccordionItem instance.
     */
    function Item(options) {
        var _this = _super.call(this, options) || this;
        /**
         * Is this item currently expanded?
         */
        _this.isExpanded = false;
        _this.delegateClick(_this.onClick, { selector: 'dt' });
        return _this;
    }
    /**
     * Set whether this item is expanded or not.
     */
    Item.prototype.setExpanded = function (value) {
        var _this = this;
        if (this.isExpanded == value)
            return;
        this.isExpanded = value;
        heightTransition(this.element, function () {
            _this.toggleClass('expanded', value);
            if (value && _this.components === void 0) {
                _this.components = Registry.getInstance().create(_this.element);
            }
        }).then(function () {
            Viewport.getInstance().triggerResize();
        });
        this.trigger('changed', this, value);
    };
    /**
     * Triggered when the user clicks onto the item header.
     */
    Item.prototype.onClick = function () {
        this.setExpanded(!this.isExpanded);
        return true;
    };
    return Item;
}(View));
export default Item;
tslib_1.__decorate([
    option({ type: 'element', defaultValue: 'dd' })
], Item.prototype, "body", void 0);
