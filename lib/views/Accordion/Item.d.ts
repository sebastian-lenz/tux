import View, { ViewOptions } from '../View';
/**
 * Constructor options for the AccordionItem class.
 */
export interface ItemOptions extends ViewOptions {
}
/**
 * Display a single item within an accordion.
 *
 * As accordions are definition lists theis view represents the
 * title of the item wrapped inside <dt>. The actual content is
 * the sibling <dd>.
 *
 * @event "changed" (item:AccordionItem, isExpanded:boolean):void
 *   Triggered after the expanded state of this item has changed.
 */
export default class Item extends View {
    /**
     * The content of this item.
     */
    body: HTMLElement;
    /**
     * The child components of the body.
     */
    components: View[];
    /**
     * Is this item currently expanded?
     */
    isExpanded: boolean;
    /**
     * Create a new AccordionItem instance.
     */
    constructor(options?: ItemOptions);
    /**
     * Set whether this item is expanded or not.
     */
    setExpanded(value: boolean): void;
    /**
     * Triggered when the user clicks onto the item header.
     */
    onClick(): boolean;
}
