import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import { option } from '../View';
import ChildableView from '../ChildableView';
import Item from './Item';
/**
 * Display a list of expandable elements.
 */
var Accordion = (function (_super) {
    tslib_1.__extends(Accordion, _super);
    /**
     * Create a new Accordion instance.
     */
    function Accordion(options) {
        var _this = _super.call(this, defaults(options || {}, {
            childSelector: 'dl',
            childClass: Item,
        })) || this;
        if (_this.isSingleSelect) {
            for (var _i = 0, _a = _this.children; _i < _a.length; _i++) {
                var child = _a[_i];
                _this.listenTo(child, 'changed', _this.onChildChanged);
            }
        }
        return _this;
    }
    /**
     * Triggered after an item has changed the expanded state.
     */
    Accordion.prototype.onChildChanged = function (item, isExpanded) {
        if (!isExpanded) {
            return;
        }
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            if (child !== item) {
                child.setExpanded(false);
            }
        }
    };
    return Accordion;
}(ChildableView));
export default Accordion;
tslib_1.__decorate([
    option({ type: 'bool' })
], Accordion.prototype, "isSingleSelect", void 0);
