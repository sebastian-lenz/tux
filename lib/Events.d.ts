export declare type EventCallback = Function & {
    _callback?: Function;
};
export declare type EventListener = {
    obj: Events;
    objId: string;
    id: string;
    listeningTo: EventListenerMap;
    count: number;
};
export declare type EventListenerMap = {
    [id: string]: EventListener;
};
export declare type EventHandler = {
    callback: EventCallback;
    context: any;
    ctx: any;
    listening: EventListener | null;
    priority: number;
};
export declare type EventHandlerMap = {
    [name: string]: EventHandler[];
};
/**
 * An event dispatcher.
 *
 * Based on Backbone.Events
 * http://backbonejs.org/backbone.js
 */
export default class Events {
    private _events;
    private _listenId;
    private _listeners;
    private _listeningTo;
    /**
     * Bind an event to a `callback` function. Passing `"all"` will bind
     * the callback to all events fired.
     */
    on(name: string, callback: EventCallback, context?: any, priority?: number): this;
    once(name: string, callback: EventCallback, context?: any, priority?: number): this;
    /**
     * Remove one or many callbacks. If `context` is null, removes all
     * callbacks with that function. If `callback` is null, removes all
     * callbacks for the event. If `name` is null, removes all bound
     * callbacks for all events.
     */
    off(name?: string, callback?: EventCallback, context?: any): this;
    /**
     * Inversion-of-control versions of `on`. Tell *this* object to listen to
     * an event in another object... keeping track of what it's listening to
     * for easier unbinding later.
     */
    listenTo(obj: Events, name: string, callback: EventCallback, priority?: number): this;
    /**
     * Inversion-of-control versions of `once`.
     */
    listenToOnce(obj: Events, name: string, callback: EventCallback, priority?: number): this;
    /**
     * Tell this object to stop listening to either specific events ... or
     * to every object it's currently listening to.
     */
    stopListening(obj?: Events, name?: string, callback?: EventCallback): this;
    /**
     * Trigger one or many events, firing all bound callbacks. Callbacks are
     * passed the same arguments as `trigger` is, apart from the event name
     * (unless you're listening on `"all"`, which will cause your callback to
     * receive the true name of the event as the first argument).
     */
    trigger(name: string, ...args: any[]): this;
    /**
     * Guard the `listening` argument from the public API.
     */
    private addHandler(name, callback, context?, listening?, priority?);
    /**
     * Reduces the event callbacks into a map of `{event: onceWrapper}`.
     * `offer` unbinds the `onceWrapper` after it has been called.
     */
    private createOnceHandler(callback, offCallback);
}
