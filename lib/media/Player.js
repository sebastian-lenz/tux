export var PlayState;
(function (PlayState) {
    PlayState[PlayState["Empty"] = 0] = "Empty";
    PlayState[PlayState["Unstarted"] = 1] = "Unstarted";
    PlayState[PlayState["Buffering"] = 2] = "Buffering";
    PlayState[PlayState["Ended"] = 3] = "Ended";
    PlayState[PlayState["Paused"] = 4] = "Paused";
    PlayState[PlayState["Playing"] = 5] = "Playing";
    PlayState[PlayState["Error"] = 6] = "Error";
})(PlayState || (PlayState = {}));
