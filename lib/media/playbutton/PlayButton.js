import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import View from '../../views/View';
import { PlayState } from '../Player';
/**
 * Display a play/pause button.
 */
var PlayButton = (function (_super) {
    tslib_1.__extends(PlayButton, _super);
    /**
     * PlayButton constructor.
     *
     * @param options
     *   PlayButton constructor options.
     */
    function PlayButton(options) {
        var _this = _super.call(this, defaults(options || {}, {
            className: 'tuxPlayButton'
        })) || this;
        _this.delegateClick(_this.onClick);
        if (options.player) {
            _this.setPlayer(options.player);
        }
        return _this;
    }
    /**
     * Set the player this seek bar should be attached to.
     *
     * @param player
     *   The player this seekbar should be attached to.
     */
    PlayButton.prototype.setPlayer = function (player) {
        if (this.player == player)
            return;
        if (this.player) {
            this.stopListening(this.player);
        }
        this.player = player;
        if (player) {
            this.listenTo(player, 'state', this.onStateChanged);
            this.onStateChanged(player.getPlayState());
        }
    };
    /**
     * Triggered after the play state has changed.
     */
    PlayButton.prototype.onStateChanged = function (state) {
        if (this.playStateClass) {
            this.element.classList.remove(this.playStateClass);
        }
        this.playStateClass = PlayState[state].toLowerCase();
        this.element.classList.add(this.playStateClass);
    };
    /**
     * Triggered after the user has clicked onto this button.
     */
    PlayButton.prototype.onClick = function () {
        var player = this.player;
        if (player) {
            if (!player.getTrack()) {
                this.trigger('playEmpty');
                return true;
            }
            var state = player.getPlayState();
            if (state == PlayState.Playing) {
                this.player.pause();
            }
            else {
                if (state == PlayState.Ended) {
                    player.setCurrentTime(0);
                }
                this.player.play();
            }
        }
        return true;
    };
    return PlayButton;
}(View));
export { PlayButton };
