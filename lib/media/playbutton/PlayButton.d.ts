import View, { ViewOptions } from '../../views/View';
import { IPlayer, PlayState } from '../Player';
/**
 * PlayButton constructor options.
 */
export interface IPlayButtonOptions extends ViewOptions {
    /**
     * The player the play button should be attached to.
     */
    player: IPlayer;
}
/**
 * Display a play/pause button.
 */
export declare class PlayButton extends View {
    /**
     * The player this play button is attached to.
     */
    player: IPlayer;
    /**
     * The last applied play state css class.
     */
    playStateClass: string;
    /**
     * PlayButton constructor.
     *
     * @param options
     *   PlayButton constructor options.
     */
    constructor(options?: IPlayButtonOptions);
    /**
     * Set the player this seek bar should be attached to.
     *
     * @param player
     *   The player this seekbar should be attached to.
     */
    setPlayer(player: IPlayer): void;
    /**
     * Triggered after the play state has changed.
     */
    onStateChanged(state: PlayState): void;
    /**
     * Triggered after the user has clicked onto this button.
     */
    onClick(): boolean;
}
