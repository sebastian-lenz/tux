import Events from '../Events';
import { Track, TrackList } from './Track';
export declare enum PlayState {
    Empty = 0,
    Unstarted = 1,
    Buffering = 2,
    Ended = 3,
    Paused = 4,
    Playing = 5,
    Error = 6,
}
/**
 * Basic interface all players must implement.
 */
export interface IPlayer extends Events {
    /**
     * Start this player.
     */
    play(): any;
    /**
     * Pause this player.
     */
    pause(): any;
    /**
     * Pause and rewind this player.
     */
    stop(): any;
    /**
     * Return the total duration of the media file.
     */
    getDuration(): number;
    /**
     * Return the current play state of this player.
     */
    getPlayState(): PlayState;
    /**
     * Return the current play head position.
     */
    getCurrentTime(): number;
    /**
     * Set the current play head position.
     */
    setCurrentTime(value: number): any;
    /**
     * Return the time ranges of the buffered media file parts.
     */
    getBuffer(): TimeRanges;
    getTrack(): Track;
    setTrack(track: Track): this;
    setTrackList(trackList: TrackList): this;
    getTrackList(): TrackList;
}
