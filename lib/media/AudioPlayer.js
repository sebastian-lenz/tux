import * as tslib_1 from "tslib";
import View from '../views/View';
import { PlayState } from './Player';
/**
 * A basic audio player.
 *
 * abort	Sent when playback is aborted; for example, if the media is playing and is restarted from the beginning, this event is sent.
 * emptied	The media has become empty; for example, this event is sent if the media has already been loaded (or partially loaded), and the load() method is called to reload it.
 * canplay	Sent when enough data is available that the media can be played, at least for a couple of frames.  This corresponds to the HAVE_ENOUGH_DATA readyState.
 * canplaythrough	Sent when the ready state changes to CAN_PLAY_THROUGH, indicating that the entire media can be played without interruption, assuming the download rate remains at least at the current level. It will also be fired when playback is toggled between paused and playing. Note: Manually setting the currentTime will eventually fire a canplaythrough event in firefox. Other browsers might not fire this event.
 * loadeddata	The first frame of the media has finished loading.
 * loadedmetadata	The media's metadata has finished loading; all attributes now contain as much useful information as they're going to.
 * seeked	Sent when a seek operation completes.
 * seeking	Sent when a seek operation begins.
 * stalled	Sent when the user agent is trying to fetch media data, but data is unexpectedly not forthcoming.
 * suspend	Sent when loading of the media is suspended; this may happen either because the download has completed or because it has been paused for any other reason.
 * waiting   Sent when the requested operation (such as playback) is delayed pending the completion of another operation (such as a seek).
 *
 * @event "track" (track:Track):void
 *   Triggered after the track has changed.
 * @event "state" (state:PlayState):void
 *   Triggered after the play state has changed.
 * @event "currentTime" (currentTime:number):void
 *   Triggered after the position of the play head has moved.
 * @event "duration" (duration:number):void
 *   Triggered after the duration of the media has changed.
 * @event "volume" (volume:number):void
 *   Triggered after the volume has changed.
 * @event "buffer" (buffer:TimeRanges):void
 *   Triggered after the buffer state has changed.
 */
var AudioPlayer = (function (_super) {
    tslib_1.__extends(AudioPlayer, _super);
    /**
     * AudioPlayer constructor.
     */
    function AudioPlayer(options) {
        var _this = _super.call(this, {
            tagName: 'audio',
            className: 'tuxAudioPlayer'
        }) || this;
        /**
         * The current play state of this player.
         */
        _this.state = PlayState.Empty;
        /**
         * The position of the playhead.
         */
        _this.currentTime = 0;
        _this.duration = 0;
        _this.volume = 0;
        /**
         * Triggered by one of the events indicating the media element has entered a new state.
         *
         * @param event
         */
        _this.onStateChanged = function (event) {
            var state = _this.state;
            switch (event.type) {
                // Sent when loading of the media begins.
                case 'loadstart':
                    state = PlayState.Buffering;
                    break;
                // Sent when playback of the media starts after having been paused; that is, when playback is resumed after a prior pause event.
                case 'play':
                // Sent when the media begins to play (either for the first time, after having been paused, or after ending and then restarting).
                case 'playing':
                    state = PlayState.Playing;
                    break;
                // Sent when playback completes.
                case 'ended':
                    state = PlayState.Ended;
                    break;
                // Sent when playback is paused.
                case 'pause':
                    state = PlayState.Paused;
                    break;
                // Sent when an error occurs.  The element's error attribute contains more information. See Error handling for details.
                case 'error':
                    state = PlayState.Error;
                    break;
            }
            if (_this.state != state) {
                _this.state = state;
                _this.trigger('state', state);
            }
            if (state == PlayState.Ended && _this.trackList) {
                _this.next();
            }
        };
        /**
         * The time indicated by the element's currentTime attribute has changed.
         *
         * @param event
         */
        _this.onCurrentTimeChange = function (event) {
            var currentTime = _this.element.currentTime;
            if (_this.currentTime == currentTime)
                return;
            _this.currentTime = currentTime;
            _this.trigger('currentTime', currentTime);
        };
        /**
         * The metadata has loaded or changed, indicating a change in duration of the media.
         *
         * This is sent, for example, when the media has loaded enough that the duration is known.
         *
         * @param event
         */
        _this.onDurationChange = function (event) {
            var duration = _this.element.duration;
            if (_this.duration == duration)
                return;
            _this.duration = duration;
            _this.track.setDuration(duration);
            _this.trigger('duration', duration);
        };
        /**
         * Sent when the audio volume changes (both when the volume is set and when the muted attribute is changed).
         *
         * @param event
         */
        _this.onVolumeChange = function (event) {
            var volume = _this.element.volume;
            if (_this.volume == volume)
                return;
            _this.volume = volume;
            _this.trigger('volume', volume);
        };
        /**
         * Sent periodically to inform interested parties of progress downloading the media.
         *
         * Information about the current amount of the media that has been downloaded is available
         * in the media element's buffered attribute.
         *
         * @param event
         */
        _this.onBufferChange = function (event) {
            _this.trigger('buffer', _this.element.buffered);
        };
        _this.volume = _this.element.volume;
        _this.delegate('play', null, _this.onStateChanged);
        _this.delegate('playing', null, _this.onStateChanged);
        _this.delegate('ended', null, _this.onStateChanged);
        _this.delegate('pause', null, _this.onStateChanged);
        _this.delegate('error', null, _this.onStateChanged);
        _this.delegate('timeupdate', null, _this.onCurrentTimeChange);
        _this.delegate('durationchange', null, _this.onDurationChange);
        _this.delegate('volumechange', null, _this.onVolumeChange);
        _this.delegate('progress', null, _this.onBufferChange);
        return _this;
    }
    /**
     * Start this player.
     */
    AudioPlayer.prototype.play = function () {
        if (this.track) {
            this.element.play();
        }
    };
    /**
     * Pause this player.
     */
    AudioPlayer.prototype.pause = function () {
        if (this.track) {
            this.element.pause();
        }
    };
    /**
     * Pause and rewind this player.
     */
    AudioPlayer.prototype.stop = function () {
        if (this.track) {
            this.element.pause();
            this.setCurrentTime(0);
        }
    };
    AudioPlayer.prototype.previous = function () {
        this.skip(-1);
    };
    AudioPlayer.prototype.next = function () {
        this.skip(1);
    };
    AudioPlayer.prototype.skip = function (direction) {
        var trackList = this.trackList;
        if (!trackList)
            return;
        var index = trackList.indexOf(this.track);
        if (index == -1)
            return;
        index += direction;
        if (index < 0 || index >= trackList.length)
            return;
        this.setTrack(this.trackList.at(index));
    };
    /**
     * Return the current play state of this player.
     */
    AudioPlayer.prototype.getPlayState = function () {
        return this.state;
    };
    /**
     * Return the total duration of the media file.
     */
    AudioPlayer.prototype.getDuration = function () {
        return this.duration;
    };
    /**
     * Return the current play head position.
     */
    AudioPlayer.prototype.getCurrentTime = function () {
        return this.currentTime;
    };
    /**
     * Return the time ranges of the buffered media file parts.
     */
    AudioPlayer.prototype.getBuffer = function () {
        return this.element.buffered;
    };
    AudioPlayer.prototype.setCurrentTime = function (value) {
        this.element.currentTime = value;
    };
    AudioPlayer.prototype.getVolume = function () {
        return this.volume;
    };
    AudioPlayer.prototype.setVolume = function (value) {
        this.element.volume = value;
    };
    AudioPlayer.prototype.getTrack = function () {
        return this.track;
    };
    AudioPlayer.prototype.setTrack = function (track) {
        if (this.track == track)
            return this;
        if (this.trackList && this.trackList.indexOf(track) == -1) {
            this.setTrackList(null);
        }
        this.track = track;
        if (track) {
            this.element.src = track.getSource();
            this.duration = track.getDuration();
            this.state = PlayState.Unstarted;
        }
        else {
            this.element.src = '';
            this.duration = 0;
            this.state = PlayState.Empty;
        }
        this.currentTime = 0;
        this.trigger('currentTime', 0);
        this.trigger('state', this.state);
        this.trigger('duration', this.duration);
        this.trigger('track', track);
        return this;
    };
    AudioPlayer.prototype.getTrackList = function () {
        return this.trackList;
    };
    AudioPlayer.prototype.setTrackList = function (trackList) {
        this.trackList = trackList;
        if (this.track && trackList.indexOf(this.track) == -1) {
            this.setTrack(null);
        }
        this.trackList = trackList;
        return this;
    };
    return AudioPlayer;
}(View));
export { AudioPlayer };
