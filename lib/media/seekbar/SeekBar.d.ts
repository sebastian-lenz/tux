import View, { ViewOptions } from '../../views/View';
import { IPlayer } from '../Player';
/**
 * Defines all available draw modes of the buffer chunks.
 */
export declare enum BufferDisplayMode {
    None = 0,
    Simple = 1,
    Chunks = 2,
}
/**
 * SekBar constructor options.
 */
export interface ISeekBarOptions extends ViewOptions {
    /**
     * The player the seek bar should be attached to.
     */
    player: IPlayer;
    /**
     * Should click dispatch an event?
     */
    emitClick?: boolean;
}
/**
 * Render a seek bar that displays the current play time of a player
 * and allows the user to seek the media file.
 */
export declare class SeekBar extends View {
    /**
     * The player this seek bar is attached to.
     */
    player: IPlayer;
    /**
     * The element displaying the current time.
     */
    currentTime: HTMLDivElement;
    /**
     * The element displaying the duration.
     */
    duration: HTMLDivElement;
    /**
     * The element displaying the progress of the media playback.
     */
    progress: HTMLDivElement;
    /**
     * The element displaying the buffer state.
     */
    buffer: HTMLDivElement;
    /**
     * A list of all displayed buffer chunk elements.
     */
    bufferChunks: HTMLDivElement[];
    /**
     * Specifies how buffer chunks should be rendered.
     */
    bufferDisplayMode: BufferDisplayMode;
    /**
     * Should click dispatch an event?
     */
    emitClick: boolean;
    /**
     * SeekBar constructor.
     *
     * @param options
     *   SekBar constructor options.
     */
    constructor(options?: ISeekBarOptions);
    /**
     * Set the player this seek bar should be attached to.
     *
     * @param player
     *   The player this seekbar should be attached to.
     */
    setPlayer(player: IPlayer): void;
    formatTime(time: number): string;
    /**
     * Update the progress bar.
     */
    updateProgress(): void;
    /**
     * Triggered after the position of the play head has moved.
     */
    onPlayerDuration(duration: number): void;
    /**
     * Triggered after the position of the play head has moved.
     */
    onPlayerCurrentTime(currentTime: number): void;
    /**
     * Triggered after the buffer state has changed.
     */
    onPlayerBuffer(buffers: TimeRanges): void;
    static formatTime(time: number): string;
}
