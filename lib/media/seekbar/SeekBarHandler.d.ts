import DragHandler from '../../handler/DragHandler';
import Pointer from '../../handler/Pointer';
import { SeekBar } from './SeekBar';
/**
 * User input handler for the SeekBar view.
 */
export declare class SeekBarHandler extends DragHandler {
    /**
     * The view the handler should delegate its events to.
     */
    view: SeekBar;
    /**
     * Did we pause the player when seeking began?
     */
    didPause: boolean;
    /**
     * SeekBarHandler constructor.
     *
     * @param view
     *   The view the handler should delegate its events to.
     */
    constructor(view: SeekBar);
    /**
     * Triggered after a drag has started.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the drag start.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    handleDragStart(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a drag has moved.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the move.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    handleDragMove(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a drag has ended.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    handleDragEnd(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the drag has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    handleDragCancel(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the user has clicked into the view.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    handleClick(event: Event, pointer: Pointer): void;
}
