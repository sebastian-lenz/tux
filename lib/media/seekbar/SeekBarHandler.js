import * as tslib_1 from "tslib";
import DragHandler, { DragDirection } from '../../handler/DragHandler';
import { PlayState } from '../Player';
/**
 * User input handler for the SeekBar view.
 */
var SeekBarHandler = (function (_super) {
    tslib_1.__extends(SeekBarHandler, _super);
    /**
     * SeekBarHandler constructor.
     *
     * @param view
     *   The view the handler should delegate its events to.
     */
    function SeekBarHandler(view) {
        var _this = _super.call(this, view) || this;
        /**
         * Did we pause the player when seeking began?
         */
        _this.didPause = false;
        _this.direction = DragDirection.Horizontal;
        return _this;
    }
    /**
     * Triggered after a drag has started.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the drag start.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    SeekBarHandler.prototype.handleDragStart = function (event, pointer) {
        var player = this.view.player;
        if (player && player.getPlayState() == PlayState.Playing) {
            player.pause();
            this.didPause = true;
        }
        else {
            this.didPause = false;
        }
        return true;
    };
    /**
     * Triggered after a drag has moved.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the move.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    SeekBarHandler.prototype.handleDragMove = function (event, pointer) {
        var seekbar = this.view;
        var player = seekbar.player;
        var rect = seekbar.element.getBoundingClientRect();
        var percent = (pointer.x - rect.left) / rect.width;
        if (player) {
            player.setCurrentTime(percent * player.getDuration());
        }
        return true;
    };
    /**
     * Triggered after a drag has ended.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    SeekBarHandler.prototype.handleDragEnd = function (event, pointer) {
        var player = this.view.player;
        if (player && this.didPause) {
            player.play();
        }
    };
    /**
     * Triggered after the drag has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    SeekBarHandler.prototype.handleDragCancel = function (event, pointer) {
        this.handleDragEnd(event, pointer);
    };
    /**
     * Triggered after the user has clicked into the view.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    SeekBarHandler.prototype.handleClick = function (event, pointer) {
        if (this.view.emitClick) {
            this.view.trigger('click', event);
        }
        else {
            this.handleDragMove(event, pointer);
        }
    };
    return SeekBarHandler;
}(DragHandler));
export { SeekBarHandler };
