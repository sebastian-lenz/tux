import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import View, { option } from '../../views/View';
import { SeekBarHandler } from './SeekBarHandler';
/**
 * Defines all available draw modes of the buffer chunks.
 */
export var BufferDisplayMode;
(function (BufferDisplayMode) {
    BufferDisplayMode[BufferDisplayMode["None"] = 0] = "None";
    BufferDisplayMode[BufferDisplayMode["Simple"] = 1] = "Simple";
    BufferDisplayMode[BufferDisplayMode["Chunks"] = 2] = "Chunks";
})(BufferDisplayMode || (BufferDisplayMode = {}));
/**
 * Render a seek bar that displays the current play time of a player
 * and allows the user to seek the media file.
 */
var SeekBar = (function (_super) {
    tslib_1.__extends(SeekBar, _super);
    /**
     * SeekBar constructor.
     *
     * @param options
     *   SekBar constructor options.
     */
    function SeekBar(options) {
        var _this = _super.call(this, defaults(options || {}, {
            className: 'tuxSeekBar'
        })) || this;
        /**
         * A list of all displayed buffer chunk elements.
         */
        _this.bufferChunks = [];
        if (options.player) {
            _this.setPlayer(options.player);
        }
        new SeekBarHandler(_this);
        return _this;
    }
    /**
     * Set the player this seek bar should be attached to.
     *
     * @param player
     *   The player this seekbar should be attached to.
     */
    SeekBar.prototype.setPlayer = function (player) {
        if (this.player) {
            this.stopListening(this.player);
        }
        this.player = player;
        if (player) {
            this.listenTo(player, 'duration', this.onPlayerDuration);
            this.listenTo(player, 'currentTime', this.onPlayerCurrentTime);
            this.listenTo(player, 'buffer', this.onPlayerBuffer);
            this.onPlayerDuration(player.getDuration());
            this.onPlayerCurrentTime(player.getCurrentTime());
            this.onPlayerBuffer(player.getBuffer());
        }
    };
    SeekBar.prototype.formatTime = function (time) {
        return SeekBar.formatTime(time);
    };
    /**
     * Update the progress bar.
     */
    SeekBar.prototype.updateProgress = function () {
        this.progress.style.width = ((this.player.getCurrentTime() / this.player.getDuration()) * 100).toFixed(3) + '%';
    };
    /**
     * Triggered after the position of the play head has moved.
     */
    SeekBar.prototype.onPlayerDuration = function (duration) {
        this.duration.innerHTML = this.formatTime(duration);
        this.updateProgress();
    };
    /**
     * Triggered after the position of the play head has moved.
     */
    SeekBar.prototype.onPlayerCurrentTime = function (currentTime) {
        this.currentTime.innerHTML = this.formatTime(currentTime);
        this.updateProgress();
    };
    /**
     * Triggered after the buffer state has changed.
     */
    SeekBar.prototype.onPlayerBuffer = function (buffers) {
        var buffer = this.buffer;
        var chunks = this.bufferChunks;
        var mode = this.bufferDisplayMode;
        var duration = this.player.getDuration();
        var count;
        switch (mode) {
            case BufferDisplayMode.None:
                count = 0;
                break;
            case BufferDisplayMode.Simple:
                count = 1;
                break;
            case BufferDisplayMode.Chunks:
                count = buffers.length;
                break;
        }
        while (chunks.length > count) {
            buffer.removeChild(chunks.pop());
        }
        while (chunks.length < count) {
            var chunk = document.createElement('div');
            chunk.className = 'tuxSeekBar__bufferChunk';
            buffer.appendChild(chunk);
            chunks.push(chunk);
        }
        if (mode == BufferDisplayMode.Simple) {
            var chunk = chunks[0];
            chunk.style.left = '0';
            if (buffers.length) {
                var end = buffers.end(buffers.length - 1);
                chunk.style.right = ((1 - end / duration) * 100).toFixed(3) + '%';
            }
            else {
                chunk.style.right = '0';
            }
        }
        else if (mode == BufferDisplayMode.Chunks) {
            for (var index = 0; index < count; index++) {
                var start = buffers.start(index);
                var end = buffers.end(index);
                var chunk = chunks[index];
                chunk.style.left = ((start / duration) * 100).toFixed(3) + '%';
                chunk.style.right = ((1 - end / duration) * 100).toFixed(3) + '%';
            }
        }
    };
    SeekBar.formatTime = function (time) {
        var minutes = Math.floor(time / 60);
        var seconds = Math.round(time % 60);
        return (minutes < 10 ? '0' : '') + minutes.toString() + ':' + (seconds < 10 ? '0' : '') + seconds.toString();
    };
    return SeekBar;
}(View));
export { SeekBar };
tslib_1.__decorate([
    option({
        type: 'element',
        defaultValue: '.tuxSeekBar__currentTime',
        tagName: 'div',
        className: 'tuxSeekBar__currentTime',
    })
], SeekBar.prototype, "currentTime", void 0);
tslib_1.__decorate([
    option({
        type: 'element',
        defaultValue: '.tuxSeekBar__duration',
        tagName: 'div',
        className: 'tuxSeekBar__duration',
    })
], SeekBar.prototype, "duration", void 0);
tslib_1.__decorate([
    option({
        type: 'element',
        defaultValue: '.tuxSeekBar__progress',
        tagName: 'div',
        className: 'tuxSeekBar__progress',
    })
], SeekBar.prototype, "progress", void 0);
tslib_1.__decorate([
    option({
        type: 'element',
        defaultValue: '.tuxSeekBar__buffer',
        tagName: 'div',
        className: 'tuxSeekBar__buffer',
    })
], SeekBar.prototype, "buffer", void 0);
tslib_1.__decorate([
    option({
        type: 'enum',
        values: BufferDisplayMode,
        defaultValue: BufferDisplayMode.Simple,
    })
], SeekBar.prototype, "bufferDisplayMode", void 0);
tslib_1.__decorate([
    option({ type: 'bool' })
], SeekBar.prototype, "emitClick", void 0);
