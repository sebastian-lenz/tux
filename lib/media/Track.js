import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import Collection from '../models/Collection';
import Model from '../models/Model';
/**
 * A model representing a single media track.
 */
var Track = (function (_super) {
    tslib_1.__extends(Track, _super);
    /**
     * Track constructor.
     *
     * @param attributes
     *   Default Track attributes.
     * @param options
     *   Track constructor options.
     */
    function Track(attributes, options) {
        return _super.call(this, defaults(attributes || {}, {
            source: null,
            duration: 0
        }), options) || this;
    }
    /**
     * Return the source url of this track.
     */
    Track.prototype.getSource = function () {
        return this.attributes.source;
    };
    /**
     * Return the duration of this track.
     */
    Track.prototype.getDuration = function () {
        return this.attributes.duration;
    };
    /**
     * Set the duration of this track.
     */
    Track.prototype.setDuration = function (value) {
        return this.set('duration', value);
    };
    /**
     * Return the title of this track.
     */
    Track.prototype.getTitle = function () {
        return this.attributes.title;
    };
    return Track;
}(Model));
export { Track };
/**
 * A collection of media tracks.
 */
var TrackList = (function (_super) {
    tslib_1.__extends(TrackList, _super);
    function TrackList() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return TrackList;
}(Collection));
export { TrackList };
