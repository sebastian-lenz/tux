import Collection from '../models/Collection';
import Model from '../models/Model';
/**
 * Attribute structure of the Track model.
 */
export interface ITrackAttributes {
    /**
     * The source url of this track.
     */
    source: string;
    /**
     * The duration of this track.
     */
    duration?: number;
    /**
     * The title of this track.
     */
    title?: string;
}
/**
 * A model representing a single media track.
 */
export declare class Track extends Model {
    /**
     * The attributes of this track.
     */
    attributes: ITrackAttributes;
    /**
     * Track constructor.
     *
     * @param attributes
     *   Default Track attributes.
     * @param options
     *   Track constructor options.
     */
    constructor(attributes?: ITrackAttributes, options?: any);
    /**
     * Return the source url of this track.
     */
    getSource(): string;
    /**
     * Return the duration of this track.
     */
    getDuration(): number;
    /**
     * Set the duration of this track.
     */
    setDuration(value: number): false | this;
    /**
     * Return the title of this track.
     */
    getTitle(): string;
}
/**
 * A collection of media tracks.
 */
export declare class TrackList extends Collection<Track> {
}
