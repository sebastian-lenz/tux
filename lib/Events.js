import { keys, once, uniqueId } from 'underscore';
/**
 * A difficult-to-believe, but optimized internal dispatch function for
 * triggering events. Tries to keep the usual cases speedy (most internal
 * Backbone events have 3 arguments).
 */
function triggerEvents(events, args) {
    var ev, i = -1;
    var l = events.length, a1 = args[0], a2 = args[1], a3 = args[2];
    switch (args.length) {
        case 0:
            while (++i < l)
                (ev = events[i]).callback.call(ev.ctx);
            return;
        case 1:
            while (++i < l)
                (ev = events[i]).callback.call(ev.ctx, a1);
            return;
        case 2:
            while (++i < l)
                (ev = events[i]).callback.call(ev.ctx, a1, a2);
            return;
        case 3:
            while (++i < l)
                (ev = events[i]).callback.call(ev.ctx, a1, a2, a3);
            return;
        default:
            while (++i < l)
                (ev = events[i]).callback.apply(ev.ctx, args);
            return;
    }
}
/**
 * An event dispatcher.
 *
 * Based on Backbone.Events
 * http://backbonejs.org/backbone.js
 */
var Events = (function () {
    function Events() {
        this._events = null;
        this._listenId = null;
        this._listeners = null;
        this._listeningTo = null;
    }
    /**
     * Bind an event to a `callback` function. Passing `"all"` will bind
     * the callback to all events fired.
     */
    Events.prototype.on = function (name, callback, context, priority) {
        return this.addHandler(name, callback, context, null, priority);
    };
    /*
     * Bind an event to only be triggered a single time. After the first time
     * the callback is invoked, its listener will be removed. If multiple events
     * are passed in using the space-separated syntax, the handler will fire
     * once for each event, not once for a combination of all events.
     */
    Events.prototype.once = function (name, callback, context, priority) {
        var _this = this;
        var onceCallback = this.createOnceHandler(callback, function (handler) {
            _this.off(name, handler);
        });
        return this.addHandler(name, onceCallback, context, null, priority);
    };
    /**
     * Remove one or many callbacks. If `context` is null, removes all
     * callbacks with that function. If `callback` is null, removes all
     * callbacks for the event. If `name` is null, removes all bound
     * callbacks for all events.
     */
    Events.prototype.off = function (name, callback, context) {
        var events = this._events;
        var listeners = this._listeners;
        if (events === null)
            return this;
        // Delete all events listeners and "drop" events.
        if (!name && !callback && !context) {
            if (listeners) {
                var ids = keys(listeners);
                for (var i = 0; i < ids.length; i++) {
                    var listening = listeners[ids[i]];
                    delete listeners[listening.id];
                    delete listening.listeningTo[listening.objId];
                }
            }
            this._events = null;
            return this;
        }
        var names = name ? [name] : keys(events);
        for (var i = 0; i < names.length; i++) {
            var name_1 = names[i];
            var handlers = events[name_1];
            if (!handlers)
                break;
            // Replace events if there are any remaining.  Otherwise, clean up.
            var remaining = [];
            for (var j = 0; j < handlers.length; j++) {
                var handler = handlers[j];
                if (callback && (callback !== handler.callback &&
                    callback !== handler.callback._callback) ||
                    context && context !== handler.context) {
                    remaining.push(handler);
                }
                else {
                    var listening = handler.listening;
                    if (listening && --listening.count === 0 && listeners) {
                        delete listeners[listening.id];
                        delete listening.listeningTo[listening.objId];
                    }
                }
            }
            // Update tail event if the list has any events.  Otherwise, clean up.
            if (remaining.length) {
                events[name_1] = remaining;
            }
            else {
                delete events[name_1];
            }
        }
        return this;
    };
    /**
     * Inversion-of-control versions of `on`. Tell *this* object to listen to
     * an event in another object... keeping track of what it's listening to
     * for easier unbinding later.
     */
    Events.prototype.listenTo = function (obj, name, callback, priority) {
        if (!obj)
            return this;
        var objId = obj._listenId || (obj._listenId = uniqueId('l'));
        var listeningTo = this._listeningTo || (this._listeningTo = {});
        var listening = listeningTo[objId];
        // This object is not listening to any other events on `obj` yet.
        // Setup the necessary references to track the listening callbacks.
        if (!listening) {
            var id = this._listenId || (this._listenId = uniqueId('l'));
            listening = { count: 0, id: id, listeningTo: listeningTo, obj: obj, objId: objId };
            listeningTo[objId] = listening;
        }
        // Bind callbacks on obj, and keep track of them on listening.
        obj.addHandler(name, callback, this, listening, priority);
        return this;
    };
    /**
     * Inversion-of-control versions of `once`.
     */
    Events.prototype.listenToOnce = function (obj, name, callback, priority) {
        var _this = this;
        var onceCallback = this.createOnceHandler(callback, function (handler) {
            _this.stopListening(obj, name, handler);
        });
        return this.listenTo(obj, name, onceCallback, priority);
    };
    /**
     * Tell this object to stop listening to either specific events ... or
     * to every object it's currently listening to.
     */
    Events.prototype.stopListening = function (obj, name, callback) {
        var listeningTo = this._listeningTo;
        if (!listeningTo)
            return this;
        if (obj && !obj._listenId)
            return this;
        var ids = obj
            ? [obj._listenId ? obj._listenId : '']
            : keys(listeningTo);
        for (var i = 0; i < ids.length; i++) {
            var listening = listeningTo[ids[i]];
            // If listening doesn't exist, this object is not currently
            // listening to obj. Break out early.
            if (!listening)
                break;
            listening.obj.off(name, callback, this);
        }
        return this;
    };
    /**
     * Trigger one or many events, firing all bound callbacks. Callbacks are
     * passed the same arguments as `trigger` is, apart from the event name
     * (unless you're listening on `"all"`, which will cause your callback to
     * receive the true name of the event as the first argument).
     */
    Events.prototype.trigger = function (name) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var events = this._events;
        if (!events)
            return this;
        var handlers = events[name];
        var allHandlers = events['all'];
        if (handlers && allHandlers)
            allHandlers = allHandlers.slice();
        if (handlers)
            triggerEvents(handlers, args);
        if (allHandlers)
            triggerEvents(allHandlers, [name].concat(args));
        return this;
    };
    /**
     * Guard the `listening` argument from the public API.
     */
    Events.prototype.addHandler = function (name, callback, context, listening, priority) {
        if (listening === void 0) { listening = null; }
        if (priority === void 0) { priority = 0; }
        var events = this._events || (this._events = {});
        var handlers = events[name] || (events[name] = []);
        handlers.push({
            callback: callback,
            context: context,
            ctx: context || this,
            listening: listening,
            priority: priority,
        });
        handlers.sort(function (a, b) { return b.priority - a.priority; });
        if (listening) {
            var listeners = this._listeners || (this._listeners = {});
            listeners[listening.id] = listening;
            listening.count++;
        }
        return this;
    };
    /**
     * Reduces the event callbacks into a map of `{event: onceWrapper}`.
     * `offer` unbinds the `onceWrapper` after it has been called.
     */
    Events.prototype.createOnceHandler = function (callback, offCallback) {
        var that = this;
        var onceCallback = once(function () {
            offCallback(onceCallback);
            callback.apply(that, arguments);
        });
        onceCallback._callback = callback;
        return onceCallback;
    };
    return Events;
}());
export default Events;
