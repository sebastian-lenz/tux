var NumericValue = (function () {
    function NumericValue(value) {
        this.value = value;
    }
    NumericValue.prototype.clone = function () {
        return new NumericValue(this.value);
    };
    NumericValue.prototype.equals = function (other) {
        return this.value == other.value;
    };
    NumericValue.prototype.convert = function (value) {
        if (value instanceof NumericValue) {
            return value;
        }
        else if (typeof value == 'number') {
            return new NumericValue(value);
        }
        return null;
    };
    NumericValue.prototype.reset = function () {
        this.value = 0;
        return this;
    };
    NumericValue.prototype.copy = function (source) {
        this.value = source.value;
        return this;
    };
    NumericValue.prototype.length = function () {
        return Math.abs(this.value);
    };
    NumericValue.prototype.add = function (summand) {
        this.value += summand.value;
        return this;
    };
    NumericValue.prototype.subtract = function (subtrahend) {
        this.value -= subtrahend.value;
        return this;
    };
    NumericValue.prototype.scale = function (factor) {
        this.value *= factor;
        return this;
    };
    NumericValue.prototype.setValues = function (values) {
        this.value = values[0];
        return this;
    };
    NumericValue.prototype.getValues = function () {
        return [this.value];
    };
    return NumericValue;
}());
export { NumericValue };
