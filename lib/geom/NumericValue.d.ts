import { IComputableType, IVectorType } from "./Type";
export declare class NumericValue implements IComputableType, IVectorType {
    value: number;
    constructor(value: number);
    clone(): NumericValue;
    equals(other: NumericValue): boolean;
    convert(value: any): NumericValue;
    reset(): this;
    copy(source: NumericValue): this;
    length(): number;
    add(summand: NumericValue): this;
    subtract(subtrahend: NumericValue): this;
    scale(factor: number): this;
    setValues(values: number[]): this;
    getValues(): number[];
}
