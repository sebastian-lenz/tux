import { isArray } from 'underscore';
/**
 * A vector with two dimensions like a point on a flat surface.
 */
var Vector2 = (function () {
    /**
     * Vector2 constructor.
     * @param x
     *   The horizontal position of the vector.
     * @param y
     *   The vertical position of the vector.
     */
    function Vector2(x, y) {
        this.x = x;
        this.y = y;
    }
    /**
     * Return a copy of this type.
     * Implements IType.
     *
     * @returns
     *   A copy of this type and all of its values.
     */
    Vector2.prototype.clone = function () {
        return new Vector2(this.x, this.y);
    };
    /**
     * Test whether this instance equals the given type instance.
     *
     * @param other
     *   The other type instance that should be compared.
     * @returns
     *   TRUE if the values of this instance and the given instance match.
     */
    Vector2.prototype.equals = function (other) {
        return this.x == other.x && this.y == other.y;
    };
    /**
     * Try to convert the given value to a Vector2 instance.
     * Implements IType.
     *
     * @param value
     *   The value that should be converted.
     */
    Vector2.prototype.convert = function (value) {
        return Vector2.convert(value);
    };
    /**
     * Return the length of this vector.
     * Implements IComputableType.
     *
     * @returns
     *   The length of this vector.
     */
    Vector2.prototype.length = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    };
    /**
     * Reset all dimensions of this vector to zero.
     * Implements IComputableType.
     */
    Vector2.prototype.reset = function () {
        this.x = 0;
        this.y = 0;
        return this;
    };
    /**
     * Copy the dimensions of the given vector to this vector.
     * Implements IComputableType.
     *
     * @param source
     *   The vector whose dimensions should be copied.
     */
    Vector2.prototype.copy = function (source) {
        this.x = source.x;
        this.y = source.y;
        return this;
    };
    Vector2.prototype.add = function (x, y) {
        if (isIVector2(x)) {
            this.x += x.x;
            this.y += x.y;
        }
        else {
            this.x += x;
            this.y += y;
        }
        return this;
    };
    Vector2.prototype.subtract = function (x, y) {
        if (isIVector2(x)) {
            this.x -= x.x;
            this.y -= x.y;
        }
        else {
            this.x -= x;
            this.y -= y;
        }
        return this;
    };
    Vector2.prototype.min = function (x, y) {
        if (isIVector2(x)) {
            if (this.x > x.x)
                this.x = x.x;
            if (this.y > x.y)
                this.y = x.y;
        }
        else {
            if (this.x > x)
                this.x = x;
            if (this.y > y)
                this.y = y;
        }
        return this;
    };
    Vector2.prototype.max = function (x, y) {
        if (isIVector2(x)) {
            if (this.x < x.x)
                this.x = x.x;
            if (this.y < x.y)
                this.y = x.y;
        }
        else {
            if (this.x < x)
                this.x = x;
            if (this.y < y)
                this.y = y;
        }
        return this;
    };
    /**
     * Scale all dimensions of this vector by the given factor.
     * Implements IComputableType.
     *
     * @param factor
     *   The scaling factor that should be applied to this vector.
     */
    Vector2.prototype.scale = function (factor) {
        this.x *= factor;
        this.y *= factor;
        return this;
    };
    /**
     * Set the values of this vector from the given array.
     * Implements IVectorType.
     *
     * @param values
     *   An array containing the x and y values that should be set.
     */
    Vector2.prototype.setValues = function (values) {
        this.x = values[0];
        this.y = values[1];
        return this;
    };
    /**
     * Return the raw values of this vector as an array.
     * Implements IVectorType.
     *
     * @returns
     *   An array containing the x and y values of this vector.
     */
    Vector2.prototype.getValues = function () {
        return [this.x, this.y];
    };
    /**
     * Try to convert the given value to a Vector2 instance.
     *
     * @param value
     *   The value that should be converted.
     */
    Vector2.convert = function (value) {
        if (value instanceof Vector2) {
            return value;
        }
        else if (isIVector2(value)) {
            return new Vector2(value.x * 1, value.y * 1);
        }
        else if (isArray(value) && value.length >= 2) {
            var x_1 = value[0], y = value[1];
            return new Vector2(parseFloat(x_1), parseFloat(y));
        }
        return null;
    };
    return Vector2;
}());
export { Vector2 };
/**
 * A type guard that checks whether the given value implements IVector2.
 *
 * @param value
 *   The value that should be tested.
 * @returns
 *   TRUE if the given value implements IVector2, FALSE otherwise.
 */
export function isIVector2(value) {
    return (typeof value === 'object' &&
        'x' in value &&
        'y' in value);
}
