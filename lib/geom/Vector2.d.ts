import { IComputableType, IVectorType } from './Type';
/**
 * Data structure of vectors with two dimensions.
 */
export interface IVector2 {
    /**
     * The horizontal position of the vector.
     */
    x: number;
    /**
     * The vertical position of the vector.
     */
    y: number;
}
/**
 * A vector with two dimensions like a point on a flat surface.
 */
export declare class Vector2 implements IVector2, IComputableType, IVectorType {
    /**
     * The horizontal position of the vector.
     */
    x: number;
    /**
     * The vertical position of the vector.
     */
    y: number;
    /**
     * Vector2 constructor.
     * @param x
     *   The horizontal position of the vector.
     * @param y
     *   The vertical position of the vector.
     */
    constructor(x: number, y: number);
    /**
     * Return a copy of this type.
     * Implements IType.
     *
     * @returns
     *   A copy of this type and all of its values.
     */
    clone(): Vector2;
    /**
     * Test whether this instance equals the given type instance.
     *
     * @param other
     *   The other type instance that should be compared.
     * @returns
     *   TRUE if the values of this instance and the given instance match.
     */
    equals(other: Vector2): boolean;
    /**
     * Try to convert the given value to a Vector2 instance.
     * Implements IType.
     *
     * @param value
     *   The value that should be converted.
     */
    convert(value: any): Vector2;
    /**
     * Return the length of this vector.
     * Implements IComputableType.
     *
     * @returns
     *   The length of this vector.
     */
    length(): number;
    /**
     * Reset all dimensions of this vector to zero.
     * Implements IComputableType.
     */
    reset(): this;
    /**
     * Copy the dimensions of the given vector to this vector.
     * Implements IComputableType.
     *
     * @param source
     *   The vector whose dimensions should be copied.
     */
    copy(source: Vector2): this;
    /**
     * Add the given vector dimensions to this vector.
     * Implements IComputableType.
     *
     * @param summand
     *   The vector whose dimensions should be added.
     */
    add(x: number, y: number): this;
    add(summand: Vector2): this;
    add(summand: IVector2): this;
    /**
     * Subtract the given vector dimensions from this vector.
     * Implements IComputableType.
     *
     * @param subtrahend
     *   The vector whose dimensions should be subtracted.
     */
    subtract(x: number, y: number): this;
    subtract(subtrahend: Vector2): this;
    subtract(subtrahend: IVector2): this;
    min(x: number, y: number): this;
    min(subtrahend: IVector2): this;
    max(x: number, y: number): this;
    max(subtrahend: IVector2): this;
    /**
     * Scale all dimensions of this vector by the given factor.
     * Implements IComputableType.
     *
     * @param factor
     *   The scaling factor that should be applied to this vector.
     */
    scale(factor: number): this;
    /**
     * Set the values of this vector from the given array.
     * Implements IVectorType.
     *
     * @param values
     *   An array containing the x and y values that should be set.
     */
    setValues(values: number[]): this;
    /**
     * Return the raw values of this vector as an array.
     * Implements IVectorType.
     *
     * @returns
     *   An array containing the x and y values of this vector.
     */
    getValues(): number[];
    /**
     * Try to convert the given value to a Vector2 instance.
     *
     * @param value
     *   The value that should be converted.
     */
    static convert(value: any): Vector2;
}
/**
 * A type guard that checks whether the given value implements IVector2.
 *
 * @param value
 *   The value that should be tested.
 * @returns
 *   TRUE if the given value implements IVector2, FALSE otherwise.
 */
export declare function isIVector2(value: any): value is IVector2;
