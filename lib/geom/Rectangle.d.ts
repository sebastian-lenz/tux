export interface IRectangle {
    x: number;
    y: number;
    width: number;
    height: number;
}
export declare class Rectangle implements IRectangle {
    x: number;
    y: number;
    width: number;
    height: number;
}
