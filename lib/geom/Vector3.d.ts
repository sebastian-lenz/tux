export interface IVector3 {
    x: number;
    y: number;
    z: number;
}
export declare class Vector3 implements IVector3 {
    x: number;
    y: number;
    z: number;
}
