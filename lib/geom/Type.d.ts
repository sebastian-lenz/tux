/**
 * Defines a common set of methods all types must implement.
 */
export interface IType {
    /**
     * Return a copy of this type.
     *
     * @returns
     *   A copy of this type and all of its values.
     */
    clone(): IType;
    /**
     * Test whether this instance equals the given type instance.
     *
     * @param other
     *   The other type instance that should be compared.
     * @returns
     *   TRUE if the values of this instance and the given instance match.
     */
    equals(other: IType): boolean;
    /**
     * Try to convert the given value to the own type.
     *
     * @param value
     *   The value that should be converted.
     * @returns
     *   An instance of this type or null if the value could not be converted.
     */
    convert(value: any): IType;
}
/**
 * A type that exposes a linear interpolation function.
 */
export interface ILerpableType extends IType {
    /**
     * Calculate the linear interpolation between this type instance and the given type instance.
     *
     * @param other
     *   The target type instance to interpolate to.
     * @param amount
     *   The desired interpolation position between 0 and 1.
     * @returns
     *   The interpolated type value at the given position.
     */
    lerp(other: ILerpableType, amount: number): ILerpableType;
}
/**
 * A type that consists of multiple values like a vector or a color.
 */
export interface IVectorType extends IType {
    /**
     * Return a copy of this type.
     *
     * @returns
     *   A copy of this type and all of its values.
     */
    clone(): IVectorType;
    /**
     * Test whether this instance equals the given type instance.
     *
     * @param other
     *   The other type instance that should be compared.
     * @returns
     *   TRUE if the values of this instance and the given instance match.
     */
    equals(other: IVectorType): boolean;
    /**
     * Try to convert the given value to the own type.
     *
     * @param value
     *   The value that should be converted.
     * @returns
     *   An instance of this type or null if the value could not be converted.
     */
    convert(value: any): IVectorType;
    /**
     * Return the raw values of this type as an array.
     *
     * @returns
     *   An array containing raw values of this type.
     */
    getValues(): number[];
    /**
     * Set the values of this type from the given array.
     *
     * @param values
     *   An array containing the values that should be set.
     */
    setValues(values: number[]): IVectorType;
}
/**
 * A type that supports basic math operations.
 *
 * Math operations generally alter the instance itself, they don't return a
 * new instance of the type. Use the clone method to create a copy if needed.
 */
export interface IComputableType extends IType {
    /**
     * Return a copy of this type.
     *
     * @returns
     *   A copy of this type and all of its values.
     */
    clone(): IComputableType;
    /**
     * Test whether this instance equals the given type instance.
     *
     * @param other
     *   The other type instance that should be compared.
     * @returns
     *   TRUE if the values of this instance and the given instance match.
     */
    equals(other: IComputableType): boolean;
    /**
     * Try to convert the given value to the own type.
     *
     * @param value
     *   The value that should be converted.
     * @returns
     *   An instance of this type or null if the value could not be converted.
     */
    convert(value: any): IComputableType;
    /**
     * Return the length of this type.
     *
     * @returns
     *   The length of this type.
     */
    length(): number;
    /**
     * Reset this instance.
     */
    reset(): IComputableType;
    /**
     * Copy the values of the given instance to this instance.
     *
     * @param source
     *   The type instance whose values should be copied.
     */
    copy(source: IComputableType): IComputableType;
    /**
     * Add the given instance values to this instance.
     *
     * @param summand
     *   The type instance whose values should be added.
     */
    add(summand: IComputableType): IComputableType;
    /**
     * Subtract the given instance values from this instance.
     *
     * @param subtrahend
     *   The type instance whose values should be subtracted.
     */
    subtract(subtrahend: IComputableType): IComputableType;
    /**
     * Scale all values of this type by the given factor.
     *
     * @param factor
     *   The scaling factor that should be applied to this instance.
     */
    scale(factor: number): IComputableType;
}
/**
 * A type guard that checks whether the given value implements ILerpableType.
 *
 * @param value
 *   The value that should be tested.
 * @returns
 *   TRUE if the given value implements ILerpableType, FALSE otherwise.
 */
export declare function isLerpableType(value: any): value is ILerpableType;
/**
 * A type guard that checks whether the given value implements IVectorType.
 *
 * @param value
 *   The value that should be tested.
 * @returns
 *   TRUE if the given value implements IVectorType, FALSE otherwise.
 */
export declare function isVectorType(value: any): value is IVectorType;
/**
 * A type guard that checks whether the given value implements IComputableType.
 *
 * @param value
 *   The value that should be tested.
 * @returns
 *   TRUE if the given value implements IComputableType, FALSE otherwise.
 */
export declare function isComputableType(value: any): value is IComputableType;
