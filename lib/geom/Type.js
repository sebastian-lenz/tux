/**
 * A type guard that checks whether the given value implements ILerpableType.
 *
 * @param value
 *   The value that should be tested.
 * @returns
 *   TRUE if the given value implements ILerpableType, FALSE otherwise.
 */
export function isLerpableType(value) {
    return (typeof value === 'object' &&
        value.lerp && typeof value.lerp === 'function');
}
/**
 * A type guard that checks whether the given value implements IVectorType.
 *
 * @param value
 *   The value that should be tested.
 * @returns
 *   TRUE if the given value implements IVectorType, FALSE otherwise.
 */
export function isVectorType(value) {
    return (typeof value === 'object' &&
        value.getValues && typeof value.getValues === 'function' &&
        value.setValues && typeof value.setValues === 'function');
}
/**
 * A type guard that checks whether the given value implements IComputableType.
 *
 * @param value
 *   The value that should be tested.
 * @returns
 *   TRUE if the given value implements IComputableType, FALSE otherwise.
 */
export function isComputableType(value) {
    return (typeof value === 'object' &&
        value.length && typeof value.length === 'function' &&
        value.reset && typeof value.reset === 'function' &&
        value.copy && typeof value.copy === 'function' &&
        value.add && typeof value.add === 'function' &&
        value.subtract && typeof value.subtract === 'function' &&
        value.scale && typeof value.scale === 'function');
}
