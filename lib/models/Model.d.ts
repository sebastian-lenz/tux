import Events from '../Events';
import Collection from './Collection';
export interface ModelOptions {
    collection?: Collection;
    parse?: Function;
}
export default class Model extends Events {
    cid: string;
    id: any;
    attributes: any;
    validate: Function;
    private _changing;
    private _previousAttributes;
    private _pending;
    changed: any;
    collection: Collection;
    validationError: any;
    idAttribute: string;
    cidPrefix: string;
    constructor(attributes?: {}, options?: ModelOptions);
    initialize(): void;
    toJSON(options: any): any;
    sync(...args: any[]): any;
    get(attr: any): any;
    escape(attr: any): string;
    has(attr: any): boolean;
    set(key: any, val: any, options?: any): false | this;
    unset(attr: any, options: any): false | this;
    clear(options: any): false | this;
    hasChanged(attr?: any): boolean;
    changedAttributes(diff: any): any;
    previous(attr: any): any;
    previousAttributes(): any;
    fetch(options: any): any;
    save(key: any, val: any, options: any): any;
    destroy(options: any): boolean;
    url(): string;
    parse(resp: any, options: any): any;
    clone(): void;
    isNew(): boolean;
    isValid(options: any): boolean;
    _validate(attrs: any, options: any): boolean;
}
