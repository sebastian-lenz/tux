import * as _ from 'underscore';
// Create a local reference to a common array method we'll want to use later.
var slice = Array.prototype.slice;
var modelMatcher = function (attrs) {
    var matcher = _.matches(attrs);
    return function (model) {
        return matcher(model.attributes);
    };
};
// Support `collection.sortBy('attr')` and `collection.findWhere({id: 1})`.
var cb = function (iteratee, instance) {
    if (_.isFunction(iteratee))
        return iteratee;
    if (_.isObject(iteratee) && !instance._isModel(iteratee))
        return modelMatcher(iteratee);
    if (_.isString(iteratee))
        return function (model) { return model.get(iteratee); };
    return iteratee;
};
// Proxy Backbone class methods to Underscore functions, wrapping the model's
// `attributes` object or collection's `models` array behind the scenes.
//
// collection.filter(function(model) { return model.get('age') > 10 });
// collection.each(this.addView);
//
// `Function#apply` can be slow so we use the method's arg count, if we know it.
function addMethod(length, method, attribute) {
    switch (length) {
        case 1: return function () {
            return _[method](this[attribute]);
        };
        case 2: return function (value) {
            return _[method](this[attribute], value);
        };
        case 3: return function (iteratee, context) {
            return _[method](this[attribute], cb(iteratee, this), context);
        };
        case 4: return function (iteratee, defaultVal, context) {
            return _[method](this[attribute], cb(iteratee, this), defaultVal, context);
        };
        default: return function () {
            var args = slice.call(arguments);
            args.unshift(this[attribute]);
            return _[method].apply(_, args);
        };
    }
}
export function addUnderscoreMethods(Class, methods, attribute) {
    _.each(methods, function (length, method) {
        if (_[method])
            Class.prototype[method] = addMethod(length, method, attribute);
    });
}
// Wrap an optional error callback with a fallback error event.
export function wrapError(model, options) {
    var error = options.error;
    options.error = function (resp) {
        if (error)
            error.call(options.context, model, resp, options);
        model.trigger('error', model, resp, options);
    };
}
;
