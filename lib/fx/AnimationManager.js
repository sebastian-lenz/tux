import * as tslib_1 from "tslib";
import services, { service } from '../services';
import Dispatcher from '../services/Dispatcher';
/**
 * A central animation manager and updater.
 */
var AnimationManager = AnimationManager_1 = (function () {
    /**
     * AnimationManager constructor.
     */
    function AnimationManager() {
        var _this = this;
        /**
         * A list of all active animations.
         */
        this.animations = [];
        /**
         * Triggered on every browser repaint.
         */
        this.onFrame = function (timeStep) {
            var animations = _this.animations.slice();
            var timeScale = timeStep / 16.666;
            for (var _i = 0, animations_1 = animations; _i < animations_1.length; _i++) {
                var animation = animations_1[_i];
                if (!animation.update(timeStep, timeScale)) {
                    _this.remove(animation);
                }
            }
        };
        Dispatcher.getInstance().on('frame', this.onFrame);
    }
    /**
     * Add an animation to the manager.
     *
     * @param animation
     *   The animation that should be added.
     */
    AnimationManager.prototype.add = function (animation) {
        var animations = this.animations;
        var accessor = animation.accessor;
        var count = animations.length;
        var index = 0;
        while (index < count) {
            var animation_1 = animations[index];
            if (animation_1.accessor.equals(accessor)) {
                animation_1.stop();
                animations.splice(index, 1);
                count--;
            }
            else {
                index++;
            }
        }
        animations.push(animation);
    };
    /**
     * Stop all animations matching the given arguments.
     *
     * @param target
     *   The target object animations should be stopped for.
     * @param property
     *   The property name animations should be stopped for.
     */
    AnimationManager.prototype.stop = function (target, property) {
        var animations = this.animations.slice();
        for (var _i = 0, animations_2 = animations; _i < animations_2.length; _i++) {
            var animation = animations_2[_i];
            var accessor = animation.accessor;
            if (target && accessor.target != target)
                continue;
            if (property && accessor.property != property)
                continue;
            this.remove(animation);
        }
    };
    /**
     * Remove an animation from the manager.
     *
     * @param animation
     *   The animation that should be removed.
     */
    AnimationManager.prototype.remove = function (animation) {
        var index = this.animations.indexOf(animation);
        if (index != -1) {
            animation.stop();
            this.animations.splice(index, 1);
        }
    };
    /**
     * Return the current service instance of the AnimationManager.
     *
     * @returns
     *    The current service instance of the AnimationManager.
     */
    AnimationManager.getInstance = function () {
        return services.get('AnimationManager', AnimationManager_1);
    };
    return AnimationManager;
}());
AnimationManager = AnimationManager_1 = tslib_1.__decorate([
    service('AnimationManager')
], AnimationManager);
export { AnimationManager };
/**
 * Stop all animations matching the given arguments.
 *
 * @param target
 *   The target object animations should be stopped for.
 * @param property
 *   The property name animations should be stopped for.
 */
export function stopAnimations(target, property) {
    AnimationManager.getInstance().stop(target, property);
}
var AnimationManager_1;
