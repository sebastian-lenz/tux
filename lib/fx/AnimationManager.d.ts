import { IAnimation } from './Animation';
/**
 * A central animation manager and updater.
 */
export declare class AnimationManager {
    /**
     * A list of all active animations.
     */
    animations: IAnimation[];
    /**
     * AnimationManager constructor.
     */
    constructor();
    /**
     * Add an animation to the manager.
     *
     * @param animation
     *   The animation that should be added.
     */
    add(animation: IAnimation): void;
    /**
     * Stop all animations matching the given arguments.
     *
     * @param target
     *   The target object animations should be stopped for.
     * @param property
     *   The property name animations should be stopped for.
     */
    stop(target?: any, property?: string): void;
    /**
     * Remove an animation from the manager.
     *
     * @param animation
     *   The animation that should be removed.
     */
    remove(animation: IAnimation): void;
    /**
     * Triggered on every browser repaint.
     */
    onFrame: (timeStep: number) => void;
    /**
     * Return the current service instance of the AnimationManager.
     *
     * @returns
     *    The current service instance of the AnimationManager.
     */
    static getInstance(): AnimationManager;
}
/**
 * Stop all animations matching the given arguments.
 *
 * @param target
 *   The target object animations should be stopped for.
 * @param property
 *   The property name animations should be stopped for.
 */
export declare function stopAnimations(target?: any, property?: string): void;
