import { defaults, forEach } from 'underscore';
import diffPositions from './diffPositions';
import animation from '../../util/vendor/animation';
/**
 * Animate the changed positions of the given elements, fade in new elements and fade out deleted elements.
 *
 * @param initialElements  A list of all persistent elements.
 * @param callback  A callback that changes the visible elements and returns the new list.
 * @param options   The advanced options used to animate the elements.
 */
export default function diffAnimate(initialElements, callback, options) {
    var result = diffPositions(initialElements, callback, options);
    if (!animation.endEvent)
        return result;
    var endEvent = animation.endEvent;
    var _a = defaults(options || {}, {
        detach: false,
    }), detach = _a.detach, origin = _a.origin;
    var shiftTop = 0, shiftLeft = 0;
    if (origin) {
        var originRect = origin.getBoundingClientRect();
        shiftTop = -originRect.top;
        shiftLeft = -originRect.left;
    }
    forEach(result.created, function (created) {
        var element = created.element, inViewport = created.inViewport;
        if (!inViewport)
            return;
        var onEnd = function () {
            element.classList.remove('fadeIn');
            element.removeEventListener(endEvent, onEnd);
        };
        element.addEventListener(endEvent, onEnd);
        element.classList.add('fadeIn');
    });
    forEach(result.deleted, function (deleted) {
        var element = deleted.element, inViewport = deleted.inViewport, position = deleted.position;
        if (!inViewport)
            return;
        var onEnd = function () {
            element.classList.remove('fadeOut');
            element.style.position = '';
            element.style.top = '';
            element.style.left = '';
            element.removeEventListener(endEvent, onEnd);
            if (detach && element.parentNode) {
                element.parentNode.removeChild(element);
            }
        };
        element.addEventListener(endEvent, onEnd);
        element.classList.add('fadeOut');
        element.style.position = 'absolute';
        element.style.top = position.top + shiftTop + "px";
        element.style.left = position.left + shiftLeft + "px";
    });
    return result;
}
