import { forEach, indexOf, map } from 'underscore';
import Viewport from '../../services/Viewport';
/**
 * Calculate the difference between two lists of elements.
 *
 * @param initialElements  A list of all persistent elements.
 * @param callback  A callback that changes the visible elements and returns the new list.
 */
export function diff(initialElements, callback) {
    var viewport = Viewport.getInstance();
    var positions = map(initialElements, function (el) {
        return el.getBoundingClientRect();
    });
    var result = {
        deleted: [],
        created: [],
        changed: [],
    };
    var min = 0;
    var max = viewport.height;
    var isInViewport = function (top, bottom) {
        return bottom > min && top < max;
    };
    forEach(callback(), function (element) {
        var position = element.getBoundingClientRect();
        var index = indexOf(initialElements, element);
        var inViewport = isInViewport(position.top, position.bottom);
        if (index == -1) {
            result.created.push({ element: element, position: position, inViewport: inViewport });
        }
        else {
            var oldPosition = positions[index];
            positions[index] = null;
            result.changed.push({
                element: element,
                from: oldPosition,
                to: position,
                inViewport: inViewport || isInViewport(oldPosition.top, oldPosition.bottom),
            });
        }
    });
    forEach(initialElements, function (element, index) {
        var position = positions[index];
        if (position) {
            result.deleted.push({
                element: element,
                position: position,
                inViewport: isInViewport(position.top, position.bottom),
            });
        }
    });
    return result;
}
