import { defaults, forEach } from 'underscore';
import { diff } from './diff';
import transform from '../../util/vendor/transform';
import transition from '../../util/vendor/transition';
import withoutTransition from './withoutTransition';
/**
 * Animate the changed positions of the given elements.
 *
 * @param initialElements  A list of all persistent elements.
 * @param callback  A callback that changes the visible elements and returns the new list.
 * @param options   The advanced options used to animate the elements.
 */
export default function diffPositions(initialElements, callback, options) {
    var result = diff(initialElements, callback);
    if (!transform.styleName || !transition.endEvent) {
        return result;
    }
    var styleName = transform.styleName;
    var endEvent = transition.endEvent;
    var _a = defaults(options || {}, {
        useTransform3D: false,
        finished: null,
    }), useTransform3D = _a.useTransform3D, finished = _a.finished;
    var active = 0;
    forEach(result.changed, function (changed) {
        var element = changed.element, from = changed.from, to = changed.to;
        if (!changed.inViewport)
            return;
        var left = options.ignoreX ? 0 : from.left - to.left;
        var top = options.ignoreY ? 0 : from.top - to.top;
        if (left == 0 && top == 0)
            return;
        var onEnd = function () {
            element.style[styleName] = '';
            element.removeEventListener(endEvent, onEnd);
            active -= 1;
            if (active == 0 && finished)
                finished();
        };
        active += 1;
        withoutTransition(element, function () {
            element.style[styleName] = useTransform3D
                ? "translate3d(" + left + "px, " + top + "px, 0)"
                : "translate(" + left + "px, " + top + "px)";
        });
        element.addEventListener(endEvent, onEnd);
        element.style[styleName] = useTransform3D
            ? "translate3d(0, 0, 0)"
            : "translate(0, 0)";
    });
    return result;
}
