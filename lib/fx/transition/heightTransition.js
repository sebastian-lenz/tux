import * as Q from 'q';
import Viewport from '../../services/Viewport';
import transition from '../../util/vendor/transition';
import withoutTransition from './withoutTransition';
/**
 * Transist the height of the given element.
 *
 * This method assumes that the height transition is already
 * applied to the target element via css.
 */
export default function heightTransition(element, callback, className) {
    if (className === void 0) { className = 'heightTransition'; }
    var deferred = Q.defer();
    var from = element.clientHeight;
    var to = from;
    var transitionEnd = function (e) {
        if (e.target !== element)
            return;
        element.removeEventListener(transition.endEvent, transitionEnd);
        withoutTransition(element, function () {
            element.style.height = '';
            element.classList.remove(className);
            Viewport.getInstance().triggerResize();
            deferred.resolve();
        });
    };
    if (!transition.endEvent) {
        callback();
        Viewport.getInstance().triggerResize();
        deferred.resolve();
    }
    else {
        withoutTransition(element, function () {
            element.classList.remove(className);
            element.style.height = '';
            callback();
            to = element.clientHeight;
            if (from != to) {
                element.style.height = from + 'px';
            }
        });
        if (from != to) {
            element.addEventListener(transition.endEvent, transitionEnd);
            element.classList.add(className);
            element.style.height = Math.ceil(to) + 'px';
        }
        else {
            deferred.resolve();
        }
    }
    return deferred.promise;
}
