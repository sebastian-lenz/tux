/// <reference types="q" />
import * as Q from 'q';
/**
 * Transist the height of the given element.
 *
 * This method assumes that the height transition is already
 * applied to the target element via css.
 */
export default function heightTransition(element: HTMLElement, callback: Function, className?: string): Q.Promise<any>;
