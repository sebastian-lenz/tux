/**
 * Run the given callback while all transitions on the given
 * element are blocked.
 */
export default function withoutTransition(element: HTMLElement, callback: Function): void;
