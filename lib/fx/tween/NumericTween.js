import * as tslib_1 from "tslib";
import { plugin } from '../../services/PluginManager';
import { Tween } from './Tween';
/**
 * A tween that animates a single numeric value.
 */
var NumericTween = NumericTween_1 = (function (_super) {
    tslib_1.__extends(NumericTween, _super);
    function NumericTween() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Initialize this tween. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    NumericTween.prototype.initialize = function (options) {
        this.valueChange = this.targetValue - this.initialValue;
    };
    /**
     * Calculate and return the current tween value.
     *
     * @returns
     *   The current tween value.
     */
    NumericTween.prototype.getCurrentValue = function () {
        return this.easing(this.currentTime, this.initialValue, this.valueChange, this.duration);
    };
    /**
     * Try to create an instance of this tween plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the tween plugin class if applicable, NULL otherwise.
     */
    NumericTween.tryCreate = function (accessor, options, initialValue) {
        if (typeof initialValue !== 'number') {
            return null;
        }
        if (typeof options.to !== 'number') {
            throw new Error('Type mismatch: Tween target value must be numeric.');
        }
        return new NumericTween_1(accessor, options);
    };
    return NumericTween;
}(Tween));
NumericTween = NumericTween_1 = tslib_1.__decorate([
    plugin('tween', { tryCreate: NumericTween_1.tryCreate })
], NumericTween);
export { NumericTween };
var NumericTween_1;
