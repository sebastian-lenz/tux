import { Accessor } from '../accessor/Accessor';
import { Tween, ITweenOptions } from './Tween';
/**
 * A tween that animates a single numeric value.
 */
export declare class NumericTween extends Tween<number> {
    /**
     * The delta between the initial value and the target value.
     */
    valueChange: number;
    /**
     * Initialize this tween. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    protected initialize(options: ITweenOptions<number>): void;
    /**
     * Calculate and return the current tween value.
     *
     * @returns
     *   The current tween value.
     */
    getCurrentValue(): number;
    /**
     * Try to create an instance of this tween plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the tween plugin class if applicable, NULL otherwise.
     */
    static tryCreate(accessor: Accessor, options: ITweenOptions<any>, initialValue: any): NumericTween;
}
