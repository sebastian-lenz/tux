import * as tslib_1 from "tslib";
import { plugin } from '../../services/PluginManager';
import { isLerpableType } from '../../geom/Type';
import { Tween } from './Tween';
/**
 * A tween that animates types exposing a lerp function.
 */
var LerpTween = LerpTween_1 = (function (_super) {
    tslib_1.__extends(LerpTween, _super);
    function LerpTween() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Calculate and return the current tween value.
     *
     * @returns
     *   The current tween value.
     */
    LerpTween.prototype.getCurrentValue = function () {
        return this.initialValue.lerp(this.targetValue, this.easing(this.currentTime, 0, 1, this.duration));
    };
    /**
     * Try to create an instance of this tween plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the tween plugin class if applicable, NULL otherwise.
     */
    LerpTween.tryCreate = function (accessor, options, initialValue) {
        if (isLerpableType(initialValue)) {
            var to = initialValue.convert(options.to);
            if (to == null) {
                throw new Error('Type mismatch: Tween initial and target value must be of the same type.');
            }
            options.to = to;
            return new LerpTween_1(accessor, options);
        }
        else {
            return null;
        }
    };
    return LerpTween;
}(Tween));
LerpTween = LerpTween_1 = tslib_1.__decorate([
    plugin('tween', { tryCreate: LerpTween_1.tryCreate })
], LerpTween);
export { LerpTween };
var LerpTween_1;
