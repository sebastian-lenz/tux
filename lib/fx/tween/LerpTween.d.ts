import { ILerpableType } from '../../geom/Type';
import { Accessor } from '../accessor/Accessor';
import { Tween, ITweenOptions } from './Tween';
/**
 * A tween that animates types exposing a lerp function.
 */
export declare class LerpTween extends Tween<ILerpableType> {
    /**
     * Calculate and return the current tween value.
     *
     * @returns
     *   The current tween value.
     */
    getCurrentValue(): ILerpableType;
    /**
     * Try to create an instance of this tween plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the tween plugin class if applicable, NULL otherwise.
     */
    static tryCreate(accessor: Accessor, options: ITweenOptions<any>, initialValue: any): LerpTween;
}
