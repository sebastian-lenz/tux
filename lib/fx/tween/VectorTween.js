import * as tslib_1 from "tslib";
import { plugin } from '../../services/PluginManager';
import { isVectorType } from '../../geom/Type';
import { Tween } from './Tween';
/**
 * A tween that animates types with multiple numeric fields like vectors or colors.
 */
var VectorTween = VectorTween_1 = (function (_super) {
    tslib_1.__extends(VectorTween, _super);
    function VectorTween() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Initialize this tween. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    VectorTween.prototype.initialize = function (options) {
        var from = this.initialValue.getValues();
        var delta = this.targetValue.getValues();
        for (var index = 0, count = from.length; index < count; index++) {
            delta[index] -= from[index];
        }
        this.initialValues = from;
        this.deltaValues = delta;
    };
    /**
     * Calculate and return the current tween value.
     *
     * @returns
     *   The current tween value.
     */
    VectorTween.prototype.getCurrentValue = function () {
        var current = this.currentTime;
        var duration = this.duration;
        var base = this.initialValues;
        var change = this.deltaValues;
        var values = [];
        for (var index = 0, count = base.length; index < count; index++) {
            values[index] = this.easing(current, base[index], change[index], duration);
        }
        return this.initialValue.clone().setValues(values);
    };
    /**
     * Try to create an instance of this tween plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the tween plugin class if applicable, NULL otherwise.
     */
    VectorTween.tryCreate = function (accessor, options, initialValue) {
        if (isVectorType(initialValue)) {
            var to = initialValue.convert(options.to);
            if (to == null) {
                throw new Error('Type mismatch: Tween initial and target value must be of the same type.');
            }
            options.to = to;
            return new VectorTween_1(accessor, options);
        }
        else {
            return null;
        }
    };
    return VectorTween;
}(Tween));
VectorTween = VectorTween_1 = tslib_1.__decorate([
    plugin('tween', { tryCreate: VectorTween_1.tryCreate })
], VectorTween);
export { VectorTween };
var VectorTween_1;
