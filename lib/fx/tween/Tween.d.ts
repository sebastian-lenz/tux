import { IPlugin } from '../../services/PluginManager';
import { Accessor } from '../accessor/Accessor';
import { EasingFunction } from '../easing/Type';
import { Animation, IAnimationOptions } from '../Animation';
import { IVectorType, ILerpableType } from '../../geom/Type';
export declare type AnyTween = Tween<AnyTweenDataType>;
export declare type AnyTweenDataType = number | IVectorType | ILerpableType;
export declare type AnyITweenOptions = ITweenOptions<AnyTweenDataType>;
/**
 * Constructor options for the Tween class.
 */
export interface ITweenOptions<T> extends IAnimationOptions {
    /**
     * The initial property value.
     */
    from?: T;
    /**
     * The value the property should be animated to.
     */
    to: T;
    /**
     * Duration of the tween in milliseconds.
     */
    duration?: number;
    /**
     * Delay till the tween starts in milliseconds.
     */
    delay?: number;
    /**
     * The easing function that should be used.
     */
    easing?: EasingFunction;
}
/**
 * Default options applied to all tweens.
 */
export declare const defaultTweenOptions: ITweenOptions<any>;
/**
 * Tween plugin interface.
 */
export interface ITweenPlugin extends IPlugin {
    /**
     * Try to create an instance of this tween plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the tween plugin class if applicable, NULL otherwise.
     */
    tryCreate(accessor: Accessor, options: ITweenOptions<any>, initialValue: any): AnyTween;
}
/**
 * A simple time based property tween.
 */
export declare abstract class Tween<T> extends Animation {
    /**
     * The current time of this tween.
     */
    currentTime: number;
    /**
     * The total duration of this tween.
     */
    duration: number;
    /**
     * The easing function that should be used.
     */
    easing: EasingFunction;
    /**
     * The initial value of the tweened property.
     */
    initialValue: T;
    /**
     * The target value of the tweened property.
     */
    targetValue: T;
    /**
     * Tween constructor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween options.
     */
    constructor(accessor: Accessor, options: ITweenOptions<T>);
    /**
     * Initialize this tween. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    protected initialize(options: ITweenOptions<T>): void;
    /**
     * Calculate and return the current tween value.
     *
     * @returns
     *   The current tween value.
     */
    abstract getCurrentValue(): T;
    /**
     * Update this animation.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @returns
     *   TRUE if this animation continues, FALSE if it has stopped.
     */
    update(timeStep: number): boolean;
    /**
     * Create a new tween for the given accessor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween options.
     * @returns
     *   A tween instance or null if no tween could be created.
     */
    static create(accessor: Accessor, options: AnyITweenOptions): AnyTween;
}
/**
 * Create a new tween for the given property on the target object.
 *
 * @param target
 *   The object whose property should be animated.
 * @param property
 *   The property that should be animated.
 * @param options
 *   The desired tween options.
 * @returns
 *   A tween instance.
 */
export declare function createTween(target: any, property: string, options: AnyITweenOptions): AnyTween;
