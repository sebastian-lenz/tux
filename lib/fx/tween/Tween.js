import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import PluginManager from '../../services/PluginManager';
import { Accessor } from '../accessor/Accessor';
import { easeInOutQuart } from '../easing/quart';
import { Animation } from '../Animation';
import { AnimationManager } from '../AnimationManager';
/**
 * Default options applied to all tweens.
 */
export var defaultTweenOptions = {
    to: void 0,
    duration: 500,
    delay: 0,
    easing: easeInOutQuart
};
/**
 * The plugin group used by the tween factory.
 */
var pluginGroup = PluginManager.getInstance().getGroup('tween');
/**
 * A simple time based property tween.
 */
var Tween = (function (_super) {
    tslib_1.__extends(Tween, _super);
    /**
     * Tween constructor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween options.
     */
    function Tween(accessor, options) {
        var _this = _super.call(this, accessor, options) || this;
        options = defaults(options, defaultTweenOptions);
        _this.currentTime = -options.delay;
        _this.duration = options.duration;
        _this.easing = options.easing;
        _this.initialValue = options.from ? options.from : accessor.getValue();
        _this.targetValue = options.to;
        _this.initialize(options);
        return _this;
    }
    /**
     * Initialize this tween. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    Tween.prototype.initialize = function (options) { };
    /**
     * Update this animation.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @returns
     *   TRUE if this animation continues, FALSE if it has stopped.
     */
    Tween.prototype.update = function (timeStep) {
        if (this.state & 6 /* FinishedOrStopped */) {
            return false;
        }
        this.currentTime += timeStep;
        if (this.currentTime >= this.duration) {
            this.currentTime = this.duration;
            this.accessor.setValue(this.targetValue);
            this.setState(2 /* Finished */);
            return false;
        }
        else if (this.currentTime > 0) {
            this.accessor.setValue(this.getCurrentValue());
            this.setState(1 /* Playing */);
        }
        return true;
    };
    /**
     * Create a new tween for the given accessor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween options.
     * @returns
     *   A tween instance or null if no tween could be created.
     */
    Tween.create = function (accessor, options) {
        var initialValue = accessor.getValue();
        for (var _i = 0, _a = pluginGroup.plugins; _i < _a.length; _i++) {
            var plugin = _a[_i];
            var tween = plugin.tryCreate(accessor, options, initialValue);
            if (tween != null) {
                return tween;
            }
        }
        return null;
    };
    return Tween;
}(Animation));
export { Tween };
/**
 * Create a new tween for the given property on the target object.
 *
 * @param target
 *   The object whose property should be animated.
 * @param property
 *   The property that should be animated.
 * @param options
 *   The desired tween options.
 * @returns
 *   A tween instance.
 */
export function createTween(target, property, options) {
    var accessor = Accessor.create(target, property);
    if (accessor == null) {
        throw new Error("Could not create an accessor for the property \"" + property + "\" on \"" + target + "\", did you import all necessary accessor classes?");
    }
    var tween = Tween.create(accessor, options);
    if (tween == null) {
        throw new Error("Could not create a tween for the property \"" + property + "\" on \"" + target + "\", did you import all necessary tween classes?");
    }
    AnimationManager.getInstance().add(tween);
    return tween;
}
