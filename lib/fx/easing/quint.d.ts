export declare function easeInQuint(t: any, b: any, c: any, d: any): any;
export declare function easeOutQuint(t: any, b: any, c: any, d: any): any;
export declare function easeInOutQuint(t: any, b: any, c: any, d: any): any;
