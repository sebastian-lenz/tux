export declare function easeInExpo(t: any, b: any, c: any, d: any): any;
export declare function easeOutExpo(t: any, b: any, c: any, d: any): any;
export declare function easeInOutExpo(t: any, b: any, c: any, d: any): any;
