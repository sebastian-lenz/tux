export declare function easeInElastic(t: any, b: any, c: any, d: any): any;
export declare function easeOutElastic(t: any, b: any, c: any, d: any): any;
export declare function easeInOutElastic(t: any, b: any, c: any, d: any): any;
