export function linear(t, b, c, d) {
    return b + c * (t / d);
}
