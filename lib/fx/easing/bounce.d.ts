export declare function easeInBounce(t: any, b: any, c: any, d: any): any;
export declare function easeOutBounce(t: any, b: any, c: any, d: any): any;
export declare function easeInOutBounce(t: any, b: any, c: any, d: any): any;
