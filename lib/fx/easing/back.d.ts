export declare function easeInBack(t: any, b: any, c: any, d: any, s: any): any;
export declare function easeOutBack(t: any, b: any, c: any, d: any, s: any): any;
export declare function easeInOutBack(t: any, b: any, c: any, d: any, s: any): any;
