export declare function easeInQuart(t: any, b: any, c: any, d: any): any;
export declare function easeOutQuart(t: any, b: any, c: any, d: any): any;
export declare function easeInOutQuart(t: any, b: any, c: any, d: any): any;
