export declare function easeInQuad(t: any, b: any, c: any, d: any): any;
export declare function easeOutQuad(t: any, b: any, c: any, d: any): any;
export declare function easeInOutQuad(t: any, b: any, c: any, d: any): any;
