export declare function easeInSine(t: any, b: any, c: any, d: any): any;
export declare function easeOutSine(t: any, b: any, c: any, d: any): any;
export declare function easeInOutSine(t: any, b: any, c: any, d: any): any;
