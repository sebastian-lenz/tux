export declare function easeInCirc(t: any, b: any, c: any, d: any): any;
export declare function easeOutCirc(t: any, b: any, c: any, d: any): any;
export declare function easeInOutCirc(t: any, b: any, c: any, d: any): any;
