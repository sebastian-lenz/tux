/**
 * Easing function declaration.
 */
export declare type EasingFunction = {
    (time: number, base: number, change: number, duration: number): number;
};
