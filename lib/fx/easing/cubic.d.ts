export declare function easeInCubic(t: any, b: any, c: any, d: any): any;
export declare function easeOutCubic(t: any, b: any, c: any, d: any): any;
export declare function easeInOutCubic(t: any, b: any, c: any, d: any): any;
