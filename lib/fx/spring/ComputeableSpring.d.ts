import { IComputableType } from '../../geom/Type';
import { Accessor } from '../accessor/Accessor';
import { Spring, ISpringOptions } from './Spring';
/**
 * A spring animation that operates on a computable type.
 */
export declare class ComputeableSpring extends Spring<IComputableType> {
    /**
     * Initialize this spring. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    protected initialize(options: ISpringOptions<IComputableType>): void;
    /**
     * Test whether the given values equal each other.
     *
     * @param oldValue
     *   The old value to test.
     * @param newValue
     *   The new value to test.
     * @returns
     *   TRUE if the two given values differ, FALSE otherwise.
     */
    protected areValuesEqual(oldValue: IComputableType, newValue: IComputableType): boolean;
    /**
     * Advance the spring simulation, update the current value of the spring.
     *
     * @param progress
     *   The desired progress to apply based upon the current frame time.
     * @returns
     *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
     */
    protected advance(progress: number): boolean;
    /**
     * Try to create an instance of this spring plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired spring constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the spring plugin class if applicable, NULL otherwise.
     */
    static tryCreate(accessor: Accessor, options: ISpringOptions<any>, initialValue: any): ComputeableSpring;
}
