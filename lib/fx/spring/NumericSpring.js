import * as tslib_1 from "tslib";
import { plugin } from '../../services/PluginManager';
import { Spring } from './Spring';
/**
 * A spring that operates on a numeric value.
 */
var NumericSpring = NumericSpring_1 = (function (_super) {
    tslib_1.__extends(NumericSpring, _super);
    function NumericSpring() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Initialize this spring. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    NumericSpring.prototype.initialize = function (options) {
        this.velocity = 0;
    };
    /**
     * Advance the spring simulation, update the current value of the spring.
     *
     * @param progress
     *   The desired progress to apply based upon the current frame time.
     * @returns
     *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
     */
    NumericSpring.prototype.advance = function (progress) {
        var delta = this.targetValue - this.currentValue;
        if (Math.abs(delta) < this.epsilon) {
            this.currentValue = this.targetValue;
            return false;
        }
        else {
            this.velocity = (this.velocity * this.damp + delta * this.acceleration);
            this.currentValue += this.velocity;
            return true;
        }
    };
    /**
     * Try to create an instance of this spring plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired spring constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the spring plugin class if applicable, NULL otherwise.
     */
    NumericSpring.tryCreate = function (accessor, options, initialValue) {
        if (typeof initialValue !== 'number') {
            return null;
        }
        if (typeof options.to !== 'number') {
            throw new Error('Type mismatch: Spring target value must be numeric.');
        }
        return new NumericSpring_1(accessor, options);
    };
    return NumericSpring;
}(Spring));
NumericSpring = NumericSpring_1 = tslib_1.__decorate([
    plugin('spring', { tryCreate: NumericSpring_1.tryCreate })
], NumericSpring);
export { NumericSpring };
var NumericSpring_1;
