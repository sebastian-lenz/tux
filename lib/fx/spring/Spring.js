import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import { Accessor } from '../accessor/Accessor';
import { Animation } from '../Animation';
import PluginManager from '../../services/PluginManager';
import { AnimationManager } from '../AnimationManager';
/**
 * The plugin group used by the spring factory.
 */
var pluginGroup = PluginManager
    .getInstance()
    .getGroup('spring');
/**
 * Default options applied to all spring animations.
 */
export var defaultSpringOptions = {
    to: void 0,
    damp: 0.3,
    acceleration: 0.15,
    epsilon: 0.001
};
/**
 * A spring based animation.
 */
var Spring = (function (_super) {
    tslib_1.__extends(Spring, _super);
    /**
     * Spring constructor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired spring options.
     */
    function Spring(accessor, options) {
        var _this = _super.call(this, accessor, options) || this;
        _this.currentValue = options.from ? options.from : accessor.getValue();
        _this.state = 1 /* Playing */;
        options = defaults(options, defaultSpringOptions);
        _this.initialize(options);
        _this.proceed(options);
        return _this;
    }
    /**
     * Initialize this spring. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    Spring.prototype.initialize = function (options) { };
    /**
     * Test whether the given values equal each other.
     *
     * @param oldValue
     *   The old value to test.
     * @param newValue
     *   The new value to test.
     * @returns
     *   TRUE if the two given values differ, FALSE otherwise.
     */
    Spring.prototype.areValuesEqual = function (oldValue, newValue) {
        return oldValue == newValue;
    };
    /**
     * Continue this spring animations with the given options.
     *
     * If the animation was finished or stopped it will be added to the list of
     * active animations again. When reusing an old spring instance keep in mind
     * that callbacks of the promise might not work as expected.
     *
     * @param options
     *   The desired spring options.
     */
    Spring.prototype.proceed = function (options) {
        this.targetValue = options.to;
        if (options.acceleration !== void 0) {
            this.acceleration = options.acceleration;
        }
        if (options.damp !== void 0) {
            this.damp = options.damp;
        }
        if (options.epsilon !== void 0) {
            this.epsilon = options.epsilon;
        }
        if (this.state != 1 /* Playing */ && !this.areValuesEqual(this.targetValue, options.to)) {
            this.state = 1 /* Playing */;
            AnimationManager.getInstance().add(this);
        }
    };
    /**
     * Update this animation.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @param timeScale
     *   The relative length of the last frame (timeStep / 16).
     * @returns
     *   TRUE if this animation continues, FALSE if it has stopped.
     */
    Spring.prototype.update = function (timeStep, timeScale) {
        if (this.state & 6 /* FinishedOrStopped */) {
            return false;
        }
        if (this.advance(timeScale)) {
            this.accessor.setValue(this.currentValue);
            return true;
        }
        else {
            this.accessor.setValue(this.targetValue);
            this.setState(2 /* Finished */);
            return false;
        }
    };
    /**
     * Create a new spring for the given accessor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired spring options.
     * @returns
     *   A spring instance or null if no spring could be created.
     */
    Spring.create = function (accessor, options) {
        var initialValue = accessor.getValue();
        for (var _i = 0, _a = pluginGroup.plugins; _i < _a.length; _i++) {
            var plugin = _a[_i];
            var spring = plugin.tryCreate(accessor, options, initialValue);
            if (spring != null) {
                return spring;
            }
        }
        return null;
    };
    return Spring;
}(Animation));
export { Spring };
export function retrieveSpring(target, property) {
    var manager = AnimationManager.getInstance();
    for (var _i = 0, _a = manager.animations; _i < _a.length; _i++) {
        var animation = _a[_i];
        var accessor = animation.accessor;
        if (accessor.target == target && accessor.property == property) {
            if (animation instanceof Spring) {
                return animation;
            }
            else {
                return null;
            }
        }
    }
    return null;
}
/**
 * Create a new spring for the given property on the target object.
 *
 * @param target
 *   The object whose property should be animated.
 * @param property
 *   The property that should be animated.
 * @param options
 *   The desired spring options.
 * @returns
 *   A spring instance.
 */
export function createSpring(target, property, options) {
    var accessor = Accessor.create(target, property);
    if (accessor == null) {
        throw new Error("Could not create an accessor for the property \"" + property + "\" on \"" + target + "\", did you import all necessary accessor classes?");
    }
    var spring = Spring.create(accessor, options);
    if (spring == null) {
        throw new Error("Could not create a spring for the property \"" + property + "\" on \"" + target + "\", did you import all necessary spring classes?");
    }
    AnimationManager.getInstance().add(spring);
    return spring;
}
