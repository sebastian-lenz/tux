import * as tslib_1 from "tslib";
import { plugin } from '../../services/PluginManager';
import { isComputableType } from '../../geom/Type';
import { Spring } from './Spring';
/**
 * A spring animation that operates on a computable type.
 */
var ComputeableSpring = ComputeableSpring_1 = (function (_super) {
    tslib_1.__extends(ComputeableSpring, _super);
    function ComputeableSpring() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Initialize this spring. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    ComputeableSpring.prototype.initialize = function (options) {
        this.velocity = this.currentValue.clone().reset();
        this.currentValue = this.currentValue.clone();
    };
    /**
     * Test whether the given values equal each other.
     *
     * @param oldValue
     *   The old value to test.
     * @param newValue
     *   The new value to test.
     * @returns
     *   TRUE if the two given values differ, FALSE otherwise.
     */
    ComputeableSpring.prototype.areValuesEqual = function (oldValue, newValue) {
        return oldValue.equals(newValue);
    };
    /**
     * Advance the spring simulation, update the current value of the spring.
     *
     * @param progress
     *   The desired progress to apply based upon the current frame time.
     * @returns
     *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
     */
    ComputeableSpring.prototype.advance = function (progress) {
        var value = this.currentValue;
        var target = this.targetValue;
        var delta = target.clone().subtract(value);
        if (delta.length() < this.epsilon) {
            value.copy(target);
            return false;
        }
        else {
            delta.scale(this.acceleration);
            value.add(this.velocity.scale(this.damp).add(delta));
            return true;
        }
    };
    /**
     * Try to create an instance of this spring plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired spring constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the spring plugin class if applicable, NULL otherwise.
     */
    ComputeableSpring.tryCreate = function (accessor, options, initialValue) {
        if (isComputableType(initialValue)) {
            var to = initialValue.convert(options.to);
            if (to == null) {
                throw new Error('Type mismatch: Spring initial and target value must be of the same type.');
            }
            options.to = to;
            return new ComputeableSpring_1(accessor, options);
        }
        else {
            return null;
        }
    };
    return ComputeableSpring;
}(Spring));
ComputeableSpring = ComputeableSpring_1 = tslib_1.__decorate([
    plugin('spring', { tryCreate: ComputeableSpring_1.tryCreate })
], ComputeableSpring);
export { ComputeableSpring };
var ComputeableSpring_1;
