import { Accessor } from '../accessor/Accessor';
import { Animation, IAnimationOptions } from '../Animation';
import { IPlugin } from '../../services/PluginManager';
import { IComputableType } from '../../geom/Type';
export declare type AnySpringDataType = number | IComputableType;
export declare type AnySpring = Spring<AnySpringDataType>;
export declare type AnyISpringOptions = ISpringOptions<AnySpringDataType>;
/**
 * Constructor options for the Spring class.
 */
export interface ISpringOptions<T> extends IAnimationOptions {
    /**
     * The initial property value.
     */
    from?: T;
    /**
     * The value the property should be animated to.
     */
    to: T;
    /**
     * The amount of velocity to keep between steps.
     */
    damp?: number;
    /**
     * The amount of power to add to the velocity each frame.
     */
    acceleration?: number;
    /**
     * The value difference the spring should stop at.
     */
    epsilon?: number;
}
/**
 * Spring plugin interface.
 */
export interface ISpringPlugin extends IPlugin {
    /**
     * Try to create an instance of this tween plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired tween constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the tween plugin class if applicable, NULL otherwise.
     */
    tryCreate(accessor: Accessor, options: ISpringOptions<any>, initialValue: any): AnySpring;
}
/**
 * Default options applied to all spring animations.
 */
export declare const defaultSpringOptions: ISpringOptions<any>;
/**
 * A spring based animation.
 */
export declare abstract class Spring<T> extends Animation {
    /**
     * The accessor used by this animation to read and write the affected property.
     */
    accessor: Accessor;
    /**
     * The current value.
     */
    currentValue: T;
    /**
     * The target value.
     */
    targetValue: T;
    /**
     * The current velocity.
     */
    velocity: T;
    /**
     * The amount of velocity to keep between steps.
     */
    damp: number;
    /**
     * The amount of power to add to the velocity each frame.
     */
    acceleration: number;
    /**
     * The value difference the spring should stop at.
     */
    epsilon: number;
    /**
     * Spring constructor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired spring options.
     */
    constructor(accessor: Accessor, options: ISpringOptions<T>);
    /**
     * Initialize this spring. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    protected initialize(options: ISpringOptions<T>): void;
    /**
     * Test whether the given values equal each other.
     *
     * @param oldValue
     *   The old value to test.
     * @param newValue
     *   The new value to test.
     * @returns
     *   TRUE if the two given values differ, FALSE otherwise.
     */
    protected areValuesEqual(oldValue: T, newValue: T): boolean;
    /**
     * Continue this spring animations with the given options.
     *
     * If the animation was finished or stopped it will be added to the list of
     * active animations again. When reusing an old spring instance keep in mind
     * that callbacks of the promise might not work as expected.
     *
     * @param options
     *   The desired spring options.
     */
    proceed(options: ISpringOptions<T>): void;
    /**
     * Update this animation.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @param timeScale
     *   The relative length of the last frame (timeStep / 16).
     * @returns
     *   TRUE if this animation continues, FALSE if it has stopped.
     */
    update(timeStep: number, timeScale: any): boolean;
    /**
     * Advance the spring simulation, update the current value of the spring.
     *
     * @param progress
     *   The desired progress to apply based upon the current frame time.
     * @returns
     *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
     */
    protected abstract advance(progress: number): boolean;
    /**
     * Create a new spring for the given accessor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired spring options.
     * @returns
     *   A spring instance or null if no spring could be created.
     */
    static create(accessor: Accessor, options: ISpringOptions<any>): AnySpring;
}
export declare function retrieveSpring(target: any, property: string): AnySpring;
/**
 * Create a new spring for the given property on the target object.
 *
 * @param target
 *   The object whose property should be animated.
 * @param property
 *   The property that should be animated.
 * @param options
 *   The desired spring options.
 * @returns
 *   A spring instance.
 */
export declare function createSpring(target: any, property: string, options: ISpringOptions<any>): AnySpring;
