import * as Q from 'q';
/**
 * Base class of all animations.
 */
var Animation = (function () {
    /**
     * Animation constructor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired animation options.
     */
    function Animation(accessor, options) {
        /**
         * The current state the animation is in.
         */
        this.state = 0 /* Idle */;
        this.accessor = accessor;
        this.finishedCallback = options.finished;
        this.stoppedCallback = options.stopped;
        this.removedCallback = options.removed;
    }
    /**
     * Update the current state of this tween.
     *
     * @param state
     *   The new state this tween is in.
     */
    Animation.prototype.setState = function (state) {
        if (this.state == state)
            return;
        if (this.state & 6 /* FinishedOrStopped */)
            return;
        this.state = state;
        if (state == 2 /* Finished */ && this.finishedCallback) {
            this.finishedCallback();
        }
        if (state == 4 /* Stopped */ && this.stoppedCallback) {
            this.stoppedCallback();
        }
        if (state & 6 /* FinishedOrStopped */ && this.removedCallback) {
            this.removedCallback();
        }
        if (this.deferred) {
            if (state == 2 /* Finished */) {
                this.deferred.resolve();
            }
            else if (state == 4 /* Stopped */) {
                this.deferred.reject(null);
            }
        }
    };
    /**
     * Return a promise for this animation.
     *
     * The promise will be resolved if the animations ends normally. If the tween
     * becomes interrupted the promise will be rejected.
     *
     * @returns
     *   A promise that will resolve after the animation has finished.
     */
    Animation.prototype.getPromise = function () {
        if (!this.deferred) {
            this.deferred = Q.defer();
        }
        return this.deferred.promise;
    };
    /**
     * Stop this animation.
     */
    Animation.prototype.stop = function () {
        this.setState(4 /* Stopped */);
    };
    return Animation;
}());
export { Animation };
