import { IPlugin } from '../../services/PluginManager';
/**
 * Accessor plugin definition.
 */
export interface IAccessorPlugin extends IPlugin {
    isApplicable: {
        (target: any, property: string): boolean;
    };
    priority?: number;
}
/**
 * Base class of all property accessors.
 */
export declare abstract class Accessor {
    /**
     * The target object the property should be fetched from.
     */
    target: any;
    /**
     * The name of the property that should be accessed.
     */
    property: string;
    /**
     * Accessor constructor.
     *
     * @param target
     *   The target object the property should be fetched from.
     * @param property
     *   The name of the property that should be accessed.
     */
    constructor(target: any, property: string);
    /**
     * Return the raw value of this accessor.
     *
     * @returns
     *   The raw property value.
     */
    abstract getValue(): any;
    /**
     * Set the raw value of this accessor.
     *
     * @param value
     *   The raw value that should be set.
     */
    abstract setValue(value: any): any;
    /**
     * Test whether the given accessor equals this accessor.
     *
     * @param other
     *   The other accessor that should be tested against.
     * @returns
     *   TRUE if this and the other accessor affect the same object and
     *   property, FALSE otherwise.
     */
    equals(other: Accessor): boolean;
    /**
     * Create a new accessor for the given property on the target object.
     *
     * @param target
     *   The target object the property should be fetched from.
     * @param property
     *   The name of the property that should be accessed.
     * @returns
     *   An accessor instance or null if no accessor could be created.
     */
    static create(target: any, property: string): Accessor;
}
