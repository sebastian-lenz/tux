import { Accessor } from "./Accessor";
/**
 * An accessor that maps properties that are directly accessed.
 */
export declare class PropertyAccessor extends Accessor {
    /**
     * Return the raw value of this accessor.
     *
     * @returns
     *   The raw property value.
     */
    getValue(): any;
    /**
     * Set the raw value of this accessor.
     *
     * @param value
     *   The raw value that should be set.
     */
    setValue(value: any): void;
    /**
     * Test whether this accessor is applicable on the given target and property.
     *
     * @param target
     *   The target object that should be tested.
     * @param property
     *   The property name that should be tested.
     * @returns
     *   TRUE if this accessor class can access the property, FALSE otherwise.
     */
    static isApplicable(target: any, property: string): boolean;
}
