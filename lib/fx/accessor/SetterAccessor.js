import * as tslib_1 from "tslib";
import { plugin } from "../../services/PluginManager";
import { Accessor } from "./Accessor";
/**
 * An accessor that maps properties that can be directly read but have a setter method.
 */
var SetterAccessor = SetterAccessor_1 = (function (_super) {
    tslib_1.__extends(SetterAccessor, _super);
    /**
     * Accessor constructor.
     *
     * @param target
     *   The target object the property should be fetched from.
     * @param property
     *   The name of the property that should be accessed.
     */
    function SetterAccessor(target, property) {
        var _this = _super.call(this, target, property) || this;
        _this.setter = SetterAccessor_1.getMethod(target, property, 'set');
        return _this;
    }
    /**
     * Return the raw value of this accessor.
     *
     * @returns
     *   The raw property value.
     */
    SetterAccessor.prototype.getValue = function () {
        return this.target[this.property];
    };
    /**
     * Set the raw value of this accessor.
     *
     * @param value
     *   The raw value that should be set.
     */
    SetterAccessor.prototype.setValue = function (value) {
        this.setter.call(this.target, value);
    };
    /**
     * Test whether this accessor is applicable on the given target and property.
     *
     * @param target
     *   The target object that should be tested.
     * @param property
     *   The property name that should be tested.
     * @returns
     *   TRUE if this accessor class can access the property, FALSE otherwise.
     */
    SetterAccessor.isApplicable = function (target, property) {
        return !!SetterAccessor_1.getMethod(target, property, 'set');
    };
    /**
     * Return the method with the given name of the target object.
     *
     * @param target
     *   The target object whose method should be returned.
     * @param name
     *   The name of the method to retrieve.
     * @param prefix
     *   An optional prefix for the given name.
     * @returns
     *   The found method or null if the method does not exist.
     */
    SetterAccessor.getMethod = function (target, name, prefix) {
        if (prefix) {
            name = SetterAccessor_1.getMethodName(prefix, name);
        }
        if (!target[name]) {
            return null;
        }
        var method = target[name];
        if (typeof method === 'function') {
            return method;
        }
        else {
            return null;
        }
    };
    /**
     * Return the name of an accessor method.
     *
     * @param prefix
     *   The prefix of the method name, e.g. 'get'.
     * @param name
     *   The name of the matching property.
     * @return
     *   The name of the accessor method.
     */
    SetterAccessor.getMethodName = function (prefix, name) {
        return prefix + name.charAt(0).toUpperCase() + name.slice(1);
    };
    return SetterAccessor;
}(Accessor));
SetterAccessor = SetterAccessor_1 = tslib_1.__decorate([
    plugin('accessor', { priority: -5, isApplicable: SetterAccessor_1.isApplicable })
], SetterAccessor);
export { SetterAccessor };
var SetterAccessor_1;
