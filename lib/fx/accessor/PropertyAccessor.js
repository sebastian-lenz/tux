import * as tslib_1 from "tslib";
import { plugin } from "../../services/PluginManager";
import { Accessor } from "./Accessor";
/**
 * An accessor that maps properties that are directly accessed.
 */
var PropertyAccessor = PropertyAccessor_1 = (function (_super) {
    tslib_1.__extends(PropertyAccessor, _super);
    function PropertyAccessor() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Return the raw value of this accessor.
     *
     * @returns
     *   The raw property value.
     */
    PropertyAccessor.prototype.getValue = function () {
        return this.target[this.property];
    };
    /**
     * Set the raw value of this accessor.
     *
     * @param value
     *   The raw value that should be set.
     */
    PropertyAccessor.prototype.setValue = function (value) {
        this.target[this.property] = value;
    };
    /**
     * Test whether this accessor is applicable on the given target and property.
     *
     * @param target
     *   The target object that should be tested.
     * @param property
     *   The property name that should be tested.
     * @returns
     *   TRUE if this accessor class can access the property, FALSE otherwise.
     */
    PropertyAccessor.isApplicable = function (target, property) {
        return target.hasOwnProperty(property);
    };
    return PropertyAccessor;
}(Accessor));
PropertyAccessor = PropertyAccessor_1 = tslib_1.__decorate([
    plugin('accessor', { priority: -10, isApplicable: PropertyAccessor_1.isApplicable })
], PropertyAccessor);
export { PropertyAccessor };
var PropertyAccessor_1;
