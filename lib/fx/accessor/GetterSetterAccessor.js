import * as tslib_1 from "tslib";
import { plugin } from "../../services/PluginManager";
import { SetterAccessor } from "./SetterAccessor";
/**
 * An accessor that maps properties that are accessed through get and set methods.
 */
var GetterSetterAccessor = GetterSetterAccessor_1 = (function (_super) {
    tslib_1.__extends(GetterSetterAccessor, _super);
    /**
     * Accessor constructor.
     *
     * @param target
     *   The target object the property should be fetched from.
     * @param property
     *   The name of the property that should be accessed.
     */
    function GetterSetterAccessor(target, property) {
        var _this = _super.call(this, target, property) || this;
        _this.getter = SetterAccessor.getMethod(target, property, 'get');
        return _this;
    }
    /**
     * Return the raw value of this accessor.
     *
     * @returns
     *   The raw property value.
     */
    GetterSetterAccessor.prototype.getValue = function () {
        return this.getter.call(this.target);
    };
    /**
     * Test whether this accessor is applicable on the given target and property.
     *
     * @param target
     *   The target object that should be tested.
     * @param property
     *   The property name that should be tested.
     * @returns
     *   TRUE if this accessor class can access the property, FALSE otherwise.
     */
    GetterSetterAccessor.isApplicable = function (target, property) {
        var getter = SetterAccessor.getMethod(target, property, 'get');
        var setter = SetterAccessor.getMethod(target, property, 'set');
        return !!getter && !!setter;
    };
    return GetterSetterAccessor;
}(SetterAccessor));
GetterSetterAccessor = GetterSetterAccessor_1 = tslib_1.__decorate([
    plugin('accessor', { isApplicable: GetterSetterAccessor_1.isApplicable })
], GetterSetterAccessor);
export { GetterSetterAccessor };
var GetterSetterAccessor_1;
