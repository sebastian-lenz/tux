import { SetterAccessor } from "./SetterAccessor";
/**
 * An accessor that maps properties that are accessed through get and set methods.
 */
export declare class GetterSetterAccessor extends SetterAccessor {
    /**
     * The function used to retrieve the property value.
     */
    getter: Function;
    /**
     * Accessor constructor.
     *
     * @param target
     *   The target object the property should be fetched from.
     * @param property
     *   The name of the property that should be accessed.
     */
    constructor(target: any, property: string);
    /**
     * Return the raw value of this accessor.
     *
     * @returns
     *   The raw property value.
     */
    getValue(): any;
    /**
     * Test whether this accessor is applicable on the given target and property.
     *
     * @param target
     *   The target object that should be tested.
     * @param property
     *   The property name that should be tested.
     * @returns
     *   TRUE if this accessor class can access the property, FALSE otherwise.
     */
    static isApplicable(target: any, property: string): boolean;
}
