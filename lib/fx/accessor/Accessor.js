import PluginManager from '../../services/PluginManager';
/**
 * The plugin group used by the accessor factory.
 */
var pluginGroup = PluginManager
    .getInstance()
    .getGroup('accessor');
/**
 * Base class of all property accessors.
 */
var Accessor = (function () {
    /**
     * Accessor constructor.
     *
     * @param target
     *   The target object the property should be fetched from.
     * @param property
     *   The name of the property that should be accessed.
     */
    function Accessor(target, property) {
        this.target = target;
        this.property = property;
    }
    /**
     * Test whether the given accessor equals this accessor.
     *
     * @param other
     *   The other accessor that should be tested against.
     * @returns
     *   TRUE if this and the other accessor affect the same object and
     *   property, FALSE otherwise.
     */
    Accessor.prototype.equals = function (other) {
        return this.target == other.target && this.property == other.property;
    };
    /**
     * Create a new accessor for the given property on the target object.
     *
     * @param target
     *   The target object the property should be fetched from.
     * @param property
     *   The name of the property that should be accessed.
     * @returns
     *   An accessor instance or null if no accessor could be created.
     */
    Accessor.create = function (target, property) {
        var pluginClass = null;
        var priority = 0;
        for (var _i = 0, _a = pluginGroup.plugins; _i < _a.length; _i++) {
            var plugin = _a[_i];
            if (plugin.isApplicable(target, property)) {
                var pluginPriority = plugin.priority ? plugin.priority : 0;
                if (!pluginClass || priority < pluginPriority) {
                    pluginClass = plugin.pluginClass;
                    priority = pluginPriority;
                }
            }
        }
        if (pluginClass) {
            return new pluginClass(target, property);
        }
        else {
            return null;
        }
    };
    return Accessor;
}());
export { Accessor };
