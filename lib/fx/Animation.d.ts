/// <reference types="q" />
import * as Q from 'q';
import { Accessor } from './accessor';
/**
 * Common interface for all animation implementations.
 */
export interface IAnimation {
    /**
     * The accessor used by this animation to read and write the affected property.
     */
    accessor: Accessor;
    /**
     * Stop this animation.
     */
    stop(): any;
    /**
     * Update this animation.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @param timeScale
     *   The relative length of the last frame (timeStep / 16).
     * @returns
     *   TRUE if this animation continues, FALSE if it has stopped.
     */
    update(timeStep: number, timeScale: number): boolean;
}
/**
 * Constructor options for the class Animation.
 */
export interface IAnimationOptions {
    /**
     * A callback that will be invoked once the animation has finished.
     */
    finished?: Function;
    /**
     * A callback that will be invoked once the animation has been stopped.
     */
    stopped?: Function;
    /**
     * A callback that will be invoked once the animation is removed from the manager.
     */
    removed?: Function;
}
/**
 * Defines the states an animation can be in.
 */
export declare const enum AnimationState {
    Idle = 0,
    Playing = 1,
    Finished = 2,
    Stopped = 4,
    FinishedOrStopped = 6,
}
/**
 * Base class of all animations.
 */
export declare abstract class Animation implements IAnimation {
    /**
     * The accessor used by this animation to read and write the affected property.
     */
    accessor: Accessor;
    /**
     * The current state the animation is in.
     */
    state: AnimationState;
    /**
     * A callback that will be invoked once the animation has finished.
     */
    finishedCallback: Function;
    /**
     * A callback that will be invoked once the animation has been stopped.
     */
    stoppedCallback: Function;
    /**
     * A callback that will be invoked once the animation is removed from the manager.
     */
    removedCallback: Function;
    /**
     * Deferred object used to create promises for this tween.
     */
    protected deferred: Q.Deferred<any>;
    /**
     * Animation constructor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired animation options.
     */
    constructor(accessor: Accessor, options: IAnimationOptions);
    /**
     * Update the current state of this tween.
     *
     * @param state
     *   The new state this tween is in.
     */
    protected setState(state: AnimationState): void;
    /**
     * Return a promise for this animation.
     *
     * The promise will be resolved if the animations ends normally. If the tween
     * becomes interrupted the promise will be rejected.
     *
     * @returns
     *   A promise that will resolve after the animation has finished.
     */
    getPromise(): Q.Promise<any>;
    /**
     * Stop this animation.
     */
    stop(): void;
    /**
     * Update this animation.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @param timeScale
     *   The relative length of the last frame (timeStep / 16).
     * @returns
     *   TRUE if this animation continues, FALSE if it has stopped.
     */
    abstract update(timeStep: number, timeScale: number): boolean;
}
