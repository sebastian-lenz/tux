import { IVectorType } from '../../geom/Type';
import { Momentum, IMomentumOptions } from './Momentum';
import { Axis } from './Axis';
import { Accessor } from '../accessor/Accessor';
/**
 * A momentum animation that handles vector types.
 */
export declare class VectorMomentum extends Momentum<IVectorType> {
    /**
     * The axes used by this momentum.
     */
    axes: Axis[];
    /**
     * Initialize this momentum. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    protected initialize(options: IMomentumOptions<IVectorType>): void;
    /**
     * Advance the momentum animation and update the current value.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @returns
     *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
     */
    protected advance(timeStep: number): boolean;
    /**
     * Try to create an instance of this momentum plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired momentum constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the momentum plugin class if applicable, NULL otherwise.
     */
    static tryCreate(accessor: Accessor, options: IMomentumOptions<any>, initialValue: any): VectorMomentum;
}
