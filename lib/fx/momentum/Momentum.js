import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import PluginManager from '../../services/PluginManager';
import { Accessor } from '../accessor';
import { easeOutExpo } from '../easing/expo';
import { Animation } from '../Animation';
import { AnimationManager } from '../AnimationManager';
/**
 * Default options applied to all momentum animations.
 */
export var defaultMomentumOptions = {
    velocity: void 0,
    friction: 0.95,
    deceleration: 0.05,
    epsilon: 0.05,
    bounceDuration: 1000,
    bounceEasing: easeOutExpo
};
/**
 * The plugin group used by the spring factory.
 */
var pluginGroup = PluginManager
    .getInstance()
    .getGroup('momentum');
/**
 * A momentum animation.
 */
var Momentum = (function (_super) {
    tslib_1.__extends(Momentum, _super);
    /**
     * Momentum constructor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired momentum options.
     */
    function Momentum(accessor, options) {
        var _this = _super.call(this, accessor, options) || this;
        _this.currentValue = accessor.getValue();
        _this.state = 1 /* Playing */;
        options = defaults(options, defaultMomentumOptions);
        _this.initialize(options);
        return _this;
    }
    /**
     * Initialize this momentum. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    Momentum.prototype.initialize = function (options) { };
    /**
     * Update this animation.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @returns
     *   TRUE if this animation continues, FALSE if it has stopped.
     */
    Momentum.prototype.update = function (timeStep) {
        if (this.state & 6 /* FinishedOrStopped */) {
            return false;
        }
        if (this.advance(timeStep)) {
            this.accessor.setValue(this.currentValue);
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * Create a new momentum for the given accessor.
     *
     * @param accessor
     *   An accessor pointing to the property that should be animated.
     * @param options
     *   The desired momentum options.
     * @returns
     *   A momentum instance or null if no momentum could be created.
     */
    Momentum.create = function (accessor, options) {
        var initialValue = accessor.getValue();
        for (var _i = 0, _a = pluginGroup.plugins; _i < _a.length; _i++) {
            var plugin = _a[_i];
            var momentum = plugin.tryCreate(accessor, options, initialValue);
            if (momentum != null) {
                return momentum;
            }
        }
        return null;
    };
    return Momentum;
}(Animation));
export { Momentum };
export function retrieveMomentum(target, property) {
    var manager = AnimationManager.getInstance();
    for (var _i = 0, _a = manager.animations; _i < _a.length; _i++) {
        var animation = _a[_i];
        var accessor = animation.accessor;
        if (accessor.target == target && accessor.property == property) {
            if (animation instanceof Momentum) {
                return animation;
            }
            else {
                return null;
            }
        }
    }
    return null;
}
/**
 * Create a new momentum for the given property on the target object.
 *
 * @param target
 *   The object whose property should be animated.
 * @param property
 *   The property that should be animated.
 * @param options
 *   The desired momentum options.
 * @returns
 *   A momentum instance.
 */
export function createMomentum(target, property, options) {
    var accessor = Accessor.create(target, property);
    if (accessor == null) {
        throw new Error("Could not create an accessor for the property \"" + property + "\" on \"" + target + "\", did you import all necessary accessor classes?");
    }
    var momentum = Momentum.create(accessor, options);
    if (momentum == null) {
        throw new Error("Could not create a momentum for the property \"" + property + "\" on \"" + target + "\", did you import all necessary momentum classes?");
    }
    AnimationManager.getInstance().add(momentum);
    return momentum;
}
