/**
 * Calculates the momentum of a single axis.
 */
var Axis = (function () {
    /**
     * Axis constructor.
     *
     * @param options
     *   The desired momentum options.
     */
    function Axis(options) {
        /**
         * The minimum allowed position on this axis.
         * Set to NaN to bypass the minimum check.
         */
        this.minValue = Number.NaN;
        /**
         * The maximum allowed position on this axis.
         * Set to NaN to bypass the maximum check.
         */
        this.maxValue = Number.NaN;
        /**
         * The current velocity on this axis.
         */
        this.velocity = 0;
        /**
         * The current time step of the bounce animation.
         */
        this.bounceStep = 0;
        /**
         * The initial value of the bounce animation.
         */
        this.bounceInitial = Number.NaN;
        /**
         * The target value of the bounce animation.
         */
        this.bounceTarget = 0;
        /**
         * The value change of the bounce animation.
         */
        this.bounceDelta = 0;
        this.friction = options.friction;
        this.deceleration = options.deceleration;
        this.epsilon = options.epsilon;
        this.bounceDuration = options.bounceDuration;
        this.bounceEasing = options.bounceEasing;
    }
    /**
     * Advance the movement along this axis and update the position property.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @returns
     *   TRUE if this animation continues, FALSE if it has stopped.
     */
    Axis.prototype.advance = function (timeStep) {
        var max = this.maxValue;
        var min = this.minValue;
        var position = this.currentValue;
        var velocity = this.velocity;
        if (!isNaN(this.bounceInitial)) {
            this.bounceStep += timeStep;
            if (this.bounceStep >= this.bounceDuration) {
                this.currentValue = this.bounceTarget;
                return false;
            }
            else {
                this.currentValue = this.bounceEasing(this.bounceStep, this.bounceInitial, this.bounceDelta, this.bounceDuration);
                return true;
            }
        }
        if (!isNaN(max) && position > max) {
            var excess = max - position;
            if (velocity > 0) {
                velocity = (velocity + excess * this.deceleration) * this.friction;
                this.velocity = velocity;
                this.currentValue += velocity;
            }
            if (velocity <= 0) {
                this.bounceInitial = position;
                this.bounceTarget = max;
                this.bounceDelta = excess;
            }
            return true;
        }
        if (!isNaN(min) && position < min) {
            var excess = min - position;
            if (velocity < 0) {
                velocity = (velocity + excess * this.deceleration) * this.friction;
                this.velocity = velocity;
                this.currentValue += velocity;
            }
            if (velocity >= 0) {
                this.bounceInitial = position;
                this.bounceTarget = min;
                this.bounceDelta = excess;
            }
            return true;
        }
        if (Math.abs(velocity) < this.epsilon) {
            return false;
        }
        velocity *= this.friction;
        this.velocity = velocity;
        this.currentValue += velocity;
        return true;
    };
    return Axis;
}());
export { Axis };
