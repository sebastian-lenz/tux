import { EasingFunction } from '../easing/Type';
import { IMomentumOptions } from './Momentum';
/**
 * Calculates the momentum of a single axis.
 */
export declare class Axis {
    /**
     * The current position on this axis.
     */
    currentValue: number;
    /**
     * The minimum allowed position on this axis.
     * Set to NaN to bypass the minimum check.
     */
    minValue: number;
    /**
     * The maximum allowed position on this axis.
     * Set to NaN to bypass the maximum check.
     */
    maxValue: number;
    /**
     * The current velocity on this axis.
     */
    velocity: number;
    /**
     * The friction applied while decelerating.
     */
    friction: number;
    /**
     * Rate of deceleration when content has overscrolled and is slowing down before bouncing back.
     */
    deceleration: number;
    /**
     * The value difference the momentum should stop at.
     */
    epsilon: number;
    /**
     * Duration of animation when bouncing back.
     */
    bounceDuration: number;
    /**
     * The easing function that will be used to create the bounce animation.
     */
    bounceEasing: EasingFunction;
    /**
     * The current time step of the bounce animation.
     */
    bounceStep: number;
    /**
     * The initial value of the bounce animation.
     */
    bounceInitial: number;
    /**
     * The target value of the bounce animation.
     */
    bounceTarget: number;
    /**
     * The value change of the bounce animation.
     */
    bounceDelta: number;
    /**
     * Axis constructor.
     *
     * @param options
     *   The desired momentum options.
     */
    constructor(options: IMomentumOptions<any>);
    /**
     * Advance the movement along this axis and update the position property.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @returns
     *   TRUE if this animation continues, FALSE if it has stopped.
     */
    advance(timeStep: number): boolean;
}
