import * as tslib_1 from "tslib";
import { plugin } from '../../services/PluginManager';
import { isVectorType } from '../../geom/Type';
import { Momentum } from './Momentum';
import { Axis } from './Axis';
/**
 * A momentum animation that handles vector types.
 */
var VectorMomentum = VectorMomentum_1 = (function (_super) {
    tslib_1.__extends(VectorMomentum, _super);
    function VectorMomentum() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Initialize this momentum. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    VectorMomentum.prototype.initialize = function (options) {
        var values = this.currentValue.getValues();
        var velocities = options.velocity.getValues();
        var min = options.min ? options.min.getValues() : null;
        var max = options.max ? options.max.getValues() : null;
        var axes = [];
        for (var index = 0, count = values.length; index < count; index++) {
            var axis = new Axis(options);
            axis.currentValue = values[index];
            axis.minValue = min ? min[index] : Number.NaN;
            axis.maxValue = max ? max[index] : Number.NaN;
            axis.velocity = velocities[index];
            axes.push(axis);
        }
        this.axes = axes;
    };
    /**
     * Advance the momentum animation and update the current value.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @returns
     *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
     */
    VectorMomentum.prototype.advance = function (timeStep) {
        var values = [];
        var axes = this.axes;
        var continues = false;
        for (var index = 0, count = axes.length; index < count; index++) {
            var axis = axes[index];
            continues = axis.advance(timeStep) || continues;
            values.push(axis.currentValue);
        }
        this.currentValue = this.currentValue.clone().setValues(values);
        return continues;
    };
    /**
     * Try to create an instance of this momentum plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired momentum constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the momentum plugin class if applicable, NULL otherwise.
     */
    VectorMomentum.tryCreate = function (accessor, options, initialValue) {
        if (isVectorType(initialValue)) {
            var velocity = initialValue.convert(options.velocity);
            if (velocity == null) {
                throw new Error('Type mismatch: Momentum initial and velocity value must be of the same type.');
            }
            else {
                options.velocity = velocity;
            }
            if (options.min) {
                var min = initialValue.convert(options.min);
                if (min == null) {
                    throw new Error('Type mismatch: Momentum initial and minimum value must be of the same type.');
                }
                else {
                    options.min = min;
                }
            }
            if (options.max) {
                var max = initialValue.convert(options.max);
                if (max == null) {
                    throw new Error('Type mismatch: Momentum initial and maximum value must be of the same type.');
                }
                else {
                    options.max = max;
                }
            }
            return new VectorMomentum_1(accessor, options);
        }
        else {
            return null;
        }
    };
    return VectorMomentum;
}(Momentum));
VectorMomentum = VectorMomentum_1 = tslib_1.__decorate([
    plugin('momentum', { tryCreate: VectorMomentum_1.tryCreate })
], VectorMomentum);
export { VectorMomentum };
var VectorMomentum_1;
