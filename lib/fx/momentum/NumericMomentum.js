import * as tslib_1 from "tslib";
import { plugin } from '../../services/PluginManager';
import { Momentum } from './Momentum';
import { Axis } from './Axis';
/**
 * A momentum animation that operates on a single axis.
 */
var NumericMomentum = NumericMomentum_1 = (function (_super) {
    tslib_1.__extends(NumericMomentum, _super);
    function NumericMomentum() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Initialize this momentum. Called by the constructor.
     *
     * @param options
     *   The processed options passed to the constructor.
     */
    NumericMomentum.prototype.initialize = function (options) {
        var axis = new Axis(options);
        axis.currentValue = this.currentValue;
        axis.minValue = options.min;
        axis.maxValue = options.max;
        axis.velocity = options.velocity;
        this.axis = axis;
    };
    /**
     * Advance the momentum animation and update the current value.
     *
     * @param timeStep
     *   The time passed since the last update call in milliseconds.
     * @returns
     *   TRUE if the spring simulation should continue, FALSE if the animation is finished.
     */
    NumericMomentum.prototype.advance = function (timeStep) {
        var continues = this.axis.advance(timeStep);
        this.currentValue = this.axis.currentValue;
        return continues;
    };
    /**
     * Try to create an instance of this momentum plugin.
     *
     * @param accessor
     *   The accessor pointing to the property that should be animated.
     * @param options
     *   The desired momentum constructor options.
     * @param initialValue
     *   The current value of the target property.
     * @returns
     *   An instance of the momentum plugin class if applicable, NULL otherwise.
     */
    NumericMomentum.tryCreate = function (accessor, options, initialValue) {
        if (typeof initialValue !== 'number') {
            return null;
        }
        if (typeof options.velocity !== 'number') {
            throw new Error('Type mismatch: Momentum initial and velocity value must be of the same type.');
        }
        if (options.min && typeof options.min !== 'number') {
            throw new Error('Type mismatch: Momentum initial and minimum value must be of the same type.');
        }
        if (options.max && typeof options.max !== 'number') {
            throw new Error('Type mismatch: Momentum initial and maximum value must be of the same type.');
        }
        return new NumericMomentum_1(accessor, options);
    };
    return NumericMomentum;
}(Momentum));
NumericMomentum = NumericMomentum_1 = tslib_1.__decorate([
    plugin('momentum', { tryCreate: NumericMomentum_1.tryCreate })
], NumericMomentum);
export { NumericMomentum };
var NumericMomentum_1;
