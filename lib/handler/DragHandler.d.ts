import Delegate from '../views/View/Delegate';
import PointerHandler from './PointerHandler';
import Pointer, { PointerType } from './Pointer';
/**
 * Defines the states a DragHandler instance can be in.
 */
export declare const enum DragState {
    Idle = 0,
    Watching = 1,
    Draging = 2,
}
/**
 * Defines the available drag directions.
 */
export declare enum DragDirection {
    Horizontal = 1,
    Vertical = 2,
    Both = 3,
}
/**
 * A pointer handler that generates drag events.
 */
export default class DragHandler extends PointerHandler {
    /**
     * The current state the drag handler is in.
     */
    state: DragState;
    /**
     * The allowed drag direction.
     */
    direction: DragDirection;
    /**
     * The distance the pointer has to move to be seen as dragging.
     */
    threshold: number;
    constructor(view: Delegate, selector?: string, passive?: boolean);
    /**
     * Triggered after a drag has started.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the drag start.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    handleDragStart(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a drag has moved.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the move.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    handleDragMove(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a drag has ended.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    handleDragEnd(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the drag has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    handleDragCancel(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the user has clicked into the view.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    handleClick(event: Event, pointer: Pointer): void;
    /**
     * Triggered after a pointer has been pressed.
     *
     * @param event
     *   The original DOM event.
     * @param type
     *   The type of pointer that triggered the event.
     * @param x
     *   The horizontal position of the pointer.
     * @param y
     *   The vertical position of the pointer.
     * @returns
     *   A Pointer instance if the handler should become active, NULL otherwise.
     */
    handlePointerDown(event: Event, type: PointerType, x: number, y: number): Pointer;
    /**
     * Triggered after the currently handeled pointer has moved.
     *
     * @param event
     *   The dom event that triggered the event.
     * @param pointer
     *   The affected point.
     * @returns
     *   TRUE if the handler should still be active, FALSE otherwise.
     */
    handlePointerMove(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a pointer has been released.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    handlePointerUp(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the pointer has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    handlePointerCancel(event: Event, pointer: Pointer): void;
}
