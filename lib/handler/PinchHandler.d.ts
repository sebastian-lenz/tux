import View from '../views/View';
import { IVector2 } from '../geom/Vector2';
import PointerHandler from './PointerHandler';
import Pointer, { PointerType } from './Pointer';
import { DragDirection } from './DragHandler';
/**
 * Defines the states a PinchHandler instance can be in.
 */
export declare const enum PinchState {
    Idle = 0,
    Watching = 1,
    Draging = 2,
    Pinching = 3,
}
/**
 * Data structure used to store tracking points.
 */
export interface IVelocity extends IVector2 {
    distance: number;
}
export interface ITrackingPoint extends IVelocity {
    time: number;
}
/**
 * A handler that generated multi touch transforms.
 */
export default class PinchHandler extends PointerHandler {
    /**
     * The current state the handler is in.
     */
    state: PinchState;
    /**
     * The allowed drag direction.
     */
    direction: DragDirection;
    /**
     * The first pointer the handler is watching.
     */
    firstPoint: Pointer;
    /**
     * The second pointer the handler is watching.
     */
    secondPoint: Pointer;
    /**
     * The distance the pointer has to move to be seen as dragging.
     */
    threshold: number;
    /**
     * The initial position of the pointer or touch center.
     */
    initialPosition: IVector2;
    /**
     * The initial distance between the two touch points.
     */
    initialDistance: number;
    /**
     * A list of tracking points used to calculate the velocity.
     */
    track: ITrackingPoint[];
    /**
     * The duration in milliseconds tracking points should be collected.
     */
    trackPeriod: number;
    /**
     * PointerHandler constructor.
     *
     * @param view
     *   The view the handler should delegate its events to.
     * @param selector
     *   An optional css selector to filter events for.
     */
    constructor(view: View, selector?: string);
    /**
     * Return the center of the two active pointers.
     *
     * @returns
     *   An object describing the center of the two active pointers.
     */
    getPointerCenter(): IVector2;
    /**
     * Return the distance between the two active touch points.
     *
     * @returns
     *   The distance between the two active touch points.
     */
    getPointerDistance(): number;
    /**
     * Return the current velocity of the pointer.
     *
     * @returns
     *   An object defining the current velocity.
     */
    getVelocity(): IVelocity;
    /**
     * Add a tracking point for velocity calculation.
     *
     * @param x
     *   The horizontal position that should be tracked.
     * @param y
     *   The vertical position that should be tracked.
     * @param distance
     *   The distance that should be tracked.
     * @param time
     *   The current timestamp if known.
     */
    protected addTrackingPoint(x: number, y: number, distance: number, time?: number): void;
    /**
     * Remove tracking points that are no longer within the tracking period.
     *
     * @param time
     *   The current timestamp if known.
     */
    protected reviseTrackingPoints(time?: number): void;
    /**
     * Triggered if the state of the adapter is about to change.
     *
     * @param newState
     *   The new state the adapter wants to transform to.
     * @param oldState
     *   The old state the adapter was in.
     */
    handleStateChange(newState: PinchState, oldState: PinchState): void;
    /**
     * Triggered if the transform of the pinch gesture has changed.
     *
     * @param x
     *   The horizontal translation of the transform.
     * @param y
     *   The vertical translation of the transform.
     * @param scale
     *   The relative scale of the transform.
     */
    handleTransform(x: number, y: number, scale: number): void;
    /**
     * Triggered after a pointer has been pressed.
     *
     * @param event
     *   The original DOM event.
     * @param type
     *   The type of pointer that triggered the event.
     * @param x
     *   The horizontal position of the pointer.
     * @param y
     *   The vertical position of the pointer.
     * @returns
     *   A Pointer instance if the handler should become active, NULL otherwise.
     */
    handlePointerDown(event: Event, type: PointerType, x: number, y: number): Pointer;
    /**
     * Triggered after the currently handeled pointer has moved.
     *
     * @param event
     *   The dom event that triggered the event.
     * @param pointer
     *   The affected point.
     * @returns
     *   TRUE if the handler should still be active, FALSE otherwise.
     */
    handlePointerMove(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a pointer has been released.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    handlePointerUp(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the pointer has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    handlePointerCancel(event: Event, pointer: Pointer): void;
}
