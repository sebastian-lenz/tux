import '../util/polyfill/matches';
import View from '../views/View';
import PointerHandler from './PointerHandler';
import Pointer, { PointerType } from './Pointer';
export declare type ClickHandlerCallback = {
    (event: Event, target: HTMLElement, pointer: Pointer): boolean;
};
/**
 * A handler that watches for click events.
 */
export default class ClickHandler extends PointerHandler {
    /**
     * Whether the pointer is currently pressed or not.
     */
    isDown: boolean;
    /**
     * A callback that is invoked after a click has occurred.
     */
    callback: ClickHandlerCallback;
    scope: any;
    /**
     * The distance the pointer is allowed to move and still qualify as a click.
     */
    threshold: number;
    /**
     * Callback that allows filtering the down event.
     */
    downFilter: {
        (event: Event): boolean;
    };
    /**
     * ClickHandler constructor.
     *
     * @param view
     *   The view whose events should be watched.
     * @param callback
     *   A callback that is invoked after a click has occurred.
     * @param selector
     *   An optional css selector that should be applied to triggered events.
     */
    constructor(view: View, callback?: ClickHandlerCallback, selector?: string, scope?: any);
    /**
     * Triggered after a pointer has been pressed.
     *
     * @param event
     *   The original dom event.
     * @param type
     *   The type of pointer that triggered the event.
     * @param x
     *   The horizontal position of the pointer.
     * @param y
     *   The vertical position of the pointer.
     * @returns
     *   A Point instance if the handler should become active, NULL otherwise.
     */
    handlePointerDown(event: Event, type: PointerType, x: number, y: number): Pointer;
    /**
     * Triggered after the currently handeled pointer has moved.
     *
     * @param event
     *   The dom event that triggered the event.
     * @param pointer
     *   The affected pointer.
     * @returns
     *   TRUE if the handler should still be active, FALSE otherwise.
     */
    handlePointerMove(event: Event, pointer: Pointer): boolean;
    /**
     * Triggered after a pointer has been released.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    handlePointerUp(event: Event, pointer: Pointer): void;
    /**
     * Triggered after the pointer has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    handlePointerCancel(event: Event, pointer: Pointer): void;
}
