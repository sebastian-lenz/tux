/**
 * A single pointer.
 */
var Pointer = (function () {
    /**
     * Pointer constructor.
     *
     * @param type
     *   The input type of the pointer.
     * @param x
     *   The initial horizontal position.
     * @param y
     *   The initial vertical position.
     */
    function Pointer(type, x, y) {
        /**
         * A list of tracking points used to calculate the velocity.
         */
        this.track = [];
        /**
         * The duration in milliseconds tracking points should be collected.
         */
        this.trackPeriod = 100;
        this.type = type;
        this.x = this.startX = this.lastX = x;
        this.y = this.startY = this.lastY = y;
    }
    /**
     * Set the current position of the pointer.
     *
     * @param x
     *   The current horizontal position.
     * @param y
     *   The current vertical position.
     */
    Pointer.prototype.setPosition = function (x, y) {
        this.lastX = this.x;
        this.lastY = this.y;
        this.x = x;
        this.y = y;
        if (this.trackPeriod > 0) {
            this.addTrackingPoint(x, y);
        }
    };
    /**
     * Return the relative movement of the pointer between the current
     * and previous sampled position.
     *
     * @returns
     *   An object defining the relative movement.
     */
    Pointer.prototype.getRelativeDelta = function () {
        return {
            x: this.x - this.lastX,
            y: this.y - this.lastY
        };
    };
    /**
     * Return the absolute movement of the pointer between the current
     * and initial position.
     *
     * @returns
     *   An object defining the absolute movement.
     */
    Pointer.prototype.getAbsoluteDelta = function () {
        return {
            x: this.x - this.startX,
            y: this.y - this.startY
        };
    };
    /**
     * Return the absolute distance of the pointer between the current
     * and initial position.
     *
     * @returns
     *   The absolute distance.
     */
    Pointer.prototype.getAbsoluteDistance = function () {
        var x = this.x - this.startX;
        var y = this.y - this.startY;
        return Math.sqrt(x * x + y * y);
    };
    /**
     * Return the current velocity of the pointer.
     *
     * @returns
     *   An object defining the current velocity.
     */
    Pointer.prototype.getVelocity = function () {
        this.reviseTrackingPoints();
        var count = this.track.length;
        if (count < 2) {
            return { x: 0, y: 0 };
        }
        var first = this.track[0];
        var last = this.track[count - 1];
        var duration = (last.time - first.time) / 15;
        return {
            x: (last.x - first.x) / duration,
            y: (last.y - first.y) / duration
        };
    };
    /**
     * Add a tracking point for velocity calculation.
     *
     * @param x
     *   The horizontal position that should be tracked.
     * @param y
     *   The vertical position that should be tracked.
     * @param time
     *   The current timestamp if known.
     */
    Pointer.prototype.addTrackingPoint = function (x, y, time) {
        if (time === void 0) { time = +(new Date()); }
        this.reviseTrackingPoints(time);
        this.track.push({ x: x, y: y, time: time });
    };
    /**
     * Remove tracking points that are no longer within the tracking period.
     *
     * @param time
     *   The current timestamp if known.
     */
    Pointer.prototype.reviseTrackingPoints = function (time) {
        if (time === void 0) { time = +(new Date()); }
        while (this.track.length > 0) {
            if (time - this.track[0].time <= this.trackPeriod)
                break;
            this.track.shift();
        }
    };
    return Pointer;
}());
export default Pointer;
