import * as tslib_1 from "tslib";
import DocumentView from '../services/DocumentView';
import PointerHandler from './PointerHandler';
import Pointer from './Pointer';
import { DragDirection } from './DragHandler';
/**
 * An event listener that prevents the default action.
 */
var preventDefault = function (e) { return e.preventDefault(); };
/**
 * A handler that generated multi touch transforms.
 */
var PinchHandler = (function (_super) {
    tslib_1.__extends(PinchHandler, _super);
    /**
     * PointerHandler constructor.
     *
     * @param view
     *   The view the handler should delegate its events to.
     * @param selector
     *   An optional css selector to filter events for.
     */
    function PinchHandler(view, selector) {
        var _this = _super.call(this, view, selector) || this;
        /**
         * The current state the handler is in.
         */
        _this.state = 0 /* Idle */;
        /**
         * The allowed drag direction.
         */
        _this.direction = DragDirection.Both;
        /**
         * The first pointer the handler is watching.
         */
        _this.firstPoint = null;
        /**
         * The second pointer the handler is watching.
         */
        _this.secondPoint = null;
        /**
         * The distance the pointer has to move to be seen as dragging.
         */
        _this.threshold = 5;
        /**
         * The initial position of the pointer or touch center.
         */
        _this.initialPosition = null;
        /**
         * The initial distance between the two touch points.
         */
        _this.initialDistance = 0;
        /**
         * A list of tracking points used to calculate the velocity.
         */
        _this.track = [];
        /**
         * The duration in milliseconds tracking points should be collected.
         */
        _this.trackPeriod = 100;
        view.delegate('gesturestart', preventDefault, { selector: selector });
        view.delegate('gesturechange', preventDefault, { selector: selector });
        return _this;
    }
    /**
     * Return the center of the two active pointers.
     *
     * @returns
     *   An object describing the center of the two active pointers.
     */
    PinchHandler.prototype.getPointerCenter = function () {
        if (!this.firstPoint) {
            return { x: 0, y: 0 };
        }
        else if (!this.secondPoint) {
            return { x: this.firstPoint.x, y: this.firstPoint.y };
        }
        return {
            x: (this.firstPoint.x + this.secondPoint.x) * 0.5,
            y: (this.firstPoint.y + this.secondPoint.y) * 0.5,
        };
    };
    /**
     * Return the distance between the two active touch points.
     *
     * @returns
     *   The distance between the two active touch points.
     */
    PinchHandler.prototype.getPointerDistance = function () {
        if (!this.firstPoint || !this.secondPoint) {
            return 0;
        }
        var x = this.firstPoint.x - this.secondPoint.x;
        var y = this.firstPoint.y - this.secondPoint.y;
        return Math.sqrt(x * x + y * y);
    };
    /**
     * Return the current velocity of the pointer.
     *
     * @returns
     *   An object defining the current velocity.
     */
    PinchHandler.prototype.getVelocity = function () {
        this.reviseTrackingPoints();
        var count = this.track.length;
        if (count < 2) {
            return { x: 0, y: 0, distance: 0 };
        }
        var first = this.track[0];
        var last = this.track[count - 1];
        var duration = (last.time - first.time) / 15;
        return {
            x: (last.x - first.x) / duration,
            y: (last.y - first.y) / duration,
            distance: (last.distance - first.distance) / duration,
        };
    };
    /**
     * Add a tracking point for velocity calculation.
     *
     * @param x
     *   The horizontal position that should be tracked.
     * @param y
     *   The vertical position that should be tracked.
     * @param distance
     *   The distance that should be tracked.
     * @param time
     *   The current timestamp if known.
     */
    PinchHandler.prototype.addTrackingPoint = function (x, y, distance, time) {
        if (time === void 0) { time = +(new Date()); }
        this.reviseTrackingPoints(time);
        this.track.push({ x: x, y: y, distance: distance, time: time });
    };
    /**
     * Remove tracking points that are no longer within the tracking period.
     *
     * @param time
     *   The current timestamp if known.
     */
    PinchHandler.prototype.reviseTrackingPoints = function (time) {
        if (time === void 0) { time = +(new Date()); }
        while (this.track.length > 0) {
            if (time - this.track[0].time <= this.trackPeriod)
                break;
            this.track.shift();
        }
    };
    /**
     * Triggered if the state of the adapter is about to change.
     *
     * @param newState
     *   The new state the adapter wants to transform to.
     * @param oldState
     *   The old state the adapter was in.
     */
    PinchHandler.prototype.handleStateChange = function (newState, oldState) { };
    /**
     * Triggered if the transform of the pinch gesture has changed.
     *
     * @param x
     *   The horizontal translation of the transform.
     * @param y
     *   The vertical translation of the transform.
     * @param scale
     *   The relative scale of the transform.
     */
    PinchHandler.prototype.handleTransform = function (x, y, scale) { };
    /**
     * Triggered after a pointer has been pressed.
     *
     * @param event
     *   The original DOM event.
     * @param type
     *   The type of pointer that triggered the event.
     * @param x
     *   The horizontal position of the pointer.
     * @param y
     *   The vertical position of the pointer.
     * @returns
     *   A Pointer instance if the handler should become active, NULL otherwise.
     */
    PinchHandler.prototype.handlePointerDown = function (event, type, x, y) {
        if (this.state == 3 /* Pinching */) {
            return null;
        }
        if (type == 0 /* Mouse */) {
            event.preventDefault();
        }
        var oldState = this.state;
        var point = new Pointer(type, x, y);
        if (this.state == 0 /* Idle */) {
            this.state = 1 /* Watching */;
            this.firstPoint = point;
        }
        else {
            this.state = 3 /* Pinching */;
            this.secondPoint = point;
            this.initialPosition = this.getPointerCenter();
            this.initialDistance = this.getPointerDistance();
        }
        this.handleStateChange(this.state, oldState);
        return point;
    };
    /**
     * Triggered after the currently handeled pointer has moved.
     *
     * @param event
     *   The dom event that triggered the event.
     * @param pointer
     *   The affected point.
     * @returns
     *   TRUE if the handler should still be active, FALSE otherwise.
     */
    PinchHandler.prototype.handlePointerMove = function (event, pointer) {
        var delta = pointer.getAbsoluteDelta();
        if (this.state === 1 /* Watching */) {
            if (pointer.type == 0 /* Mouse */) {
                event.preventDefault();
            }
            var distance = Math.sqrt(delta.x * delta.x + delta.y * delta.y);
            if (distance < this.threshold) {
                return true;
            }
            if (this.direction == DragDirection.Horizontal) {
                if (Math.abs(delta.x) < Math.abs(delta.y)) {
                    return false;
                }
            }
            else if (this.direction == DragDirection.Vertical) {
                if (Math.abs(delta.x) > Math.abs(delta.y)) {
                    return false;
                }
            }
            this.state = 2 /* Draging */;
            this.initialPosition = { x: pointer.x, y: pointer.y };
            this.initialDistance = 0;
            event.stopImmediatePropagation();
            this.handleStateChange(2 /* Draging */, 1 /* Watching */);
        }
        if (this.state === 2 /* Draging */) {
            event.stopImmediatePropagation();
            event.preventDefault();
            if (this.trackPeriod > 0) {
                this.addTrackingPoint(pointer.x, pointer.y, 0);
            }
            this.handleTransform(pointer.x - this.initialPosition.x, pointer.y - this.initialPosition.y, 1);
        }
        else if (this.state == 3 /* Pinching */) {
            var center = this.getPointerCenter();
            var distance_1 = this.getPointerDistance();
            if (this.trackPeriod > 0) {
                this.addTrackingPoint(center.x, center.y, distance_1 / this.initialDistance);
            }
            this.handleTransform(center.x - this.initialPosition.x, center.y - this.initialPosition.y, distance_1 / this.initialDistance);
        }
        return true;
    };
    /**
     * Triggered after a pointer has been released.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    PinchHandler.prototype.handlePointerUp = function (event, pointer) {
        var oldState = this.state;
        if (this.state == 3 /* Pinching */) {
            if (pointer == this.firstPoint) {
                this.firstPoint = this.secondPoint;
            }
            this.firstPoint.startX = this.firstPoint.x;
            this.firstPoint.startY = this.firstPoint.y;
            this.secondPoint = null;
            this.initialPosition = { x: this.firstPoint.x, y: this.firstPoint.y };
            this.state = 1 /* Watching */;
        }
        else {
            this.state = 0 /* Idle */;
        }
        this.handleStateChange(this.state, oldState);
        this.track.length = 0;
        DocumentView.getInstance().preventClick();
    };
    /**
     * Triggered after the pointer has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    PinchHandler.prototype.handlePointerCancel = function (event, pointer) {
        this.handlePointerUp(event, pointer);
    };
    return PinchHandler;
}(PointerHandler));
export default PinchHandler;
