import * as tslib_1 from "tslib";
import DocumentView from '../services/DocumentView';
import PointerHandler from './PointerHandler';
import Pointer from './Pointer';
/**
 * Defines the available drag directions.
 */
export var DragDirection;
(function (DragDirection) {
    DragDirection[DragDirection["Horizontal"] = 1] = "Horizontal";
    DragDirection[DragDirection["Vertical"] = 2] = "Vertical";
    DragDirection[DragDirection["Both"] = 3] = "Both";
})(DragDirection || (DragDirection = {}));
/**
 * A pointer handler that generates drag events.
 */
var DragHandler = (function (_super) {
    tslib_1.__extends(DragHandler, _super);
    function DragHandler(view, selector, passive) {
        if (passive === void 0) { passive = false; }
        var _this = _super.call(this, view, selector, passive) || this;
        /**
         * The current state the drag handler is in.
         */
        _this.state = 0 /* Idle */;
        /**
         * The allowed drag direction.
         */
        _this.direction = DragDirection.Both;
        /**
         * The distance the pointer has to move to be seen as dragging.
         */
        _this.threshold = 3;
        return _this;
    }
    /**
     * Triggered after a drag has started.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the drag start.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    DragHandler.prototype.handleDragStart = function (event, pointer) {
        return true;
    };
    /**
     * Triggered after a drag has moved.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the move.
     * @returns
     *   TRUE if the drag operation should be started, FALSE otherwise.
     */
    DragHandler.prototype.handleDragMove = function (event, pointer) {
        return true;
    };
    /**
     * Triggered after a drag has ended.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    DragHandler.prototype.handleDragEnd = function (event, pointer) { };
    /**
     * Triggered after the drag has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the movement.
     */
    DragHandler.prototype.handleDragCancel = function (event, pointer) { };
    /**
     * Triggered after the user has clicked into the view.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    DragHandler.prototype.handleClick = function (event, pointer) { };
    /**
     * Triggered after a pointer has been pressed.
     *
     * @param event
     *   The original DOM event.
     * @param type
     *   The type of pointer that triggered the event.
     * @param x
     *   The horizontal position of the pointer.
     * @param y
     *   The vertical position of the pointer.
     * @returns
     *   A Pointer instance if the handler should become active, NULL otherwise.
     */
    DragHandler.prototype.handlePointerDown = function (event, type, x, y) {
        if (this.state != 0 /* Idle */) {
            return null;
        }
        if (type == 0 /* Mouse */) {
            event.preventDefault();
        }
        this.state = 1 /* Watching */;
        return new Pointer(type, x, y);
    };
    /**
     * Triggered after the currently handeled pointer has moved.
     *
     * @param event
     *   The dom event that triggered the event.
     * @param pointer
     *   The affected point.
     * @returns
     *   TRUE if the handler should still be active, FALSE otherwise.
     */
    DragHandler.prototype.handlePointerMove = function (event, pointer) {
        var delta = pointer.getAbsoluteDelta();
        if (this.state === 1 /* Watching */) {
            if (pointer.type == 0 /* Mouse */) {
                event.preventDefault();
            }
            var distance = Math.sqrt(delta.x * delta.x + delta.y * delta.y);
            if (distance < this.threshold) {
                return true;
            }
            if (this.direction == DragDirection.Horizontal) {
                if (Math.abs(delta.x) < Math.abs(delta.y)) {
                    return false;
                }
            }
            else if (this.direction == DragDirection.Vertical) {
                if (Math.abs(delta.x) > Math.abs(delta.y)) {
                    return false;
                }
            }
            if (this.handleDragStart(event, pointer)) {
                event.stopImmediatePropagation();
                this.state = 2 /* Draging */;
            }
            else {
                return false;
            }
        }
        if (this.state === 2 /* Draging */) {
            event.stopImmediatePropagation();
            event.preventDefault();
            if (this.handleDragMove(event, pointer)) {
                return true;
            }
            else {
                this.state = 0 /* Idle */;
                this.handleDragCancel(event, pointer);
                return false;
            }
        }
        return false;
    };
    /**
     * Triggered after a pointer has been released.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    DragHandler.prototype.handlePointerUp = function (event, pointer) {
        if (this.state == 2 /* Draging */) {
            this.handleDragEnd(event, pointer);
            DocumentView.getInstance().preventClick();
        }
        else {
            this.handleClick(event, pointer);
        }
        this.state = 0 /* Idle */;
    };
    /**
     * Triggered after the pointer has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    DragHandler.prototype.handlePointerCancel = function (event, pointer) {
        if (this.state == 2 /* Draging */) {
            this.handleDragCancel(event, pointer);
        }
        this.state = 0 /* Idle */;
    };
    return DragHandler;
}(PointerHandler));
export default DragHandler;
