import { filter, forEach } from 'underscore';
var Handler = (function () {
    function Handler(view, selector, passive) {
        if (passive === void 0) { passive = true; }
        this.listeners = [];
        this.view = view;
        this.selector = selector;
        this.passive = passive;
    }
    /**
     * Dispose this handler.
     */
    Handler.prototype.remove = function () {
        var _this = this;
        forEach(this.listeners, function (_a) {
            var type = _a.type, listener = _a.listener;
            return _this.undelegate(type, listener);
        });
        this.listeners.length = 0;
    };
    /**
     * Delegate an event from the dom element to the given callback.
     *
     * @param type
     *   The name of the event that should be delegated.
     * @param listener
     *   The listener function that should be called.
     */
    Handler.prototype.delegate = function (type, listener, scope) {
        if (scope === void 0) { scope = this; }
        var _a = this, passive = _a.passive, selector = _a.selector, view = _a.view;
        this.listeners.push({ type: type, listener: listener });
        view.delegate(type, listener, { scope: scope, selector: selector, passive: passive });
    };
    /**
     * Undelegate an event from the dom element to the given callback.
     *
     * @param type
     *   The name of the event that should be delegated.
     * @param listener
     *   The listener function that should be called.
     */
    Handler.prototype.undelegate = function (type, listener) {
        var _a = this, selector = _a.selector, view = _a.view;
        this.listeners = filter(this.listeners, function (delegate) {
            return delegate.type !== type || delegate.listener !== listener;
        });
        view.undelegate(type, listener, { selector: selector });
    };
    return Handler;
}());
export default Handler;
