import { IVector2 } from '../geom/Vector2';
/**
 * Data structure used to store tracking points.
 */
export interface ITrackingPoint extends IVector2 {
    time: number;
}
/**
 * Defines the known pointer types.
 */
export declare const enum PointerType {
    Mouse = 0,
    Touch = 1,
    Pen = 2,
}
/**
 * A single pointer.
 */
export default class Pointer {
    /**
     * The input type of this pointer.
     */
    type: PointerType;
    /**
     * The current horizontal position.
     */
    x: number;
    /**
     * The current vertical position.
     */
    y: number;
    /**
     * The previous horizontal position.
     */
    lastX: number;
    /**
     * The previous vertical position.
     */
    lastY: number;
    /**
     * The initial horizontal position.
     */
    startX: number;
    /**
     * The initial vertical position.
     */
    startY: number;
    /**
     * A list of tracking points used to calculate the velocity.
     */
    track: ITrackingPoint[];
    /**
     * The duration in milliseconds tracking points should be collected.
     */
    trackPeriod: number;
    /**
     * Pointer constructor.
     *
     * @param type
     *   The input type of the pointer.
     * @param x
     *   The initial horizontal position.
     * @param y
     *   The initial vertical position.
     */
    constructor(type: PointerType, x: number, y: number);
    /**
     * Set the current position of the pointer.
     *
     * @param x
     *   The current horizontal position.
     * @param y
     *   The current vertical position.
     */
    setPosition(x: number, y: number): void;
    /**
     * Return the relative movement of the pointer between the current
     * and previous sampled position.
     *
     * @returns
     *   An object defining the relative movement.
     */
    getRelativeDelta(): IVector2;
    /**
     * Return the absolute movement of the pointer between the current
     * and initial position.
     *
     * @returns
     *   An object defining the absolute movement.
     */
    getAbsoluteDelta(): IVector2;
    /**
     * Return the absolute distance of the pointer between the current
     * and initial position.
     *
     * @returns
     *   The absolute distance.
     */
    getAbsoluteDistance(): number;
    /**
     * Return the current velocity of the pointer.
     *
     * @returns
     *   An object defining the current velocity.
     */
    getVelocity(): IVector2;
    /**
     * Add a tracking point for velocity calculation.
     *
     * @param x
     *   The horizontal position that should be tracked.
     * @param y
     *   The vertical position that should be tracked.
     * @param time
     *   The current timestamp if known.
     */
    protected addTrackingPoint(x: number, y: number, time?: number): void;
    /**
     * Remove tracking points that are no longer within the tracking period.
     *
     * @param time
     *   The current timestamp if known.
     */
    protected reviseTrackingPoints(time?: number): void;
}
