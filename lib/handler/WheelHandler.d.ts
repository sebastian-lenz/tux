import View from '../views/View';
import { IWheelData } from '../util/polyfill/normalizeWheel';
import Handler from './Handler';
export { IWheelData } from '../util/polyfill/normalizeWheel';
export default class WheelHandler extends Handler {
    /**
     * The view the handler should delegate its events to.
     */
    view: View;
    selector: string;
    constructor(view: View, selector?: string);
    /**
     * Dispose this handler.
     */
    remove(): void;
    /**
     * Triggered after the user has used the mouse wheel.
     *
     * @param event
     *   The original dom event.
     * @param data
     *   The normalized scroll data.
     * @returns
     *   TRUE if the event has been handled, FALSE otherwise.
     */
    handleWheel(event: Event, data: IWheelData): boolean;
    onWheel(event: Event): void;
}
