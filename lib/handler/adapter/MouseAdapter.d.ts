import Adapter from './Adapter';
import PointerHandler from '../PointerHandler';
import Pointer from '../Pointer';
/**
 * An adapter that delegates mouse events.
 */
export default class MouseAdapter extends Adapter {
    /**
     * The current mouse pointer.
     */
    pointer: Pointer;
    /**
     * Whether this adapter is currently listening for events or not.
     */
    isListening: boolean;
    /**
     * MouseAdapter constructor.
     *
     * @param handler
     *   The handler this adapter should be bound to.
     */
    constructor(handler: PointerHandler);
    /**
     * Dispose this adapter.
     */
    remove(): void;
    /**
     * Start listening for global mouse events.
     */
    startListening(): void;
    /**
     * Stop listening for global mouse events.
     */
    stopListening(): void;
    /**
     * Release the current pointer.
     */
    releasePointer(): void;
    /**
     * Triggered if a mouse button has been pressed.
     *
     * @param event
     *   The triggering mouse event.
     */
    onMouseDown(event: MouseEvent): void;
    /**
     * Triggered if the mouse has moved while the adapter is listening.
     *
     * @param event
     *   The triggering mouse event.
     */
    onMouseMove(event: MouseEvent): void;
    /**
     * Triggered if a mouse button has been released while the adapter is listening.
     *
     * @param event
     *   The triggering mouse event.
     */
    onMouseUp(event: MouseEvent): void;
}
