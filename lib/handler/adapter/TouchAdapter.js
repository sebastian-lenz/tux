import * as tslib_1 from "tslib";
import Adapter from './Adapter';
import DocumentView from '../../services/DocumentView';
/**
 * Test whether touch events are supported or not.
 */
var hasTouchEvents = 'ontouchstart' in window;
if (hasTouchEvents) {
    // iOS fix for event.preventDefault() not working
    try {
        window.addEventListener('touchmove', function () { }, {
            passive: false,
        });
    }
    catch (e) {
        window.addEventListener('touchmove', function () { });
    }
}
/**
 * An adapter that delegates touch events.
 */
var TouchAdapter = (function (_super) {
    tslib_1.__extends(TouchAdapter, _super);
    /**
     * TouchAdapter constructor.
     *
     * @param handler
     *   The handler this adapter should be bound to.
     */
    function TouchAdapter(handler) {
        var _this = _super.call(this, handler) || this;
        /**
         * Whether this adapter is currently listening for events or not.
         */
        _this.isListening = false;
        /**
         * A map of all pointers the adapter is currently tracking.
         */
        _this.pointers = {};
        /**
         * The number of pointers the adapter is currently tracking.
         */
        _this.numPointers = 0;
        handler.delegate('touchstart', _this.onTouchStart, _this);
        return _this;
    }
    /**
     * Dispose this adapter.
     */
    TouchAdapter.prototype.remove = function () {
        this.handler.undelegate('touchstart', this.onTouchStart);
        this.stopListening();
    };
    /**
     * Start listening for global touch events.
     */
    TouchAdapter.prototype.startListening = function () {
        if (this.isListening)
            return;
        this.isListening = true;
        var passive = this.handler.passive;
        var document = DocumentView.getInstance();
        document.delegate('touchmove', this.onTouchMove, { scope: this, passive: passive });
        document.delegate('touchend', this.onTouchEnd, { scope: this, passive: passive });
        document.delegate('touchcancel', this.onTouchCancel, {
            scope: this,
            passive: passive,
        });
    };
    /**
     * Stop listening for global touch events.
     */
    TouchAdapter.prototype.stopListening = function () {
        if (!this.isListening)
            return;
        this.isListening = false;
        var document = DocumentView.getInstance();
        document.undelegate('touchmove', this.onTouchMove, { scope: this });
        document.undelegate('touchend', this.onTouchEnd, { scope: this });
        document.undelegate('touchcancel', this.onTouchCancel, { scope: this });
    };
    /**
     * Release the pointer with the given identifier.
     *
     * @param identifier
     *   The identifier of the pointer that should be released.
     */
    TouchAdapter.prototype.releasePointer = function (identifier) {
        delete this.pointers[identifier];
        this.numPointers -= 1;
        if (!this.numPointers && this.isListening) {
            this.stopListening();
        }
    };
    /**
     * Iterate over all changed touches of the given event and call the given
     * callback if a matching pointer is registered.
     *
     * @param event
     *   The event whose changed touches should be iterated.
     * @param callback
     *   The callback that will be invoked for each matched pointer.
     */
    TouchAdapter.prototype.eachChangedTouch = function (event, callback) {
        var count = event.changedTouches.length;
        var points = this.pointers;
        for (var index = 0; index < count; index++) {
            var touch = event.changedTouches[index];
            if (!points[touch.identifier]) {
                continue;
            }
            callback(touch, points[touch.identifier]);
        }
    };
    /**
     * Triggered if one or more touch points have been pressed.
     *
     * @param event
     *   The triggering touch event.
     */
    TouchAdapter.prototype.onTouchStart = function (event) {
        var count = event.changedTouches.length;
        for (var index = 0; index < count; index++) {
            var touch = event.changedTouches[index];
            var point = this.handler.handlePointerDown(event, 1 /* Touch */, touch.pageX, touch.pageY);
            if (point) {
                this.pointers[touch.identifier] = point;
                this.numPointers += 1;
            }
        }
        if (this.numPointers && !this.isListening) {
            this.startListening();
        }
    };
    /**
     * Triggered if one or more touch points have moved while the adapter is listening.
     *
     * @param event
     *   The triggering touch event.
     */
    TouchAdapter.prototype.onTouchMove = function (event) {
        var _this = this;
        this.eachChangedTouch(event, function (touch, point) {
            point.setPosition(touch.pageX, touch.pageY);
            if (!_this.handler.handlePointerMove(event, point)) {
                _this.handler.handlePointerCancel(event, point);
                _this.releasePointer(touch.identifier);
            }
        });
    };
    /**
     * Triggered if one or more touch points have been released while the adapter is listening.
     *
     * @param event
     *   The triggering touch event.
     */
    TouchAdapter.prototype.onTouchEnd = function (event) {
        var _this = this;
        this.eachChangedTouch(event, function (touch, point) {
            point.setPosition(touch.pageX, touch.pageY);
            _this.handler.handlePointerUp(event, point);
            _this.releasePointer(touch.identifier);
        });
        this.handler.blockMouseEvents();
    };
    /**
     * Triggered if one or more touch points have been canceled while the adapter is listening.
     *
     * @param event
     *   The triggering touch event.
     */
    TouchAdapter.prototype.onTouchCancel = function (event) {
        var _this = this;
        this.eachChangedTouch(event, function (touch, point) {
            _this.handler.handlePointerCancel(event, point);
            _this.releasePointer(touch.identifier);
        });
        this.handler.blockMouseEvents();
    };
    /**
     * Return whether touch events are supported or not.
     *
     * @returns
     *   TRUE if touch events are supported or not, FALSE otherwise.
     */
    TouchAdapter.isSupported = function () {
        return hasTouchEvents;
    };
    return TouchAdapter;
}(Adapter));
export default TouchAdapter;
