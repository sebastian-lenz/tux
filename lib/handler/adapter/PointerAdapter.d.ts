import Adapter from './Adapter';
import PointerHandler from '../PointerHandler';
import Pointer, { PointerType } from '../Pointer';
export declare const eventDown: string;
export declare const eventMove: string;
export declare const eventUp: string;
export declare const eventCancel: string;
/**
 * An adapter that delegates pointer events.
 */
export default class PointerAdapter extends Adapter {
    /**
     * Whether this adapter is currently listening for events or not.
     */
    isListening: boolean;
    /**
     * A map of all pointers the adapter is currently tracking.
     */
    pointers: {
        [pointerId: number]: Pointer;
    };
    /**
     * The number of pointers the adapter is currently tracking.
     */
    numPointers: number;
    /**
     * PointerAdapter constructor.
     *
     * @param handler
     *   The handler this adapter should be bound to.
     */
    constructor(handler: PointerHandler);
    /**
     * Dispose this adapter.
     */
    remove(): void;
    /**
     * Start listening for global pointer events.
     */
    startListening(): void;
    /**
     * Stop listening for global pointer events.
     */
    stopListening(): void;
    /**
     * Release the pointer with the given pointer id.
     *
     * @param pointerId
     *   The id of the pointer that should be released.
     */
    releasePointer(pointerId: number): void;
    /**
     * Triggered if a pointer has been pressed.
     *
     * @param event
     *   The triggering pointer event.
     */
    onPointerDown(event: PointerEvent): void;
    /**
     * Triggered if a pointer has moved while the adapter is listening.
     *
     * @param event
     *   The triggering pointer event.
     */
    onPointerMove(event: PointerEvent): void;
    /**
     * Triggered if a pointer has been released while the adapter is listening.
     *
     * @param event
     *   The triggering pointer event.
     */
    onPointerUp(event: PointerEvent): void;
    /**
     * Triggered if a pointer has been canceled while the adapter is listening.
     *
     * @param event
     *   The triggering pointer event.
     */
    onPointerCancel(event: PointerEvent): void;
    /**
     * Convert the given type identifier to a PointerType value.
     *
     * @param type
     *   The type identifier as returned by a pointer event.
     * @returns
     *   The matching internal PointerType value.
     */
    static toPointerType(type: string): PointerType;
    /**
     * Return whether pointer events are supported or not.
     *
     * @returns
     *   TRUE if pointer events are supported or not, FALSE otherwise.
     */
    static isSupported(): boolean;
}
