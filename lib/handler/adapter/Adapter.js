var Adapter = (function () {
    /**
     * Adapter constructor.
     *
     * @param handler
     *   The handler this adapter should be bound to.
     */
    function Adapter(handler) {
        this.handler = handler;
    }
    /**
     * Dispose this adapter.
     */
    Adapter.prototype.remove = function () { };
    return Adapter;
}());
export default Adapter;
