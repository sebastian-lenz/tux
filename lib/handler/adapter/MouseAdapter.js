import * as tslib_1 from "tslib";
import Adapter from './Adapter';
import DocumentView from '../../services/DocumentView';
/**
 * An adapter that delegates mouse events.
 */
var MouseAdapter = (function (_super) {
    tslib_1.__extends(MouseAdapter, _super);
    /**
     * MouseAdapter constructor.
     *
     * @param handler
     *   The handler this adapter should be bound to.
     */
    function MouseAdapter(handler) {
        var _this = _super.call(this, handler) || this;
        /**
         * Whether this adapter is currently listening for events or not.
         */
        _this.isListening = false;
        handler.delegate('mousedown', _this.onMouseDown, _this);
        return _this;
    }
    /**
     * Dispose this adapter.
     */
    MouseAdapter.prototype.remove = function () {
        this.handler.undelegate('mousedown', this.onMouseDown);
        this.stopListening();
    };
    /**
     * Start listening for global mouse events.
     */
    MouseAdapter.prototype.startListening = function () {
        if (this.isListening)
            return;
        this.isListening = true;
        var passive = this.handler.passive;
        var document = DocumentView.getInstance();
        document.delegate('mousemove', this.onMouseMove, { scope: this, passive: passive });
        document.delegate('mouseup', this.onMouseUp, { scope: this, passive: passive });
    };
    /**
     * Stop listening for global mouse events.
     */
    MouseAdapter.prototype.stopListening = function () {
        if (!this.isListening)
            return;
        this.isListening = false;
        var document = DocumentView.getInstance();
        document.undelegate('mousemove', this.onMouseMove, { scope: this });
        document.undelegate('mouseup', this.onMouseUp, { scope: this });
    };
    /**
     * Release the current pointer.
     */
    MouseAdapter.prototype.releasePointer = function () {
        this.pointer = null;
        this.stopListening();
    };
    /**
     * Triggered if a mouse button has been pressed.
     *
     * @param event
     *   The triggering mouse event.
     */
    MouseAdapter.prototype.onMouseDown = function (event) {
        if (this.pointer || this.handler.isMouseBlocked()) {
            return;
        }
        var point = this.handler.handlePointerDown(event, 0 /* Mouse */, event.pageX, event.pageY);
        if (point) {
            event.preventDefault();
            this.pointer = point;
            this.startListening();
        }
    };
    /**
     * Triggered if the mouse has moved while the adapter is listening.
     *
     * @param event
     *   The triggering mouse event.
     */
    MouseAdapter.prototype.onMouseMove = function (event) {
        this.pointer.setPosition(event.pageX, event.pageY);
        if (!this.handler.handlePointerMove(event, this.pointer)) {
            this.handler.handlePointerCancel(event, this.pointer);
            this.releasePointer();
        }
    };
    /**
     * Triggered if a mouse button has been released while the adapter is listening.
     *
     * @param event
     *   The triggering mouse event.
     */
    MouseAdapter.prototype.onMouseUp = function (event) {
        this.pointer.setPosition(event.pageX, event.pageY);
        this.handler.handlePointerUp(event, this.pointer);
        this.releasePointer();
    };
    return MouseAdapter;
}(Adapter));
export default MouseAdapter;
