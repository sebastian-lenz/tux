import Adapter from './Adapter';
import PointerHandler from '../PointerHandler';
import Pointer from '../Pointer';
/**
 * An adapter that delegates touch events.
 */
export default class TouchAdapter extends Adapter {
    /**
     * The handler this adapter is bound to.
     */
    handler: PointerHandler;
    /**
     * Whether this adapter is currently listening for events or not.
     */
    isListening: boolean;
    /**
     * A map of all pointers the adapter is currently tracking.
     */
    pointers: {
        [identifier: number]: Pointer;
    };
    /**
     * The number of pointers the adapter is currently tracking.
     */
    numPointers: number;
    /**
     * TouchAdapter constructor.
     *
     * @param handler
     *   The handler this adapter should be bound to.
     */
    constructor(handler: PointerHandler);
    /**
     * Dispose this adapter.
     */
    remove(): void;
    /**
     * Start listening for global touch events.
     */
    startListening(): void;
    /**
     * Stop listening for global touch events.
     */
    stopListening(): void;
    /**
     * Release the pointer with the given identifier.
     *
     * @param identifier
     *   The identifier of the pointer that should be released.
     */
    releasePointer(identifier: number): void;
    /**
     * Iterate over all changed touches of the given event and call the given
     * callback if a matching pointer is registered.
     *
     * @param event
     *   The event whose changed touches should be iterated.
     * @param callback
     *   The callback that will be invoked for each matched pointer.
     */
    eachChangedTouch(event: TouchEvent, callback: {
        (touch: Touch, point: Pointer);
    }): void;
    /**
     * Triggered if one or more touch points have been pressed.
     *
     * @param event
     *   The triggering touch event.
     */
    onTouchStart(event: TouchEvent): void;
    /**
     * Triggered if one or more touch points have moved while the adapter is listening.
     *
     * @param event
     *   The triggering touch event.
     */
    onTouchMove(event: TouchEvent): void;
    /**
     * Triggered if one or more touch points have been released while the adapter is listening.
     *
     * @param event
     *   The triggering touch event.
     */
    onTouchEnd(event: TouchEvent): void;
    /**
     * Triggered if one or more touch points have been canceled while the adapter is listening.
     *
     * @param event
     *   The triggering touch event.
     */
    onTouchCancel(event: TouchEvent): void;
    /**
     * Return whether touch events are supported or not.
     *
     * @returns
     *   TRUE if touch events are supported or not, FALSE otherwise.
     */
    static isSupported(): boolean;
}
