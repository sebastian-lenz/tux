import * as tslib_1 from "tslib";
import Adapter from './Adapter';
import DocumentView from '../../services/DocumentView';
/**
 * Define the vendor prefixed event names.
 */
var eventPrefix = window.navigator.pointerEnabled ? '' : window.navigator.msPointerEnabled ? 'ms' : '';
export var eventDown = eventPrefix ? 'MSPointerDown' : 'pointerdown';
export var eventMove = eventPrefix ? 'MSPointerMove' : 'pointermove';
export var eventUp = eventPrefix ? 'MSPointerUp' : 'pointerup';
export var eventCancel = eventPrefix ? 'MSPointerCancel' : 'pointercancel';
/**
 * Test whether pointer events are supported or not.
 */
var hasPointerEvents = 'on' + eventDown in window;
/**
 * An adapter that delegates pointer events.
 */
var PointerAdapter = (function (_super) {
    tslib_1.__extends(PointerAdapter, _super);
    /**
     * PointerAdapter constructor.
     *
     * @param handler
     *   The handler this adapter should be bound to.
     */
    function PointerAdapter(handler) {
        var _this = _super.call(this, handler) || this;
        /**
         * Whether this adapter is currently listening for events or not.
         */
        _this.isListening = false;
        /**
         * A map of all pointers the adapter is currently tracking.
         */
        _this.pointers = {};
        /**
         * The number of pointers the adapter is currently tracking.
         */
        _this.numPointers = 0;
        handler.delegate(eventDown, _this.onPointerDown, _this);
        return _this;
    }
    /**
     * Dispose this adapter.
     */
    PointerAdapter.prototype.remove = function () {
        this.handler.undelegate(eventDown, this.onPointerDown);
        this.stopListening();
    };
    /**
     * Start listening for global pointer events.
     */
    PointerAdapter.prototype.startListening = function () {
        if (this.isListening)
            return;
        this.isListening = true;
        var passive = this.handler.passive;
        var document = DocumentView.getInstance();
        document.delegate(eventMove, this.onPointerMove, { scope: this, passive: passive });
        document.delegate(eventUp, this.onPointerUp, { scope: this, passive: passive });
        document.delegate(eventCancel, this.onPointerCancel, { scope: this, passive: passive });
    };
    /**
     * Stop listening for global pointer events.
     */
    PointerAdapter.prototype.stopListening = function () {
        if (!this.isListening)
            return;
        this.isListening = false;
        var document = DocumentView.getInstance();
        document.undelegate(eventMove, this.onPointerMove, { scope: this });
        document.undelegate(eventUp, this.onPointerUp, { scope: this });
        document.undelegate(eventCancel, this.onPointerCancel, { scope: this });
    };
    /**
     * Release the pointer with the given pointer id.
     *
     * @param pointerId
     *   The id of the pointer that should be released.
     */
    PointerAdapter.prototype.releasePointer = function (pointerId) {
        delete this.pointers[pointerId];
        this.numPointers -= 1;
        if (!this.numPointers && this.isListening) {
            this.stopListening();
        }
    };
    /**
     * Triggered if a pointer has been pressed.
     *
     * @param event
     *   The triggering pointer event.
     */
    PointerAdapter.prototype.onPointerDown = function (event) {
        var type = PointerAdapter.toPointerType(event.pointerType);
        var point = this.handler.handlePointerDown(event, type, event.pageX, event.pageY);
        if (point) {
            this.pointers[event.pointerId] = point;
            this.numPointers += 1;
        }
        if (this.numPointers && !this.isListening) {
            this.startListening();
        }
    };
    ;
    /**
     * Triggered if a pointer has moved while the adapter is listening.
     *
     * @param event
     *   The triggering pointer event.
     */
    PointerAdapter.prototype.onPointerMove = function (event) {
        var point = this.pointers[event.pointerId];
        if (point) {
            point.setPosition(event.pageX, event.pageY);
            if (!this.handler.handlePointerMove(event, point)) {
                this.handler.handlePointerCancel(event, point);
                this.releasePointer(event.pointerId);
            }
        }
    };
    ;
    /**
     * Triggered if a pointer has been released while the adapter is listening.
     *
     * @param event
     *   The triggering pointer event.
     */
    PointerAdapter.prototype.onPointerUp = function (event) {
        var point = this.pointers[event.pointerId];
        if (point) {
            point.setPosition(event.pageX, event.pageY);
            this.handler.handlePointerUp(event, point);
            this.releasePointer(event.pointerId);
        }
    };
    ;
    /**
     * Triggered if a pointer has been canceled while the adapter is listening.
     *
     * @param event
     *   The triggering pointer event.
     */
    PointerAdapter.prototype.onPointerCancel = function (event) {
        var point = this.pointers[event.pointerId];
        if (point) {
            this.handler.handlePointerCancel(event, point);
            this.releasePointer(event.pointerId);
        }
    };
    ;
    /**
     * Convert the given type identifier to a PointerType value.
     *
     * @param type
     *   The type identifier as returned by a pointer event.
     * @returns
     *   The matching internal PointerType value.
     */
    PointerAdapter.toPointerType = function (type) {
        switch (type) {
            case 'pen': return 2 /* Pen */;
            case 'touch': return 1 /* Touch */;
        }
        return 0 /* Mouse */;
    };
    /**
     * Return whether pointer events are supported or not.
     *
     * @returns
     *   TRUE if pointer events are supported or not, FALSE otherwise.
     */
    PointerAdapter.isSupported = function () {
        return hasPointerEvents;
    };
    return PointerAdapter;
}(Adapter));
export default PointerAdapter;
