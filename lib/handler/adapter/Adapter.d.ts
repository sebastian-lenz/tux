import PointerHandler from '../PointerHandler';
export default class Adapter {
    /**
     * The handler this adapter is bound to.
     */
    handler: PointerHandler;
    /**
     * Adapter constructor.
     *
     * @param handler
     *   The handler this adapter should be bound to.
     */
    constructor(handler: PointerHandler);
    /**
     * Dispose this adapter.
     */
    remove(): void;
}
