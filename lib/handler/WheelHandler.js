import * as tslib_1 from "tslib";
import { getWheelEventName, normalizeWheel } from '../util/polyfill/normalizeWheel';
import Handler from './Handler';
var WHEEL_EVENT = getWheelEventName();
var WheelHandler = (function (_super) {
    tslib_1.__extends(WheelHandler, _super);
    function WheelHandler(view, selector) {
        var _this = _super.call(this, view, selector) || this;
        _this.delegate(WHEEL_EVENT, _this.onWheel);
        return _this;
    }
    /**
     * Dispose this handler.
     */
    WheelHandler.prototype.remove = function () {
        this.undelegate(WHEEL_EVENT, this.onWheel);
    };
    /**
     * Triggered after the user has used the mouse wheel.
     *
     * @param event
     *   The original dom event.
     * @param data
     *   The normalized scroll data.
     * @returns
     *   TRUE if the event has been handled, FALSE otherwise.
     */
    WheelHandler.prototype.handleWheel = function (event, data) {
        return false;
    };
    WheelHandler.prototype.onWheel = function (event) {
        if (this.handleWheel(event, normalizeWheel(event))) {
            event.preventDefault();
        }
    };
    return WheelHandler;
}(Handler));
export default WheelHandler;
