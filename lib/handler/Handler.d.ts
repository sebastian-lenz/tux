import Delegate from '../views/View/Delegate';
export interface Listener {
    type: string;
    listener: EventListener;
}
export default class Handler {
    /**
     * The view the handler should delegate its events to.
     */
    view: Delegate;
    /**
     * A css selector to filter events for.
     */
    selector: string | null;
    /**
     * Whether delegates should be registeres as passive handlers.
     */
    passive: boolean;
    listeners: Listener[];
    constructor(view: Delegate, selector?: string, passive?: boolean);
    /**
     * Dispose this handler.
     */
    remove(): void;
    /**
     * Delegate an event from the dom element to the given callback.
     *
     * @param type
     *   The name of the event that should be delegated.
     * @param listener
     *   The listener function that should be called.
     */
    delegate(type: string, listener: EventListener, scope?: any): void;
    /**
     * Undelegate an event from the dom element to the given callback.
     *
     * @param type
     *   The name of the event that should be delegated.
     * @param listener
     *   The listener function that should be called.
     */
    undelegate(type: string, listener: EventListener): void;
}
