import * as tslib_1 from "tslib";
import '../util/polyfill/matches';
import DocumentView from '../services/DocumentView';
import PointerHandler from './PointerHandler';
import Pointer from './Pointer';
/**
 * A handler that watches for click events.
 */
var ClickHandler = (function (_super) {
    tslib_1.__extends(ClickHandler, _super);
    /**
     * ClickHandler constructor.
     *
     * @param view
     *   The view whose events should be watched.
     * @param callback
     *   A callback that is invoked after a click has occurred.
     * @param selector
     *   An optional css selector that should be applied to triggered events.
     */
    function ClickHandler(view, callback, selector, scope) {
        var _this = _super.call(this, view, selector) || this;
        /**
         * The distance the pointer is allowed to move and still qualify as a click.
         */
        _this.threshold = 8;
        _this.callback = callback;
        _this.scope = scope || view;
        return _this;
    }
    /**
     * Triggered after a pointer has been pressed.
     *
     * @param event
     *   The original dom event.
     * @param type
     *   The type of pointer that triggered the event.
     * @param x
     *   The horizontal position of the pointer.
     * @param y
     *   The vertical position of the pointer.
     * @returns
     *   A Point instance if the handler should become active, NULL otherwise.
     */
    ClickHandler.prototype.handlePointerDown = function (event, type, x, y) {
        if (this.isDown)
            return null;
        if (this.downFilter && !this.downFilter(event)) {
            return null;
        }
        this.isDown = true;
        return new Pointer(type, x, y);
    };
    /**
     * Triggered after the currently handeled pointer has moved.
     *
     * @param event
     *   The dom event that triggered the event.
     * @param pointer
     *   The affected pointer.
     * @returns
     *   TRUE if the handler should still be active, FALSE otherwise.
     */
    ClickHandler.prototype.handlePointerMove = function (event, pointer) {
        return pointer.getAbsoluteDistance() < this.threshold;
    };
    /**
     * Triggered after a pointer has been released.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    ClickHandler.prototype.handlePointerUp = function (event, pointer) {
        if (!this.isDown)
            return;
        this.isDown = false;
        var target = null;
        if (this.selector) {
            var element = event.target;
            while (element && element.matches) {
                if (element.matches(this.selector)) {
                    target = element;
                    break;
                }
                else {
                    element = element.parentNode;
                }
            }
        }
        if (this.callback && this.callback.call(this.scope, event, target, pointer)) {
            DocumentView.getInstance().preventClick();
        }
    };
    /**
     * Triggered after the pointer has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected pointer.
     */
    ClickHandler.prototype.handlePointerCancel = function (event, pointer) {
        this.isDown = false;
    };
    return ClickHandler;
}(PointerHandler));
export default ClickHandler;
