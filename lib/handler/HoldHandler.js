import * as tslib_1 from "tslib";
import PointerHandler from './PointerHandler';
import Pointer from './Pointer';
/**
 * Defines all states the HoldHandler can be in.
 */
export var HoldState;
(function (HoldState) {
    HoldState[HoldState["Idle"] = 0] = "Idle";
    HoldState[HoldState["Pressed"] = 1] = "Pressed";
    HoldState[HoldState["Holding"] = 2] = "Holding";
})(HoldState || (HoldState = {}));
/**
 * A handler that watches for long button presses.
 */
var HoldHandler = (function (_super) {
    tslib_1.__extends(HoldHandler, _super);
    function HoldHandler() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * The current state the handler is in.
         */
        _this.state = HoldState.Idle;
        /**
         * Minimum amount of time in milliseconds until a pointer press is qualified as a hold action.
         */
        _this.initialDelay = 400;
        /**
         * Timeout handle of the initial delay after the user has pressed the pointer.
         */
        _this.initialDelayHandle = Number.NaN;
        /**
         * The distance the pointer is allowed to move and still qualify as a click.
         */
        _this.threshold = 5;
        return _this;
    }
    /**
     * Clear all running timeouts and intervals.
     */
    HoldHandler.prototype.clearAllHandles = function () {
        if (!isNaN(this.initialDelayHandle)) {
            clearTimeout(this.initialDelayHandle);
            this.initialDelayHandle = Number.NaN;
        }
    };
    /**
     * Triggered after the user has pressed the pointer a minimum amount of
     * time over the view. Note that the given event is the initial start event.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     * @return
     *   TRUE if the event should be seen as a hold action, FALSE to abort the action.
     */
    HoldHandler.prototype.handleHoldStart = function (event, pointer) {
        return true;
    };
    /**
     * Triggered after the user has releases a pointer hold action.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    HoldHandler.prototype.handleHoldEnd = function (event, pointer) { };
    /**
     * Triggered after a hold action has been canceled.
     *
     * Delegated to handleHoldEnd by default.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    HoldHandler.prototype.handleHoldCancel = function (event, pointer) {
        this.handleHoldEnd(event, pointer);
    };
    /**
     * Triggered after the user has pressed and released the pointer ere the
     * minimum amount of time to be qualified as a press.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The pointer which triggered the click.
     */
    HoldHandler.prototype.handleClick = function (event, pointer) { };
    /**
     * Triggered after a pointer has been pressed.
     *
     * @param event
     *   The original DOM event.
     * @param type
     *   The type of pointer that triggered the event.
     * @param x
     *   The horizontal position of the pointer.
     * @param y
     *   The vertical position of the pointer.
     * @returns
     *   A Pointer instance if the handler should become active, NULL otherwise.
     */
    HoldHandler.prototype.handlePointerDown = function (event, type, x, y) {
        var _this = this;
        if (this.state != HoldState.Idle)
            return null;
        this.state = HoldState.Pressed;
        var pointer = new Pointer(type, x, y);
        this.clearAllHandles();
        this.initialDelayHandle = setTimeout(function () {
            _this.initialDelayHandle = Number.NaN;
            if (_this.handleHoldStart(event, pointer)) {
                _this.state = HoldState.Holding;
            }
            else {
                _this.state = HoldState.Idle;
            }
        }, this.initialDelay);
        return pointer;
    };
    /**
     * Triggered after the currently handled pointer has moved.
     *
     * @param event
     *   The dom event that triggered the event.
     * @param pointer
     *   The affected point.
     * @returns
     *   TRUE if the handler should still be active, FALSE otherwise.
     */
    HoldHandler.prototype.handlePointerMove = function (event, pointer) {
        return pointer.getAbsoluteDistance() < this.threshold;
    };
    /**
     * Triggered after a pointer has been released.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected point.
     */
    HoldHandler.prototype.handlePointerUp = function (event, pointer) {
        if (this.state == HoldState.Pressed) {
            this.handleClick(event, pointer);
        }
        else if (this.state == HoldState.Holding) {
            this.handleHoldEnd(event, pointer);
        }
        this.clearAllHandles();
        this.state = HoldState.Idle;
    };
    /**
     * Triggered after the pointer has been canceled.
     *
     * @param event
     *   The original dom event.
     * @param pointer
     *   The affected point.
     */
    HoldHandler.prototype.handlePointerCancel = function (event, pointer) {
        if (this.state == HoldState.Holding) {
            this.handleHoldCancel(event, pointer);
        }
        this.clearAllHandles();
        this.state = HoldState.Idle;
    };
    return HoldHandler;
}(PointerHandler));
export default HoldHandler;
