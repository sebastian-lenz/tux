import Events from '../../Events';
import '../../util/polyfill/animationFrame';
/**
 * A service that triggers a frame event.
 *
 * @event "frame"
 *   Triggered on each browser repaint.
 */
export default class Dispatcher extends Events {
    /**
     * Timestamp of the last frame.
     */
    lastTime: number;
    /**
     * Dispatcher constructor.
     */
    constructor();
    /**
     * Triggered for each browser repaint.
     */
    onFrame: (time: number) => void;
    /**
     * Return the current service instance of the Dispatcher.
     *
     * @returns
     *   The current service instance of the Dispatcher.
     */
    static getInstance(): Dispatcher;
}
