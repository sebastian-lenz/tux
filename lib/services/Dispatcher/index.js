import * as tslib_1 from "tslib";
import Events from '../../Events';
import services, { service } from '../index';
import { whenDomReady } from '../../util/domready';
import '../../util/polyfill/animationFrame';
/**
 * A service that triggers a frame event.
 *
 * @event "frame"
 *   Triggered on each browser repaint.
 */
var Dispatcher = Dispatcher_1 = (function (_super) {
    tslib_1.__extends(Dispatcher, _super);
    /**
     * Dispatcher constructor.
     */
    function Dispatcher() {
        var _this = _super.call(this) || this;
        /**
         * Triggered for each browser repaint.
         */
        _this.onFrame = function (time) {
            var timeStep = time - _this.lastTime;
            if (timeStep > 0) {
                _this.trigger('frame', timeStep);
            }
            _this.lastTime = time;
            window.requestAnimationFrame(_this.onFrame);
        };
        whenDomReady(function () {
            _this.lastTime = window.performance.now();
            window.requestAnimationFrame(_this.onFrame);
        });
        return _this;
    }
    /**
     * Return the current service instance of the Dispatcher.
     *
     * @returns
     *   The current service instance of the Dispatcher.
     */
    Dispatcher.getInstance = function () {
        return services.get('Dispatcher', Dispatcher_1);
    };
    return Dispatcher;
}(Events));
Dispatcher = Dispatcher_1 = tslib_1.__decorate([
    service('Dispatcher')
], Dispatcher);
export default Dispatcher;
var Dispatcher_1;
