import * as tslib_1 from "tslib";
import Delegate from '../../views/View/Delegate';
import PointerAdapter from '../../handler/adapter/PointerAdapter';
import PointerHandler from '../../handler/PointerHandler';
import services, { service } from '../index';
import TouchAdapter from '../../handler/adapter/TouchAdapter';
/**
 * A view that wraps the document element.
 *
 * This service is mainly used to capture global mouse events.
 */
var DocumentView = DocumentView_1 = (function (_super) {
    tslib_1.__extends(DocumentView, _super);
    /**
     * DocumentView constructor.
     */
    function DocumentView() {
        var _this = _super.call(this, document) || this;
        /**
         * Whether the next vlick should be prevented or not.
         */
        _this.isClickPrevented = false;
        /**
         * Triggered after the user has clicked into the document.
         */
        _this.onClick = function (event) {
            if (_this.isClickPrevented) {
                event.preventDefault();
                event.stopImmediatePropagation();
                _this.isClickPrevented = false;
            }
        };
        _this.onPointerDown = function (event) {
            _this.trigger('pointerDown', event);
        };
        _this.delegate('click', _this.onClick);
        var element = _this.element;
        if ('addEventListener' in element) {
            if (PointerAdapter.isSupported()) {
                element.addEventListener('pointerdown', _this.onPointerDown, true);
                element.addEventListener('mspointerdown', _this.onPointerDown, true);
            }
            else {
                element.addEventListener('mousedown', _this.onPointerDown, true);
                if (TouchAdapter.isSupported()) {
                    element.addEventListener('touchstart', _this.onPointerDown, true);
                }
            }
        }
        else {
            var handler = new PointerHandler(_this);
            handler.handlePointerDown = _this.onPointerDown;
        }
        return _this;
    }
    /**
     * Prevent the next click event.
     */
    DocumentView.prototype.preventClick = function () {
        var _this = this;
        if (this.preventClickTimeout) {
            clearTimeout(this.preventClickTimeout);
        }
        this.isClickPrevented = true;
        this.preventClickTimeout = setTimeout(function () {
            _this.isClickPrevented = false;
            _this.preventClickTimeout = null;
        }, 500);
    };
    /**
     * Return the current service instance of the YouTube API loader.
     *
     * @returns
     *    The current service instance of the YouTube API loader.
     */
    DocumentView.getInstance = function () {
        return services.get('DocumentView', DocumentView_1);
    };
    return DocumentView;
}(Delegate));
DocumentView = DocumentView_1 = tslib_1.__decorate([
    service('DocumentView')
], DocumentView);
export default DocumentView;
var DocumentView_1;
