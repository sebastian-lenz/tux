import Delegate from '../../views/View/Delegate';
/**
 * A view that wraps the document element.
 *
 * This service is mainly used to capture global mouse events.
 */
export default class DocumentView extends Delegate {
    /**
     * Whether the next vlick should be prevented or not.
     */
    isClickPrevented: boolean;
    /**
     * Timeout id for the current click preventer.
     */
    preventClickTimeout: any;
    /**
     * DocumentView constructor.
     */
    constructor();
    /**
     * Prevent the next click event.
     */
    preventClick(): void;
    /**
     * Triggered after the user has clicked into the document.
     */
    onClick: (event: Event) => void;
    onPointerDown: (event: Event) => void;
    /**
     * Return the current service instance of the YouTube API loader.
     *
     * @returns
     *    The current service instance of the YouTube API loader.
     */
    static getInstance(): DocumentView;
}
