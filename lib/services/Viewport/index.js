import * as tslib_1 from "tslib";
import { indexOf } from 'underscore';
import Delegate from '../../views/View/Delegate';
import Dispatcher from '../Dispatcher';
import services, { service } from '../index';
/**
 * Test whether pageXOffset and pageYOffset are available on the window object.
 */
var supportsPageOffset = ('pageXOffset' in window);
/**
 * Test whether stupid IE is in css compat mode.
 */
var isCSS1Compat = ((document.compatMode || '') === 'CSS1Compat');
/**
 * A service that triggers events on browser resize and document scroll actions.
 *
 * Both the resize and scrolling events will be synchronized with an animation frame
 * event to speed up page rendering.
 *
 * @event "scrollbars" (enabled:boolean):void
 *   Triggered after the scrollbars have been enabled or disabled.
 * @event "resize" (width:number, height:number):void
 * @event "scroll" (scrollLeft:number, scrollTop:number):void
 */
var Viewport = Viewport_1 = (function (_super) {
    tslib_1.__extends(Viewport, _super);
    /**
     * Viewport constructor.
     */
    function Viewport() {
        var _this = _super.call(this, document.documentElement) || this;
        /**
         * The width of the viewport.
         */
        _this.width = 0;
        /**
         * The height of the viewport.
         */
        _this.height = 0;
        /**
         * The horizontal scroll position of the document.
         */
        _this.scrollLeft = 0;
        /**
         * The vertical scroll position of the document.
         */
        _this.scrollTop = 0;
        /**
         * A list of initiators that have disabled the scrollbars of the document.
         */
        _this.scrollInitiators = [];
        /**
         * The size of the scrollbar. Only available after disabling the scrollbars once.
         */
        _this.scrollBarSize = Number.NaN;
        /**
         * Whether the size of the viewport has changed or not.
         */
        _this.hasSizeChanged = false;
        /**
         * Whether the scroll position of the document has changed or not.
         */
        _this.hasScrollChanged = false;
        if (window.addEventListener) {
            window.addEventListener('resize', function () { return _this.onWindowResize(); });
            window.addEventListener('scroll', function () { return _this.onWindowScroll(); });
        }
        else {
            window.attachEvent('onresize', function () { return _this.onWindowResize(); });
            window.attachEvent('onscroll', function () { return _this.onWindowScroll(); });
            setTimeout(function () { return _this.trigger('resize', _this.width, _this.height); }, 50);
        }
        _this.listenTo(Dispatcher.getInstance(), 'frame', _this.onFrame);
        _this.onWindowResize();
        _this.onWindowScroll();
        _this.hasSizeChanged = false;
        _this.hasScrollChanged = false;
        return _this;
    }
    Viewport.prototype.setScrollLeft = function (value) {
        if (this.scrollLeft == value)
            return;
        window.scrollTo(value, this.scrollTop);
    };
    Viewport.prototype.setScrollTop = function (value) {
        if (this.scrollTop == value)
            return;
        window.scrollTo(this.scrollLeft, value);
    };
    /**
     * Trigger a resize event.
     */
    Viewport.prototype.triggerResize = function () {
        this.hasSizeChanged = true;
    };
    /**
     * Disable the document scrollbars.
     *
     * @param initiator
     *   An identifier of the service or class that disables the scrollbars.
     *   Same value must be passed to enableScrolling, used to manage multiple
     *   scripts that turn scrolling on or off.
     */
    Viewport.prototype.disableScrollbars = function (initiator) {
        var index = indexOf(this.scrollInitiators, initiator);
        if (index == -1) {
            this.scrollInitiators.push(initiator);
        }
        if (this.scrollInitiators.length != 1) {
            return;
        }
        var style = this.element.style;
        if (isNaN(this.scrollBarSize)) {
            var width = this.element.offsetWidth;
            style.overflow = 'hidden';
            this.scrollBarSize = this.element.offsetWidth - width;
            style.paddingRight = this.scrollBarSize + 'px';
        }
        else {
            style.overflow = 'hidden';
            style.paddingRight = this.scrollBarSize + 'px';
        }
        this.onWindowResize();
        this.trigger('scrollbars', false);
    };
    /**
     * Enable the document scrollbars.
     *
     * @param initiator
     *   An identifier of the service or class that enabled the scrollbars.
     */
    Viewport.prototype.enableScrollbars = function (initiator) {
        var index = indexOf(this.scrollInitiators, initiator);
        if (index != -1) {
            this.scrollInitiators.splice(index, 1);
        }
        if (this.scrollInitiators.length != 0)
            return;
        this.element.style.overflow = '';
        this.element.style.paddingRight = '';
        this.onWindowResize();
        this.trigger('scrollbars', true);
    };
    /**
     * Triggered on every page repaint.
     */
    Viewport.prototype.onFrame = function () {
        if (this.hasSizeChanged) {
            this.hasSizeChanged = false;
            var width = this.width;
            var height = this.height;
            this.trigger('resize', width, height);
        }
        if (this.hasScrollChanged) {
            this.hasScrollChanged = false;
            var scrollLeft = this.scrollLeft;
            var scrollTop = this.scrollTop;
            this.trigger('scroll', scrollLeft, scrollTop);
        }
    };
    /**
     * Triggered after the size of the window has changed.
     */
    Viewport.prototype.onWindowResize = function () {
        var width = Math.max(document.documentElement.clientWidth, 0);
        var height = Math.max(document.documentElement.clientHeight, 0);
        if (this.width != width || this.height != height) {
            this.width = width;
            this.height = height;
            this.hasSizeChanged = true;
        }
    };
    /**
     * Triggered after the scroll position of the document has changed.
     */
    Viewport.prototype.onWindowScroll = function () {
        var scrollLeft = supportsPageOffset
            ? window.pageXOffset
            : isCSS1Compat
                ? document.documentElement.scrollLeft
                : document.body.scrollLeft;
        var scrollTop = supportsPageOffset
            ? window.pageYOffset
            : isCSS1Compat
                ? document.documentElement.scrollTop
                : document.body.scrollTop;
        if (this.scrollLeft != scrollLeft || this.scrollTop != scrollTop) {
            this.scrollLeft = scrollLeft;
            this.scrollTop = scrollTop;
            this.hasScrollChanged = true;
        }
    };
    /**
     * Return the current instance of the Viewport service.
     *
     * @returns
     *    The current instance of the Viewport service.
     */
    Viewport.getInstance = function () {
        return services.get('Viewport', Viewport_1);
    };
    return Viewport;
}(Delegate));
Viewport = Viewport_1 = tslib_1.__decorate([
    service('Viewport')
], Viewport);
export default Viewport;
var Viewport_1;
