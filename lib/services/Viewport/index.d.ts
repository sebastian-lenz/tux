import Delegate from '../../views/View/Delegate';
/**
 * A service that triggers events on browser resize and document scroll actions.
 *
 * Both the resize and scrolling events will be synchronized with an animation frame
 * event to speed up page rendering.
 *
 * @event "scrollbars" (enabled:boolean):void
 *   Triggered after the scrollbars have been enabled or disabled.
 * @event "resize" (width:number, height:number):void
 * @event "scroll" (scrollLeft:number, scrollTop:number):void
 */
export default class Viewport extends Delegate {
    element: HTMLElement;
    /**
     * The width of the viewport.
     */
    width: number;
    /**
     * The height of the viewport.
     */
    height: number;
    /**
     * The horizontal scroll position of the document.
     */
    scrollLeft: number;
    /**
     * The vertical scroll position of the document.
     */
    scrollTop: number;
    /**
     * A list of initiators that have disabled the scrollbars of the document.
     */
    scrollInitiators: any[];
    /**
     * The size of the scrollbar. Only available after disabling the scrollbars once.
     */
    scrollBarSize: number;
    /**
     * Whether the size of the viewport has changed or not.
     */
    hasSizeChanged: boolean;
    /**
     * Whether the scroll position of the document has changed or not.
     */
    hasScrollChanged: boolean;
    /**
     * Viewport constructor.
     */
    constructor();
    setScrollLeft(value: number): void;
    setScrollTop(value: number): void;
    /**
     * Trigger a resize event.
     */
    triggerResize(): void;
    /**
     * Disable the document scrollbars.
     *
     * @param initiator
     *   An identifier of the service or class that disables the scrollbars.
     *   Same value must be passed to enableScrolling, used to manage multiple
     *   scripts that turn scrolling on or off.
     */
    disableScrollbars(initiator: any): void;
    /**
     * Enable the document scrollbars.
     *
     * @param initiator
     *   An identifier of the service or class that enabled the scrollbars.
     */
    enableScrollbars(initiator: any): void;
    /**
     * Triggered on every page repaint.
     */
    onFrame(): void;
    /**
     * Triggered after the size of the window has changed.
     */
    protected onWindowResize(): void;
    /**
     * Triggered after the scroll position of the document has changed.
     */
    protected onWindowScroll(): void;
    /**
     * Return the current instance of the Viewport service.
     *
     * @returns
     *    The current instance of the Viewport service.
     */
    static getInstance(): Viewport;
}
