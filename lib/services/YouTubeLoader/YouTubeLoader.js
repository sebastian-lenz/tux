import * as tslib_1 from "tslib";
import * as Q from 'q';
import services, { service } from '../index';
/**
 * A service class that loads the YouTube iFrame API.
 *
 * ´´´
 * YouTubeLoader.require(() => {
 *   const player = new YT.Player('player', {
 *     height: '390',
 *     width: '640',
 *     videoId: 'M7lc1UVf-VE',
 *     events: {
 *       'onReady': function(event:YT.EventArgs) {},
 *       'onStateChange': function(event:YT.EventArgs) {}
 *     }
 *   });
 * });
 * ´´´
 *
 * Documentation of the YouTube API can be found here:
 * https://developers.google.com/youtube/iframe_api_reference
 */
var YouTubeLoader = YouTubeLoader_1 = (function () {
    function YouTubeLoader() {
        /**
         * Current state of the API loader.
         */
        this.state = 0 /* Idle */;
        /**
         * Deferred object for creating load promises.
         */
        this.deferred = null;
    }
    /**
     * Return the current load state of the YoutTube API.
     *
     * @returns
     *   The current load state of the YoutTube API.
     */
    YouTubeLoader.prototype.getState = function () {
        return this.state;
    };
    /**
     * Return a promise that
     *
     * @returns {Promise<YouTubeApi>}
     */
    YouTubeLoader.prototype.getPromise = function () {
        var _this = this;
        if (this.state != 0 /* Idle */) {
            return this.deferred.promise;
        }
        this.state = 1 /* Loading */;
        this.deferred = Q.defer();
        var script = document.createElement('script');
        script.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(script, firstScriptTag);
        window['onYouTubeIframeAPIReady'] = function () {
            _this.state = 2 /* Loaded */;
            _this.deferred.resolve(YT);
        };
        return this.deferred.promise;
    };
    /**
     * Require the YouTube API.
     *
     * @param callback
     *   The callback that should be invoked as soon as the YouTube API
     *   is loaded.
     */
    YouTubeLoader.require = function (callback) {
        var loader = YouTubeLoader_1.getInstance();
        if (loader.state == 2 /* Loaded */) {
            callback(YT);
        }
        else {
            loader.getPromise().then(function () {
                callback(YT);
            });
        }
    };
    /**
     * Return the current service instance of the YouTube API loader.
     *
     * @returns
     *    The current service instance of the YouTube API loader.
     */
    YouTubeLoader.getInstance = function () {
        return services.get('YouTubeLoader', YouTubeLoader_1);
    };
    return YouTubeLoader;
}());
YouTubeLoader = YouTubeLoader_1 = tslib_1.__decorate([
    service('YouTubeLoader')
], YouTubeLoader);
export { YouTubeLoader };
var YouTubeLoader_1;
