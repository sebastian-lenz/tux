/// <reference types="youtube" />
/// <reference types="q" />
import * as Q from 'q';
/**
 * A type definition of the YouTube API.
 */
export declare type YouTubeApi = typeof YT;
/**
 * Loader callback definition.
 */
export declare type YouTubeLoaderCallback = {
    (api?: YouTubeApi): void;
};
/**
 * Defines the possible load states.
 */
export declare const enum YouTubeApiState {
    Idle = 0,
    Loading = 1,
    Loaded = 2,
}
/**
 * A service class that loads the YouTube iFrame API.
 *
 * ´´´
 * YouTubeLoader.require(() => {
 *   const player = new YT.Player('player', {
 *     height: '390',
 *     width: '640',
 *     videoId: 'M7lc1UVf-VE',
 *     events: {
 *       'onReady': function(event:YT.EventArgs) {},
 *       'onStateChange': function(event:YT.EventArgs) {}
 *     }
 *   });
 * });
 * ´´´
 *
 * Documentation of the YouTube API can be found here:
 * https://developers.google.com/youtube/iframe_api_reference
 */
export declare class YouTubeLoader {
    /**
     * Current state of the API loader.
     */
    state: YouTubeApiState;
    /**
     * Deferred object for creating load promises.
     */
    deferred: Q.Deferred<YouTubeApi>;
    /**
     * Return the current load state of the YoutTube API.
     *
     * @returns
     *   The current load state of the YoutTube API.
     */
    getState(): YouTubeApiState;
    /**
     * Return a promise that
     *
     * @returns {Promise<YouTubeApi>}
     */
    getPromise(): Q.Promise<YouTubeApi>;
    /**
     * Require the YouTube API.
     *
     * @param callback
     *   The callback that should be invoked as soon as the YouTube API
     *   is loaded.
     */
    static require(callback: YouTubeLoaderCallback): void;
    /**
     * Return the current service instance of the YouTube API loader.
     *
     * @returns
     *    The current service instance of the YouTube API loader.
     */
    static getInstance(): YouTubeLoader;
}
