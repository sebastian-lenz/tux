import * as tslib_1 from "tslib";
import * as Q from 'q';
import services, { service } from '../index';
/**
 * A service class that loads the GoogleMaps API.
 *
 * ´´´
 * GoogleMapsLoader.require(() => {
 *   ...
 * });
 * ´´´
 */
var GoogleMapsLoader = GoogleMapsLoader_1 = (function () {
    function GoogleMapsLoader() {
        /**
         * Current state of the API loader.
         */
        this.state = 0 /* Idle */;
        /**
         * Deferred object for creating load promises.
         */
        this.deferred = null;
    }
    /**
     * Return the current load state of the loader.
     *
     * @returns
     *   The current load state of the loader.
     */
    GoogleMapsLoader.prototype.getState = function () {
        return this.state;
    };
    /**
     * Return a promise that
     *
     * @returns {Promise<YouTubeApi>}
     */
    GoogleMapsLoader.prototype.getPromise = function () {
        var _this = this;
        if (this.state != 0 /* Idle */) {
            return this.deferred.promise;
        }
        this.state = 1 /* Loading */;
        this.deferred = Q.defer();
        var script = document.createElement('script');
        script.src = "https://maps.googleapis.com/maps/api/js?key=" + this.apiKey + "&callback=onGoogleMapsAPIReady";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(script, firstScriptTag);
        window['onGoogleMapsAPIReady'] = function () {
            _this.state = 2 /* Loaded */;
            _this.deferred.resolve();
        };
        return this.deferred.promise;
    };
    /**
     * Require the GoogleMaps API.
     *
     * @param callback
     *   The callback that should be invoked as soon as the API is loaded.
     */
    GoogleMapsLoader.require = function (callback) {
        var loader = GoogleMapsLoader_1.getInstance();
        if (loader.state == 2 /* Loaded */) {
            callback();
        }
        else {
            loader.getPromise().then(function () {
                callback();
            });
        }
    };
    /**
     * Return the current service instance of the API loader.
     *
     * @returns
     *    The current service instance of the API loader.
     */
    GoogleMapsLoader.getInstance = function () {
        return services.get('GoogleMapsLoader', GoogleMapsLoader_1);
    };
    return GoogleMapsLoader;
}());
GoogleMapsLoader = GoogleMapsLoader_1 = tslib_1.__decorate([
    service('GoogleMapsLoader')
], GoogleMapsLoader);
export default GoogleMapsLoader;
var GoogleMapsLoader_1;
