/// <reference types="q" />
import * as Q from 'q';
/**
 * Loader callback definition.
 */
export declare type GoogleMapsLoaderCallback = {
    (): void;
};
/**
 * Defines the possible load states.
 */
export declare const enum GoogleMapsApiState {
    Idle = 0,
    Loading = 1,
    Loaded = 2,
}
/**
 * A service class that loads the GoogleMaps API.
 *
 * ´´´
 * GoogleMapsLoader.require(() => {
 *   ...
 * });
 * ´´´
 */
export default class GoogleMapsLoader {
    /**
     * The API key that should be used.
     */
    apiKey: string;
    /**
     * Current state of the API loader.
     */
    state: GoogleMapsApiState;
    /**
     * Deferred object for creating load promises.
     */
    deferred: Q.Deferred<any>;
    /**
     * Return the current load state of the loader.
     *
     * @returns
     *   The current load state of the loader.
     */
    getState(): GoogleMapsApiState;
    /**
     * Return a promise that
     *
     * @returns {Promise<YouTubeApi>}
     */
    getPromise(): Q.Promise<any>;
    /**
     * Require the GoogleMaps API.
     *
     * @param callback
     *   The callback that should be invoked as soon as the API is loaded.
     */
    static require(callback: GoogleMapsLoaderCallback): void;
    /**
     * Return the current service instance of the API loader.
     *
     * @returns
     *    The current service instance of the API loader.
     */
    static getInstance(): GoogleMapsLoader;
}
