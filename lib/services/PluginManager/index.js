import * as tslib_1 from "tslib";
import { defaults } from 'underscore';
import services, { service } from '../index';
/**
 * A central plugin manager.
 */
var PluginManager = PluginManager_1 = (function () {
    function PluginManager() {
        /**
         * A map of all known plugin groups.
         */
        this.groups = {};
    }
    /**
     * Return a plugin group by its name.
     *
     * @param name
     *   The name of the plugin group to resolve.
     * @returns
     *   The plugin group with the given name.
     */
    PluginManager.prototype.getGroup = function (name) {
        if (!this.groups.hasOwnProperty(name)) {
            this.groups[name] = {
                hosts: [],
                plugins: []
            };
        }
        return this.groups[name];
    };
    /**
     * Register a plugin class.
     *
     * @param name
     *   The name of the plugin group.
     * @param plugin
     *   The plugin data that should be registered.
     */
    PluginManager.prototype.registerPlugin = function (name, plugin) {
        var group = this.getGroup(name);
        group.plugins.push(plugin);
        for (var _i = 0, _a = group.hosts; _i < _a.length; _i++) {
            var host = _a[_i];
            host.registerPlugin(plugin);
        }
    };
    /**
     * Register a host that is consuming plugin data.
     *
     * @param name
     *   The name of the plugin group.
     * @param host
     *   The plugin host instance that should be registered.
     */
    PluginManager.prototype.registerHost = function (name, host) {
        var group = this.getGroup(name);
        group.hosts.push(host);
        for (var _i = 0, _a = group.plugins; _i < _a.length; _i++) {
            var plugin = _a[_i];
            host.registerPlugin(plugin);
        }
    };
    /**
     * Return the current service instance of the PluginManager.
     *
     * @returns
     *    The current service instance of the PluginManager.
     */
    PluginManager.getInstance = function () {
        return services.get('PluginManager', PluginManager_1);
    };
    return PluginManager;
}());
PluginManager = PluginManager_1 = tslib_1.__decorate([
    service('PluginManager')
], PluginManager);
export default PluginManager;
/**
 * A class annotation that registers classes as plugins.
 *
 * @param name
 *   The name of the plugin group the class should be registered for.
 * @param plugin
 *   The plugin data that should be registered.
 */
export function plugin(name, plugin) {
    return function (pluginClass) {
        PluginManager
            .getInstance()
            .registerPlugin(name, defaults({ pluginClass: pluginClass }, plugin));
    };
}
var PluginManager_1;
