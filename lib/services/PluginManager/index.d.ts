/**
 *
 */
export interface IPlugin {
    pluginClass?: any;
}
/**
 *
 */
export interface IPluginHost {
    registerPlugin(plugin: IPlugin): any;
}
/**
 * Data structure used to store plugin group data.
 */
export interface IPluginGroup<T extends IPlugin> {
    hosts: IPluginHost[];
    plugins: T[];
}
/**
 * A central plugin manager.
 */
export default class PluginManager {
    /**
     * A map of all known plugin groups.
     */
    groups: {
        [name: string]: IPluginGroup<IPlugin>;
    };
    /**
     * Return a plugin group by its name.
     *
     * @param name
     *   The name of the plugin group to resolve.
     * @returns
     *   The plugin group with the given name.
     */
    getGroup<T extends IPlugin>(name: string): IPluginGroup<T>;
    /**
     * Register a plugin class.
     *
     * @param name
     *   The name of the plugin group.
     * @param plugin
     *   The plugin data that should be registered.
     */
    registerPlugin(name: string, plugin: IPlugin): void;
    /**
     * Register a host that is consuming plugin data.
     *
     * @param name
     *   The name of the plugin group.
     * @param host
     *   The plugin host instance that should be registered.
     */
    registerHost(name: string, host: IPluginHost): void;
    /**
     * Return the current service instance of the PluginManager.
     *
     * @returns
     *    The current service instance of the PluginManager.
     */
    static getInstance(): PluginManager;
}
/**
 * A class annotation that registers classes as plugins.
 *
 * @param name
 *   The name of the plugin group the class should be registered for.
 * @param plugin
 *   The plugin data that should be registered.
 */
export declare function plugin<T extends IPlugin>(name: string, plugin?: T): ClassDecorator;
