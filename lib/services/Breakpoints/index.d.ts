import Events from '../../Events';
export declare var getDeviceWidth: {
    (): number;
};
export declare var getDeviceHeight: {
    (): number;
};
export interface Breakpoint {
    name: string;
    minWidth: number;
    containerWidth: number;
    update?: {
        (breakpoint: Breakpoint, width: number);
    };
}
/**
 * @event "change" (breakpoint:IBreakpoint):void
 *   Triggered after the breakpoint configuration has changed.
 */
export default class Breakpoints extends Events {
    index: number;
    breakpoints: Breakpoint[];
    constructor();
    setBreakpoints(value: Breakpoint[]): void;
    getCurrent(): Breakpoint;
    update(): void;
    onViewportResize(width: number): void;
    static getInstance(): Breakpoints;
}
