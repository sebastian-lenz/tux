import * as tslib_1 from "tslib";
import Events from '../../Events';
import services, { service } from '../index';
import Viewport from '../Viewport';
export var getDeviceWidth;
export var getDeviceHeight;
(function () {
    var scope = window;
    var prefix = 'inner';
    if (!('innerWidth' in window)) {
        prefix = 'client';
        scope = document.documentElement || document.body;
    }
    getDeviceWidth = function () { return scope[prefix + 'Width']; };
    getDeviceHeight = function () { return scope[prefix + 'Height']; };
})();
/**
 * @event "change" (breakpoint:IBreakpoint):void
 *   Triggered after the breakpoint configuration has changed.
 */
var Breakpoints = Breakpoints_1 = (function (_super) {
    tslib_1.__extends(Breakpoints, _super);
    function Breakpoints() {
        var _this = _super.call(this) || this;
        _this.index = 0;
        _this.breakpoints = [{
                name: 'xs',
                minWidth: 0,
                containerWidth: 0,
                update: function (breakpoint, width) {
                    breakpoint.containerWidth = width - 30;
                },
            }, {
                name: 'sm',
                minWidth: 768,
                containerWidth: 720,
            }, {
                name: 'md',
                minWidth: 992,
                containerWidth: 940
            }, {
                name: 'lg',
                minWidth: 1200,
                containerWidth: 1140
            }];
        var viewport = Viewport.getInstance();
        _this.listenTo(viewport, 'resize', _this.onViewportResize, 10);
        _this.update();
        return _this;
    }
    Breakpoints.prototype.setBreakpoints = function (value) {
        this.breakpoints = value;
        this.update();
    };
    Breakpoints.prototype.getCurrent = function () {
        return this.breakpoints[this.index];
    };
    Breakpoints.prototype.update = function () {
        var viewport = Viewport.getInstance();
        this.onViewportResize(viewport.width);
    };
    Breakpoints.prototype.onViewportResize = function (width) {
        var deviceWidth = getDeviceWidth();
        var index = 0;
        var length = this.breakpoints.length;
        while (index < length - 1) {
            if (this.breakpoints[index + 1].minWidth > deviceWidth) {
                break;
            }
            else {
                index += 1;
            }
        }
        var breakpoint = this.breakpoints[index];
        if (breakpoint.update) {
            breakpoint.update(breakpoint, width);
        }
        if (this.index != index || breakpoint.update) {
            this.index = index;
            this.trigger('change', breakpoint);
        }
    };
    Breakpoints.getInstance = function () {
        return services.get('Breakpoints', Breakpoints_1);
    };
    return Breakpoints;
}(Events));
Breakpoints = Breakpoints_1 = tslib_1.__decorate([
    service('Breakpoints')
], Breakpoints);
export default Breakpoints;
var Breakpoints_1;
