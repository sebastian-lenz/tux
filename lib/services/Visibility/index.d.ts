import Events from '../../Events';
import Viewport from '../Viewport';
export interface VisibilityTarget {
    element?: HTMLElement;
    getBounds?: {
        (): {
            min: number;
            max: number;
        };
    };
    setInViewport(value: boolean): any;
}
export declare class VisibilityItem {
    isVisible: boolean;
    max: number;
    min: number;
    target: VisibilityTarget;
    constructor(target: VisibilityTarget);
    updateBounds(scrollTop: number, viewMin: number, viewMax: number): void;
    updateVisibility(viewMin: number, viewMax: number): void;
}
export default class Visibility extends Events {
    bleed: number;
    items: VisibilityItem[];
    itemClass: typeof VisibilityItem;
    max: number;
    min: number;
    viewport: Viewport;
    constructor();
    register(target: VisibilityTarget): void;
    unregister(target: VisibilityTarget): void;
    update(target: VisibilityTarget): void;
    updateBounds(): void;
    onViewportResize(): void;
    onViewportScroll(): void;
    static getInstance(): Visibility;
}
