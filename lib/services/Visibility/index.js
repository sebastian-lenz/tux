import * as tslib_1 from "tslib";
import { find, filter, some } from 'underscore';
import services, { service } from '../index';
import Events from '../../Events';
import Viewport from '../Viewport';
var VisibilityItem = (function () {
    function VisibilityItem(target) {
        this.isVisible = false;
        this.max = Number.MIN_VALUE;
        this.min = Number.MAX_VALUE;
        this.target = target;
    }
    VisibilityItem.prototype.updateBounds = function (scrollTop, viewMin, viewMax) {
        var min = Number.MAX_VALUE;
        var max = Number.MIN_VALUE;
        if (this.target.element) {
            var bounds = this.target.element.getBoundingClientRect();
            min = bounds.top + scrollTop;
            max = min + (bounds.height || this.target.element.offsetHeight);
        }
        else if (this.target.getBounds) {
            var bounds = this.target.getBounds();
            min = bounds.min;
            max = bounds.max;
        }
        if (this.min != min || this.max != max) {
            this.min = min;
            this.max = max;
            this.updateVisibility(viewMin, viewMax);
        }
    };
    VisibilityItem.prototype.updateVisibility = function (viewMin, viewMax) {
        var isVisible = this.max > viewMin && this.min < viewMax;
        if (this.isVisible != isVisible) {
            this.isVisible = isVisible;
            this.target.setInViewport(isVisible);
        }
    };
    return VisibilityItem;
}());
export { VisibilityItem };
var Visibility = Visibility_1 = (function (_super) {
    tslib_1.__extends(Visibility, _super);
    function Visibility() {
        var _this = _super.call(this) || this;
        _this.bleed = 500;
        _this.items = [];
        _this.itemClass = VisibilityItem;
        _this.max = 0;
        _this.min = 0;
        var viewport = _this.viewport = Viewport.getInstance();
        _this.listenTo(viewport, 'resize', _this.onViewportResize, -10);
        _this.listenTo(viewport, 'scroll', _this.onViewportScroll, -10);
        _this.updateBounds();
        return _this;
    }
    Visibility.prototype.register = function (target) {
        var _a = this, items = _a.items, itemClass = _a.itemClass;
        if (some(items, function (item) { return item.target === target; })) {
            return;
        }
        var item = new itemClass(target);
        item.updateBounds(this.viewport.scrollTop, this.min, this.max);
        items.push(item);
    };
    Visibility.prototype.unregister = function (target) {
        var items = this.items;
        this.items = filter(items, function (item) { return item.target !== target; });
    };
    Visibility.prototype.update = function (target) {
        var item = find(this.items, function (item) { return item.target === target; });
        if (item) {
            item.updateBounds(this.viewport.scrollTop, this.min, this.max);
        }
    };
    Visibility.prototype.updateBounds = function () {
        var _a = this, bleed = _a.bleed, viewport = _a.viewport;
        this.min = viewport.scrollTop - bleed;
        this.max = viewport.scrollTop + viewport.height + bleed;
    };
    Visibility.prototype.onViewportResize = function () {
        this.updateBounds();
        var _a = this, items = _a.items, max = _a.max, min = _a.min;
        var scrollTop = this.viewport.scrollTop;
        for (var index = 0, count = items.length; index < count; ++index) {
            items[index].updateBounds(scrollTop, min, max);
        }
    };
    Visibility.prototype.onViewportScroll = function () {
        this.updateBounds();
        var _a = this, items = _a.items, max = _a.max, min = _a.min;
        for (var index = 0, count = items.length; index < count; ++index) {
            items[index].updateVisibility(min, max);
        }
    };
    Visibility.getInstance = function () {
        return services.get('Visibility', Visibility_1);
    };
    return Visibility;
}(Events));
Visibility = Visibility_1 = tslib_1.__decorate([
    service('Visibility')
], Visibility);
export default Visibility;
var Visibility_1;
