var ServiceRegistry = (function () {
    function ServiceRegistry() {
        this.instances = {};
        this.classes = {};
    }
    ServiceRegistry.prototype.set = function (serviceName, instance) {
        this.instances[serviceName] = instance;
    };
    ServiceRegistry.prototype.get = function (serviceName, fallbackClass) {
        if (this.instances[serviceName]) {
            return this.instances[serviceName];
        }
        if (this.classes[serviceName]) {
            return this.instances[serviceName] = new this.classes[serviceName]();
        }
        if (fallbackClass) {
            return this.instances[serviceName] = new fallbackClass();
        }
        return null;
    };
    ServiceRegistry.prototype.register = function (serviceName, serviceClass) {
        var instance = this.instances[serviceName];
        if (instance) {
            if (!(instance instanceof serviceClass)) {
                throw new Error('Service `' + serviceName + '` already initialized.');
            }
        }
        else {
            this.classes[serviceName] = serviceClass;
        }
    };
    return ServiceRegistry;
}());
export { ServiceRegistry };
var services = new ServiceRegistry();
export default services;
export function service(serviceName) {
    return function (servicePrototype) {
        services.register(serviceName, servicePrototype);
        return servicePrototype;
    };
}
