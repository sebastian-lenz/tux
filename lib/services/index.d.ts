export declare class ServiceRegistry {
    instances: {
        [serviceName: string]: Object;
    };
    classes: {
        [serviceName: string]: any;
    };
    set<T>(serviceName: string, instance: T): void;
    get<T>(serviceName: string, fallbackClass?: {
        new (): T;
    }): T;
    register(serviceName: string, serviceClass: any): void;
}
declare const services: ServiceRegistry;
export default services;
export declare function service(serviceName: string): (servicePrototype: any) => any;
