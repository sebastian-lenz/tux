import * as tslib_1 from "tslib";
import Events from '../../Events';
import services, { service } from '../index';
import Viewport from '../Viewport';
import Worker from './Worker';
var FontObserver = FontObserver_1 = (function (_super) {
    tslib_1.__extends(FontObserver, _super);
    function FontObserver() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.workers = [];
        return _this;
    }
    FontObserver.prototype.add = function (markup) {
        this.workers.push(new Worker({ observer: this, markup: markup }));
    };
    FontObserver.prototype.handleFontLoaded = function () {
        Viewport.getInstance().triggerResize();
        FontObserver_1.getInstance().trigger('fontLoaded');
    };
    FontObserver.getInstance = function () {
        return services.get('FontObserver', FontObserver_1);
    };
    return FontObserver;
}(Events));
FontObserver = FontObserver_1 = tslib_1.__decorate([
    service('FontObserver')
], FontObserver);
export default FontObserver;
var FontObserver_1;
