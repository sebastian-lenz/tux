import * as tslib_1 from "tslib";
import View from '../../views/View';
var FontObserverWorker = (function (_super) {
    tslib_1.__extends(FontObserverWorker, _super);
    function FontObserverWorker(options) {
        var _this = _super.call(this, options) || this;
        _this.observer = options.observer;
        var element = _this.element;
        element.style.position = 'absolute';
        element.style.left = '-1024px';
        element.style.top = '-1024px';
        element.style.overflow = 'hidden';
        var inner = _this.innerContent = document.createElement('div');
        var wrapper = _this.innerWrapper = document.createElement('div');
        wrapper.style.position = 'absolute';
        wrapper.style.overflow = 'hidden';
        wrapper.style.width = '100%';
        wrapper.style.height = '100%';
        wrapper.appendChild(inner);
        var content = _this.content = document.createElement('div');
        content.style.position = 'relative';
        content.innerHTML = options.markup;
        content.appendChild(wrapper);
        _this.element.appendChild(content);
        document.body.appendChild(_this.element);
        _this.reset();
        _this.element.addEventListener('scroll', function () { return _this.onScroll(); });
        wrapper.addEventListener('scroll', function () { return _this.onScroll(); });
        return _this;
    }
    FontObserverWorker.prototype.reset = function () {
        this.isReset = true;
        var _a = this, content = _a.content, element = _a.element, innerContent = _a.innerContent, innerWrapper = _a.innerWrapper;
        var width = content.offsetWidth;
        var height = content.offsetHeight;
        element.style.width = (width - 1) + 'px';
        element.style.height = (height - 1) + 'px';
        element.scrollLeft = element.scrollWidth - width - 1;
        element.scrollTop = element.scrollHeight - height - 1;
        innerContent.style.width = (width + 1) + 'px';
        innerContent.style.height = (height + 1) + 'px';
        innerContent.scrollLeft = innerWrapper.scrollWidth - width + 1;
        innerContent.scrollTop = innerWrapper.scrollHeight - height + 1;
        this.isReset = false;
    };
    FontObserverWorker.prototype.onScroll = function () {
        if (this.isReset)
            return;
        this.observer.handleFontLoaded();
        this.reset();
    };
    return FontObserverWorker;
}(View));
export default FontObserverWorker;
