import Events from '../../Events';
export default class FontObserver extends Events {
    private workers;
    add(markup: string): void;
    handleFontLoaded(): void;
    static getInstance(): FontObserver;
}
