import FontObserver from './index';
import View, { ViewOptions } from '../../views/View';
export interface WorkerOptions extends ViewOptions {
    markup: string;
    observer: FontObserver;
}
export default class FontObserverWorker extends View {
    observer: FontObserver;
    isReset: boolean;
    content: HTMLDivElement;
    innerWrapper: HTMLDivElement;
    innerContent: HTMLDivElement;
    constructor(options: WorkerOptions);
    reset(): void;
    onScroll(): void;
}
